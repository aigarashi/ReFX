open Tezos_client_017_PtNairob
open Protocol
open Lwt_result_syntax

let read_file (fn : string) : string tzresult Lwt.t =
  let*! fd = Lwt_unix.(openfile fn [ O_RDONLY; O_NONBLOCK; O_CLOEXEC ] 0) in
  let read_fd fd =
    let buffer_size = 4096 in
    let buffer = Bytes.create buffer_size in
    let rec read_chunks sum =
      let*! n = Lwt_unix.read fd buffer 0 buffer_size in
      if Stdlib.( = ) n 0 then return sum
      else
        let str =
          Bytes.to_string
            (if Stdlib.( < ) n buffer_size then Bytes.sub buffer 0 n else buffer)
        in
        read_chunks (sum ^ str)
    in
    read_chunks ""
  in
  read_fd fd

let typecheck (program : Michelson_v1_parser.parsed) :
    Script_tc_errors.type_map tzresult Lwt.t =
  let rng_state = Random.State.make [||] in
  let* ctxt, _ = Execution_context.make ~rng_state in
  let type_map = ref [] in
  let type_logger loc ~stack_ty_before ~stack_ty_after =
    type_map := (loc, (stack_ty_before, stack_ty_after)) :: !type_map
  in
  let elab_conf =
    Script_ir_translator_config.make ~legacy:false ~type_logger ()
  in
  let*! t =
    Script_ir_translator.parse_code ~elab_conf ctxt
      ~code:(Alpha_context.Script.lazy_expr program.expanded)
  in
  match t with
  | Ok _ -> return !type_map
  | Error e ->
      failwith "%a"
        (Michelson_v1_error_reporter.report_errors ~details:true
           ~show_source:true ~parsed:program)
      @@ Environment.wrap_tztrace e

open Cmdliner

let verify_cmd =
  let info = Cmd.info "helmholtz" in
  let input_file_arg =
    Arg.(
      required
        (pos 0 (some file) None
        & info ~docv:"FILE" ~doc:"Michelson program file being verified." []))
  in
  let logfile_path_arg =
    Arg.(
      value & opt string ".refx"
      & info ~docv:"DIR" ~doc:"Log files are stored in $(docv)" [ "log-dir" ])
  in
  let solver_arg =
    Arg.(
      value & opt string "z3"
      & info ~doc:"solver name used for solving VCs" [ "solver" ])
  in
  let show_ce_flag =
    Arg.(
      value & flag
      & info ~doc:"show counter example when verification fails" [ "show-ce" ])
  in
  let show_slice_flag =
    Arg.(
      value & flag
      & info ~doc:"show ContractAnnot slice when verification fails"
          [ "show-slice" ])
  in
  let verify logfile_path solver show_ce show_slice filename =
    let p =
      let* source = read_file filename in
      let program = source in
      let* program, decorations =
        Tezos_refx.Decoration.split_decoration program
      in
      Format.printf "program =@.%s@.decoration =@.%a@." program
        (Format.pp_print_list Tezos_refx.Syntax.pp_parsed_decoration)
        (List.map (fun (_, _, d) -> d) decorations);
      let program, _ = Michelson_v1_parser.parse_toplevel program in
      let* type_map = typecheck program in
      let* _ =
        Tezos_refx.Verify.print_typecheck_result ~show_ce ~show_slice program
          decorations type_map ~logfile_path ~solver
      in
      return ()
    in
    Lwt_main.run p |> function
    | Ok () -> ()
    | Error e -> Format.eprintf "%a@." Error_monad.pp_print_trace e
  in
  Cmd.v info
    Term.(
      const verify $ logfile_path_arg $ solver_arg $ show_ce_flag
      $ show_slice_flag $ input_file_arg)

let () = Cmd.eval verify_cmd |> exit
