Finding counter-example for ab-postcondition of ContractAnnot at line 0, characters -1--1
unsat
START_MODEL
(error "line 169 column 10: model is not available")
END_MODEL
Finding counter-example for postcondition of ContractAnnot at line 0, characters -1--1
sat
START_MODEL
(
  (define-fun _internal_dup_l21_c7-10_0052 () (Pair (Pair (Pair String String) (Option String))
                                                    (Pair (Pair String String)
                                                          (Option String)))
    (mk-pair (mk-pair (mk-pair "t" "%default") (as none (Option String)))
         (mk-pair (mk-pair "t" "%default") (some "nit"))))
  (define-fun _internal_nil_l26_c30-43_0062 () (Seq Operation)
    (seq.unit (setDelegate (some "!0!"))))
  (define-fun _internal_car_l15_c27-35_0024 () (Pair (Pair String String) (Option String))
    (mk-pair (mk-pair "t" "%default") (as none (Option String))))
  (define-fun _internal_dup_l15_c27-35_0023 () (Pair (Pair (Pair String String) (Option String))
                                                     (Pair (Pair String String)
                                                           (Option String)))
    (mk-pair (mk-pair (mk-pair "t" "%default") (as none (Option String)))
         (mk-pair (mk-pair "t" "%default") (some "nit"))))
  (define-fun _internal_car_l21_c13-17_0053 () (Pair (Pair String String) (Option String))
    (mk-pair (mk-pair "t" "%default") (as none (Option String))))
  (define-fun _internal_cdr_l13_c13-18_0012 () (Pair (Pair (Pair String String) (Option String))
                                                     (Pair (Pair String String)
                                                           (Option String)))
    (mk-pair (mk-pair (mk-pair "t" "%default") (as none (Option String)))
         (mk-pair (mk-pair "t" "%default") (some "nit"))))
  (define-fun _internal_cdr_l22_c19-23_0056 () (Pair (Pair String String) (Option String))
    (mk-pair (mk-pair "t" "%default") (some "nit")))
  (define-fun _internal_cdr_l21_c13-17_0054 () (Option String)
    (as none (Option String)))
  (define-fun _internal_unpair_l15_c11-17_0021 () (Option String)
    (as none (Option String)))
  (define-fun _internal_dup_l13_c7-10_0011 () (Pair (Option String)
                                                    (Pair (Pair (Pair String
                                                                      String)
                                                                (Option String))
                                                          (Pair (Pair String
                                                                      String)
                                                                (Option String))))
    (mk-pair (as none (Option String))
         (mk-pair (mk-pair (mk-pair "t" "%default") (as none (Option String)))
                  (mk-pair (mk-pair "t" "%default") (some "nit")))))
  (define-fun amount () Int
    0)
  (define-fun _internal_dup_l18_c31-39_0040 () (Pair (Pair (Pair String String) (Option String))
                                                     (Pair (Pair String String)
                                                           (Option String)))
    (mk-pair (mk-pair (mk-pair "" "i") (as none (Option String)))
         (mk-pair (mk-pair "" "") (as none (Option String)))))
  (define-fun _internal_result_0085 () (Pair (Seq Operation)
                                             (Pair (Pair (Pair String String)
                                                         (Option String))
                                                   (Pair (Pair String String)
                                                         (Option String))))
    (mk-pair (as seq.empty (Seq Operation))
         (mk-pair (mk-pair (mk-pair "t" "%default") (as none (Option String)))
                  (mk-pair (mk-pair "t" "%default") (some "nit")))))
  (define-fun _internal_some_l32_c19-23_0072 () (Option String)
    (as none (Option String)))
  (define-fun _internal_unpair_l18_c15-21_0038 () (Option String)
    (as none (Option String)))
  (define-fun _internal_compare_l14_c7-23_0019 () Int
    0)
  (define-fun _internal_pair_l18_c31-39_0045 () (Pair (Pair String String) (Option String))
    (mk-pair (mk-pair "" "i") (as none (Option String))))
  (define-fun _internal_cdr_l15_c27-35_0026 () (Option String)
    (as none (Option String)))
  (define-fun source () (Pair String String)
    (mk-pair "n" "%default"))
  (define-fun sender () (Pair String String)
    (mk-pair "t" "%default"))
  (define-fun _internal_if_none_l23_c7-38_0058 () String
    "nE")
  (define-fun balance () Int
    1)
  (define-fun initial_0004 () (Pair (Option String)
                                    (Pair (Pair (Pair String String)
                                                (Option String))
                                          (Pair (Pair String String)
                                                (Option String))))
    (mk-pair (as none (Option String))
         (mk-pair (mk-pair (mk-pair "t" "%default") (as none (Option String)))
                  (mk-pair (mk-pair "t" "%default") (some "nit")))))
  (define-fun _internal_param_stor_0010 () (Pair (Option String)
                                                 (Pair (Pair (Pair String
                                                                   String)
                                                             (Option String))
                                                       (Pair (Pair String
                                                                   String)
                                                             (Option String))))
    (mk-pair (as none (Option String))
         (mk-pair (mk-pair (mk-pair "t" "%default") (as none (Option String)))
                  (mk-pair (mk-pair "t" "%default") (some "nit")))))
  (define-fun _internal_unpair_l13_c50-56_0017 () (Pair String String)
    (mk-pair "t" "%default"))
  (define-fun _internal_set_delegate_l33_c19-31_0073 () Operation
    (setDelegate (some "!1!")))
  (define-fun _internal_unpair_l18_c15-21_0039 () (Pair (Pair (Pair String String) (Option String))
                                                        (Pair (Pair String
                                                                    String)
                                                              (Option String)))
    (mk-pair (mk-pair (mk-pair "" "") (as none (Option String)))
         (mk-pair (mk-pair "" "") (as none (Option String)))))
  (define-fun _internal_car_l13_c13-18_0014 () (Pair String String)
    (mk-pair "t" "%default"))
  (define-fun _internal_dup_l15_c27-35_0025 () (Pair (Pair String String) (Option String))
    (mk-pair (mk-pair "t" "%default") (as none (Option String))))
  (define-fun _internal_eq_l14_c7-23_0020 () Bool
    true)
  (define-fun _internal_unpair_l15_c11-17_0022 () (Pair (Pair (Pair String String) (Option String))
                                                        (Pair (Pair String
                                                                    String)
                                                              (Option String)))
    (mk-pair (mk-pair (mk-pair "t" "%default") (as none (Option String)))
         (mk-pair (mk-pair "t" "%default") (some "nit"))))
  (define-fun _internal_cdr_l18_c31-39_0041 () (Pair (Pair String String) (Option String))
    (mk-pair (mk-pair "" "i") (as none (Option String))))
  (define-fun _internal_sender_l16_c25-31_0035 () (Pair String String)
    (mk-pair "" "i"))
  (define-fun _internal_cdr_l15_c27-35_0029 () (Pair (Pair String String) (Option String))
    (mk-pair (mk-pair "t" "%default") (some "nit")))
  (define-fun _internal_unpair_l13_c50-56_0018 () (Pair String String)
    (mk-pair "t" "%default"))
  (define-fun _internal_car_l18_c31-39_0044 () (Pair String String)
    (mk-pair "" ""))
  (define-fun _internal_nil_l33_c34-47_0074 () (Seq Operation)
    (seq.unit (setDelegate (as none (Option String)))))
  (define-fun _fresh_binding_0081 () (Seq Operation)
    (as seq.empty (Seq Operation)))
  (define-fun _internal_set_delegate_l26_c15-27_0061 () Operation
    (setDelegate (some "!2!")))
  (define-fun _internal_pair_l13_c38-42_0016 () (Pair (Pair String String) (Pair String String))
    (mk-pair (mk-pair "t" "%default") (mk-pair "t" "%default")))
  (define-fun _internal_sender_l13_c29-35_0015 () (Pair String String)
    (mk-pair "t" "%default"))
  (define-fun self_addr () (Pair String String)
    (mk-pair "i" "%default"))
  (define-fun _internal_cdr_l22_c19-23_0057 () (Option String)
    (some "nit"))
  (define-fun _internal_car_l18_c31-39_0046 () (Pair (Pair String String) (Option String))
    (mk-pair (mk-pair "" "") (as none (Option String))))
  (define-fun _internal_if_none_l29_c11-37_0067 () String
    "n")
  (define-fun _fresh_binding_0082 () (Pair (Pair (Pair String String) (Option String))
                                           (Pair (Pair String String)
                                                 (Option String)))
    (mk-pair (mk-pair (mk-pair "t" "%default") (as none (Option String)))
         (mk-pair (mk-pair "t" "%default") (some "nit"))))
  (define-fun _internal_dup_l22_c13-16_0055 () (Pair (Pair (Pair String String) (Option String))
                                                     (Pair (Pair String String)
                                                           (Option String)))
    (mk-pair (mk-pair (mk-pair "t" "%default") (as none (Option String)))
         (mk-pair (mk-pair "t" "%default") (some "nit"))))
  (define-fun _internal_cdr_l16_c17-22_0033 () (Pair (Pair String String) (Option String))
    (mk-pair (mk-pair "" "") (as none (Option String))))
  (define-fun _internal_cdr_l16_c17-22_0032 () (Pair (Pair (Pair String String) (Option String))
                                                     (Pair (Pair String String)
                                                           (Option String)))
    (mk-pair (mk-pair (mk-pair "" "") (as none (Option String)))
         (mk-pair (mk-pair "" "i") (as none (Option String)))))
  (define-fun _internal_if_none_l24_c11-37_0059 () String
    "nit")
  (define-fun _internal_car_l16_c17-22_0034 () (Pair String String)
    (mk-pair "" "it"))
  (define-fun _internal_pair_l15_c27-35_0028 () (Pair (Pair String String) (Option String))
    (mk-pair (mk-pair "t" "%default") (as none (Option String))))
  (define-fun _internal_dup_l16_c11-14_0031 () (Pair (Option String)
                                                     (Pair (Pair (Pair String
                                                                       String)
                                                                 (Option String))
                                                           (Pair (Pair String
                                                                       String)
                                                                 (Option String))))
    (mk-pair (as none (Option String))
         (mk-pair (mk-pair (mk-pair "" "") (as none (Option String)))
                  (mk-pair (mk-pair "" "") (as none (Option String))))))
  (define-fun _internal_car_l13_c13-18_0013 () (Pair (Pair String String) (Option String))
    (mk-pair (mk-pair "t" "%default") (as none (Option String))))
  (define-fun _internal_none_l25_c15-28_0060 () (Option String)
    (some "unit"))
  (define-fun _internal_car_l15_c27-35_0027 () (Pair String String)
    (mk-pair "t" "%default"))
  (define-fun _internal_dup_l18_c31-39_0042 () (Pair (Pair String String) (Option String))
    (mk-pair (mk-pair "" "") (as none (Option String))))
  (define-fun _internal_dup_l30_c21-24_0069 () String
    "nE")
  (define-fun _internal_eq_l31_c15-34_0071 () Bool
    false)
  (define-fun _internal_compare_l17_c11-21_0036 () Int
    0)
  (define-fun _internal_cdr_l18_c31-39_0043 () (Option String)
    (as none (Option String)))
  (define-fun _internal_eq_l17_c11-21_0037 () Bool
    false)
  (define-fun _internal_compare_l31_c15-34_0070 () Int
    0)
  (define-fun contract_ty_opt ((x!0 (Pair String String))) (Option String)
    (ite (= x!0
            (mk-pair "i"
                     (str.++ (seq.unit (_ Char 37))
                             (seq.unit (_ Char 100))
                             (seq.unit (_ Char 101))
                             (seq.unit (_ Char 102))
                             (seq.unit (_ Char 97))
                             (seq.unit (_ Char 117))
                             (seq.unit (_ Char 108))
                             (seq.unit (_ Char 116)))))
      (some "(option key_hash)")
      (some "unit")))
  (define-fun unpack_opt!unit ((x!0 String)) (Option Unit)
    (as none (Option Unit)))
  (define-fun pack!unit ((x!0 Unit)) String
    "")
)
END_MODEL