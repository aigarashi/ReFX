(set-logic ALL)
(declare-datatypes ((Pair 2))
  ((par (T1 T2) ((mk-pair (first T1) (second T2))))))
(declare-datatypes ((Unit 0)) (((unit))))
(declare-datatypes ((Or 2))
  ((par (T1 T2) ((left (extractLeft T1)) (right (extractRight T2))))))
(declare-datatypes ((Option 1)) ((par (T) ((none) (some (extractSome T))))))
(declare-datatypes ((Contract 0))
  (((contract (extractContract (Pair String String))))))
(define-sort Fun () Int)
(declare-datatypes ((Operation 0))
  (((setDelegate (delegateAddress (Option String)))
     (transferTokens (transferData String) (transferMutez Int)
       (transferContract Contract))
     (createContract (createContractDelegateAddress (Option String))
       (createContractMutez Int) (createContractInitStorage String)
       (createContractAddress (Pair String String))))))
(declare-datatypes ((Exception 0))
  (((overflow) (error (extractError String)))))
(declare-fun amount () Int)
(declare-fun balance () Int)
(declare-fun contract_ty_opt ((Pair String String)) (Option String))
(declare-fun pack!unit (Unit) String)
(declare-fun self_addr () (Pair String String))
(declare-fun sender () (Pair String String))
(declare-fun source () (Pair String String))
(declare-fun unpack_opt!unit (String) (Option Unit))
(assert (or (= amount balance) (> balance amount)))
(assert (= (contract_ty_opt source) ((as some (Option String)) "unit")))
(assert (= (second source) "%default"))
(assert (= (second sender) "%default"))
(assert (= (second self_addr) "%default"))
(push 1)
(declare-const _internal_car_l13_c13-18_0013
  (Pair (Pair String String) (Option String)))
(declare-const _internal_car_l13_c13-18_0014 (Pair String String))
(declare-const _internal_car_l16_c17-22_0034 (Pair String String))
(declare-const _internal_cdr_l13_c13-18_0012
  (Pair (Pair (Pair String String) (Option String))
    (Pair (Pair String String) (Option String))))
(declare-const _internal_cdr_l16_c17-22_0032
  (Pair (Pair (Pair String String) (Option String))
    (Pair (Pair String String) (Option String))))
(declare-const _internal_cdr_l16_c17-22_0033
  (Pair (Pair String String) (Option String)))
(declare-const _internal_compare_l14_c7-23_0019 Int)
(declare-const _internal_compare_l17_c11-21_0036 Int)
(declare-const _internal_dup_l13_c7-10_0011
  (Pair (Option String)
    (Pair (Pair (Pair String String) (Option String))
      (Pair (Pair String String) (Option String)))))
(declare-const _internal_dup_l16_c11-14_0031
  (Pair (Option String)
    (Pair (Pair (Pair String String) (Option String))
      (Pair (Pair String String) (Option String)))))
(declare-const _internal_eq_l14_c7-23_0020 Bool)
(declare-const _internal_eq_l17_c11-21_0037 Bool)
(declare-const _internal_pair_l13_c38-42_0016
  (Pair (Pair String String) (Pair String String)))
(declare-const _internal_param_stor_0010
  (Pair (Option String)
    (Pair (Pair (Pair String String) (Option String))
      (Pair (Pair String String) (Option String)))))
(declare-const _internal_result_0084 Exception)
(declare-const _internal_sender_l13_c29-35_0015 (Pair String String))
(declare-const _internal_sender_l16_c25-31_0035 (Pair String String))
(declare-const _internal_unit_l19_c15-19_0048 Unit)
(declare-const _internal_unpair_l13_c50-56_0017 (Pair String String))
(declare-const _internal_unpair_l13_c50-56_0018 (Pair String String))
(declare-const initial_0004
  (Pair (Option String)
    (Pair (Pair (Pair String String) (Option String))
      (Pair (Pair String String) (Option String)))))
(assert
  (and
    (= (unpack_opt!unit (pack!unit _internal_unit_l19_c15-19_0048))
      ((as some (Option Unit)) _internal_unit_l19_c15-19_0048))
    (= (unpack_opt!unit (pack!unit unit)) ((as some (Option Unit)) unit))
    (ite (is-some (first initial_0004)) true true)
    (ite (is-some (second (first (second initial_0004)))) true true)
    (ite (is-some (second (second (second initial_0004)))) true true)
    (ite (is-some (first _internal_param_stor_0010)) true true)
    (ite (is-some (second (first (second _internal_param_stor_0010)))) true
      true)
    (ite (is-some (second (second (second _internal_param_stor_0010)))) true
      true) (ite (is-some (second _internal_cdr_l16_c17-22_0033)) true true)
    (ite (is-some (second (first _internal_cdr_l16_c17-22_0032))) true true)
    (ite (is-some (second (second _internal_cdr_l16_c17-22_0032))) true true)
    (ite (is-some (first _internal_dup_l16_c11-14_0031)) true true)
    (ite (is-some (second (first (second _internal_dup_l16_c11-14_0031))))
      true true)
    (ite (is-some (second (second (second _internal_dup_l16_c11-14_0031))))
      true true)
    (ite (is-some (second _internal_car_l13_c13-18_0013)) true true)
    (ite (is-some (second (first _internal_cdr_l13_c13-18_0012))) true true)
    (ite (is-some (second (second _internal_cdr_l13_c13-18_0012))) true true)
    (ite (is-some (first _internal_dup_l13_c7-10_0011)) true true)
    (ite (is-some (second (first (second _internal_dup_l13_c7-10_0011))))
      true true)
    (ite (is-some (second (second (second _internal_dup_l13_c7-10_0011))))
      true true)
    (= _internal_result_0084
      ((as error Exception) (pack!unit _internal_unit_l19_c15-19_0048)))
    (= _internal_unit_l19_c15-19_0048 unit)
    (= _internal_eq_l17_c11-21_0037 false)
    (= _internal_eq_l17_c11-21_0037 (= _internal_compare_l17_c11-21_0036 0))
    (ite
      (= (first _internal_sender_l16_c25-31_0035)
        (first _internal_car_l16_c17-22_0034))
      (ite
        (= (second _internal_sender_l16_c25-31_0035)
          (second _internal_car_l16_c17-22_0034))
        (= _internal_compare_l17_c11-21_0036 0)
        (ite
          (str.< (second _internal_car_l16_c17-22_0034)
            (second _internal_sender_l16_c25-31_0035))
          (= _internal_compare_l17_c11-21_0036 1)
          (= _internal_compare_l17_c11-21_0036 (- 1))))
      (ite
        (str.< (first _internal_car_l16_c17-22_0034)
          (first _internal_sender_l16_c25-31_0035))
        (= _internal_compare_l17_c11-21_0036 1)
        (= _internal_compare_l17_c11-21_0036 (- 1))))
    (= _internal_sender_l16_c25-31_0035 sender)
    (= _internal_car_l16_c17-22_0034 (first _internal_cdr_l16_c17-22_0033))
    (= _internal_cdr_l16_c17-22_0033 (second _internal_cdr_l16_c17-22_0032))
    (= _internal_cdr_l16_c17-22_0032 (second _internal_dup_l16_c11-14_0031))
    (= _internal_dup_l16_c11-14_0031 _internal_param_stor_0010)
    (= _internal_eq_l14_c7-23_0020 false)
    (= _internal_eq_l14_c7-23_0020 (= _internal_compare_l14_c7-23_0019 0))
    (ite
      (= (first _internal_unpair_l13_c50-56_0017)
        (first _internal_unpair_l13_c50-56_0018))
      (ite
        (= (second _internal_unpair_l13_c50-56_0017)
          (second _internal_unpair_l13_c50-56_0018))
        (= _internal_compare_l14_c7-23_0019 0)
        (ite
          (str.< (second _internal_unpair_l13_c50-56_0018)
            (second _internal_unpair_l13_c50-56_0017))
          (= _internal_compare_l14_c7-23_0019 1)
          (= _internal_compare_l14_c7-23_0019 (- 1))))
      (ite
        (str.< (first _internal_unpair_l13_c50-56_0018)
          (first _internal_unpair_l13_c50-56_0017))
        (= _internal_compare_l14_c7-23_0019 1)
        (= _internal_compare_l14_c7-23_0019 (- 1))))
    (= _internal_pair_l13_c38-42_0016
      ((as mk-pair (Pair (Pair String String) (Pair String String)))
        _internal_unpair_l13_c50-56_0017 _internal_unpair_l13_c50-56_0018))
    (= _internal_pair_l13_c38-42_0016
      ((as mk-pair (Pair (Pair String String) (Pair String String)))
        _internal_sender_l13_c29-35_0015 _internal_car_l13_c13-18_0014))
    (= _internal_sender_l13_c29-35_0015 sender)
    (= _internal_car_l13_c13-18_0014 (first _internal_car_l13_c13-18_0013))
    (= _internal_car_l13_c13-18_0013 (first _internal_cdr_l13_c13-18_0012))
    (= _internal_cdr_l13_c13-18_0012 (second _internal_dup_l13_c7-10_0011))
    (= _internal_dup_l13_c7-10_0011 _internal_param_stor_0010)
    (= _internal_param_stor_0010 initial_0004)
    (=
      (contract_ty_opt
        ((as mk-pair (Pair String String)) (first self_addr) "%default"))
      ((as some (Option String)) "(option key_hash)"))
    (not (= _internal_result_0084 ((as error Exception) (pack!unit unit))))))
(echo
  "Finding counter-example for ab-postcondition of ContractAnnot at line 0, characters -1--1")
(check-sat)
(echo "START_MODEL")
(get-model)
(echo "END_MODEL")
(pop 1)
(push 1)
(declare-const _fresh_binding_0081 (Seq Operation))
(declare-const _fresh_binding_0082
  (Pair (Pair (Pair String String) (Option String))
    (Pair (Pair String String) (Option String))))
(declare-const _internal_car_l13_c13-18_0013
  (Pair (Pair String String) (Option String)))
(declare-const _internal_car_l13_c13-18_0014 (Pair String String))
(declare-const _internal_car_l15_c27-35_0024
  (Pair (Pair String String) (Option String)))
(declare-const _internal_car_l15_c27-35_0027 (Pair String String))
(declare-const _internal_car_l16_c17-22_0034 (Pair String String))
(declare-const _internal_car_l18_c31-39_0044 (Pair String String))
(declare-const _internal_car_l18_c31-39_0046
  (Pair (Pair String String) (Option String)))
(declare-const _internal_car_l21_c13-17_0053
  (Pair (Pair String String) (Option String)))
(declare-const _internal_cdr_l13_c13-18_0012
  (Pair (Pair (Pair String String) (Option String))
    (Pair (Pair String String) (Option String))))
(declare-const _internal_cdr_l15_c27-35_0026 (Option String))
(declare-const _internal_cdr_l15_c27-35_0029
  (Pair (Pair String String) (Option String)))
(declare-const _internal_cdr_l16_c17-22_0032
  (Pair (Pair (Pair String String) (Option String))
    (Pair (Pair String String) (Option String))))
(declare-const _internal_cdr_l16_c17-22_0033
  (Pair (Pair String String) (Option String)))
(declare-const _internal_cdr_l18_c31-39_0041
  (Pair (Pair String String) (Option String)))
(declare-const _internal_cdr_l18_c31-39_0043 (Option String))
(declare-const _internal_cdr_l21_c13-17_0054 (Option String))
(declare-const _internal_cdr_l22_c19-23_0056
  (Pair (Pair String String) (Option String)))
(declare-const _internal_cdr_l22_c19-23_0057 (Option String))
(declare-const _internal_compare_l14_c7-23_0019 Int)
(declare-const _internal_compare_l17_c11-21_0036 Int)
(declare-const _internal_compare_l31_c15-34_0070 Int)
(declare-const _internal_dup_l13_c7-10_0011
  (Pair (Option String)
    (Pair (Pair (Pair String String) (Option String))
      (Pair (Pair String String) (Option String)))))
(declare-const _internal_dup_l15_c27-35_0023
  (Pair (Pair (Pair String String) (Option String))
    (Pair (Pair String String) (Option String))))
(declare-const _internal_dup_l15_c27-35_0025
  (Pair (Pair String String) (Option String)))
(declare-const _internal_dup_l16_c11-14_0031
  (Pair (Option String)
    (Pair (Pair (Pair String String) (Option String))
      (Pair (Pair String String) (Option String)))))
(declare-const _internal_dup_l18_c31-39_0040
  (Pair (Pair (Pair String String) (Option String))
    (Pair (Pair String String) (Option String))))
(declare-const _internal_dup_l18_c31-39_0042
  (Pair (Pair String String) (Option String)))
(declare-const _internal_dup_l21_c7-10_0052
  (Pair (Pair (Pair String String) (Option String))
    (Pair (Pair String String) (Option String))))
(declare-const _internal_dup_l22_c13-16_0055
  (Pair (Pair (Pair String String) (Option String))
    (Pair (Pair String String) (Option String))))
(declare-const _internal_dup_l30_c21-24_0069 String)
(declare-const _internal_eq_l14_c7-23_0020 Bool)
(declare-const _internal_eq_l17_c11-21_0037 Bool)
(declare-const _internal_eq_l31_c15-34_0071 Bool)
(declare-const _internal_if_none_l23_c7-38_0058 String)
(declare-const _internal_if_none_l24_c11-37_0059 String)
(declare-const _internal_if_none_l29_c11-37_0067 String)
(declare-const _internal_nil_l26_c30-43_0062 (Seq Operation))
(declare-const _internal_nil_l33_c34-47_0074 (Seq Operation))
(declare-const _internal_none_l25_c15-28_0060 (Option String))
(declare-const _internal_pair_l13_c38-42_0016
  (Pair (Pair String String) (Pair String String)))
(declare-const _internal_pair_l15_c27-35_0028
  (Pair (Pair String String) (Option String)))
(declare-const _internal_pair_l18_c31-39_0045
  (Pair (Pair String String) (Option String)))
(declare-const _internal_param_stor_0010
  (Pair (Option String)
    (Pair (Pair (Pair String String) (Option String))
      (Pair (Pair String String) (Option String)))))
(declare-const _internal_result_0085
  (Pair (Seq Operation)
    (Pair (Pair (Pair String String) (Option String))
      (Pair (Pair String String) (Option String)))))
(declare-const _internal_sender_l13_c29-35_0015 (Pair String String))
(declare-const _internal_sender_l16_c25-31_0035 (Pair String String))
(declare-const _internal_set_delegate_l26_c15-27_0061 Operation)
(declare-const _internal_set_delegate_l33_c19-31_0073 Operation)
(declare-const _internal_some_l32_c19-23_0072 (Option String))
(declare-const _internal_unpair_l13_c50-56_0017 (Pair String String))
(declare-const _internal_unpair_l13_c50-56_0018 (Pair String String))
(declare-const _internal_unpair_l15_c11-17_0021 (Option String))
(declare-const _internal_unpair_l15_c11-17_0022
  (Pair (Pair (Pair String String) (Option String))
    (Pair (Pair String String) (Option String))))
(declare-const _internal_unpair_l18_c15-21_0038 (Option String))
(declare-const _internal_unpair_l18_c15-21_0039
  (Pair (Pair (Pair String String) (Option String))
    (Pair (Pair String String) (Option String))))
(declare-const initial_0004
  (Pair (Option String)
    (Pair (Pair (Pair String String) (Option String))
      (Pair (Pair String String) (Option String)))))
(assert
  (and (ite (is-some (first initial_0004)) true true)
    (ite (is-some (second (first (second initial_0004)))) true true)
    (ite (is-some (second (second (second initial_0004)))) true true)
    (ite (is-some (second (first (second _internal_result_0085)))) true true)
    (ite (is-some (second (second (second _internal_result_0085)))) true
      true) (ite (is-some (second (first _fresh_binding_0082))) true true)
    (ite (is-some (second (second _fresh_binding_0082))) true true)
    (ite (is-some _internal_none_l25_c15-28_0060) true true)
    (ite (is-some _internal_cdr_l22_c19-23_0057) true true)
    (ite (is-some _internal_cdr_l21_c13-17_0054) true true)
    (ite (is-some (second _internal_cdr_l22_c19-23_0056)) true true)
    (ite (is-some (second (first _internal_dup_l22_c13-16_0055))) true true)
    (ite (is-some (second (second _internal_dup_l22_c13-16_0055))) true true)
    (ite (is-some (second _internal_car_l21_c13-17_0053)) true true)
    (ite (is-some (second (first _internal_dup_l21_c7-10_0052))) true true)
    (ite (is-some (second (second _internal_dup_l21_c7-10_0052))) true true)
    (ite (is-some (second _internal_pair_l15_c27-35_0028)) true true)
    (ite (is-some (second _internal_cdr_l15_c27-35_0029)) true true)
    (ite (is-some (second (first _internal_dup_l15_c27-35_0023))) true true)
    (ite (is-some (second (second _internal_dup_l15_c27-35_0023))) true true)
    (ite (is-some _internal_unpair_l15_c11-17_0021) true true)
    (ite (is-some (second _internal_car_l15_c27-35_0024)) true true)
    (ite (is-some _internal_cdr_l15_c27-35_0026) true true)
    (ite (is-some (second _internal_dup_l15_c27-35_0025)) true true)
    (ite (is-some (second (first _internal_unpair_l15_c11-17_0022))) true
      true)
    (ite (is-some (second (second _internal_unpair_l15_c11-17_0022))) true
      true) (ite (is-some (first _internal_param_stor_0010)) true true)
    (ite (is-some (second (first (second _internal_param_stor_0010)))) true
      true)
    (ite (is-some (second (second (second _internal_param_stor_0010)))) true
      true) (ite (is-some (second _internal_car_l13_c13-18_0013)) true true)
    (ite (is-some (second (first _internal_cdr_l13_c13-18_0012))) true true)
    (ite (is-some (second (second _internal_cdr_l13_c13-18_0012))) true true)
    (ite (is-some (first _internal_dup_l13_c7-10_0011)) true true)
    (ite (is-some (second (first (second _internal_dup_l13_c7-10_0011))))
      true true)
    (ite (is-some (second (second (second _internal_dup_l13_c7-10_0011))))
      true true)
    (ite (is-some (second _internal_car_l18_c31-39_0046)) true true)
    (ite (is-some (second _internal_pair_l18_c31-39_0045)) true true)
    (ite (is-some (second (first _internal_dup_l18_c31-39_0040))) true true)
    (ite (is-some (second (second _internal_dup_l18_c31-39_0040))) true true)
    (ite (is-some _internal_unpair_l18_c15-21_0038) true true)
    (ite (is-some (second _internal_cdr_l18_c31-39_0041)) true true)
    (ite (is-some _internal_cdr_l18_c31-39_0043) true true)
    (ite (is-some (second _internal_dup_l18_c31-39_0042)) true true)
    (ite (is-some (second (first _internal_unpair_l18_c15-21_0039))) true
      true)
    (ite (is-some (second (second _internal_unpair_l18_c15-21_0039))) true
      true) (ite (is-some (first _internal_param_stor_0010)) true true)
    (ite (is-some (second (first (second _internal_param_stor_0010)))) true
      true)
    (ite (is-some (second (second (second _internal_param_stor_0010)))) true
      true) (ite (is-some (second _internal_cdr_l16_c17-22_0033)) true true)
    (ite (is-some (second (first _internal_cdr_l16_c17-22_0032))) true true)
    (ite (is-some (second (second _internal_cdr_l16_c17-22_0032))) true true)
    (ite (is-some (first _internal_dup_l16_c11-14_0031)) true true)
    (ite (is-some (second (first (second _internal_dup_l16_c11-14_0031))))
      true true)
    (ite (is-some (second (second (second _internal_dup_l16_c11-14_0031))))
      true true)
    (ite (is-some (second _internal_car_l13_c13-18_0013)) true true)
    (ite (is-some (second (first _internal_cdr_l13_c13-18_0012))) true true)
    (ite (is-some (second (second _internal_cdr_l13_c13-18_0012))) true true)
    (ite (is-some (first _internal_dup_l13_c7-10_0011)) true true)
    (ite (is-some (second (first (second _internal_dup_l13_c7-10_0011))))
      true true)
    (ite (is-some (second (second (second _internal_dup_l13_c7-10_0011))))
      true true) (ite (is-some _internal_cdr_l22_c19-23_0057) true true)
    (ite (is-some _internal_cdr_l21_c13-17_0054) true true)
    (ite (is-some (second _internal_cdr_l22_c19-23_0056)) true true)
    (ite (is-some (second (first _internal_dup_l22_c13-16_0055))) true true)
    (ite (is-some (second (second _internal_dup_l22_c13-16_0055))) true true)
    (ite (is-some (second _internal_car_l21_c13-17_0053)) true true)
    (ite (is-some (second (first _internal_dup_l21_c7-10_0052))) true true)
    (ite (is-some (second (second _internal_dup_l21_c7-10_0052))) true true)
    (ite (is-some (second _internal_pair_l15_c27-35_0028)) true true)
    (ite (is-some (second _internal_cdr_l15_c27-35_0029)) true true)
    (ite (is-some (second (first _internal_dup_l15_c27-35_0023))) true true)
    (ite (is-some (second (second _internal_dup_l15_c27-35_0023))) true true)
    (ite (is-some _internal_unpair_l15_c11-17_0021) true true)
    (ite (is-some (second _internal_car_l15_c27-35_0024)) true true)
    (ite (is-some _internal_cdr_l15_c27-35_0026) true true)
    (ite (is-some (second _internal_dup_l15_c27-35_0025)) true true)
    (ite (is-some (second (first _internal_unpair_l15_c11-17_0022))) true
      true)
    (ite (is-some (second (second _internal_unpair_l15_c11-17_0022))) true
      true) (ite (is-some (first _internal_param_stor_0010)) true true)
    (ite (is-some (second (first (second _internal_param_stor_0010)))) true
      true)
    (ite (is-some (second (second (second _internal_param_stor_0010)))) true
      true) (ite (is-some (second _internal_car_l13_c13-18_0013)) true true)
    (ite (is-some (second (first _internal_cdr_l13_c13-18_0012))) true true)
    (ite (is-some (second (second _internal_cdr_l13_c13-18_0012))) true true)
    (ite (is-some (first _internal_dup_l13_c7-10_0011)) true true)
    (ite (is-some (second (first (second _internal_dup_l13_c7-10_0011))))
      true true)
    (ite (is-some (second (second (second _internal_dup_l13_c7-10_0011))))
      true true)
    (ite (is-some (second _internal_car_l18_c31-39_0046)) true true)
    (ite (is-some (second _internal_pair_l18_c31-39_0045)) true true)
    (ite (is-some (second (first _internal_dup_l18_c31-39_0040))) true true)
    (ite (is-some (second (second _internal_dup_l18_c31-39_0040))) true true)
    (ite (is-some _internal_unpair_l18_c15-21_0038) true true)
    (ite (is-some (second _internal_cdr_l18_c31-39_0041)) true true)
    (ite (is-some _internal_cdr_l18_c31-39_0043) true true)
    (ite (is-some (second _internal_dup_l18_c31-39_0042)) true true)
    (ite (is-some (second (first _internal_unpair_l18_c15-21_0039))) true
      true)
    (ite (is-some (second (second _internal_unpair_l18_c15-21_0039))) true
      true) (ite (is-some (first _internal_param_stor_0010)) true true)
    (ite (is-some (second (first (second _internal_param_stor_0010)))) true
      true)
    (ite (is-some (second (second (second _internal_param_stor_0010)))) true
      true) (ite (is-some (second _internal_cdr_l16_c17-22_0033)) true true)
    (ite (is-some (second (first _internal_cdr_l16_c17-22_0032))) true true)
    (ite (is-some (second (second _internal_cdr_l16_c17-22_0032))) true true)
    (ite (is-some (first _internal_dup_l16_c11-14_0031)) true true)
    (ite (is-some (second (first (second _internal_dup_l16_c11-14_0031))))
      true true)
    (ite (is-some (second (second (second _internal_dup_l16_c11-14_0031))))
      true true)
    (ite (is-some (second _internal_car_l13_c13-18_0013)) true true)
    (ite (is-some (second (first _internal_cdr_l13_c13-18_0012))) true true)
    (ite (is-some (second (second _internal_cdr_l13_c13-18_0012))) true true)
    (ite (is-some (first _internal_dup_l13_c7-10_0011)) true true)
    (ite (is-some (second (first (second _internal_dup_l13_c7-10_0011))))
      true true)
    (ite (is-some (second (second (second _internal_dup_l13_c7-10_0011))))
      true true) (ite (is-some _internal_cdr_l22_c19-23_0057) true true)
    (ite (is-some _internal_cdr_l21_c13-17_0054) true true)
    (ite (is-some (second _internal_cdr_l22_c19-23_0056)) true true)
    (ite (is-some (second (first _internal_dup_l22_c13-16_0055))) true true)
    (ite (is-some (second (second _internal_dup_l22_c13-16_0055))) true true)
    (ite (is-some (second _internal_car_l21_c13-17_0053)) true true)
    (ite (is-some (second (first _internal_dup_l21_c7-10_0052))) true true)
    (ite (is-some (second (second _internal_dup_l21_c7-10_0052))) true true)
    (ite (is-some (second _internal_pair_l15_c27-35_0028)) true true)
    (ite (is-some (second _internal_cdr_l15_c27-35_0029)) true true)
    (ite (is-some (second (first _internal_dup_l15_c27-35_0023))) true true)
    (ite (is-some (second (second _internal_dup_l15_c27-35_0023))) true true)
    (ite (is-some _internal_unpair_l15_c11-17_0021) true true)
    (ite (is-some (second _internal_car_l15_c27-35_0024)) true true)
    (ite (is-some _internal_cdr_l15_c27-35_0026) true true)
    (ite (is-some (second _internal_dup_l15_c27-35_0025)) true true)
    (ite (is-some (second (first _internal_unpair_l15_c11-17_0022))) true
      true)
    (ite (is-some (second (second _internal_unpair_l15_c11-17_0022))) true
      true) (ite (is-some (first _internal_param_stor_0010)) true true)
    (ite (is-some (second (first (second _internal_param_stor_0010)))) true
      true)
    (ite (is-some (second (second (second _internal_param_stor_0010)))) true
      true) (ite (is-some (second _internal_car_l13_c13-18_0013)) true true)
    (ite (is-some (second (first _internal_cdr_l13_c13-18_0012))) true true)
    (ite (is-some (second (second _internal_cdr_l13_c13-18_0012))) true true)
    (ite (is-some (first _internal_dup_l13_c7-10_0011)) true true)
    (ite (is-some (second (first (second _internal_dup_l13_c7-10_0011))))
      true true)
    (ite (is-some (second (second (second _internal_dup_l13_c7-10_0011))))
      true true)
    (ite (is-some (second _internal_car_l18_c31-39_0046)) true true)
    (ite (is-some (second _internal_pair_l18_c31-39_0045)) true true)
    (ite (is-some (second (first _internal_dup_l18_c31-39_0040))) true true)
    (ite (is-some (second (second _internal_dup_l18_c31-39_0040))) true true)
    (ite (is-some _internal_unpair_l18_c15-21_0038) true true)
    (ite (is-some (second _internal_cdr_l18_c31-39_0041)) true true)
    (ite (is-some _internal_cdr_l18_c31-39_0043) true true)
    (ite (is-some (second _internal_dup_l18_c31-39_0042)) true true)
    (ite (is-some (second (first _internal_unpair_l18_c15-21_0039))) true
      true)
    (ite (is-some (second (second _internal_unpair_l18_c15-21_0039))) true
      true) (ite (is-some (first _internal_param_stor_0010)) true true)
    (ite (is-some (second (first (second _internal_param_stor_0010)))) true
      true)
    (ite (is-some (second (second (second _internal_param_stor_0010)))) true
      true) (ite (is-some (second _internal_cdr_l16_c17-22_0033)) true true)
    (ite (is-some (second (first _internal_cdr_l16_c17-22_0032))) true true)
    (ite (is-some (second (second _internal_cdr_l16_c17-22_0032))) true true)
    (ite (is-some (first _internal_dup_l16_c11-14_0031)) true true)
    (ite (is-some (second (first (second _internal_dup_l16_c11-14_0031))))
      true true)
    (ite (is-some (second (second (second _internal_dup_l16_c11-14_0031))))
      true true)
    (ite (is-some (second _internal_car_l13_c13-18_0013)) true true)
    (ite (is-some (second (first _internal_cdr_l13_c13-18_0012))) true true)
    (ite (is-some (second (second _internal_cdr_l13_c13-18_0012))) true true)
    (ite (is-some (first _internal_dup_l13_c7-10_0011)) true true)
    (ite (is-some (second (first (second _internal_dup_l13_c7-10_0011))))
      true true)
    (ite (is-some (second (second (second _internal_dup_l13_c7-10_0011))))
      true true) (ite (is-some _internal_some_l32_c19-23_0072) true true)
    (ite (is-some _internal_cdr_l22_c19-23_0057) true true)
    (ite (is-some _internal_cdr_l21_c13-17_0054) true true)
    (ite (is-some (second _internal_cdr_l22_c19-23_0056)) true true)
    (ite (is-some (second (first _internal_dup_l22_c13-16_0055))) true true)
    (ite (is-some (second (second _internal_dup_l22_c13-16_0055))) true true)
    (ite (is-some (second _internal_car_l21_c13-17_0053)) true true)
    (ite (is-some (second (first _internal_dup_l21_c7-10_0052))) true true)
    (ite (is-some (second (second _internal_dup_l21_c7-10_0052))) true true)
    (ite (is-some (second _internal_pair_l15_c27-35_0028)) true true)
    (ite (is-some (second _internal_cdr_l15_c27-35_0029)) true true)
    (ite (is-some (second (first _internal_dup_l15_c27-35_0023))) true true)
    (ite (is-some (second (second _internal_dup_l15_c27-35_0023))) true true)
    (ite (is-some _internal_unpair_l15_c11-17_0021) true true)
    (ite (is-some (second _internal_car_l15_c27-35_0024)) true true)
    (ite (is-some _internal_cdr_l15_c27-35_0026) true true)
    (ite (is-some (second _internal_dup_l15_c27-35_0025)) true true)
    (ite (is-some (second (first _internal_unpair_l15_c11-17_0022))) true
      true)
    (ite (is-some (second (second _internal_unpair_l15_c11-17_0022))) true
      true) (ite (is-some (first _internal_param_stor_0010)) true true)
    (ite (is-some (second (first (second _internal_param_stor_0010)))) true
      true)
    (ite (is-some (second (second (second _internal_param_stor_0010)))) true
      true) (ite (is-some (second _internal_car_l13_c13-18_0013)) true true)
    (ite (is-some (second (first _internal_cdr_l13_c13-18_0012))) true true)
    (ite (is-some (second (second _internal_cdr_l13_c13-18_0012))) true true)
    (ite (is-some (first _internal_dup_l13_c7-10_0011)) true true)
    (ite (is-some (second (first (second _internal_dup_l13_c7-10_0011))))
      true true)
    (ite (is-some (second (second (second _internal_dup_l13_c7-10_0011))))
      true true)
    (ite (is-some (second _internal_car_l18_c31-39_0046)) true true)
    (ite (is-some (second _internal_pair_l18_c31-39_0045)) true true)
    (ite (is-some (second (first _internal_dup_l18_c31-39_0040))) true true)
    (ite (is-some (second (second _internal_dup_l18_c31-39_0040))) true true)
    (ite (is-some _internal_unpair_l18_c15-21_0038) true true)
    (ite (is-some (second _internal_cdr_l18_c31-39_0041)) true true)
    (ite (is-some _internal_cdr_l18_c31-39_0043) true true)
    (ite (is-some (second _internal_dup_l18_c31-39_0042)) true true)
    (ite (is-some (second (first _internal_unpair_l18_c15-21_0039))) true
      true)
    (ite (is-some (second (second _internal_unpair_l18_c15-21_0039))) true
      true) (ite (is-some (first _internal_param_stor_0010)) true true)
    (ite (is-some (second (first (second _internal_param_stor_0010)))) true
      true)
    (ite (is-some (second (second (second _internal_param_stor_0010)))) true
      true) (ite (is-some (second _internal_cdr_l16_c17-22_0033)) true true)
    (ite (is-some (second (first _internal_cdr_l16_c17-22_0032))) true true)
    (ite (is-some (second (second _internal_cdr_l16_c17-22_0032))) true true)
    (ite (is-some (first _internal_dup_l16_c11-14_0031)) true true)
    (ite (is-some (second (first (second _internal_dup_l16_c11-14_0031))))
      true true)
    (ite (is-some (second (second (second _internal_dup_l16_c11-14_0031))))
      true true)
    (ite (is-some (second _internal_car_l13_c13-18_0013)) true true)
    (ite (is-some (second (first _internal_cdr_l13_c13-18_0012))) true true)
    (ite (is-some (second (second _internal_cdr_l13_c13-18_0012))) true true)
    (ite (is-some (first _internal_dup_l13_c7-10_0011)) true true)
    (ite (is-some (second (first (second _internal_dup_l13_c7-10_0011))))
      true true)
    (ite (is-some (second (second (second _internal_dup_l13_c7-10_0011))))
      true true) (ite (is-some _internal_cdr_l22_c19-23_0057) true true)
    (ite (is-some _internal_cdr_l21_c13-17_0054) true true)
    (ite (is-some (second _internal_cdr_l22_c19-23_0056)) true true)
    (ite (is-some (second (first _internal_dup_l22_c13-16_0055))) true true)
    (ite (is-some (second (second _internal_dup_l22_c13-16_0055))) true true)
    (ite (is-some (second _internal_car_l21_c13-17_0053)) true true)
    (ite (is-some (second (first _internal_dup_l21_c7-10_0052))) true true)
    (ite (is-some (second (second _internal_dup_l21_c7-10_0052))) true true)
    (ite (is-some (second _internal_pair_l15_c27-35_0028)) true true)
    (ite (is-some (second _internal_cdr_l15_c27-35_0029)) true true)
    (ite (is-some (second (first _internal_dup_l15_c27-35_0023))) true true)
    (ite (is-some (second (second _internal_dup_l15_c27-35_0023))) true true)
    (ite (is-some _internal_unpair_l15_c11-17_0021) true true)
    (ite (is-some (second _internal_car_l15_c27-35_0024)) true true)
    (ite (is-some _internal_cdr_l15_c27-35_0026) true true)
    (ite (is-some (second _internal_dup_l15_c27-35_0025)) true true)
    (ite (is-some (second (first _internal_unpair_l15_c11-17_0022))) true
      true)
    (ite (is-some (second (second _internal_unpair_l15_c11-17_0022))) true
      true) (ite (is-some (first _internal_param_stor_0010)) true true)
    (ite (is-some (second (first (second _internal_param_stor_0010)))) true
      true)
    (ite (is-some (second (second (second _internal_param_stor_0010)))) true
      true) (ite (is-some (second _internal_car_l13_c13-18_0013)) true true)
    (ite (is-some (second (first _internal_cdr_l13_c13-18_0012))) true true)
    (ite (is-some (second (second _internal_cdr_l13_c13-18_0012))) true true)
    (ite (is-some (first _internal_dup_l13_c7-10_0011)) true true)
    (ite (is-some (second (first (second _internal_dup_l13_c7-10_0011))))
      true true)
    (ite (is-some (second (second (second _internal_dup_l13_c7-10_0011))))
      true true)
    (ite (is-some (second _internal_car_l18_c31-39_0046)) true true)
    (ite (is-some (second _internal_pair_l18_c31-39_0045)) true true)
    (ite (is-some (second (first _internal_dup_l18_c31-39_0040))) true true)
    (ite (is-some (second (second _internal_dup_l18_c31-39_0040))) true true)
    (ite (is-some _internal_unpair_l18_c15-21_0038) true true)
    (ite (is-some (second _internal_cdr_l18_c31-39_0041)) true true)
    (ite (is-some _internal_cdr_l18_c31-39_0043) true true)
    (ite (is-some (second _internal_dup_l18_c31-39_0042)) true true)
    (ite (is-some (second (first _internal_unpair_l18_c15-21_0039))) true
      true)
    (ite (is-some (second (second _internal_unpair_l18_c15-21_0039))) true
      true) (ite (is-some (first _internal_param_stor_0010)) true true)
    (ite (is-some (second (first (second _internal_param_stor_0010)))) true
      true)
    (ite (is-some (second (second (second _internal_param_stor_0010)))) true
      true) (ite (is-some (second _internal_cdr_l16_c17-22_0033)) true true)
    (ite (is-some (second (first _internal_cdr_l16_c17-22_0032))) true true)
    (ite (is-some (second (second _internal_cdr_l16_c17-22_0032))) true true)
    (ite (is-some (first _internal_dup_l16_c11-14_0031)) true true)
    (ite (is-some (second (first (second _internal_dup_l16_c11-14_0031))))
      true true)
    (ite (is-some (second (second (second _internal_dup_l16_c11-14_0031))))
      true true)
    (ite (is-some (second _internal_car_l13_c13-18_0013)) true true)
    (ite (is-some (second (first _internal_cdr_l13_c13-18_0012))) true true)
    (ite (is-some (second (second _internal_cdr_l13_c13-18_0012))) true true)
    (ite (is-some (first _internal_dup_l13_c7-10_0011)) true true)
    (ite (is-some (second (first (second _internal_dup_l13_c7-10_0011))))
      true true)
    (ite (is-some (second (second (second _internal_dup_l13_c7-10_0011))))
      true true)
    (= _internal_result_0085
      ((as mk-pair
         (Pair (Seq Operation)
           (Pair (Pair (Pair String String) (Option String))
             (Pair (Pair String String) (Option String)))))
        _fresh_binding_0081 _fresh_binding_0082))
    (or
      (and
        (= _fresh_binding_0081
          (seq.++ (seq.unit _internal_set_delegate_l26_c15-27_0061)
            _internal_nil_l26_c30-43_0062))
        (= _internal_nil_l26_c30-43_0062 (as seq.empty (Seq Operation)))
        (= ((as setDelegate Operation) _internal_none_l25_c15-28_0060)
          _internal_set_delegate_l26_c15-27_0061)
        (= _internal_none_l25_c15-28_0060 (as none (Option String)))
        (= _internal_cdr_l22_c19-23_0057 (as none (Option String)))
        (= _internal_cdr_l21_c13-17_0054 (as none (Option String)))
        (= _internal_cdr_l22_c19-23_0057
          (second _internal_cdr_l22_c19-23_0056))
        (= _internal_cdr_l22_c19-23_0056
          (second _internal_dup_l22_c13-16_0055))
        (= _internal_dup_l22_c13-16_0055 _fresh_binding_0082)
        (= _internal_cdr_l21_c13-17_0054
          (second _internal_car_l21_c13-17_0053))
        (= _internal_car_l21_c13-17_0053
          (first _internal_dup_l21_c7-10_0052))
        (= _internal_dup_l21_c7-10_0052 _fresh_binding_0082)
        (or
          (and
            (= _fresh_binding_0082
              ((as mk-pair
                 (Pair (Pair (Pair String String) (Option String))
                   (Pair (Pair String String) (Option String))))
                _internal_pair_l15_c27-35_0028 _internal_cdr_l15_c27-35_0029))
            (= _internal_cdr_l15_c27-35_0029
              (second _internal_dup_l15_c27-35_0023))
            (= _internal_pair_l15_c27-35_0028
              ((as mk-pair (Pair (Pair String String) (Option String)))
                _internal_car_l15_c27-35_0027
                _internal_unpair_l15_c11-17_0021))
            (= _internal_car_l15_c27-35_0027
              (first _internal_car_l15_c27-35_0024))
            (= _internal_cdr_l15_c27-35_0026
              (second _internal_dup_l15_c27-35_0025))
            (= _internal_dup_l15_c27-35_0025 _internal_car_l15_c27-35_0024)
            (= _internal_car_l15_c27-35_0024
              (first _internal_unpair_l15_c11-17_0022))
            (= _internal_dup_l15_c27-35_0023
              _internal_unpair_l15_c11-17_0022)
            (= _internal_param_stor_0010
              ((as mk-pair
                 (Pair (Option String)
                   (Pair (Pair (Pair String String) (Option String))
                     (Pair (Pair String String) (Option String)))))
                _internal_unpair_l15_c11-17_0021
                _internal_unpair_l15_c11-17_0022))
            (= _internal_eq_l14_c7-23_0020 true)
            (= _internal_eq_l14_c7-23_0020
              (= _internal_compare_l14_c7-23_0019 0))
            (ite
              (= (first _internal_unpair_l13_c50-56_0017)
                (first _internal_unpair_l13_c50-56_0018))
              (ite
                (= (second _internal_unpair_l13_c50-56_0017)
                  (second _internal_unpair_l13_c50-56_0018))
                (= _internal_compare_l14_c7-23_0019 0)
                (ite
                  (str.< (second _internal_unpair_l13_c50-56_0018)
                    (second _internal_unpair_l13_c50-56_0017))
                  (= _internal_compare_l14_c7-23_0019 1)
                  (= _internal_compare_l14_c7-23_0019 (- 1))))
              (ite
                (str.< (first _internal_unpair_l13_c50-56_0018)
                  (first _internal_unpair_l13_c50-56_0017))
                (= _internal_compare_l14_c7-23_0019 1)
                (= _internal_compare_l14_c7-23_0019 (- 1))))
            (= _internal_pair_l13_c38-42_0016
              ((as mk-pair (Pair (Pair String String) (Pair String String)))
                _internal_unpair_l13_c50-56_0017
                _internal_unpair_l13_c50-56_0018))
            (= _internal_pair_l13_c38-42_0016
              ((as mk-pair (Pair (Pair String String) (Pair String String)))
                _internal_sender_l13_c29-35_0015
                _internal_car_l13_c13-18_0014))
            (= _internal_sender_l13_c29-35_0015 sender)
            (= _internal_car_l13_c13-18_0014
              (first _internal_car_l13_c13-18_0013))
            (= _internal_car_l13_c13-18_0013
              (first _internal_cdr_l13_c13-18_0012))
            (= _internal_cdr_l13_c13-18_0012
              (second _internal_dup_l13_c7-10_0011))
            (= _internal_dup_l13_c7-10_0011 _internal_param_stor_0010)
            (= _internal_param_stor_0010 initial_0004)
            (=
              (contract_ty_opt
                ((as mk-pair (Pair String String)) (first self_addr)
                  "%default"))
              ((as some (Option String)) "(option key_hash)")))
          (and
            (= _fresh_binding_0082
              ((as mk-pair
                 (Pair (Pair (Pair String String) (Option String))
                   (Pair (Pair String String) (Option String))))
                _internal_car_l18_c31-39_0046 _internal_pair_l18_c31-39_0045))
            (= _internal_car_l18_c31-39_0046
              (first _internal_dup_l18_c31-39_0040))
            (= _internal_pair_l18_c31-39_0045
              ((as mk-pair (Pair (Pair String String) (Option String)))
                _internal_car_l18_c31-39_0044
                _internal_unpair_l18_c15-21_0038))
            (= _internal_car_l18_c31-39_0044
              (first _internal_cdr_l18_c31-39_0041))
            (= _internal_cdr_l18_c31-39_0043
              (second _internal_dup_l18_c31-39_0042))
            (= _internal_dup_l18_c31-39_0042 _internal_cdr_l18_c31-39_0041)
            (= _internal_cdr_l18_c31-39_0041
              (second _internal_unpair_l18_c15-21_0039))
            (= _internal_dup_l18_c31-39_0040
              _internal_unpair_l18_c15-21_0039)
            (= _internal_param_stor_0010
              ((as mk-pair
                 (Pair (Option String)
                   (Pair (Pair (Pair String String) (Option String))
                     (Pair (Pair String String) (Option String)))))
                _internal_unpair_l18_c15-21_0038
                _internal_unpair_l18_c15-21_0039))
            (= _internal_eq_l17_c11-21_0037 true)
            (= _internal_eq_l17_c11-21_0037
              (= _internal_compare_l17_c11-21_0036 0))
            (ite
              (= (first _internal_sender_l16_c25-31_0035)
                (first _internal_car_l16_c17-22_0034))
              (ite
                (= (second _internal_sender_l16_c25-31_0035)
                  (second _internal_car_l16_c17-22_0034))
                (= _internal_compare_l17_c11-21_0036 0)
                (ite
                  (str.< (second _internal_car_l16_c17-22_0034)
                    (second _internal_sender_l16_c25-31_0035))
                  (= _internal_compare_l17_c11-21_0036 1)
                  (= _internal_compare_l17_c11-21_0036 (- 1))))
              (ite
                (str.< (first _internal_car_l16_c17-22_0034)
                  (first _internal_sender_l16_c25-31_0035))
                (= _internal_compare_l17_c11-21_0036 1)
                (= _internal_compare_l17_c11-21_0036 (- 1))))
            (= _internal_sender_l16_c25-31_0035 sender)
            (= _internal_car_l16_c17-22_0034
              (first _internal_cdr_l16_c17-22_0033))
            (= _internal_cdr_l16_c17-22_0033
              (second _internal_cdr_l16_c17-22_0032))
            (= _internal_cdr_l16_c17-22_0032
              (second _internal_dup_l16_c11-14_0031))
            (= _internal_dup_l16_c11-14_0031 _internal_param_stor_0010)
            (= _internal_eq_l14_c7-23_0020 false)
            (= _internal_eq_l14_c7-23_0020
              (= _internal_compare_l14_c7-23_0019 0))
            (ite
              (= (first _internal_unpair_l13_c50-56_0017)
                (first _internal_unpair_l13_c50-56_0018))
              (ite
                (= (second _internal_unpair_l13_c50-56_0017)
                  (second _internal_unpair_l13_c50-56_0018))
                (= _internal_compare_l14_c7-23_0019 0)
                (ite
                  (str.< (second _internal_unpair_l13_c50-56_0018)
                    (second _internal_unpair_l13_c50-56_0017))
                  (= _internal_compare_l14_c7-23_0019 1)
                  (= _internal_compare_l14_c7-23_0019 (- 1))))
              (ite
                (str.< (first _internal_unpair_l13_c50-56_0018)
                  (first _internal_unpair_l13_c50-56_0017))
                (= _internal_compare_l14_c7-23_0019 1)
                (= _internal_compare_l14_c7-23_0019 (- 1))))
            (= _internal_pair_l13_c38-42_0016
              ((as mk-pair (Pair (Pair String String) (Pair String String)))
                _internal_unpair_l13_c50-56_0017
                _internal_unpair_l13_c50-56_0018))
            (= _internal_pair_l13_c38-42_0016
              ((as mk-pair (Pair (Pair String String) (Pair String String)))
                _internal_sender_l13_c29-35_0015
                _internal_car_l13_c13-18_0014))
            (= _internal_sender_l13_c29-35_0015 sender)
            (= _internal_car_l13_c13-18_0014
              (first _internal_car_l13_c13-18_0013))
            (= _internal_car_l13_c13-18_0013
              (first _internal_cdr_l13_c13-18_0012))
            (= _internal_cdr_l13_c13-18_0012
              (second _internal_dup_l13_c7-10_0011))
            (= _internal_dup_l13_c7-10_0011 _internal_param_stor_0010)
            (= _internal_param_stor_0010 initial_0004)
            (=
              (contract_ty_opt
                ((as mk-pair (Pair String String)) (first self_addr)
                  "%default"))
              ((as some (Option String)) "(option key_hash)")))))
      (and (= _fresh_binding_0081 (as seq.empty (Seq Operation)))
        (= _internal_cdr_l22_c19-23_0057
          ((as some (Option String)) _internal_if_none_l24_c11-37_0059))
        (= _internal_cdr_l21_c13-17_0054 (as none (Option String)))
        (= _internal_cdr_l22_c19-23_0057
          (second _internal_cdr_l22_c19-23_0056))
        (= _internal_cdr_l22_c19-23_0056
          (second _internal_dup_l22_c13-16_0055))
        (= _internal_dup_l22_c13-16_0055 _fresh_binding_0082)
        (= _internal_cdr_l21_c13-17_0054
          (second _internal_car_l21_c13-17_0053))
        (= _internal_car_l21_c13-17_0053
          (first _internal_dup_l21_c7-10_0052))
        (= _internal_dup_l21_c7-10_0052 _fresh_binding_0082)
        (or
          (and
            (= _fresh_binding_0082
              ((as mk-pair
                 (Pair (Pair (Pair String String) (Option String))
                   (Pair (Pair String String) (Option String))))
                _internal_pair_l15_c27-35_0028 _internal_cdr_l15_c27-35_0029))
            (= _internal_cdr_l15_c27-35_0029
              (second _internal_dup_l15_c27-35_0023))
            (= _internal_pair_l15_c27-35_0028
              ((as mk-pair (Pair (Pair String String) (Option String)))
                _internal_car_l15_c27-35_0027
                _internal_unpair_l15_c11-17_0021))
            (= _internal_car_l15_c27-35_0027
              (first _internal_car_l15_c27-35_0024))
            (= _internal_cdr_l15_c27-35_0026
              (second _internal_dup_l15_c27-35_0025))
            (= _internal_dup_l15_c27-35_0025 _internal_car_l15_c27-35_0024)
            (= _internal_car_l15_c27-35_0024
              (first _internal_unpair_l15_c11-17_0022))
            (= _internal_dup_l15_c27-35_0023
              _internal_unpair_l15_c11-17_0022)
            (= _internal_param_stor_0010
              ((as mk-pair
                 (Pair (Option String)
                   (Pair (Pair (Pair String String) (Option String))
                     (Pair (Pair String String) (Option String)))))
                _internal_unpair_l15_c11-17_0021
                _internal_unpair_l15_c11-17_0022))
            (= _internal_eq_l14_c7-23_0020 true)
            (= _internal_eq_l14_c7-23_0020
              (= _internal_compare_l14_c7-23_0019 0))
            (ite
              (= (first _internal_unpair_l13_c50-56_0017)
                (first _internal_unpair_l13_c50-56_0018))
              (ite
                (= (second _internal_unpair_l13_c50-56_0017)
                  (second _internal_unpair_l13_c50-56_0018))
                (= _internal_compare_l14_c7-23_0019 0)
                (ite
                  (str.< (second _internal_unpair_l13_c50-56_0018)
                    (second _internal_unpair_l13_c50-56_0017))
                  (= _internal_compare_l14_c7-23_0019 1)
                  (= _internal_compare_l14_c7-23_0019 (- 1))))
              (ite
                (str.< (first _internal_unpair_l13_c50-56_0018)
                  (first _internal_unpair_l13_c50-56_0017))
                (= _internal_compare_l14_c7-23_0019 1)
                (= _internal_compare_l14_c7-23_0019 (- 1))))
            (= _internal_pair_l13_c38-42_0016
              ((as mk-pair (Pair (Pair String String) (Pair String String)))
                _internal_unpair_l13_c50-56_0017
                _internal_unpair_l13_c50-56_0018))
            (= _internal_pair_l13_c38-42_0016
              ((as mk-pair (Pair (Pair String String) (Pair String String)))
                _internal_sender_l13_c29-35_0015
                _internal_car_l13_c13-18_0014))
            (= _internal_sender_l13_c29-35_0015 sender)
            (= _internal_car_l13_c13-18_0014
              (first _internal_car_l13_c13-18_0013))
            (= _internal_car_l13_c13-18_0013
              (first _internal_cdr_l13_c13-18_0012))
            (= _internal_cdr_l13_c13-18_0012
              (second _internal_dup_l13_c7-10_0011))
            (= _internal_dup_l13_c7-10_0011 _internal_param_stor_0010)
            (= _internal_param_stor_0010 initial_0004)
            (=
              (contract_ty_opt
                ((as mk-pair (Pair String String)) (first self_addr)
                  "%default"))
              ((as some (Option String)) "(option key_hash)")))
          (and
            (= _fresh_binding_0082
              ((as mk-pair
                 (Pair (Pair (Pair String String) (Option String))
                   (Pair (Pair String String) (Option String))))
                _internal_car_l18_c31-39_0046 _internal_pair_l18_c31-39_0045))
            (= _internal_car_l18_c31-39_0046
              (first _internal_dup_l18_c31-39_0040))
            (= _internal_pair_l18_c31-39_0045
              ((as mk-pair (Pair (Pair String String) (Option String)))
                _internal_car_l18_c31-39_0044
                _internal_unpair_l18_c15-21_0038))
            (= _internal_car_l18_c31-39_0044
              (first _internal_cdr_l18_c31-39_0041))
            (= _internal_cdr_l18_c31-39_0043
              (second _internal_dup_l18_c31-39_0042))
            (= _internal_dup_l18_c31-39_0042 _internal_cdr_l18_c31-39_0041)
            (= _internal_cdr_l18_c31-39_0041
              (second _internal_unpair_l18_c15-21_0039))
            (= _internal_dup_l18_c31-39_0040
              _internal_unpair_l18_c15-21_0039)
            (= _internal_param_stor_0010
              ((as mk-pair
                 (Pair (Option String)
                   (Pair (Pair (Pair String String) (Option String))
                     (Pair (Pair String String) (Option String)))))
                _internal_unpair_l18_c15-21_0038
                _internal_unpair_l18_c15-21_0039))
            (= _internal_eq_l17_c11-21_0037 true)
            (= _internal_eq_l17_c11-21_0037
              (= _internal_compare_l17_c11-21_0036 0))
            (ite
              (= (first _internal_sender_l16_c25-31_0035)
                (first _internal_car_l16_c17-22_0034))
              (ite
                (= (second _internal_sender_l16_c25-31_0035)
                  (second _internal_car_l16_c17-22_0034))
                (= _internal_compare_l17_c11-21_0036 0)
                (ite
                  (str.< (second _internal_car_l16_c17-22_0034)
                    (second _internal_sender_l16_c25-31_0035))
                  (= _internal_compare_l17_c11-21_0036 1)
                  (= _internal_compare_l17_c11-21_0036 (- 1))))
              (ite
                (str.< (first _internal_car_l16_c17-22_0034)
                  (first _internal_sender_l16_c25-31_0035))
                (= _internal_compare_l17_c11-21_0036 1)
                (= _internal_compare_l17_c11-21_0036 (- 1))))
            (= _internal_sender_l16_c25-31_0035 sender)
            (= _internal_car_l16_c17-22_0034
              (first _internal_cdr_l16_c17-22_0033))
            (= _internal_cdr_l16_c17-22_0033
              (second _internal_cdr_l16_c17-22_0032))
            (= _internal_cdr_l16_c17-22_0032
              (second _internal_dup_l16_c11-14_0031))
            (= _internal_dup_l16_c11-14_0031 _internal_param_stor_0010)
            (= _internal_eq_l14_c7-23_0020 false)
            (= _internal_eq_l14_c7-23_0020
              (= _internal_compare_l14_c7-23_0019 0))
            (ite
              (= (first _internal_unpair_l13_c50-56_0017)
                (first _internal_unpair_l13_c50-56_0018))
              (ite
                (= (second _internal_unpair_l13_c50-56_0017)
                  (second _internal_unpair_l13_c50-56_0018))
                (= _internal_compare_l14_c7-23_0019 0)
                (ite
                  (str.< (second _internal_unpair_l13_c50-56_0018)
                    (second _internal_unpair_l13_c50-56_0017))
                  (= _internal_compare_l14_c7-23_0019 1)
                  (= _internal_compare_l14_c7-23_0019 (- 1))))
              (ite
                (str.< (first _internal_unpair_l13_c50-56_0018)
                  (first _internal_unpair_l13_c50-56_0017))
                (= _internal_compare_l14_c7-23_0019 1)
                (= _internal_compare_l14_c7-23_0019 (- 1))))
            (= _internal_pair_l13_c38-42_0016
              ((as mk-pair (Pair (Pair String String) (Pair String String)))
                _internal_unpair_l13_c50-56_0017
                _internal_unpair_l13_c50-56_0018))
            (= _internal_pair_l13_c38-42_0016
              ((as mk-pair (Pair (Pair String String) (Pair String String)))
                _internal_sender_l13_c29-35_0015
                _internal_car_l13_c13-18_0014))
            (= _internal_sender_l13_c29-35_0015 sender)
            (= _internal_car_l13_c13-18_0014
              (first _internal_car_l13_c13-18_0013))
            (= _internal_car_l13_c13-18_0013
              (first _internal_cdr_l13_c13-18_0012))
            (= _internal_cdr_l13_c13-18_0012
              (second _internal_dup_l13_c7-10_0011))
            (= _internal_dup_l13_c7-10_0011 _internal_param_stor_0010)
            (= _internal_param_stor_0010 initial_0004)
            (=
              (contract_ty_opt
                ((as mk-pair (Pair String String)) (first self_addr)
                  "%default"))
              ((as some (Option String)) "(option key_hash)")))))
      (and (= _fresh_binding_0081 (as seq.empty (Seq Operation)))
        (= _internal_cdr_l22_c19-23_0057 (as none (Option String)))
        (= _internal_cdr_l21_c13-17_0054
          ((as some (Option String)) _internal_if_none_l23_c7-38_0058))
        (= _internal_cdr_l22_c19-23_0057
          (second _internal_cdr_l22_c19-23_0056))
        (= _internal_cdr_l22_c19-23_0056
          (second _internal_dup_l22_c13-16_0055))
        (= _internal_dup_l22_c13-16_0055 _fresh_binding_0082)
        (= _internal_cdr_l21_c13-17_0054
          (second _internal_car_l21_c13-17_0053))
        (= _internal_car_l21_c13-17_0053
          (first _internal_dup_l21_c7-10_0052))
        (= _internal_dup_l21_c7-10_0052 _fresh_binding_0082)
        (or
          (and
            (= _fresh_binding_0082
              ((as mk-pair
                 (Pair (Pair (Pair String String) (Option String))
                   (Pair (Pair String String) (Option String))))
                _internal_pair_l15_c27-35_0028 _internal_cdr_l15_c27-35_0029))
            (= _internal_cdr_l15_c27-35_0029
              (second _internal_dup_l15_c27-35_0023))
            (= _internal_pair_l15_c27-35_0028
              ((as mk-pair (Pair (Pair String String) (Option String)))
                _internal_car_l15_c27-35_0027
                _internal_unpair_l15_c11-17_0021))
            (= _internal_car_l15_c27-35_0027
              (first _internal_car_l15_c27-35_0024))
            (= _internal_cdr_l15_c27-35_0026
              (second _internal_dup_l15_c27-35_0025))
            (= _internal_dup_l15_c27-35_0025 _internal_car_l15_c27-35_0024)
            (= _internal_car_l15_c27-35_0024
              (first _internal_unpair_l15_c11-17_0022))
            (= _internal_dup_l15_c27-35_0023
              _internal_unpair_l15_c11-17_0022)
            (= _internal_param_stor_0010
              ((as mk-pair
                 (Pair (Option String)
                   (Pair (Pair (Pair String String) (Option String))
                     (Pair (Pair String String) (Option String)))))
                _internal_unpair_l15_c11-17_0021
                _internal_unpair_l15_c11-17_0022))
            (= _internal_eq_l14_c7-23_0020 true)
            (= _internal_eq_l14_c7-23_0020
              (= _internal_compare_l14_c7-23_0019 0))
            (ite
              (= (first _internal_unpair_l13_c50-56_0017)
                (first _internal_unpair_l13_c50-56_0018))
              (ite
                (= (second _internal_unpair_l13_c50-56_0017)
                  (second _internal_unpair_l13_c50-56_0018))
                (= _internal_compare_l14_c7-23_0019 0)
                (ite
                  (str.< (second _internal_unpair_l13_c50-56_0018)
                    (second _internal_unpair_l13_c50-56_0017))
                  (= _internal_compare_l14_c7-23_0019 1)
                  (= _internal_compare_l14_c7-23_0019 (- 1))))
              (ite
                (str.< (first _internal_unpair_l13_c50-56_0018)
                  (first _internal_unpair_l13_c50-56_0017))
                (= _internal_compare_l14_c7-23_0019 1)
                (= _internal_compare_l14_c7-23_0019 (- 1))))
            (= _internal_pair_l13_c38-42_0016
              ((as mk-pair (Pair (Pair String String) (Pair String String)))
                _internal_unpair_l13_c50-56_0017
                _internal_unpair_l13_c50-56_0018))
            (= _internal_pair_l13_c38-42_0016
              ((as mk-pair (Pair (Pair String String) (Pair String String)))
                _internal_sender_l13_c29-35_0015
                _internal_car_l13_c13-18_0014))
            (= _internal_sender_l13_c29-35_0015 sender)
            (= _internal_car_l13_c13-18_0014
              (first _internal_car_l13_c13-18_0013))
            (= _internal_car_l13_c13-18_0013
              (first _internal_cdr_l13_c13-18_0012))
            (= _internal_cdr_l13_c13-18_0012
              (second _internal_dup_l13_c7-10_0011))
            (= _internal_dup_l13_c7-10_0011 _internal_param_stor_0010)
            (= _internal_param_stor_0010 initial_0004)
            (=
              (contract_ty_opt
                ((as mk-pair (Pair String String)) (first self_addr)
                  "%default"))
              ((as some (Option String)) "(option key_hash)")))
          (and
            (= _fresh_binding_0082
              ((as mk-pair
                 (Pair (Pair (Pair String String) (Option String))
                   (Pair (Pair String String) (Option String))))
                _internal_car_l18_c31-39_0046 _internal_pair_l18_c31-39_0045))
            (= _internal_car_l18_c31-39_0046
              (first _internal_dup_l18_c31-39_0040))
            (= _internal_pair_l18_c31-39_0045
              ((as mk-pair (Pair (Pair String String) (Option String)))
                _internal_car_l18_c31-39_0044
                _internal_unpair_l18_c15-21_0038))
            (= _internal_car_l18_c31-39_0044
              (first _internal_cdr_l18_c31-39_0041))
            (= _internal_cdr_l18_c31-39_0043
              (second _internal_dup_l18_c31-39_0042))
            (= _internal_dup_l18_c31-39_0042 _internal_cdr_l18_c31-39_0041)
            (= _internal_cdr_l18_c31-39_0041
              (second _internal_unpair_l18_c15-21_0039))
            (= _internal_dup_l18_c31-39_0040
              _internal_unpair_l18_c15-21_0039)
            (= _internal_param_stor_0010
              ((as mk-pair
                 (Pair (Option String)
                   (Pair (Pair (Pair String String) (Option String))
                     (Pair (Pair String String) (Option String)))))
                _internal_unpair_l18_c15-21_0038
                _internal_unpair_l18_c15-21_0039))
            (= _internal_eq_l17_c11-21_0037 true)
            (= _internal_eq_l17_c11-21_0037
              (= _internal_compare_l17_c11-21_0036 0))
            (ite
              (= (first _internal_sender_l16_c25-31_0035)
                (first _internal_car_l16_c17-22_0034))
              (ite
                (= (second _internal_sender_l16_c25-31_0035)
                  (second _internal_car_l16_c17-22_0034))
                (= _internal_compare_l17_c11-21_0036 0)
                (ite
                  (str.< (second _internal_car_l16_c17-22_0034)
                    (second _internal_sender_l16_c25-31_0035))
                  (= _internal_compare_l17_c11-21_0036 1)
                  (= _internal_compare_l17_c11-21_0036 (- 1))))
              (ite
                (str.< (first _internal_car_l16_c17-22_0034)
                  (first _internal_sender_l16_c25-31_0035))
                (= _internal_compare_l17_c11-21_0036 1)
                (= _internal_compare_l17_c11-21_0036 (- 1))))
            (= _internal_sender_l16_c25-31_0035 sender)
            (= _internal_car_l16_c17-22_0034
              (first _internal_cdr_l16_c17-22_0033))
            (= _internal_cdr_l16_c17-22_0033
              (second _internal_cdr_l16_c17-22_0032))
            (= _internal_cdr_l16_c17-22_0032
              (second _internal_dup_l16_c11-14_0031))
            (= _internal_dup_l16_c11-14_0031 _internal_param_stor_0010)
            (= _internal_eq_l14_c7-23_0020 false)
            (= _internal_eq_l14_c7-23_0020
              (= _internal_compare_l14_c7-23_0019 0))
            (ite
              (= (first _internal_unpair_l13_c50-56_0017)
                (first _internal_unpair_l13_c50-56_0018))
              (ite
                (= (second _internal_unpair_l13_c50-56_0017)
                  (second _internal_unpair_l13_c50-56_0018))
                (= _internal_compare_l14_c7-23_0019 0)
                (ite
                  (str.< (second _internal_unpair_l13_c50-56_0018)
                    (second _internal_unpair_l13_c50-56_0017))
                  (= _internal_compare_l14_c7-23_0019 1)
                  (= _internal_compare_l14_c7-23_0019 (- 1))))
              (ite
                (str.< (first _internal_unpair_l13_c50-56_0018)
                  (first _internal_unpair_l13_c50-56_0017))
                (= _internal_compare_l14_c7-23_0019 1)
                (= _internal_compare_l14_c7-23_0019 (- 1))))
            (= _internal_pair_l13_c38-42_0016
              ((as mk-pair (Pair (Pair String String) (Pair String String)))
                _internal_unpair_l13_c50-56_0017
                _internal_unpair_l13_c50-56_0018))
            (= _internal_pair_l13_c38-42_0016
              ((as mk-pair (Pair (Pair String String) (Pair String String)))
                _internal_sender_l13_c29-35_0015
                _internal_car_l13_c13-18_0014))
            (= _internal_sender_l13_c29-35_0015 sender)
            (= _internal_car_l13_c13-18_0014
              (first _internal_car_l13_c13-18_0013))
            (= _internal_car_l13_c13-18_0013
              (first _internal_cdr_l13_c13-18_0012))
            (= _internal_cdr_l13_c13-18_0012
              (second _internal_dup_l13_c7-10_0011))
            (= _internal_dup_l13_c7-10_0011 _internal_param_stor_0010)
            (= _internal_param_stor_0010 initial_0004)
            (=
              (contract_ty_opt
                ((as mk-pair (Pair String String)) (first self_addr)
                  "%default"))
              ((as some (Option String)) "(option key_hash)")))))
      (and
        (= _fresh_binding_0081
          (seq.++ (seq.unit _internal_set_delegate_l33_c19-31_0073)
            _internal_nil_l33_c34-47_0074))
        (= _internal_nil_l33_c34-47_0074 (as seq.empty (Seq Operation)))
        (= ((as setDelegate Operation) _internal_some_l32_c19-23_0072)
          _internal_set_delegate_l33_c19-31_0073)
        (= _internal_some_l32_c19-23_0072
          ((as some (Option String)) _internal_if_none_l23_c7-38_0058))
        (= _internal_eq_l31_c15-34_0071 true)
        (= _internal_eq_l31_c15-34_0071
          (= _internal_compare_l31_c15-34_0070 0))
        (ite
          (= _internal_if_none_l29_c11-37_0067 _internal_dup_l30_c21-24_0069)
          (= _internal_compare_l31_c15-34_0070 0)
          (ite
            (str.< _internal_dup_l30_c21-24_0069
              _internal_if_none_l29_c11-37_0067)
            (= _internal_compare_l31_c15-34_0070 1)
            (= _internal_compare_l31_c15-34_0070 (- 1))))
        (= _internal_dup_l30_c21-24_0069 _internal_if_none_l23_c7-38_0058)
        (= _internal_cdr_l22_c19-23_0057
          ((as some (Option String)) _internal_if_none_l29_c11-37_0067))
        (= _internal_cdr_l21_c13-17_0054
          ((as some (Option String)) _internal_if_none_l23_c7-38_0058))
        (= _internal_cdr_l22_c19-23_0057
          (second _internal_cdr_l22_c19-23_0056))
        (= _internal_cdr_l22_c19-23_0056
          (second _internal_dup_l22_c13-16_0055))
        (= _internal_dup_l22_c13-16_0055 _fresh_binding_0082)
        (= _internal_cdr_l21_c13-17_0054
          (second _internal_car_l21_c13-17_0053))
        (= _internal_car_l21_c13-17_0053
          (first _internal_dup_l21_c7-10_0052))
        (= _internal_dup_l21_c7-10_0052 _fresh_binding_0082)
        (or
          (and
            (= _fresh_binding_0082
              ((as mk-pair
                 (Pair (Pair (Pair String String) (Option String))
                   (Pair (Pair String String) (Option String))))
                _internal_pair_l15_c27-35_0028 _internal_cdr_l15_c27-35_0029))
            (= _internal_cdr_l15_c27-35_0029
              (second _internal_dup_l15_c27-35_0023))
            (= _internal_pair_l15_c27-35_0028
              ((as mk-pair (Pair (Pair String String) (Option String)))
                _internal_car_l15_c27-35_0027
                _internal_unpair_l15_c11-17_0021))
            (= _internal_car_l15_c27-35_0027
              (first _internal_car_l15_c27-35_0024))
            (= _internal_cdr_l15_c27-35_0026
              (second _internal_dup_l15_c27-35_0025))
            (= _internal_dup_l15_c27-35_0025 _internal_car_l15_c27-35_0024)
            (= _internal_car_l15_c27-35_0024
              (first _internal_unpair_l15_c11-17_0022))
            (= _internal_dup_l15_c27-35_0023
              _internal_unpair_l15_c11-17_0022)
            (= _internal_param_stor_0010
              ((as mk-pair
                 (Pair (Option String)
                   (Pair (Pair (Pair String String) (Option String))
                     (Pair (Pair String String) (Option String)))))
                _internal_unpair_l15_c11-17_0021
                _internal_unpair_l15_c11-17_0022))
            (= _internal_eq_l14_c7-23_0020 true)
            (= _internal_eq_l14_c7-23_0020
              (= _internal_compare_l14_c7-23_0019 0))
            (ite
              (= (first _internal_unpair_l13_c50-56_0017)
                (first _internal_unpair_l13_c50-56_0018))
              (ite
                (= (second _internal_unpair_l13_c50-56_0017)
                  (second _internal_unpair_l13_c50-56_0018))
                (= _internal_compare_l14_c7-23_0019 0)
                (ite
                  (str.< (second _internal_unpair_l13_c50-56_0018)
                    (second _internal_unpair_l13_c50-56_0017))
                  (= _internal_compare_l14_c7-23_0019 1)
                  (= _internal_compare_l14_c7-23_0019 (- 1))))
              (ite
                (str.< (first _internal_unpair_l13_c50-56_0018)
                  (first _internal_unpair_l13_c50-56_0017))
                (= _internal_compare_l14_c7-23_0019 1)
                (= _internal_compare_l14_c7-23_0019 (- 1))))
            (= _internal_pair_l13_c38-42_0016
              ((as mk-pair (Pair (Pair String String) (Pair String String)))
                _internal_unpair_l13_c50-56_0017
                _internal_unpair_l13_c50-56_0018))
            (= _internal_pair_l13_c38-42_0016
              ((as mk-pair (Pair (Pair String String) (Pair String String)))
                _internal_sender_l13_c29-35_0015
                _internal_car_l13_c13-18_0014))
            (= _internal_sender_l13_c29-35_0015 sender)
            (= _internal_car_l13_c13-18_0014
              (first _internal_car_l13_c13-18_0013))
            (= _internal_car_l13_c13-18_0013
              (first _internal_cdr_l13_c13-18_0012))
            (= _internal_cdr_l13_c13-18_0012
              (second _internal_dup_l13_c7-10_0011))
            (= _internal_dup_l13_c7-10_0011 _internal_param_stor_0010)
            (= _internal_param_stor_0010 initial_0004)
            (=
              (contract_ty_opt
                ((as mk-pair (Pair String String)) (first self_addr)
                  "%default"))
              ((as some (Option String)) "(option key_hash)")))
          (and
            (= _fresh_binding_0082
              ((as mk-pair
                 (Pair (Pair (Pair String String) (Option String))
                   (Pair (Pair String String) (Option String))))
                _internal_car_l18_c31-39_0046 _internal_pair_l18_c31-39_0045))
            (= _internal_car_l18_c31-39_0046
              (first _internal_dup_l18_c31-39_0040))
            (= _internal_pair_l18_c31-39_0045
              ((as mk-pair (Pair (Pair String String) (Option String)))
                _internal_car_l18_c31-39_0044
                _internal_unpair_l18_c15-21_0038))
            (= _internal_car_l18_c31-39_0044
              (first _internal_cdr_l18_c31-39_0041))
            (= _internal_cdr_l18_c31-39_0043
              (second _internal_dup_l18_c31-39_0042))
            (= _internal_dup_l18_c31-39_0042 _internal_cdr_l18_c31-39_0041)
            (= _internal_cdr_l18_c31-39_0041
              (second _internal_unpair_l18_c15-21_0039))
            (= _internal_dup_l18_c31-39_0040
              _internal_unpair_l18_c15-21_0039)
            (= _internal_param_stor_0010
              ((as mk-pair
                 (Pair (Option String)
                   (Pair (Pair (Pair String String) (Option String))
                     (Pair (Pair String String) (Option String)))))
                _internal_unpair_l18_c15-21_0038
                _internal_unpair_l18_c15-21_0039))
            (= _internal_eq_l17_c11-21_0037 true)
            (= _internal_eq_l17_c11-21_0037
              (= _internal_compare_l17_c11-21_0036 0))
            (ite
              (= (first _internal_sender_l16_c25-31_0035)
                (first _internal_car_l16_c17-22_0034))
              (ite
                (= (second _internal_sender_l16_c25-31_0035)
                  (second _internal_car_l16_c17-22_0034))
                (= _internal_compare_l17_c11-21_0036 0)
                (ite
                  (str.< (second _internal_car_l16_c17-22_0034)
                    (second _internal_sender_l16_c25-31_0035))
                  (= _internal_compare_l17_c11-21_0036 1)
                  (= _internal_compare_l17_c11-21_0036 (- 1))))
              (ite
                (str.< (first _internal_car_l16_c17-22_0034)
                  (first _internal_sender_l16_c25-31_0035))
                (= _internal_compare_l17_c11-21_0036 1)
                (= _internal_compare_l17_c11-21_0036 (- 1))))
            (= _internal_sender_l16_c25-31_0035 sender)
            (= _internal_car_l16_c17-22_0034
              (first _internal_cdr_l16_c17-22_0033))
            (= _internal_cdr_l16_c17-22_0033
              (second _internal_cdr_l16_c17-22_0032))
            (= _internal_cdr_l16_c17-22_0032
              (second _internal_dup_l16_c11-14_0031))
            (= _internal_dup_l16_c11-14_0031 _internal_param_stor_0010)
            (= _internal_eq_l14_c7-23_0020 false)
            (= _internal_eq_l14_c7-23_0020
              (= _internal_compare_l14_c7-23_0019 0))
            (ite
              (= (first _internal_unpair_l13_c50-56_0017)
                (first _internal_unpair_l13_c50-56_0018))
              (ite
                (= (second _internal_unpair_l13_c50-56_0017)
                  (second _internal_unpair_l13_c50-56_0018))
                (= _internal_compare_l14_c7-23_0019 0)
                (ite
                  (str.< (second _internal_unpair_l13_c50-56_0018)
                    (second _internal_unpair_l13_c50-56_0017))
                  (= _internal_compare_l14_c7-23_0019 1)
                  (= _internal_compare_l14_c7-23_0019 (- 1))))
              (ite
                (str.< (first _internal_unpair_l13_c50-56_0018)
                  (first _internal_unpair_l13_c50-56_0017))
                (= _internal_compare_l14_c7-23_0019 1)
                (= _internal_compare_l14_c7-23_0019 (- 1))))
            (= _internal_pair_l13_c38-42_0016
              ((as mk-pair (Pair (Pair String String) (Pair String String)))
                _internal_unpair_l13_c50-56_0017
                _internal_unpair_l13_c50-56_0018))
            (= _internal_pair_l13_c38-42_0016
              ((as mk-pair (Pair (Pair String String) (Pair String String)))
                _internal_sender_l13_c29-35_0015
                _internal_car_l13_c13-18_0014))
            (= _internal_sender_l13_c29-35_0015 sender)
            (= _internal_car_l13_c13-18_0014
              (first _internal_car_l13_c13-18_0013))
            (= _internal_car_l13_c13-18_0013
              (first _internal_cdr_l13_c13-18_0012))
            (= _internal_cdr_l13_c13-18_0012
              (second _internal_dup_l13_c7-10_0011))
            (= _internal_dup_l13_c7-10_0011 _internal_param_stor_0010)
            (= _internal_param_stor_0010 initial_0004)
            (=
              (contract_ty_opt
                ((as mk-pair (Pair String String)) (first self_addr)
                  "%default"))
              ((as some (Option String)) "(option key_hash)")))))
      (and (= _fresh_binding_0081 (as seq.empty (Seq Operation)))
        (= _internal_eq_l31_c15-34_0071 false)
        (= _internal_eq_l31_c15-34_0071
          (= _internal_compare_l31_c15-34_0070 0))
        (ite
          (= _internal_if_none_l29_c11-37_0067 _internal_dup_l30_c21-24_0069)
          (= _internal_compare_l31_c15-34_0070 0)
          (ite
            (str.< _internal_dup_l30_c21-24_0069
              _internal_if_none_l29_c11-37_0067)
            (= _internal_compare_l31_c15-34_0070 1)
            (= _internal_compare_l31_c15-34_0070 (- 1))))
        (= _internal_dup_l30_c21-24_0069 _internal_if_none_l23_c7-38_0058)
        (= _internal_cdr_l22_c19-23_0057
          ((as some (Option String)) _internal_if_none_l29_c11-37_0067))
        (= _internal_cdr_l21_c13-17_0054
          ((as some (Option String)) _internal_if_none_l23_c7-38_0058))
        (= _internal_cdr_l22_c19-23_0057
          (second _internal_cdr_l22_c19-23_0056))
        (= _internal_cdr_l22_c19-23_0056
          (second _internal_dup_l22_c13-16_0055))
        (= _internal_dup_l22_c13-16_0055 _fresh_binding_0082)
        (= _internal_cdr_l21_c13-17_0054
          (second _internal_car_l21_c13-17_0053))
        (= _internal_car_l21_c13-17_0053
          (first _internal_dup_l21_c7-10_0052))
        (= _internal_dup_l21_c7-10_0052 _fresh_binding_0082)
        (or
          (and
            (= _fresh_binding_0082
              ((as mk-pair
                 (Pair (Pair (Pair String String) (Option String))
                   (Pair (Pair String String) (Option String))))
                _internal_pair_l15_c27-35_0028 _internal_cdr_l15_c27-35_0029))
            (= _internal_cdr_l15_c27-35_0029
              (second _internal_dup_l15_c27-35_0023))
            (= _internal_pair_l15_c27-35_0028
              ((as mk-pair (Pair (Pair String String) (Option String)))
                _internal_car_l15_c27-35_0027
                _internal_unpair_l15_c11-17_0021))
            (= _internal_car_l15_c27-35_0027
              (first _internal_car_l15_c27-35_0024))
            (= _internal_cdr_l15_c27-35_0026
              (second _internal_dup_l15_c27-35_0025))
            (= _internal_dup_l15_c27-35_0025 _internal_car_l15_c27-35_0024)
            (= _internal_car_l15_c27-35_0024
              (first _internal_unpair_l15_c11-17_0022))
            (= _internal_dup_l15_c27-35_0023
              _internal_unpair_l15_c11-17_0022)
            (= _internal_param_stor_0010
              ((as mk-pair
                 (Pair (Option String)
                   (Pair (Pair (Pair String String) (Option String))
                     (Pair (Pair String String) (Option String)))))
                _internal_unpair_l15_c11-17_0021
                _internal_unpair_l15_c11-17_0022))
            (= _internal_eq_l14_c7-23_0020 true)
            (= _internal_eq_l14_c7-23_0020
              (= _internal_compare_l14_c7-23_0019 0))
            (ite
              (= (first _internal_unpair_l13_c50-56_0017)
                (first _internal_unpair_l13_c50-56_0018))
              (ite
                (= (second _internal_unpair_l13_c50-56_0017)
                  (second _internal_unpair_l13_c50-56_0018))
                (= _internal_compare_l14_c7-23_0019 0)
                (ite
                  (str.< (second _internal_unpair_l13_c50-56_0018)
                    (second _internal_unpair_l13_c50-56_0017))
                  (= _internal_compare_l14_c7-23_0019 1)
                  (= _internal_compare_l14_c7-23_0019 (- 1))))
              (ite
                (str.< (first _internal_unpair_l13_c50-56_0018)
                  (first _internal_unpair_l13_c50-56_0017))
                (= _internal_compare_l14_c7-23_0019 1)
                (= _internal_compare_l14_c7-23_0019 (- 1))))
            (= _internal_pair_l13_c38-42_0016
              ((as mk-pair (Pair (Pair String String) (Pair String String)))
                _internal_unpair_l13_c50-56_0017
                _internal_unpair_l13_c50-56_0018))
            (= _internal_pair_l13_c38-42_0016
              ((as mk-pair (Pair (Pair String String) (Pair String String)))
                _internal_sender_l13_c29-35_0015
                _internal_car_l13_c13-18_0014))
            (= _internal_sender_l13_c29-35_0015 sender)
            (= _internal_car_l13_c13-18_0014
              (first _internal_car_l13_c13-18_0013))
            (= _internal_car_l13_c13-18_0013
              (first _internal_cdr_l13_c13-18_0012))
            (= _internal_cdr_l13_c13-18_0012
              (second _internal_dup_l13_c7-10_0011))
            (= _internal_dup_l13_c7-10_0011 _internal_param_stor_0010)
            (= _internal_param_stor_0010 initial_0004)
            (=
              (contract_ty_opt
                ((as mk-pair (Pair String String)) (first self_addr)
                  "%default"))
              ((as some (Option String)) "(option key_hash)")))
          (and
            (= _fresh_binding_0082
              ((as mk-pair
                 (Pair (Pair (Pair String String) (Option String))
                   (Pair (Pair String String) (Option String))))
                _internal_car_l18_c31-39_0046 _internal_pair_l18_c31-39_0045))
            (= _internal_car_l18_c31-39_0046
              (first _internal_dup_l18_c31-39_0040))
            (= _internal_pair_l18_c31-39_0045
              ((as mk-pair (Pair (Pair String String) (Option String)))
                _internal_car_l18_c31-39_0044
                _internal_unpair_l18_c15-21_0038))
            (= _internal_car_l18_c31-39_0044
              (first _internal_cdr_l18_c31-39_0041))
            (= _internal_cdr_l18_c31-39_0043
              (second _internal_dup_l18_c31-39_0042))
            (= _internal_dup_l18_c31-39_0042 _internal_cdr_l18_c31-39_0041)
            (= _internal_cdr_l18_c31-39_0041
              (second _internal_unpair_l18_c15-21_0039))
            (= _internal_dup_l18_c31-39_0040
              _internal_unpair_l18_c15-21_0039)
            (= _internal_param_stor_0010
              ((as mk-pair
                 (Pair (Option String)
                   (Pair (Pair (Pair String String) (Option String))
                     (Pair (Pair String String) (Option String)))))
                _internal_unpair_l18_c15-21_0038
                _internal_unpair_l18_c15-21_0039))
            (= _internal_eq_l17_c11-21_0037 true)
            (= _internal_eq_l17_c11-21_0037
              (= _internal_compare_l17_c11-21_0036 0))
            (ite
              (= (first _internal_sender_l16_c25-31_0035)
                (first _internal_car_l16_c17-22_0034))
              (ite
                (= (second _internal_sender_l16_c25-31_0035)
                  (second _internal_car_l16_c17-22_0034))
                (= _internal_compare_l17_c11-21_0036 0)
                (ite
                  (str.< (second _internal_car_l16_c17-22_0034)
                    (second _internal_sender_l16_c25-31_0035))
                  (= _internal_compare_l17_c11-21_0036 1)
                  (= _internal_compare_l17_c11-21_0036 (- 1))))
              (ite
                (str.< (first _internal_car_l16_c17-22_0034)
                  (first _internal_sender_l16_c25-31_0035))
                (= _internal_compare_l17_c11-21_0036 1)
                (= _internal_compare_l17_c11-21_0036 (- 1))))
            (= _internal_sender_l16_c25-31_0035 sender)
            (= _internal_car_l16_c17-22_0034
              (first _internal_cdr_l16_c17-22_0033))
            (= _internal_cdr_l16_c17-22_0033
              (second _internal_cdr_l16_c17-22_0032))
            (= _internal_cdr_l16_c17-22_0032
              (second _internal_dup_l16_c11-14_0031))
            (= _internal_dup_l16_c11-14_0031 _internal_param_stor_0010)
            (= _internal_eq_l14_c7-23_0020 false)
            (= _internal_eq_l14_c7-23_0020
              (= _internal_compare_l14_c7-23_0019 0))
            (ite
              (= (first _internal_unpair_l13_c50-56_0017)
                (first _internal_unpair_l13_c50-56_0018))
              (ite
                (= (second _internal_unpair_l13_c50-56_0017)
                  (second _internal_unpair_l13_c50-56_0018))
                (= _internal_compare_l14_c7-23_0019 0)
                (ite
                  (str.< (second _internal_unpair_l13_c50-56_0018)
                    (second _internal_unpair_l13_c50-56_0017))
                  (= _internal_compare_l14_c7-23_0019 1)
                  (= _internal_compare_l14_c7-23_0019 (- 1))))
              (ite
                (str.< (first _internal_unpair_l13_c50-56_0018)
                  (first _internal_unpair_l13_c50-56_0017))
                (= _internal_compare_l14_c7-23_0019 1)
                (= _internal_compare_l14_c7-23_0019 (- 1))))
            (= _internal_pair_l13_c38-42_0016
              ((as mk-pair (Pair (Pair String String) (Pair String String)))
                _internal_unpair_l13_c50-56_0017
                _internal_unpair_l13_c50-56_0018))
            (= _internal_pair_l13_c38-42_0016
              ((as mk-pair (Pair (Pair String String) (Pair String String)))
                _internal_sender_l13_c29-35_0015
                _internal_car_l13_c13-18_0014))
            (= _internal_sender_l13_c29-35_0015 sender)
            (= _internal_car_l13_c13-18_0014
              (first _internal_car_l13_c13-18_0013))
            (= _internal_car_l13_c13-18_0013
              (first _internal_cdr_l13_c13-18_0012))
            (= _internal_cdr_l13_c13-18_0012
              (second _internal_dup_l13_c7-10_0011))
            (= _internal_dup_l13_c7-10_0011 _internal_param_stor_0010)
            (= _internal_param_stor_0010 initial_0004)
            (=
              (contract_ty_opt
                ((as mk-pair (Pair String String)) (first self_addr)
                  "%default"))
              ((as some (Option String)) "(option key_hash)"))))))
    (not
      (and
        (or (not (= sender (first (first (second _internal_result_0085)))))
          (= (second (first (second _internal_result_0085)))
            (first initial_0004)))
        (or (not (= sender (first (second (second _internal_result_0085)))))
          (= (second (second (second _internal_result_0085)))
            (first initial_0004)))
        (= (first _internal_result_0085)
          (ite
            (=
              (= (second (first (second _internal_result_0085)))
                (second (second (second _internal_result_0085)))) true)
            (seq.++
              (seq.unit
                ((as setDelegate Operation)
                  (second (first (second _internal_result_0085)))))
              (as seq.empty (Seq Operation))) (as seq.empty (Seq Operation))))))))
(echo
  "Finding counter-example for postcondition of ContractAnnot at line 0, characters -1--1")
(check-sat)
(echo "START_MODEL")
(get-model)
(echo "END_MODEL")
(pop 1)


