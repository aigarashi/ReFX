Finding counter-example for ab-postcondition of ContractAnnot at line 0, characters -1--1
unsat
START_MODEL
(error "line 342 column 10: model is not available")
END_MODEL
Finding counter-example for postcondition of ContractAnnot at line 0, characters -1--1
sat
START_MODEL
(
  (define-fun _internal_contract_l20_c9-22_0021 () (Option Contract)
    (some (contract (mk-pair "t" "it"))))
  (define-fun _internal_now_l18_c4-7_0015 () Int
    0)
  (define-fun _internal_car_l17_c16-20_0013 () (Pair Int Int)
    (mk-pair 0 2))
  (define-fun _fresh_binding_0027 () (Pair (Pair Int Int) (Pair (Pair String String) (Pair String String)))
    (mk-pair (mk-pair 0 2) (mk-pair (mk-pair "t" "it") (mk-pair "t" "it"))))
  (define-fun _internal_cdr_l25_c15-19_0035 () (Pair (Pair String String) (Pair String String))
    (mk-pair (mk-pair "t" "it") (mk-pair "t" "it")))
  (define-fun _internal_dup_l25_c9-12_0034 () (Pair (Pair Int Int) (Pair (Pair String String) (Pair String String)))
    (mk-pair (mk-pair 0 2) (mk-pair (mk-pair "t" "it") (mk-pair "t" "it"))))
  (define-fun _internal_cons_l30_c32-36_0048 () (Seq Operation)
    (seq.unit (transferTokens "nit" 1 (contract (mk-pair "t" "it")))))
  (define-fun _internal_unit_l28_c9-13_0045 () Unit
    unit)
  (define-fun _fresh_binding_0043 () (Pair (Pair Int Int) (Pair (Pair String String) (Pair String String)))
    (mk-pair (mk-pair 0 2) (mk-pair (mk-pair "t" "it") (mk-pair "t" "it"))))
  (define-fun _internal_transfer_tokens_l29_c9-24_0046 () Operation
    (transferTokens "nit" 1 (contract (mk-pair "t" "it"))))
  (define-fun _internal_nil_l30_c9-22_0047 () (Seq Operation)
    (as seq.empty (Seq Operation)))
  (define-fun initial_0004 () (Pair Unit
                                   (Pair (Pair Int Int)
                                         (Pair (Pair String String)
                                               (Pair String String))))
    (mk-pair unit
         (mk-pair (mk-pair 0 2) (mk-pair (mk-pair "t" "it") (mk-pair "t" "it")))))
  (define-fun amount () Int
    0)
  (define-fun _fresh_binding_0026 () Contract
    (contract (mk-pair "t" "it")))
  (define-fun _internal_contract_l26_c9-22_0037 () (Option Contract)
    (some (contract (mk-pair "t" "it"))))
  (define-fun now () Int
    0)
  (define-fun _internal_car_l25_c15-19_0036 () (Pair String String)
    (mk-pair "t" "it"))
  (define-fun _internal_cdr_l19_c15-19_0020 () (Pair String String)
    (mk-pair "t" "it"))
  (define-fun _internal_unit_l21_c19-23_0029 () Unit
    unit)
  (define-fun _internal_balance_l21_c9-16_0028 () Int
    1)
  (define-fun _internal_car_l17_c16-20_0014 () Int
    0)
  (define-fun _internal_dup_l17_c10-13_0012 () (Pair (Pair Int Int) (Pair (Pair String String) (Pair String String)))
    (mk-pair (mk-pair 0 2) (mk-pair (mk-pair "t" "it") (mk-pair "t" "it"))))
  (define-fun _internal_balance_l27_c9-16_0044 () Int
    1)
  (define-fun _internal_result_0052 () (Pair (Seq Operation)
                                             (Pair (Pair Int Int)
                                                   (Pair (Pair String String)
                                                         (Pair String String))))
    (let ((a!1 (seq.unit (transferTokens "nit" 1 (contract (mk-pair "t" "it"))))))
  (mk-pair a!1
           (mk-pair (mk-pair 0 2)
                    (mk-pair (mk-pair "t" "it") (mk-pair "t" "it"))))))
  (define-fun _internal_unit_l18_c20-22_0017 () Bool
    true)
  (define-fun self_addr () (Pair String String)
    (mk-pair "i" "%default"))
  (define-fun _internal_cons_l23_c32-36_0032 () (Seq Operation)
    (seq.unit (transferTokens "nit" 1 (contract (mk-pair "t" "it")))))
  (define-fun _internal_param_stor_0010 () (Pair Unit
                                                 (Pair (Pair Int Int)
                                                       (Pair (Pair String
                                                                   String)
                                                             (Pair String
                                                                   String))))
    (mk-pair unit
         (mk-pair (mk-pair 0 2) (mk-pair (mk-pair "t" "it") (mk-pair "t" "it")))))
  (define-fun _internal_nil_l23_c9-22_0031 () (Seq Operation)
    (as seq.empty (Seq Operation)))
  (define-fun _internal_compare_l18_c10-17_0016 () Int
    0)
  (define-fun _internal_cdr_l19_c15-19_0019 () (Pair (Pair String String) (Pair String String))
    (mk-pair (mk-pair "t" "it") (mk-pair "t" "it")))
  (define-fun source () (Pair String String)
    (mk-pair "u" "%default"))
  (define-fun _internal_dup_l19_c9-12_0018 () (Pair (Pair Int Int) (Pair (Pair String String) (Pair String String)))
    (mk-pair (mk-pair 0 2) (mk-pair (mk-pair "t" "it") (mk-pair "t" "it"))))
  (define-fun sender () (Pair String String)
    (mk-pair "n" "%default"))
  (define-fun _internal_transfer_tokens_l22_c9-24_0030 () Operation
    (transferTokens "nit" 1 (contract (mk-pair "t" "it"))))
  (define-fun balance () Int
    1)
  (define-fun _fresh_binding_0042 () Contract
    (contract (mk-pair "t" "it")))
  (define-fun unpack_opt!unit ((x!0 String)) (Option Unit)
    (some unit))
  (define-fun contract_ty_opt ((x!0 (Pair String String))) (Option String)
    (ite (= x!0
            (mk-pair "u"
                     (str.++ (seq.unit (_ Char 37))
                             (seq.unit (_ Char 100))
                             (seq.unit (_ Char 101))
                             (seq.unit (_ Char 102))
                             (seq.unit (_ Char 97))
                             (seq.unit (_ Char 117))
                             (seq.unit (_ Char 108))
                             (seq.unit (_ Char 116)))))
      (some "unit")
    (ite (= x!0
            (mk-pair "i"
                     (str.++ (seq.unit (_ Char 37))
                             (seq.unit (_ Char 100))
                             (seq.unit (_ Char 101))
                             (seq.unit (_ Char 102))
                             (seq.unit (_ Char 97))
                             (seq.unit (_ Char 117))
                             (seq.unit (_ Char 108))
                             (seq.unit (_ Char 116)))))
      (some "unit")
    (ite (= x!0 (mk-pair "t" "it")) (some "unit")
      (some "unit")))))
  (define-fun pack!unit ((x!0 Unit)) String
    "nit")
)
END_MODEL