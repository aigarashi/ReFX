decoration =
ContractAnnot {arg: {(_, (_, (a, b))) |
                    (match ((contract_opt  a), (contract_opt  b)) with
                    ((Some (Contract<unit> _)), (Some (Contract<unit> _))) ->
                    True | _ -> False)};
               res: {(ops, ((t, n), _)) |
                    (if (now <= t) then
                    (if (balance <= n) then (ops = Nil) else
                    (match ((contract_opt  b) : (option (contract unit)))
                    with (Some c) -> ..... | None -> .....)) else
                    (match ((contract_opt  a) : (option (contract unit)))
                    with (Some c) -> ..... | None -> .....))};
               exc: {err | False}; env: }