(set-logic ALL)
(declare-datatypes ((Pair 2))
  ((par (T1 T2) ((mk-pair (first T1) (second T2))))))
(declare-datatypes ((Unit 0)) (((unit))))
(declare-datatypes ((Or 2))
  ((par (T1 T2) ((left (extractLeft T1)) (right (extractRight T2))))))
(declare-datatypes ((Option 1)) ((par (T) ((none) (some (extractSome T))))))
(declare-datatypes ((Contract 0))
  (((contract (extractContract (Pair String String))))))
(define-sort Fun () Int)
(declare-datatypes ((Operation 0))
  (((setDelegate (delegateAddress (Option String)))
     (transferTokens (transferData String) (transferMutez Int)
       (transferContract Contract))
     (createContract (createContractDelegateAddress (Option String))
       (createContractMutez Int) (createContractInitStorage String)
       (createContractAddress (Pair String String))))))
(declare-datatypes ((Exception 0))
  (((overflow) (error (extractError String)))))
(declare-fun amount () Int)
(declare-fun balance () Int)
(declare-fun contract_ty_opt ((Pair String String)) (Option String))
(declare-fun now () Int)
(declare-fun pack!unit (Unit) String)
(declare-fun self_addr () (Pair String String))
(declare-fun sender () (Pair String String))
(declare-fun source () (Pair String String))
(declare-fun unpack_opt!unit (String) (Option Unit))
(assert (or (= amount balance) (> balance amount)))
(assert (= (contract_ty_opt source) ((as some (Option String)) "unit")))
(assert (= (second source) "%default"))
(assert (= (second sender) "%default"))
(assert (= (second self_addr) "%default"))
(push 1)
(declare-const _internal_car_l17_c16-20_0013 (Pair Int Int))
(declare-const _internal_car_l17_c16-20_0014 Int)
(declare-const _internal_car_l25_c15-19_0036 (Pair String String))
(declare-const _internal_cdr_l17_c4-7_0011
  (Pair (Pair Int Int) (Pair (Pair String String) (Pair String String))))
(declare-const _internal_cdr_l19_c15-19_0019
  (Pair (Pair String String) (Pair String String)))
(declare-const _internal_cdr_l19_c15-19_0020 (Pair String String))
(declare-const _internal_cdr_l25_c15-19_0035
  (Pair (Pair String String) (Pair String String)))
(declare-const _internal_compare_l18_c10-17_0016 Int)
(declare-const _internal_contract_l20_c9-22_0021 (Option Contract))
(declare-const _internal_contract_l26_c9-22_0037 (Option Contract))
(declare-const _internal_dup_l17_c10-13_0012
  (Pair (Pair Int Int) (Pair (Pair String String) (Pair String String))))
(declare-const _internal_dup_l19_c9-12_0018
  (Pair (Pair Int Int) (Pair (Pair String String) (Pair String String))))
(declare-const _internal_dup_l25_c9-12_0034
  (Pair (Pair Int Int) (Pair (Pair String String) (Pair String String))))
(declare-const _internal_now_l18_c4-7_0015 Int)
(declare-const _internal_param_stor_0010
  (Pair Unit
    (Pair (Pair Int Int) (Pair (Pair String String) (Pair String String)))))
(declare-const _internal_result_0051 Exception)
(declare-const _internal_unit_l18_c20-22_0017 Bool)
(declare-const _internal_unit_l20_c24-35_0023 Unit)
(declare-const _internal_unit_l26_c24-35_0039 Unit)
(declare-const intial_0004
  (Pair Unit
    (Pair (Pair Int Int) (Pair (Pair String String) (Pair String String)))))
(assert
  (and
    (= (unpack_opt!unit (pack!unit _internal_unit_l20_c24-35_0023))
      ((as some (Option Unit)) _internal_unit_l20_c24-35_0023))
    (= (unpack_opt!unit (pack!unit _internal_unit_l26_c24-35_0039))
      ((as some (Option Unit)) _internal_unit_l26_c24-35_0039))
    (or (= 0 (second (first (second intial_0004))))
      (> (second (first (second intial_0004))) 0))
    (or (= (second (first (second intial_0004))) 9223372036854775807)
      (> 9223372036854775807 (second (first (second intial_0004)))))
    (or (= 0 (second (first _internal_cdr_l17_c4-7_0011)))
      (> (second (first _internal_cdr_l17_c4-7_0011)) 0))
    (or (= (second (first _internal_cdr_l17_c4-7_0011)) 9223372036854775807)
      (> 9223372036854775807 (second (first _internal_cdr_l17_c4-7_0011))))
    (ite (is-some _internal_contract_l20_c9-22_0021)
      (=
        (contract_ty_opt
          (extractContract (extractSome _internal_contract_l20_c9-22_0021)))
        ((as some (Option String)) "unit")) true)
    (or (= 0 (second (first _internal_dup_l19_c9-12_0018)))
      (> (second (first _internal_dup_l19_c9-12_0018)) 0))
    (or (= (second (first _internal_dup_l19_c9-12_0018)) 9223372036854775807)
      (> 9223372036854775807 (second (first _internal_dup_l19_c9-12_0018))))
    (or (= 0 (second _internal_car_l17_c16-20_0013))
      (> (second _internal_car_l17_c16-20_0013) 0))
    (or (= (second _internal_car_l17_c16-20_0013) 9223372036854775807)
      (> 9223372036854775807 (second _internal_car_l17_c16-20_0013)))
    (or (= 0 (second (first _internal_dup_l17_c10-13_0012)))
      (> (second (first _internal_dup_l17_c10-13_0012)) 0))
    (or
      (= (second (first _internal_dup_l17_c10-13_0012)) 9223372036854775807)
      (> 9223372036854775807 (second (first _internal_dup_l17_c10-13_0012))))
    (or (= 0 (second (first (second _internal_param_stor_0010))))
      (> (second (first (second _internal_param_stor_0010))) 0))
    (or
      (= (second (first (second _internal_param_stor_0010)))
        9223372036854775807)
      (> 9223372036854775807
        (second (first (second _internal_param_stor_0010)))))
    (or (= 0 (second (first _internal_cdr_l17_c4-7_0011)))
      (> (second (first _internal_cdr_l17_c4-7_0011)) 0))
    (or (= (second (first _internal_cdr_l17_c4-7_0011)) 9223372036854775807)
      (> 9223372036854775807 (second (first _internal_cdr_l17_c4-7_0011))))
    (ite (is-some _internal_contract_l26_c9-22_0037)
      (=
        (contract_ty_opt
          (extractContract (extractSome _internal_contract_l26_c9-22_0037)))
        ((as some (Option String)) "unit")) true)
    (or (= 0 (second (first _internal_dup_l25_c9-12_0034)))
      (> (second (first _internal_dup_l25_c9-12_0034)) 0))
    (or (= (second (first _internal_dup_l25_c9-12_0034)) 9223372036854775807)
      (> 9223372036854775807 (second (first _internal_dup_l25_c9-12_0034))))
    (or (= 0 (second _internal_car_l17_c16-20_0013))
      (> (second _internal_car_l17_c16-20_0013) 0))
    (or (= (second _internal_car_l17_c16-20_0013) 9223372036854775807)
      (> 9223372036854775807 (second _internal_car_l17_c16-20_0013)))
    (or (= 0 (second (first _internal_dup_l17_c10-13_0012)))
      (> (second (first _internal_dup_l17_c10-13_0012)) 0))
    (or
      (= (second (first _internal_dup_l17_c10-13_0012)) 9223372036854775807)
      (> 9223372036854775807 (second (first _internal_dup_l17_c10-13_0012))))
    (or (= 0 (second (first (second _internal_param_stor_0010))))
      (> (second (first (second _internal_param_stor_0010))) 0))
    (or
      (= (second (first (second _internal_param_stor_0010)))
        9223372036854775807)
      (> 9223372036854775807
        (second (first (second _internal_param_stor_0010)))))
    (or
      (and
        (= _internal_result_0051
          ((as error Exception) (pack!unit _internal_unit_l20_c24-35_0023)))
        (= _internal_unit_l20_c24-35_0023 unit)
        (= _internal_contract_l20_c9-22_0021 (as none (Option Contract)))
        (= _internal_contract_l20_c9-22_0021
          (ite
            (= (contract_ty_opt _internal_cdr_l19_c15-19_0020)
              ((as some (Option String)) "unit"))
            ((as some (Option Contract))
              ((as contract Contract) _internal_cdr_l19_c15-19_0020))
            (as none (Option Contract))))
        (= _internal_cdr_l19_c15-19_0020
          (second _internal_cdr_l19_c15-19_0019))
        (= _internal_cdr_l19_c15-19_0019
          (second _internal_dup_l19_c9-12_0018))
        (= _internal_dup_l19_c9-12_0018 _internal_cdr_l17_c4-7_0011)
        (= _internal_unit_l18_c20-22_0017 true)
        (= _internal_unit_l18_c20-22_0017
          (or (= _internal_compare_l18_c10-17_0016 0)
            (> 0 _internal_compare_l18_c10-17_0016)))
        (ite (= _internal_now_l18_c4-7_0015 _internal_car_l17_c16-20_0014)
          (= _internal_compare_l18_c10-17_0016 0)
          (ite (> _internal_now_l18_c4-7_0015 _internal_car_l17_c16-20_0014)
            (= _internal_compare_l18_c10-17_0016 1)
            (= _internal_compare_l18_c10-17_0016 (- 1))))
        (= _internal_now_l18_c4-7_0015 now)
        (= _internal_car_l17_c16-20_0014
          (first _internal_car_l17_c16-20_0013))
        (= _internal_car_l17_c16-20_0013
          (first _internal_dup_l17_c10-13_0012))
        (= _internal_dup_l17_c10-13_0012 _internal_cdr_l17_c4-7_0011)
        (= _internal_cdr_l17_c4-7_0011 (second _internal_param_stor_0010))
        (= _internal_param_stor_0010 intial_0004)
        (ite
          (and
            (is-some
              (first
                ((as mk-pair (Pair (Option Contract) (Option Contract)))
                  (ite
                    (=
                      (contract_ty_opt (first (second (second intial_0004))))
                      ((as some (Option String)) "unit"))
                    ((as some (Option Contract))
                      ((as contract Contract)
                        (first (second (second intial_0004)))))
                    (as none (Option Contract)))
                  (ite
                    (=
                      (contract_ty_opt
                        (second (second (second intial_0004))))
                      ((as some (Option String)) "unit"))
                    ((as some (Option Contract))
                      ((as contract Contract)
                        (second (second (second intial_0004)))))
                    (as none (Option Contract))))))
            (is-some
              (second
                ((as mk-pair (Pair (Option Contract) (Option Contract)))
                  (ite
                    (=
                      (contract_ty_opt (first (second (second intial_0004))))
                      ((as some (Option String)) "unit"))
                    ((as some (Option Contract))
                      ((as contract Contract)
                        (first (second (second intial_0004)))))
                    (as none (Option Contract)))
                  (ite
                    (=
                      (contract_ty_opt
                        (second (second (second intial_0004))))
                      ((as some (Option String)) "unit"))
                    ((as some (Option Contract))
                      ((as contract Contract)
                        (second (second (second intial_0004)))))
                    (as none (Option Contract))))))) true false)
        (=
          (contract_ty_opt
            ((as mk-pair (Pair String String)) (first self_addr) "%default"))
          ((as some (Option String)) "unit")))
      (and
        (= _internal_result_0051
          ((as error Exception) (pack!unit _internal_unit_l26_c24-35_0039)))
        (= _internal_unit_l26_c24-35_0039 unit)
        (= _internal_contract_l26_c9-22_0037 (as none (Option Contract)))
        (= _internal_contract_l26_c9-22_0037
          (ite
            (= (contract_ty_opt _internal_car_l25_c15-19_0036)
              ((as some (Option String)) "unit"))
            ((as some (Option Contract))
              ((as contract Contract) _internal_car_l25_c15-19_0036))
            (as none (Option Contract))))
        (= _internal_car_l25_c15-19_0036
          (first _internal_cdr_l25_c15-19_0035))
        (= _internal_cdr_l25_c15-19_0035
          (second _internal_dup_l25_c9-12_0034))
        (= _internal_dup_l25_c9-12_0034 _internal_cdr_l17_c4-7_0011)
        (= _internal_unit_l18_c20-22_0017 false)
        (= _internal_unit_l18_c20-22_0017
          (or (= _internal_compare_l18_c10-17_0016 0)
            (> 0 _internal_compare_l18_c10-17_0016)))
        (ite (= _internal_now_l18_c4-7_0015 _internal_car_l17_c16-20_0014)
          (= _internal_compare_l18_c10-17_0016 0)
          (ite (> _internal_now_l18_c4-7_0015 _internal_car_l17_c16-20_0014)
            (= _internal_compare_l18_c10-17_0016 1)
            (= _internal_compare_l18_c10-17_0016 (- 1))))
        (= _internal_now_l18_c4-7_0015 now)
        (= _internal_car_l17_c16-20_0014
          (first _internal_car_l17_c16-20_0013))
        (= _internal_car_l17_c16-20_0013
          (first _internal_dup_l17_c10-13_0012))
        (= _internal_dup_l17_c10-13_0012 _internal_cdr_l17_c4-7_0011)
        (= _internal_cdr_l17_c4-7_0011 (second _internal_param_stor_0010))
        (= _internal_param_stor_0010 intial_0004)
        (ite
          (and
            (is-some
              (first
                ((as mk-pair (Pair (Option Contract) (Option Contract)))
                  (ite
                    (=
                      (contract_ty_opt (first (second (second intial_0004))))
                      ((as some (Option String)) "unit"))
                    ((as some (Option Contract))
                      ((as contract Contract)
                        (first (second (second intial_0004)))))
                    (as none (Option Contract)))
                  (ite
                    (=
                      (contract_ty_opt
                        (second (second (second intial_0004))))
                      ((as some (Option String)) "unit"))
                    ((as some (Option Contract))
                      ((as contract Contract)
                        (second (second (second intial_0004)))))
                    (as none (Option Contract))))))
            (is-some
              (second
                ((as mk-pair (Pair (Option Contract) (Option Contract)))
                  (ite
                    (=
                      (contract_ty_opt (first (second (second intial_0004))))
                      ((as some (Option String)) "unit"))
                    ((as some (Option Contract))
                      ((as contract Contract)
                        (first (second (second intial_0004)))))
                    (as none (Option Contract)))
                  (ite
                    (=
                      (contract_ty_opt
                        (second (second (second intial_0004))))
                      ((as some (Option String)) "unit"))
                    ((as some (Option Contract))
                      ((as contract Contract)
                        (second (second (second intial_0004)))))
                    (as none (Option Contract))))))) true false)
        (=
          (contract_ty_opt
            ((as mk-pair (Pair String String)) (first self_addr) "%default"))
          ((as some (Option String)) "unit"))))
    (not
      (=>
        (ite
          (and
            (is-some
              (first
                ((as mk-pair (Pair (Option Contract) (Option Contract)))
                  (ite
                    (=
                      (contract_ty_opt (first (second (second intial_0004))))
                      ((as some (Option String)) "unit"))
                    ((as some (Option Contract))
                      ((as contract Contract)
                        (first (second (second intial_0004)))))
                    (as none (Option Contract)))
                  (ite
                    (=
                      (contract_ty_opt
                        (second (second (second intial_0004))))
                      ((as some (Option String)) "unit"))
                    ((as some (Option Contract))
                      ((as contract Contract)
                        (second (second (second intial_0004)))))
                    (as none (Option Contract))))))
            (is-some
              (second
                ((as mk-pair (Pair (Option Contract) (Option Contract)))
                  (ite
                    (=
                      (contract_ty_opt (first (second (second intial_0004))))
                      ((as some (Option String)) "unit"))
                    ((as some (Option Contract))
                      ((as contract Contract)
                        (first (second (second intial_0004)))))
                    (as none (Option Contract)))
                  (ite
                    (=
                      (contract_ty_opt
                        (second (second (second intial_0004))))
                      ((as some (Option String)) "unit"))
                    ((as some (Option Contract))
                      ((as contract Contract)
                        (second (second (second intial_0004)))))
                    (as none (Option Contract))))))) true false) false))))
(echo
  "Finding counter-example for ab-postcondition of ContractAnnot at line 0, characters -1--1")
(check-sat)
(echo "START_MODEL")
(get-model)
(echo "END_MODEL")
(pop 1)
(push 1)
(declare-const _fresh_binding_0026 Contract)
(declare-const _fresh_binding_0027
  (Pair (Pair Int Int) (Pair (Pair String String) (Pair String String))))
(declare-const _fresh_binding_0042 Contract)
(declare-const _fresh_binding_0043
  (Pair (Pair Int Int) (Pair (Pair String String) (Pair String String))))
(declare-const _internal_balance_l21_c9-16_0028 Int)
(declare-const _internal_balance_l27_c9-16_0044 Int)
(declare-const _internal_car_l17_c16-20_0013 (Pair Int Int))
(declare-const _internal_car_l17_c16-20_0014 Int)
(declare-const _internal_car_l25_c15-19_0036 (Pair String String))
(declare-const _internal_cdr_l19_c15-19_0019
  (Pair (Pair String String) (Pair String String)))
(declare-const _internal_cdr_l19_c15-19_0020 (Pair String String))
(declare-const _internal_cdr_l25_c15-19_0035
  (Pair (Pair String String) (Pair String String)))
(declare-const _internal_compare_l18_c10-17_0016 Int)
(declare-const _internal_cons_l23_c32-36_0032 (Seq Operation))
(declare-const _internal_cons_l30_c32-36_0048 (Seq Operation))
(declare-const _internal_contract_l20_c9-22_0021 (Option Contract))
(declare-const _internal_contract_l26_c9-22_0037 (Option Contract))
(declare-const _internal_dup_l17_c10-13_0012
  (Pair (Pair Int Int) (Pair (Pair String String) (Pair String String))))
(declare-const _internal_dup_l19_c9-12_0018
  (Pair (Pair Int Int) (Pair (Pair String String) (Pair String String))))
(declare-const _internal_dup_l25_c9-12_0034
  (Pair (Pair Int Int) (Pair (Pair String String) (Pair String String))))
(declare-const _internal_nil_l23_c9-22_0031 (Seq Operation))
(declare-const _internal_nil_l30_c9-22_0047 (Seq Operation))
(declare-const _internal_now_l18_c4-7_0015 Int)
(declare-const _internal_param_stor_0010
  (Pair Unit
    (Pair (Pair Int Int) (Pair (Pair String String) (Pair String String)))))
(declare-const _internal_result_0052
  (Pair (Seq Operation)
    (Pair (Pair Int Int) (Pair (Pair String String) (Pair String String)))))
(declare-const _internal_transfer_tokens_l22_c9-24_0030 Operation)
(declare-const _internal_transfer_tokens_l29_c9-24_0046 Operation)
(declare-const _internal_unit_l18_c20-22_0017 Bool)
(declare-const _internal_unit_l21_c19-23_0029 Unit)
(declare-const _internal_unit_l28_c9-13_0045 Unit)
(declare-const intial_0004
  (Pair Unit
    (Pair (Pair Int Int) (Pair (Pair String String) (Pair String String)))))
(assert
  (and (or (= 0 balance) (> balance 0))
    (or (= balance 9223372036854775807) (> 9223372036854775807 balance))
    (= (unpack_opt!unit (pack!unit _internal_unit_l21_c19-23_0029))
      ((as some (Option Unit)) _internal_unit_l21_c19-23_0029))
    (= (unpack_opt!unit (pack!unit _internal_unit_l28_c9-13_0045))
      ((as some (Option Unit)) _internal_unit_l28_c9-13_0045))
    (= (unpack_opt!unit (pack!unit unit)) ((as some (Option Unit)) unit))
    (or (= 0 (second (first (second intial_0004))))
      (> (second (first (second intial_0004))) 0))
    (or (= (second (first (second intial_0004))) 9223372036854775807)
      (> 9223372036854775807 (second (first (second intial_0004)))))
    (or (= 0 (second (first (second _internal_result_0052))))
      (> (second (first (second _internal_result_0052))) 0))
    (or
      (= (second (first (second _internal_result_0052))) 9223372036854775807)
      (> 9223372036854775807 (second (first (second _internal_result_0052)))))
    (or (= 0 (second (first _fresh_binding_0027)))
      (> (second (first _fresh_binding_0027)) 0))
    (or (= (second (first _fresh_binding_0027)) 9223372036854775807)
      (> 9223372036854775807 (second (first _fresh_binding_0027))))
    (or (= 0 _internal_balance_l21_c9-16_0028)
      (> _internal_balance_l21_c9-16_0028 0))
    (or (= _internal_balance_l21_c9-16_0028 9223372036854775807)
      (> 9223372036854775807 _internal_balance_l21_c9-16_0028))
    (= (contract_ty_opt (extractContract _fresh_binding_0026))
      ((as some (Option String)) "unit"))
    (ite (is-some _internal_contract_l20_c9-22_0021)
      (=
        (contract_ty_opt
          (extractContract (extractSome _internal_contract_l20_c9-22_0021)))
        ((as some (Option String)) "unit")) true)
    (or (= 0 (second (first _internal_dup_l19_c9-12_0018)))
      (> (second (first _internal_dup_l19_c9-12_0018)) 0))
    (or (= (second (first _internal_dup_l19_c9-12_0018)) 9223372036854775807)
      (> 9223372036854775807 (second (first _internal_dup_l19_c9-12_0018))))
    (or (= 0 (second _internal_car_l17_c16-20_0013))
      (> (second _internal_car_l17_c16-20_0013) 0))
    (or (= (second _internal_car_l17_c16-20_0013) 9223372036854775807)
      (> 9223372036854775807 (second _internal_car_l17_c16-20_0013)))
    (or (= 0 (second (first _internal_dup_l17_c10-13_0012)))
      (> (second (first _internal_dup_l17_c10-13_0012)) 0))
    (or
      (= (second (first _internal_dup_l17_c10-13_0012)) 9223372036854775807)
      (> 9223372036854775807 (second (first _internal_dup_l17_c10-13_0012))))
    (or (= 0 (second (first (second _internal_param_stor_0010))))
      (> (second (first (second _internal_param_stor_0010))) 0))
    (or
      (= (second (first (second _internal_param_stor_0010)))
        9223372036854775807)
      (> 9223372036854775807
        (second (first (second _internal_param_stor_0010)))))
    (or (= 0 (second (first _fresh_binding_0043)))
      (> (second (first _fresh_binding_0043)) 0))
    (or (= (second (first _fresh_binding_0043)) 9223372036854775807)
      (> 9223372036854775807 (second (first _fresh_binding_0043))))
    (or (= 0 _internal_balance_l27_c9-16_0044)
      (> _internal_balance_l27_c9-16_0044 0))
    (or (= _internal_balance_l27_c9-16_0044 9223372036854775807)
      (> 9223372036854775807 _internal_balance_l27_c9-16_0044))
    (= (contract_ty_opt (extractContract _fresh_binding_0042))
      ((as some (Option String)) "unit"))
    (ite (is-some _internal_contract_l26_c9-22_0037)
      (=
        (contract_ty_opt
          (extractContract (extractSome _internal_contract_l26_c9-22_0037)))
        ((as some (Option String)) "unit")) true)
    (or (= 0 (second (first _internal_dup_l25_c9-12_0034)))
      (> (second (first _internal_dup_l25_c9-12_0034)) 0))
    (or (= (second (first _internal_dup_l25_c9-12_0034)) 9223372036854775807)
      (> 9223372036854775807 (second (first _internal_dup_l25_c9-12_0034))))
    (or (= 0 (second _internal_car_l17_c16-20_0013))
      (> (second _internal_car_l17_c16-20_0013) 0))
    (or (= (second _internal_car_l17_c16-20_0013) 9223372036854775807)
      (> 9223372036854775807 (second _internal_car_l17_c16-20_0013)))
    (or (= 0 (second (first _internal_dup_l17_c10-13_0012)))
      (> (second (first _internal_dup_l17_c10-13_0012)) 0))
    (or
      (= (second (first _internal_dup_l17_c10-13_0012)) 9223372036854775807)
      (> 9223372036854775807 (second (first _internal_dup_l17_c10-13_0012))))
    (or (= 0 (second (first (second _internal_param_stor_0010))))
      (> (second (first (second _internal_param_stor_0010))) 0))
    (or
      (= (second (first (second _internal_param_stor_0010)))
        9223372036854775807)
      (> 9223372036854775807
        (second (first (second _internal_param_stor_0010)))))
    (or
      (and
        (= _internal_result_0052
          ((as mk-pair
             (Pair (Seq Operation)
               (Pair (Pair Int Int)
                 (Pair (Pair String String) (Pair String String)))))
            _internal_cons_l23_c32-36_0032 _fresh_binding_0027))
        (= _internal_cons_l23_c32-36_0032
          (seq.++ (seq.unit _internal_transfer_tokens_l22_c9-24_0030)
            _internal_nil_l23_c9-22_0031))
        (= _internal_nil_l23_c9-22_0031 (as seq.empty (Seq Operation)))
        (=
          ((as transferTokens Operation)
            (pack!unit _internal_unit_l21_c19-23_0029)
            _internal_balance_l21_c9-16_0028 _fresh_binding_0026)
          _internal_transfer_tokens_l22_c9-24_0030)
        (= _internal_unit_l21_c19-23_0029 unit)
        (= _internal_balance_l21_c9-16_0028 balance)
        (= _internal_contract_l20_c9-22_0021
          ((as some (Option Contract)) _fresh_binding_0026))
        (= _internal_contract_l20_c9-22_0021
          (ite
            (= (contract_ty_opt _internal_cdr_l19_c15-19_0020)
              ((as some (Option String)) "unit"))
            ((as some (Option Contract))
              ((as contract Contract) _internal_cdr_l19_c15-19_0020))
            (as none (Option Contract))))
        (= _internal_cdr_l19_c15-19_0020
          (second _internal_cdr_l19_c15-19_0019))
        (= _internal_cdr_l19_c15-19_0019
          (second _internal_dup_l19_c9-12_0018))
        (= _internal_dup_l19_c9-12_0018 _fresh_binding_0027)
        (= _internal_unit_l18_c20-22_0017 true)
        (= _internal_unit_l18_c20-22_0017
          (or (= _internal_compare_l18_c10-17_0016 0)
            (> 0 _internal_compare_l18_c10-17_0016)))
        (ite (= _internal_now_l18_c4-7_0015 _internal_car_l17_c16-20_0014)
          (= _internal_compare_l18_c10-17_0016 0)
          (ite (> _internal_now_l18_c4-7_0015 _internal_car_l17_c16-20_0014)
            (= _internal_compare_l18_c10-17_0016 1)
            (= _internal_compare_l18_c10-17_0016 (- 1))))
        (= _internal_now_l18_c4-7_0015 now)
        (= _internal_car_l17_c16-20_0014
          (first _internal_car_l17_c16-20_0013))
        (= _internal_car_l17_c16-20_0013
          (first _internal_dup_l17_c10-13_0012))
        (= _internal_dup_l17_c10-13_0012 _fresh_binding_0027)
        (= _fresh_binding_0027 (second _internal_param_stor_0010))
        (= _internal_param_stor_0010 intial_0004)
        (ite
          (and
            (is-some
              (first
                ((as mk-pair (Pair (Option Contract) (Option Contract)))
                  (ite
                    (=
                      (contract_ty_opt (first (second (second intial_0004))))
                      ((as some (Option String)) "unit"))
                    ((as some (Option Contract))
                      ((as contract Contract)
                        (first (second (second intial_0004)))))
                    (as none (Option Contract)))
                  (ite
                    (=
                      (contract_ty_opt
                        (second (second (second intial_0004))))
                      ((as some (Option String)) "unit"))
                    ((as some (Option Contract))
                      ((as contract Contract)
                        (second (second (second intial_0004)))))
                    (as none (Option Contract))))))
            (is-some
              (second
                ((as mk-pair (Pair (Option Contract) (Option Contract)))
                  (ite
                    (=
                      (contract_ty_opt (first (second (second intial_0004))))
                      ((as some (Option String)) "unit"))
                    ((as some (Option Contract))
                      ((as contract Contract)
                        (first (second (second intial_0004)))))
                    (as none (Option Contract)))
                  (ite
                    (=
                      (contract_ty_opt
                        (second (second (second intial_0004))))
                      ((as some (Option String)) "unit"))
                    ((as some (Option Contract))
                      ((as contract Contract)
                        (second (second (second intial_0004)))))
                    (as none (Option Contract))))))) true false)
        (=
          (contract_ty_opt
            ((as mk-pair (Pair String String)) (first self_addr) "%default"))
          ((as some (Option String)) "unit")))
      (and
        (= _internal_result_0052
          ((as mk-pair
             (Pair (Seq Operation)
               (Pair (Pair Int Int)
                 (Pair (Pair String String) (Pair String String)))))
            _internal_cons_l30_c32-36_0048 _fresh_binding_0043))
        (= _internal_cons_l30_c32-36_0048
          (seq.++ (seq.unit _internal_transfer_tokens_l29_c9-24_0046)
            _internal_nil_l30_c9-22_0047))
        (= _internal_nil_l30_c9-22_0047 (as seq.empty (Seq Operation)))
        (=
          ((as transferTokens Operation)
            (pack!unit _internal_unit_l28_c9-13_0045)
            _internal_balance_l27_c9-16_0044 _fresh_binding_0042)
          _internal_transfer_tokens_l29_c9-24_0046)
        (= _internal_unit_l28_c9-13_0045 unit)
        (= _internal_balance_l27_c9-16_0044 balance)
        (= _internal_contract_l26_c9-22_0037
          ((as some (Option Contract)) _fresh_binding_0042))
        (= _internal_contract_l26_c9-22_0037
          (ite
            (= (contract_ty_opt _internal_car_l25_c15-19_0036)
              ((as some (Option String)) "unit"))
            ((as some (Option Contract))
              ((as contract Contract) _internal_car_l25_c15-19_0036))
            (as none (Option Contract))))
        (= _internal_car_l25_c15-19_0036
          (first _internal_cdr_l25_c15-19_0035))
        (= _internal_cdr_l25_c15-19_0035
          (second _internal_dup_l25_c9-12_0034))
        (= _internal_dup_l25_c9-12_0034 _fresh_binding_0043)
        (= _internal_unit_l18_c20-22_0017 false)
        (= _internal_unit_l18_c20-22_0017
          (or (= _internal_compare_l18_c10-17_0016 0)
            (> 0 _internal_compare_l18_c10-17_0016)))
        (ite (= _internal_now_l18_c4-7_0015 _internal_car_l17_c16-20_0014)
          (= _internal_compare_l18_c10-17_0016 0)
          (ite (> _internal_now_l18_c4-7_0015 _internal_car_l17_c16-20_0014)
            (= _internal_compare_l18_c10-17_0016 1)
            (= _internal_compare_l18_c10-17_0016 (- 1))))
        (= _internal_now_l18_c4-7_0015 now)
        (= _internal_car_l17_c16-20_0014
          (first _internal_car_l17_c16-20_0013))
        (= _internal_car_l17_c16-20_0013
          (first _internal_dup_l17_c10-13_0012))
        (= _internal_dup_l17_c10-13_0012 _fresh_binding_0043)
        (= _fresh_binding_0043 (second _internal_param_stor_0010))
        (= _internal_param_stor_0010 intial_0004)
        (ite
          (and
            (is-some
              (first
                ((as mk-pair (Pair (Option Contract) (Option Contract)))
                  (ite
                    (=
                      (contract_ty_opt (first (second (second intial_0004))))
                      ((as some (Option String)) "unit"))
                    ((as some (Option Contract))
                      ((as contract Contract)
                        (first (second (second intial_0004)))))
                    (as none (Option Contract)))
                  (ite
                    (=
                      (contract_ty_opt
                        (second (second (second intial_0004))))
                      ((as some (Option String)) "unit"))
                    ((as some (Option Contract))
                      ((as contract Contract)
                        (second (second (second intial_0004)))))
                    (as none (Option Contract))))))
            (is-some
              (second
                ((as mk-pair (Pair (Option Contract) (Option Contract)))
                  (ite
                    (=
                      (contract_ty_opt (first (second (second intial_0004))))
                      ((as some (Option String)) "unit"))
                    ((as some (Option Contract))
                      ((as contract Contract)
                        (first (second (second intial_0004)))))
                    (as none (Option Contract)))
                  (ite
                    (=
                      (contract_ty_opt
                        (second (second (second intial_0004))))
                      ((as some (Option String)) "unit"))
                    ((as some (Option Contract))
                      ((as contract Contract)
                        (second (second (second intial_0004)))))
                    (as none (Option Contract))))))) true false)
        (=
          (contract_ty_opt
            ((as mk-pair (Pair String String)) (first self_addr) "%default"))
          ((as some (Option String)) "unit"))))
    (not
      (=>
        (ite
          (and
            (is-some
              (first
                ((as mk-pair (Pair (Option Contract) (Option Contract)))
                  (ite
                    (=
                      (contract_ty_opt (first (second (second intial_0004))))
                      ((as some (Option String)) "unit"))
                    ((as some (Option Contract))
                      ((as contract Contract)
                        (first (second (second intial_0004)))))
                    (as none (Option Contract)))
                  (ite
                    (=
                      (contract_ty_opt
                        (second (second (second intial_0004))))
                      ((as some (Option String)) "unit"))
                    ((as some (Option Contract))
                      ((as contract Contract)
                        (second (second (second intial_0004)))))
                    (as none (Option Contract))))))
            (is-some
              (second
                ((as mk-pair (Pair (Option Contract) (Option Contract)))
                  (ite
                    (=
                      (contract_ty_opt (first (second (second intial_0004))))
                      ((as some (Option String)) "unit"))
                    ((as some (Option Contract))
                      ((as contract Contract)
                        (first (second (second intial_0004)))))
                    (as none (Option Contract)))
                  (ite
                    (=
                      (contract_ty_opt
                        (second (second (second intial_0004))))
                      ((as some (Option String)) "unit"))
                    ((as some (Option Contract))
                      ((as contract Contract)
                        (second (second (second intial_0004)))))
                    (as none (Option Contract))))))) true false)
        (ite
          (=
            (or (= now (first (first (second _internal_result_0052))))
              (> (first (first (second _internal_result_0052))) now)) true)
          (ite
            (=
              (or (= balance (second (first (second _internal_result_0052))))
                (> (second (first (second _internal_result_0052))) balance))
              true)
            (= (first _internal_result_0052) (as seq.empty (Seq Operation)))
            (ite
              (is-some
                (ite
                  (= (contract_ty_opt (second (second (second intial_0004))))
                    ((as some (Option String)) "unit"))
                  ((as some (Option Contract))
                    ((as contract Contract)
                      (second (second (second intial_0004)))))
                  (as none (Option Contract))))
              (= (first _internal_result_0052)
                (seq.++
                  (seq.unit
                    ((as transferTokens Operation) (pack!unit unit) balance
                      (extractSome
                        (ite
                          (=
                            (contract_ty_opt
                              (second (second (second intial_0004))))
                            ((as some (Option String)) "unit"))
                          ((as some (Option Contract))
                            ((as contract Contract)
                              (second (second (second intial_0004)))))
                          (as none (Option Contract))))))
                  (as seq.empty (Seq Operation)))) false))
          (ite
            (is-some
              (ite
                (= (contract_ty_opt (first (second (second intial_0004))))
                  ((as some (Option String)) "unit"))
                ((as some (Option Contract))
                  ((as contract Contract)
                    (first (second (second intial_0004)))))
                (as none (Option Contract))))
            (= (first _internal_result_0052)
              (seq.++
                (seq.unit
                  ((as transferTokens Operation) (pack!unit unit) balance
                    (extractSome
                      (ite
                        (=
                          (contract_ty_opt
                            (first (second (second intial_0004))))
                          ((as some (Option String)) "unit"))
                        ((as some (Option Contract))
                          ((as contract Contract)
                            (first (second (second intial_0004)))))
                        (as none (Option Contract))))))
                (as seq.empty (Seq Operation)))) false))))))
(echo
  "Finding counter-example for postcondition of ContractAnnot at line 0, characters -1--1")
(check-sat)
(echo "START_MODEL")
(get-model)
(echo "END_MODEL")
(pop 1)


