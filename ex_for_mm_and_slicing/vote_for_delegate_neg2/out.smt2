(set-logic ALL)
(declare-datatypes ((Pair 2))
  ((par (T1 T2) ((mk-pair (first T1) (second T2))))))
(declare-datatypes ((Unit 0)) (((unit))))
(declare-datatypes ((Or 2))
  ((par (T1 T2) ((left (extractLeft T1)) (right (extractRight T2))))))
(declare-datatypes ((Option 1)) ((par (T) ((none) (some (extractSome T))))))
(declare-datatypes ((Contract 0))
  (((contract (extractContract (Pair String String))))))
(define-sort Fun () Int)
(declare-datatypes ((Operation 0))
  (((setDelegate (delegateAddress (Option String)))
     (transferTokens (transferData String) (transferMutez Int)
       (transferContract Contract))
     (createContract (createContractDelegateAddress (Option String))
       (createContractMutez Int) (createContractInitStorage String)
       (createContractAddress (Pair String String))))))
(declare-datatypes ((Exception 0))
  (((overflow) (error (extractError String)))))
(declare-fun amount () Int)
(declare-fun balance () Int)
(declare-fun contract_ty_opt ((Pair String String)) (Option String))
(declare-fun pack!unit (Unit) String)
(declare-fun self_addr () (Pair String String))
(declare-fun sender () (Pair String String))
(declare-fun source () (Pair String String))
(declare-fun unpack_opt!unit (String) (Option Unit))
(assert (or (= amount balance) (> balance amount)))
(assert (= (contract_ty_opt source) ((as some (Option String)) "unit")))
(assert (= (second source) "%default"))
(assert (= (second sender) "%default"))
(assert (= (second self_addr) "%default"))
(push 1)
(declare-const _internal_car_l13_c13-18_0013
  (Pair (Pair String String) (Option String)))
(declare-const _internal_car_l13_c13-18_0014 (Pair String String))
(declare-const _internal_car_l16_c17-22_0021 (Pair String String))
(declare-const _internal_cdr_l13_c13-18_0012
  (Pair (Pair (Pair String String) (Option String))
    (Pair (Pair String String) (Option String))))
(declare-const _internal_cdr_l16_c17-22_0019
  (Pair (Pair (Pair String String) (Option String))
    (Pair (Pair String String) (Option String))))
(declare-const _internal_cdr_l16_c17-22_0020
  (Pair (Pair String String) (Option String)))
(declare-const _internal_compare_l14_c7-60_0016 Int)
(declare-const _internal_compare_l17_c11-21_0023 Int)
(declare-const _internal_dup_l13_c7-10_0011
  (Pair (Option String)
    (Pair (Pair (Pair String String) (Option String))
      (Pair (Pair String String) (Option String)))))
(declare-const _internal_dup_l16_c11-14_0018
  (Pair (Option String)
    (Pair (Pair (Pair String String) (Option String))
      (Pair (Pair String String) (Option String)))))
(declare-const _internal_eq_l14_c7-60_0017 Bool)
(declare-const _internal_eq_l17_c11-21_0024 Bool)
(declare-const _internal_param_stor_0010
  (Pair (Option String)
    (Pair (Pair (Pair String String) (Option String))
      (Pair (Pair String String) (Option String)))))
(declare-const _internal_result_0081 Exception)
(declare-const _internal_sender_l13_c29-35_0015 (Pair String String))
(declare-const _internal_sender_l16_c25-31_0022 (Pair String String))
(declare-const _internal_unit_l19_c15-19_0035 Unit)
(declare-const intial_0004
  (Pair (Option String)
    (Pair (Pair (Pair String String) (Option String))
      (Pair (Pair String String) (Option String)))))
(assert
  (and
    (= (unpack_opt!unit (pack!unit _internal_unit_l19_c15-19_0035))
      ((as some (Option Unit)) _internal_unit_l19_c15-19_0035))
    (= (unpack_opt!unit (pack!unit unit)) ((as some (Option Unit)) unit))
    (ite (is-some (first intial_0004)) true true)
    (ite (is-some (second (first (second intial_0004)))) true true)
    (ite (is-some (second (second (second intial_0004)))) true true)
    (ite (is-some (first _internal_param_stor_0010)) true true)
    (ite (is-some (second (first (second _internal_param_stor_0010)))) true
      true)
    (ite (is-some (second (second (second _internal_param_stor_0010)))) true
      true) (ite (is-some (second _internal_cdr_l16_c17-22_0020)) true true)
    (ite (is-some (second (first _internal_cdr_l16_c17-22_0019))) true true)
    (ite (is-some (second (second _internal_cdr_l16_c17-22_0019))) true true)
    (ite (is-some (first _internal_dup_l16_c11-14_0018)) true true)
    (ite (is-some (second (first (second _internal_dup_l16_c11-14_0018))))
      true true)
    (ite (is-some (second (second (second _internal_dup_l16_c11-14_0018))))
      true true)
    (ite (is-some (second _internal_car_l13_c13-18_0013)) true true)
    (ite (is-some (second (first _internal_cdr_l13_c13-18_0012))) true true)
    (ite (is-some (second (second _internal_cdr_l13_c13-18_0012))) true true)
    (ite (is-some (first _internal_dup_l13_c7-10_0011)) true true)
    (ite (is-some (second (first (second _internal_dup_l13_c7-10_0011))))
      true true)
    (ite (is-some (second (second (second _internal_dup_l13_c7-10_0011))))
      true true)
    (= _internal_result_0081
      ((as error Exception) (pack!unit _internal_unit_l19_c15-19_0035)))
    (= _internal_unit_l19_c15-19_0035 unit)
    (= _internal_eq_l17_c11-21_0024 false)
    (= _internal_eq_l17_c11-21_0024 (= _internal_compare_l17_c11-21_0023 0))
    (ite
      (= (first _internal_sender_l16_c25-31_0022)
        (first _internal_car_l16_c17-22_0021))
      (ite
        (= (second _internal_sender_l16_c25-31_0022)
          (second _internal_car_l16_c17-22_0021))
        (= _internal_compare_l17_c11-21_0023 0)
        (ite
          (str.< (second _internal_car_l16_c17-22_0021)
            (second _internal_sender_l16_c25-31_0022))
          (= _internal_compare_l17_c11-21_0023 1)
          (= _internal_compare_l17_c11-21_0023 (- 1))))
      (ite
        (str.< (first _internal_car_l16_c17-22_0021)
          (first _internal_sender_l16_c25-31_0022))
        (= _internal_compare_l17_c11-21_0023 1)
        (= _internal_compare_l17_c11-21_0023 (- 1))))
    (= _internal_sender_l16_c25-31_0022 sender)
    (= _internal_car_l16_c17-22_0021 (first _internal_cdr_l16_c17-22_0020))
    (= _internal_cdr_l16_c17-22_0020 (second _internal_cdr_l16_c17-22_0019))
    (= _internal_cdr_l16_c17-22_0019 (second _internal_dup_l16_c11-14_0018))
    (= _internal_dup_l16_c11-14_0018 _internal_param_stor_0010)
    (= _internal_eq_l14_c7-60_0017 true)
    (= _internal_eq_l14_c7-60_0017 (= _internal_compare_l14_c7-60_0016 0))
    (ite
      (= (first _internal_sender_l13_c29-35_0015)
        (first _internal_car_l13_c13-18_0014))
      (ite
        (= (second _internal_sender_l13_c29-35_0015)
          (second _internal_car_l13_c13-18_0014))
        (= _internal_compare_l14_c7-60_0016 0)
        (ite
          (str.< (second _internal_car_l13_c13-18_0014)
            (second _internal_sender_l13_c29-35_0015))
          (= _internal_compare_l14_c7-60_0016 1)
          (= _internal_compare_l14_c7-60_0016 (- 1))))
      (ite
        (str.< (first _internal_car_l13_c13-18_0014)
          (first _internal_sender_l13_c29-35_0015))
        (= _internal_compare_l14_c7-60_0016 1)
        (= _internal_compare_l14_c7-60_0016 (- 1))))
    (= _internal_sender_l13_c29-35_0015 sender)
    (= _internal_car_l13_c13-18_0014 (first _internal_car_l13_c13-18_0013))
    (= _internal_car_l13_c13-18_0013 (first _internal_cdr_l13_c13-18_0012))
    (= _internal_cdr_l13_c13-18_0012 (second _internal_dup_l13_c7-10_0011))
    (= _internal_dup_l13_c7-10_0011 _internal_param_stor_0010)
    (= _internal_param_stor_0010 intial_0004)
    (not
      (= (first (first (second intial_0004)))
        (first (second (second intial_0004)))))
    (=
      (contract_ty_opt
        ((as mk-pair (Pair String String)) (first self_addr) "%default"))
      ((as some (Option String)) "(option key_hash)"))
    (not
      (=>
        (not
          (= (first (first (second intial_0004)))
            (first (second (second intial_0004)))))
        (= _internal_result_0081 ((as error Exception) (pack!unit unit)))))))
(echo
  "Finding counter-example for ab-postcondition of ContractAnnot at line 0, characters -1--1")
(check-sat)
(echo "START_MODEL")
(get-model)
(echo "END_MODEL")
(pop 1)
(push 1)
(declare-const _fresh_binding_0078 (Seq Operation))
(declare-const _fresh_binding_0079
  (Pair (Pair (Pair String String) (Option String))
    (Pair (Pair String String) (Option String))))
(declare-const _internal_car_l13_c13-18_0013
  (Pair (Pair String String) (Option String)))
(declare-const _internal_car_l13_c13-18_0014 (Pair String String))
(declare-const _internal_car_l16_c17-22_0021 (Pair String String))
(declare-const _internal_car_l18_c31-39_0031 (Pair String String))
(declare-const _internal_car_l18_c31-39_0033
  (Pair (Pair String String) (Option String)))
(declare-const _internal_car_l20_c27-35_0041
  (Pair (Pair String String) (Option String)))
(declare-const _internal_car_l20_c27-35_0044 (Pair String String))
(declare-const _internal_car_l22_c13-17_0050
  (Pair (Pair String String) (Option String)))
(declare-const _internal_cdr_l13_c13-18_0012
  (Pair (Pair (Pair String String) (Option String))
    (Pair (Pair String String) (Option String))))
(declare-const _internal_cdr_l16_c17-22_0019
  (Pair (Pair (Pair String String) (Option String))
    (Pair (Pair String String) (Option String))))
(declare-const _internal_cdr_l16_c17-22_0020
  (Pair (Pair String String) (Option String)))
(declare-const _internal_cdr_l18_c31-39_0028
  (Pair (Pair String String) (Option String)))
(declare-const _internal_cdr_l18_c31-39_0030 (Option String))
(declare-const _internal_cdr_l20_c27-35_0043 (Option String))
(declare-const _internal_cdr_l20_c27-35_0046
  (Pair (Pair String String) (Option String)))
(declare-const _internal_cdr_l22_c13-17_0051 (Option String))
(declare-const _internal_cdr_l23_c19-23_0053
  (Pair (Pair String String) (Option String)))
(declare-const _internal_cdr_l23_c19-23_0054 (Option String))
(declare-const _internal_compare_l14_c7-60_0016 Int)
(declare-const _internal_compare_l17_c11-21_0023 Int)
(declare-const _internal_compare_l32_c15-34_0067 Int)
(declare-const _internal_dup_l13_c7-10_0011
  (Pair (Option String)
    (Pair (Pair (Pair String String) (Option String))
      (Pair (Pair String String) (Option String)))))
(declare-const _internal_dup_l16_c11-14_0018
  (Pair (Option String)
    (Pair (Pair (Pair String String) (Option String))
      (Pair (Pair String String) (Option String)))))
(declare-const _internal_dup_l18_c31-39_0027
  (Pair (Pair (Pair String String) (Option String))
    (Pair (Pair String String) (Option String))))
(declare-const _internal_dup_l18_c31-39_0029
  (Pair (Pair String String) (Option String)))
(declare-const _internal_dup_l20_c27-35_0040
  (Pair (Pair (Pair String String) (Option String))
    (Pair (Pair String String) (Option String))))
(declare-const _internal_dup_l20_c27-35_0042
  (Pair (Pair String String) (Option String)))
(declare-const _internal_dup_l22_c7-10_0049
  (Pair (Pair (Pair String String) (Option String))
    (Pair (Pair String String) (Option String))))
(declare-const _internal_dup_l23_c13-16_0052
  (Pair (Pair (Pair String String) (Option String))
    (Pair (Pair String String) (Option String))))
(declare-const _internal_dup_l31_c21-24_0066 String)
(declare-const _internal_eq_l14_c7-60_0017 Bool)
(declare-const _internal_eq_l17_c11-21_0024 Bool)
(declare-const _internal_eq_l32_c15-34_0068 Bool)
(declare-const _internal_if_none_l24_c7-38_0055 String)
(declare-const _internal_if_none_l25_c11-37_0056 String)
(declare-const _internal_if_none_l30_c11-37_0064 String)
(declare-const _internal_nil_l27_c30-43_0059 (Seq Operation))
(declare-const _internal_nil_l34_c34-47_0071 (Seq Operation))
(declare-const _internal_none_l26_c15-28_0057 (Option String))
(declare-const _internal_pair_l18_c31-39_0032
  (Pair (Pair String String) (Option String)))
(declare-const _internal_pair_l20_c27-35_0045
  (Pair (Pair String String) (Option String)))
(declare-const _internal_param_stor_0010
  (Pair (Option String)
    (Pair (Pair (Pair String String) (Option String))
      (Pair (Pair String String) (Option String)))))
(declare-const _internal_result_0082
  (Pair (Seq Operation)
    (Pair (Pair (Pair String String) (Option String))
      (Pair (Pair String String) (Option String)))))
(declare-const _internal_sender_l13_c29-35_0015 (Pair String String))
(declare-const _internal_sender_l16_c25-31_0022 (Pair String String))
(declare-const _internal_set_delegate_l27_c15-27_0058 Operation)
(declare-const _internal_set_delegate_l34_c19-31_0070 Operation)
(declare-const _internal_some_l33_c19-23_0069 (Option String))
(declare-const _internal_unpair_l18_c15-21_0025 (Option String))
(declare-const _internal_unpair_l18_c15-21_0026
  (Pair (Pair (Pair String String) (Option String))
    (Pair (Pair String String) (Option String))))
(declare-const _internal_unpair_l20_c11-17_0038 (Option String))
(declare-const _internal_unpair_l20_c11-17_0039
  (Pair (Pair (Pair String String) (Option String))
    (Pair (Pair String String) (Option String))))
(declare-const intial_0004
  (Pair (Option String)
    (Pair (Pair (Pair String String) (Option String))
      (Pair (Pair String String) (Option String)))))
(assert
  (and (ite (is-some (first intial_0004)) true true)
    (ite (is-some (second (first (second intial_0004)))) true true)
    (ite (is-some (second (second (second intial_0004)))) true true)
    (ite (is-some (second (first (second _internal_result_0082)))) true true)
    (ite (is-some (second (second (second _internal_result_0082)))) true
      true) (ite (is-some (second (first _fresh_binding_0079))) true true)
    (ite (is-some (second (second _fresh_binding_0079))) true true)
    (ite (is-some _internal_none_l26_c15-28_0057) true true)
    (ite (is-some _internal_cdr_l23_c19-23_0054) true true)
    (ite (is-some _internal_cdr_l22_c13-17_0051) true true)
    (ite (is-some (second _internal_cdr_l23_c19-23_0053)) true true)
    (ite (is-some (second (first _internal_dup_l23_c13-16_0052))) true true)
    (ite (is-some (second (second _internal_dup_l23_c13-16_0052))) true true)
    (ite (is-some (second _internal_car_l22_c13-17_0050)) true true)
    (ite (is-some (second (first _internal_dup_l22_c7-10_0049))) true true)
    (ite (is-some (second (second _internal_dup_l22_c7-10_0049))) true true)
    (ite (is-some (second _internal_car_l18_c31-39_0033)) true true)
    (ite (is-some (second _internal_pair_l18_c31-39_0032)) true true)
    (ite (is-some (second (first _internal_dup_l18_c31-39_0027))) true true)
    (ite (is-some (second (second _internal_dup_l18_c31-39_0027))) true true)
    (ite (is-some _internal_unpair_l18_c15-21_0025) true true)
    (ite (is-some (second _internal_cdr_l18_c31-39_0028)) true true)
    (ite (is-some _internal_cdr_l18_c31-39_0030) true true)
    (ite (is-some (second _internal_dup_l18_c31-39_0029)) true true)
    (ite (is-some (second (first _internal_unpair_l18_c15-21_0026))) true
      true)
    (ite (is-some (second (second _internal_unpair_l18_c15-21_0026))) true
      true) (ite (is-some (first _internal_param_stor_0010)) true true)
    (ite (is-some (second (first (second _internal_param_stor_0010)))) true
      true)
    (ite (is-some (second (second (second _internal_param_stor_0010)))) true
      true) (ite (is-some (second _internal_cdr_l16_c17-22_0020)) true true)
    (ite (is-some (second (first _internal_cdr_l16_c17-22_0019))) true true)
    (ite (is-some (second (second _internal_cdr_l16_c17-22_0019))) true true)
    (ite (is-some (first _internal_dup_l16_c11-14_0018)) true true)
    (ite (is-some (second (first (second _internal_dup_l16_c11-14_0018))))
      true true)
    (ite (is-some (second (second (second _internal_dup_l16_c11-14_0018))))
      true true)
    (ite (is-some (second _internal_car_l13_c13-18_0013)) true true)
    (ite (is-some (second (first _internal_cdr_l13_c13-18_0012))) true true)
    (ite (is-some (second (second _internal_cdr_l13_c13-18_0012))) true true)
    (ite (is-some (first _internal_dup_l13_c7-10_0011)) true true)
    (ite (is-some (second (first (second _internal_dup_l13_c7-10_0011))))
      true true)
    (ite (is-some (second (second (second _internal_dup_l13_c7-10_0011))))
      true true)
    (ite (is-some (second _internal_pair_l20_c27-35_0045)) true true)
    (ite (is-some (second _internal_cdr_l20_c27-35_0046)) true true)
    (ite (is-some (second (first _internal_dup_l20_c27-35_0040))) true true)
    (ite (is-some (second (second _internal_dup_l20_c27-35_0040))) true true)
    (ite (is-some _internal_unpair_l20_c11-17_0038) true true)
    (ite (is-some (second _internal_car_l20_c27-35_0041)) true true)
    (ite (is-some _internal_cdr_l20_c27-35_0043) true true)
    (ite (is-some (second _internal_dup_l20_c27-35_0042)) true true)
    (ite (is-some (second (first _internal_unpair_l20_c11-17_0039))) true
      true)
    (ite (is-some (second (second _internal_unpair_l20_c11-17_0039))) true
      true) (ite (is-some (first _internal_param_stor_0010)) true true)
    (ite (is-some (second (first (second _internal_param_stor_0010)))) true
      true)
    (ite (is-some (second (second (second _internal_param_stor_0010)))) true
      true) (ite (is-some (second _internal_car_l13_c13-18_0013)) true true)
    (ite (is-some (second (first _internal_cdr_l13_c13-18_0012))) true true)
    (ite (is-some (second (second _internal_cdr_l13_c13-18_0012))) true true)
    (ite (is-some (first _internal_dup_l13_c7-10_0011)) true true)
    (ite (is-some (second (first (second _internal_dup_l13_c7-10_0011))))
      true true)
    (ite (is-some (second (second (second _internal_dup_l13_c7-10_0011))))
      true true) (ite (is-some _internal_cdr_l23_c19-23_0054) true true)
    (ite (is-some _internal_cdr_l22_c13-17_0051) true true)
    (ite (is-some (second _internal_cdr_l23_c19-23_0053)) true true)
    (ite (is-some (second (first _internal_dup_l23_c13-16_0052))) true true)
    (ite (is-some (second (second _internal_dup_l23_c13-16_0052))) true true)
    (ite (is-some (second _internal_car_l22_c13-17_0050)) true true)
    (ite (is-some (second (first _internal_dup_l22_c7-10_0049))) true true)
    (ite (is-some (second (second _internal_dup_l22_c7-10_0049))) true true)
    (ite (is-some (second _internal_car_l18_c31-39_0033)) true true)
    (ite (is-some (second _internal_pair_l18_c31-39_0032)) true true)
    (ite (is-some (second (first _internal_dup_l18_c31-39_0027))) true true)
    (ite (is-some (second (second _internal_dup_l18_c31-39_0027))) true true)
    (ite (is-some _internal_unpair_l18_c15-21_0025) true true)
    (ite (is-some (second _internal_cdr_l18_c31-39_0028)) true true)
    (ite (is-some _internal_cdr_l18_c31-39_0030) true true)
    (ite (is-some (second _internal_dup_l18_c31-39_0029)) true true)
    (ite (is-some (second (first _internal_unpair_l18_c15-21_0026))) true
      true)
    (ite (is-some (second (second _internal_unpair_l18_c15-21_0026))) true
      true) (ite (is-some (first _internal_param_stor_0010)) true true)
    (ite (is-some (second (first (second _internal_param_stor_0010)))) true
      true)
    (ite (is-some (second (second (second _internal_param_stor_0010)))) true
      true) (ite (is-some (second _internal_cdr_l16_c17-22_0020)) true true)
    (ite (is-some (second (first _internal_cdr_l16_c17-22_0019))) true true)
    (ite (is-some (second (second _internal_cdr_l16_c17-22_0019))) true true)
    (ite (is-some (first _internal_dup_l16_c11-14_0018)) true true)
    (ite (is-some (second (first (second _internal_dup_l16_c11-14_0018))))
      true true)
    (ite (is-some (second (second (second _internal_dup_l16_c11-14_0018))))
      true true)
    (ite (is-some (second _internal_car_l13_c13-18_0013)) true true)
    (ite (is-some (second (first _internal_cdr_l13_c13-18_0012))) true true)
    (ite (is-some (second (second _internal_cdr_l13_c13-18_0012))) true true)
    (ite (is-some (first _internal_dup_l13_c7-10_0011)) true true)
    (ite (is-some (second (first (second _internal_dup_l13_c7-10_0011))))
      true true)
    (ite (is-some (second (second (second _internal_dup_l13_c7-10_0011))))
      true true)
    (ite (is-some (second _internal_pair_l20_c27-35_0045)) true true)
    (ite (is-some (second _internal_cdr_l20_c27-35_0046)) true true)
    (ite (is-some (second (first _internal_dup_l20_c27-35_0040))) true true)
    (ite (is-some (second (second _internal_dup_l20_c27-35_0040))) true true)
    (ite (is-some _internal_unpair_l20_c11-17_0038) true true)
    (ite (is-some (second _internal_car_l20_c27-35_0041)) true true)
    (ite (is-some _internal_cdr_l20_c27-35_0043) true true)
    (ite (is-some (second _internal_dup_l20_c27-35_0042)) true true)
    (ite (is-some (second (first _internal_unpair_l20_c11-17_0039))) true
      true)
    (ite (is-some (second (second _internal_unpair_l20_c11-17_0039))) true
      true) (ite (is-some (first _internal_param_stor_0010)) true true)
    (ite (is-some (second (first (second _internal_param_stor_0010)))) true
      true)
    (ite (is-some (second (second (second _internal_param_stor_0010)))) true
      true) (ite (is-some (second _internal_car_l13_c13-18_0013)) true true)
    (ite (is-some (second (first _internal_cdr_l13_c13-18_0012))) true true)
    (ite (is-some (second (second _internal_cdr_l13_c13-18_0012))) true true)
    (ite (is-some (first _internal_dup_l13_c7-10_0011)) true true)
    (ite (is-some (second (first (second _internal_dup_l13_c7-10_0011))))
      true true)
    (ite (is-some (second (second (second _internal_dup_l13_c7-10_0011))))
      true true) (ite (is-some _internal_cdr_l23_c19-23_0054) true true)
    (ite (is-some _internal_cdr_l22_c13-17_0051) true true)
    (ite (is-some (second _internal_cdr_l23_c19-23_0053)) true true)
    (ite (is-some (second (first _internal_dup_l23_c13-16_0052))) true true)
    (ite (is-some (second (second _internal_dup_l23_c13-16_0052))) true true)
    (ite (is-some (second _internal_car_l22_c13-17_0050)) true true)
    (ite (is-some (second (first _internal_dup_l22_c7-10_0049))) true true)
    (ite (is-some (second (second _internal_dup_l22_c7-10_0049))) true true)
    (ite (is-some (second _internal_car_l18_c31-39_0033)) true true)
    (ite (is-some (second _internal_pair_l18_c31-39_0032)) true true)
    (ite (is-some (second (first _internal_dup_l18_c31-39_0027))) true true)
    (ite (is-some (second (second _internal_dup_l18_c31-39_0027))) true true)
    (ite (is-some _internal_unpair_l18_c15-21_0025) true true)
    (ite (is-some (second _internal_cdr_l18_c31-39_0028)) true true)
    (ite (is-some _internal_cdr_l18_c31-39_0030) true true)
    (ite (is-some (second _internal_dup_l18_c31-39_0029)) true true)
    (ite (is-some (second (first _internal_unpair_l18_c15-21_0026))) true
      true)
    (ite (is-some (second (second _internal_unpair_l18_c15-21_0026))) true
      true) (ite (is-some (first _internal_param_stor_0010)) true true)
    (ite (is-some (second (first (second _internal_param_stor_0010)))) true
      true)
    (ite (is-some (second (second (second _internal_param_stor_0010)))) true
      true) (ite (is-some (second _internal_cdr_l16_c17-22_0020)) true true)
    (ite (is-some (second (first _internal_cdr_l16_c17-22_0019))) true true)
    (ite (is-some (second (second _internal_cdr_l16_c17-22_0019))) true true)
    (ite (is-some (first _internal_dup_l16_c11-14_0018)) true true)
    (ite (is-some (second (first (second _internal_dup_l16_c11-14_0018))))
      true true)
    (ite (is-some (second (second (second _internal_dup_l16_c11-14_0018))))
      true true)
    (ite (is-some (second _internal_car_l13_c13-18_0013)) true true)
    (ite (is-some (second (first _internal_cdr_l13_c13-18_0012))) true true)
    (ite (is-some (second (second _internal_cdr_l13_c13-18_0012))) true true)
    (ite (is-some (first _internal_dup_l13_c7-10_0011)) true true)
    (ite (is-some (second (first (second _internal_dup_l13_c7-10_0011))))
      true true)
    (ite (is-some (second (second (second _internal_dup_l13_c7-10_0011))))
      true true)
    (ite (is-some (second _internal_pair_l20_c27-35_0045)) true true)
    (ite (is-some (second _internal_cdr_l20_c27-35_0046)) true true)
    (ite (is-some (second (first _internal_dup_l20_c27-35_0040))) true true)
    (ite (is-some (second (second _internal_dup_l20_c27-35_0040))) true true)
    (ite (is-some _internal_unpair_l20_c11-17_0038) true true)
    (ite (is-some (second _internal_car_l20_c27-35_0041)) true true)
    (ite (is-some _internal_cdr_l20_c27-35_0043) true true)
    (ite (is-some (second _internal_dup_l20_c27-35_0042)) true true)
    (ite (is-some (second (first _internal_unpair_l20_c11-17_0039))) true
      true)
    (ite (is-some (second (second _internal_unpair_l20_c11-17_0039))) true
      true) (ite (is-some (first _internal_param_stor_0010)) true true)
    (ite (is-some (second (first (second _internal_param_stor_0010)))) true
      true)
    (ite (is-some (second (second (second _internal_param_stor_0010)))) true
      true) (ite (is-some (second _internal_car_l13_c13-18_0013)) true true)
    (ite (is-some (second (first _internal_cdr_l13_c13-18_0012))) true true)
    (ite (is-some (second (second _internal_cdr_l13_c13-18_0012))) true true)
    (ite (is-some (first _internal_dup_l13_c7-10_0011)) true true)
    (ite (is-some (second (first (second _internal_dup_l13_c7-10_0011))))
      true true)
    (ite (is-some (second (second (second _internal_dup_l13_c7-10_0011))))
      true true) (ite (is-some _internal_some_l33_c19-23_0069) true true)
    (ite (is-some _internal_cdr_l23_c19-23_0054) true true)
    (ite (is-some _internal_cdr_l22_c13-17_0051) true true)
    (ite (is-some (second _internal_cdr_l23_c19-23_0053)) true true)
    (ite (is-some (second (first _internal_dup_l23_c13-16_0052))) true true)
    (ite (is-some (second (second _internal_dup_l23_c13-16_0052))) true true)
    (ite (is-some (second _internal_car_l22_c13-17_0050)) true true)
    (ite (is-some (second (first _internal_dup_l22_c7-10_0049))) true true)
    (ite (is-some (second (second _internal_dup_l22_c7-10_0049))) true true)
    (ite (is-some (second _internal_car_l18_c31-39_0033)) true true)
    (ite (is-some (second _internal_pair_l18_c31-39_0032)) true true)
    (ite (is-some (second (first _internal_dup_l18_c31-39_0027))) true true)
    (ite (is-some (second (second _internal_dup_l18_c31-39_0027))) true true)
    (ite (is-some _internal_unpair_l18_c15-21_0025) true true)
    (ite (is-some (second _internal_cdr_l18_c31-39_0028)) true true)
    (ite (is-some _internal_cdr_l18_c31-39_0030) true true)
    (ite (is-some (second _internal_dup_l18_c31-39_0029)) true true)
    (ite (is-some (second (first _internal_unpair_l18_c15-21_0026))) true
      true)
    (ite (is-some (second (second _internal_unpair_l18_c15-21_0026))) true
      true) (ite (is-some (first _internal_param_stor_0010)) true true)
    (ite (is-some (second (first (second _internal_param_stor_0010)))) true
      true)
    (ite (is-some (second (second (second _internal_param_stor_0010)))) true
      true) (ite (is-some (second _internal_cdr_l16_c17-22_0020)) true true)
    (ite (is-some (second (first _internal_cdr_l16_c17-22_0019))) true true)
    (ite (is-some (second (second _internal_cdr_l16_c17-22_0019))) true true)
    (ite (is-some (first _internal_dup_l16_c11-14_0018)) true true)
    (ite (is-some (second (first (second _internal_dup_l16_c11-14_0018))))
      true true)
    (ite (is-some (second (second (second _internal_dup_l16_c11-14_0018))))
      true true)
    (ite (is-some (second _internal_car_l13_c13-18_0013)) true true)
    (ite (is-some (second (first _internal_cdr_l13_c13-18_0012))) true true)
    (ite (is-some (second (second _internal_cdr_l13_c13-18_0012))) true true)
    (ite (is-some (first _internal_dup_l13_c7-10_0011)) true true)
    (ite (is-some (second (first (second _internal_dup_l13_c7-10_0011))))
      true true)
    (ite (is-some (second (second (second _internal_dup_l13_c7-10_0011))))
      true true)
    (ite (is-some (second _internal_pair_l20_c27-35_0045)) true true)
    (ite (is-some (second _internal_cdr_l20_c27-35_0046)) true true)
    (ite (is-some (second (first _internal_dup_l20_c27-35_0040))) true true)
    (ite (is-some (second (second _internal_dup_l20_c27-35_0040))) true true)
    (ite (is-some _internal_unpair_l20_c11-17_0038) true true)
    (ite (is-some (second _internal_car_l20_c27-35_0041)) true true)
    (ite (is-some _internal_cdr_l20_c27-35_0043) true true)
    (ite (is-some (second _internal_dup_l20_c27-35_0042)) true true)
    (ite (is-some (second (first _internal_unpair_l20_c11-17_0039))) true
      true)
    (ite (is-some (second (second _internal_unpair_l20_c11-17_0039))) true
      true) (ite (is-some (first _internal_param_stor_0010)) true true)
    (ite (is-some (second (first (second _internal_param_stor_0010)))) true
      true)
    (ite (is-some (second (second (second _internal_param_stor_0010)))) true
      true) (ite (is-some (second _internal_car_l13_c13-18_0013)) true true)
    (ite (is-some (second (first _internal_cdr_l13_c13-18_0012))) true true)
    (ite (is-some (second (second _internal_cdr_l13_c13-18_0012))) true true)
    (ite (is-some (first _internal_dup_l13_c7-10_0011)) true true)
    (ite (is-some (second (first (second _internal_dup_l13_c7-10_0011))))
      true true)
    (ite (is-some (second (second (second _internal_dup_l13_c7-10_0011))))
      true true) (ite (is-some _internal_cdr_l23_c19-23_0054) true true)
    (ite (is-some _internal_cdr_l22_c13-17_0051) true true)
    (ite (is-some (second _internal_cdr_l23_c19-23_0053)) true true)
    (ite (is-some (second (first _internal_dup_l23_c13-16_0052))) true true)
    (ite (is-some (second (second _internal_dup_l23_c13-16_0052))) true true)
    (ite (is-some (second _internal_car_l22_c13-17_0050)) true true)
    (ite (is-some (second (first _internal_dup_l22_c7-10_0049))) true true)
    (ite (is-some (second (second _internal_dup_l22_c7-10_0049))) true true)
    (ite (is-some (second _internal_car_l18_c31-39_0033)) true true)
    (ite (is-some (second _internal_pair_l18_c31-39_0032)) true true)
    (ite (is-some (second (first _internal_dup_l18_c31-39_0027))) true true)
    (ite (is-some (second (second _internal_dup_l18_c31-39_0027))) true true)
    (ite (is-some _internal_unpair_l18_c15-21_0025) true true)
    (ite (is-some (second _internal_cdr_l18_c31-39_0028)) true true)
    (ite (is-some _internal_cdr_l18_c31-39_0030) true true)
    (ite (is-some (second _internal_dup_l18_c31-39_0029)) true true)
    (ite (is-some (second (first _internal_unpair_l18_c15-21_0026))) true
      true)
    (ite (is-some (second (second _internal_unpair_l18_c15-21_0026))) true
      true) (ite (is-some (first _internal_param_stor_0010)) true true)
    (ite (is-some (second (first (second _internal_param_stor_0010)))) true
      true)
    (ite (is-some (second (second (second _internal_param_stor_0010)))) true
      true) (ite (is-some (second _internal_cdr_l16_c17-22_0020)) true true)
    (ite (is-some (second (first _internal_cdr_l16_c17-22_0019))) true true)
    (ite (is-some (second (second _internal_cdr_l16_c17-22_0019))) true true)
    (ite (is-some (first _internal_dup_l16_c11-14_0018)) true true)
    (ite (is-some (second (first (second _internal_dup_l16_c11-14_0018))))
      true true)
    (ite (is-some (second (second (second _internal_dup_l16_c11-14_0018))))
      true true)
    (ite (is-some (second _internal_car_l13_c13-18_0013)) true true)
    (ite (is-some (second (first _internal_cdr_l13_c13-18_0012))) true true)
    (ite (is-some (second (second _internal_cdr_l13_c13-18_0012))) true true)
    (ite (is-some (first _internal_dup_l13_c7-10_0011)) true true)
    (ite (is-some (second (first (second _internal_dup_l13_c7-10_0011))))
      true true)
    (ite (is-some (second (second (second _internal_dup_l13_c7-10_0011))))
      true true)
    (ite (is-some (second _internal_pair_l20_c27-35_0045)) true true)
    (ite (is-some (second _internal_cdr_l20_c27-35_0046)) true true)
    (ite (is-some (second (first _internal_dup_l20_c27-35_0040))) true true)
    (ite (is-some (second (second _internal_dup_l20_c27-35_0040))) true true)
    (ite (is-some _internal_unpair_l20_c11-17_0038) true true)
    (ite (is-some (second _internal_car_l20_c27-35_0041)) true true)
    (ite (is-some _internal_cdr_l20_c27-35_0043) true true)
    (ite (is-some (second _internal_dup_l20_c27-35_0042)) true true)
    (ite (is-some (second (first _internal_unpair_l20_c11-17_0039))) true
      true)
    (ite (is-some (second (second _internal_unpair_l20_c11-17_0039))) true
      true) (ite (is-some (first _internal_param_stor_0010)) true true)
    (ite (is-some (second (first (second _internal_param_stor_0010)))) true
      true)
    (ite (is-some (second (second (second _internal_param_stor_0010)))) true
      true) (ite (is-some (second _internal_car_l13_c13-18_0013)) true true)
    (ite (is-some (second (first _internal_cdr_l13_c13-18_0012))) true true)
    (ite (is-some (second (second _internal_cdr_l13_c13-18_0012))) true true)
    (ite (is-some (first _internal_dup_l13_c7-10_0011)) true true)
    (ite (is-some (second (first (second _internal_dup_l13_c7-10_0011))))
      true true)
    (ite (is-some (second (second (second _internal_dup_l13_c7-10_0011))))
      true true)
    (= _internal_result_0082
      ((as mk-pair
         (Pair (Seq Operation)
           (Pair (Pair (Pair String String) (Option String))
             (Pair (Pair String String) (Option String)))))
        _fresh_binding_0078 _fresh_binding_0079))
    (or
      (and
        (= _fresh_binding_0078
          (seq.++ (seq.unit _internal_set_delegate_l27_c15-27_0058)
            _internal_nil_l27_c30-43_0059))
        (= _internal_nil_l27_c30-43_0059 (as seq.empty (Seq Operation)))
        (= ((as setDelegate Operation) _internal_none_l26_c15-28_0057)
          _internal_set_delegate_l27_c15-27_0058)
        (= _internal_none_l26_c15-28_0057 (as none (Option String)))
        (= _internal_cdr_l23_c19-23_0054 (as none (Option String)))
        (= _internal_cdr_l22_c13-17_0051 (as none (Option String)))
        (= _internal_cdr_l23_c19-23_0054
          (second _internal_cdr_l23_c19-23_0053))
        (= _internal_cdr_l23_c19-23_0053
          (second _internal_dup_l23_c13-16_0052))
        (= _internal_dup_l23_c13-16_0052 _fresh_binding_0079)
        (= _internal_cdr_l22_c13-17_0051
          (second _internal_car_l22_c13-17_0050))
        (= _internal_car_l22_c13-17_0050
          (first _internal_dup_l22_c7-10_0049))
        (= _internal_dup_l22_c7-10_0049 _fresh_binding_0079)
        (or
          (and
            (= _fresh_binding_0079
              ((as mk-pair
                 (Pair (Pair (Pair String String) (Option String))
                   (Pair (Pair String String) (Option String))))
                _internal_car_l18_c31-39_0033 _internal_pair_l18_c31-39_0032))
            (= _internal_car_l18_c31-39_0033
              (first _internal_dup_l18_c31-39_0027))
            (= _internal_pair_l18_c31-39_0032
              ((as mk-pair (Pair (Pair String String) (Option String)))
                _internal_car_l18_c31-39_0031
                _internal_unpair_l18_c15-21_0025))
            (= _internal_car_l18_c31-39_0031
              (first _internal_cdr_l18_c31-39_0028))
            (= _internal_cdr_l18_c31-39_0030
              (second _internal_dup_l18_c31-39_0029))
            (= _internal_dup_l18_c31-39_0029 _internal_cdr_l18_c31-39_0028)
            (= _internal_cdr_l18_c31-39_0028
              (second _internal_unpair_l18_c15-21_0026))
            (= _internal_dup_l18_c31-39_0027
              _internal_unpair_l18_c15-21_0026)
            (= _internal_param_stor_0010
              ((as mk-pair
                 (Pair (Option String)
                   (Pair (Pair (Pair String String) (Option String))
                     (Pair (Pair String String) (Option String)))))
                _internal_unpair_l18_c15-21_0025
                _internal_unpair_l18_c15-21_0026))
            (= _internal_eq_l17_c11-21_0024 true)
            (= _internal_eq_l17_c11-21_0024
              (= _internal_compare_l17_c11-21_0023 0))
            (ite
              (= (first _internal_sender_l16_c25-31_0022)
                (first _internal_car_l16_c17-22_0021))
              (ite
                (= (second _internal_sender_l16_c25-31_0022)
                  (second _internal_car_l16_c17-22_0021))
                (= _internal_compare_l17_c11-21_0023 0)
                (ite
                  (str.< (second _internal_car_l16_c17-22_0021)
                    (second _internal_sender_l16_c25-31_0022))
                  (= _internal_compare_l17_c11-21_0023 1)
                  (= _internal_compare_l17_c11-21_0023 (- 1))))
              (ite
                (str.< (first _internal_car_l16_c17-22_0021)
                  (first _internal_sender_l16_c25-31_0022))
                (= _internal_compare_l17_c11-21_0023 1)
                (= _internal_compare_l17_c11-21_0023 (- 1))))
            (= _internal_sender_l16_c25-31_0022 sender)
            (= _internal_car_l16_c17-22_0021
              (first _internal_cdr_l16_c17-22_0020))
            (= _internal_cdr_l16_c17-22_0020
              (second _internal_cdr_l16_c17-22_0019))
            (= _internal_cdr_l16_c17-22_0019
              (second _internal_dup_l16_c11-14_0018))
            (= _internal_dup_l16_c11-14_0018 _internal_param_stor_0010)
            (= _internal_eq_l14_c7-60_0017 true)
            (= _internal_eq_l14_c7-60_0017
              (= _internal_compare_l14_c7-60_0016 0))
            (ite
              (= (first _internal_sender_l13_c29-35_0015)
                (first _internal_car_l13_c13-18_0014))
              (ite
                (= (second _internal_sender_l13_c29-35_0015)
                  (second _internal_car_l13_c13-18_0014))
                (= _internal_compare_l14_c7-60_0016 0)
                (ite
                  (str.< (second _internal_car_l13_c13-18_0014)
                    (second _internal_sender_l13_c29-35_0015))
                  (= _internal_compare_l14_c7-60_0016 1)
                  (= _internal_compare_l14_c7-60_0016 (- 1))))
              (ite
                (str.< (first _internal_car_l13_c13-18_0014)
                  (first _internal_sender_l13_c29-35_0015))
                (= _internal_compare_l14_c7-60_0016 1)
                (= _internal_compare_l14_c7-60_0016 (- 1))))
            (= _internal_sender_l13_c29-35_0015 sender)
            (= _internal_car_l13_c13-18_0014
              (first _internal_car_l13_c13-18_0013))
            (= _internal_car_l13_c13-18_0013
              (first _internal_cdr_l13_c13-18_0012))
            (= _internal_cdr_l13_c13-18_0012
              (second _internal_dup_l13_c7-10_0011))
            (= _internal_dup_l13_c7-10_0011 _internal_param_stor_0010)
            (= _internal_param_stor_0010 intial_0004)
            (not
              (= (first (first (second intial_0004)))
                (first (second (second intial_0004)))))
            (=
              (contract_ty_opt
                ((as mk-pair (Pair String String)) (first self_addr)
                  "%default"))
              ((as some (Option String)) "(option key_hash)")))
          (and
            (= _fresh_binding_0079
              ((as mk-pair
                 (Pair (Pair (Pair String String) (Option String))
                   (Pair (Pair String String) (Option String))))
                _internal_pair_l20_c27-35_0045 _internal_cdr_l20_c27-35_0046))
            (= _internal_cdr_l20_c27-35_0046
              (second _internal_dup_l20_c27-35_0040))
            (= _internal_pair_l20_c27-35_0045
              ((as mk-pair (Pair (Pair String String) (Option String)))
                _internal_car_l20_c27-35_0044
                _internal_unpair_l20_c11-17_0038))
            (= _internal_car_l20_c27-35_0044
              (first _internal_car_l20_c27-35_0041))
            (= _internal_cdr_l20_c27-35_0043
              (second _internal_dup_l20_c27-35_0042))
            (= _internal_dup_l20_c27-35_0042 _internal_car_l20_c27-35_0041)
            (= _internal_car_l20_c27-35_0041
              (first _internal_unpair_l20_c11-17_0039))
            (= _internal_dup_l20_c27-35_0040
              _internal_unpair_l20_c11-17_0039)
            (= _internal_param_stor_0010
              ((as mk-pair
                 (Pair (Option String)
                   (Pair (Pair (Pair String String) (Option String))
                     (Pair (Pair String String) (Option String)))))
                _internal_unpair_l20_c11-17_0038
                _internal_unpair_l20_c11-17_0039))
            (= _internal_eq_l14_c7-60_0017 false)
            (= _internal_eq_l14_c7-60_0017
              (= _internal_compare_l14_c7-60_0016 0))
            (ite
              (= (first _internal_sender_l13_c29-35_0015)
                (first _internal_car_l13_c13-18_0014))
              (ite
                (= (second _internal_sender_l13_c29-35_0015)
                  (second _internal_car_l13_c13-18_0014))
                (= _internal_compare_l14_c7-60_0016 0)
                (ite
                  (str.< (second _internal_car_l13_c13-18_0014)
                    (second _internal_sender_l13_c29-35_0015))
                  (= _internal_compare_l14_c7-60_0016 1)
                  (= _internal_compare_l14_c7-60_0016 (- 1))))
              (ite
                (str.< (first _internal_car_l13_c13-18_0014)
                  (first _internal_sender_l13_c29-35_0015))
                (= _internal_compare_l14_c7-60_0016 1)
                (= _internal_compare_l14_c7-60_0016 (- 1))))
            (= _internal_sender_l13_c29-35_0015 sender)
            (= _internal_car_l13_c13-18_0014
              (first _internal_car_l13_c13-18_0013))
            (= _internal_car_l13_c13-18_0013
              (first _internal_cdr_l13_c13-18_0012))
            (= _internal_cdr_l13_c13-18_0012
              (second _internal_dup_l13_c7-10_0011))
            (= _internal_dup_l13_c7-10_0011 _internal_param_stor_0010)
            (= _internal_param_stor_0010 intial_0004)
            (not
              (= (first (first (second intial_0004)))
                (first (second (second intial_0004)))))
            (=
              (contract_ty_opt
                ((as mk-pair (Pair String String)) (first self_addr)
                  "%default"))
              ((as some (Option String)) "(option key_hash)")))))
      (and (= _fresh_binding_0078 (as seq.empty (Seq Operation)))
        (= _internal_cdr_l23_c19-23_0054
          ((as some (Option String)) _internal_if_none_l25_c11-37_0056))
        (= _internal_cdr_l22_c13-17_0051 (as none (Option String)))
        (= _internal_cdr_l23_c19-23_0054
          (second _internal_cdr_l23_c19-23_0053))
        (= _internal_cdr_l23_c19-23_0053
          (second _internal_dup_l23_c13-16_0052))
        (= _internal_dup_l23_c13-16_0052 _fresh_binding_0079)
        (= _internal_cdr_l22_c13-17_0051
          (second _internal_car_l22_c13-17_0050))
        (= _internal_car_l22_c13-17_0050
          (first _internal_dup_l22_c7-10_0049))
        (= _internal_dup_l22_c7-10_0049 _fresh_binding_0079)
        (or
          (and
            (= _fresh_binding_0079
              ((as mk-pair
                 (Pair (Pair (Pair String String) (Option String))
                   (Pair (Pair String String) (Option String))))
                _internal_car_l18_c31-39_0033 _internal_pair_l18_c31-39_0032))
            (= _internal_car_l18_c31-39_0033
              (first _internal_dup_l18_c31-39_0027))
            (= _internal_pair_l18_c31-39_0032
              ((as mk-pair (Pair (Pair String String) (Option String)))
                _internal_car_l18_c31-39_0031
                _internal_unpair_l18_c15-21_0025))
            (= _internal_car_l18_c31-39_0031
              (first _internal_cdr_l18_c31-39_0028))
            (= _internal_cdr_l18_c31-39_0030
              (second _internal_dup_l18_c31-39_0029))
            (= _internal_dup_l18_c31-39_0029 _internal_cdr_l18_c31-39_0028)
            (= _internal_cdr_l18_c31-39_0028
              (second _internal_unpair_l18_c15-21_0026))
            (= _internal_dup_l18_c31-39_0027
              _internal_unpair_l18_c15-21_0026)
            (= _internal_param_stor_0010
              ((as mk-pair
                 (Pair (Option String)
                   (Pair (Pair (Pair String String) (Option String))
                     (Pair (Pair String String) (Option String)))))
                _internal_unpair_l18_c15-21_0025
                _internal_unpair_l18_c15-21_0026))
            (= _internal_eq_l17_c11-21_0024 true)
            (= _internal_eq_l17_c11-21_0024
              (= _internal_compare_l17_c11-21_0023 0))
            (ite
              (= (first _internal_sender_l16_c25-31_0022)
                (first _internal_car_l16_c17-22_0021))
              (ite
                (= (second _internal_sender_l16_c25-31_0022)
                  (second _internal_car_l16_c17-22_0021))
                (= _internal_compare_l17_c11-21_0023 0)
                (ite
                  (str.< (second _internal_car_l16_c17-22_0021)
                    (second _internal_sender_l16_c25-31_0022))
                  (= _internal_compare_l17_c11-21_0023 1)
                  (= _internal_compare_l17_c11-21_0023 (- 1))))
              (ite
                (str.< (first _internal_car_l16_c17-22_0021)
                  (first _internal_sender_l16_c25-31_0022))
                (= _internal_compare_l17_c11-21_0023 1)
                (= _internal_compare_l17_c11-21_0023 (- 1))))
            (= _internal_sender_l16_c25-31_0022 sender)
            (= _internal_car_l16_c17-22_0021
              (first _internal_cdr_l16_c17-22_0020))
            (= _internal_cdr_l16_c17-22_0020
              (second _internal_cdr_l16_c17-22_0019))
            (= _internal_cdr_l16_c17-22_0019
              (second _internal_dup_l16_c11-14_0018))
            (= _internal_dup_l16_c11-14_0018 _internal_param_stor_0010)
            (= _internal_eq_l14_c7-60_0017 true)
            (= _internal_eq_l14_c7-60_0017
              (= _internal_compare_l14_c7-60_0016 0))
            (ite
              (= (first _internal_sender_l13_c29-35_0015)
                (first _internal_car_l13_c13-18_0014))
              (ite
                (= (second _internal_sender_l13_c29-35_0015)
                  (second _internal_car_l13_c13-18_0014))
                (= _internal_compare_l14_c7-60_0016 0)
                (ite
                  (str.< (second _internal_car_l13_c13-18_0014)
                    (second _internal_sender_l13_c29-35_0015))
                  (= _internal_compare_l14_c7-60_0016 1)
                  (= _internal_compare_l14_c7-60_0016 (- 1))))
              (ite
                (str.< (first _internal_car_l13_c13-18_0014)
                  (first _internal_sender_l13_c29-35_0015))
                (= _internal_compare_l14_c7-60_0016 1)
                (= _internal_compare_l14_c7-60_0016 (- 1))))
            (= _internal_sender_l13_c29-35_0015 sender)
            (= _internal_car_l13_c13-18_0014
              (first _internal_car_l13_c13-18_0013))
            (= _internal_car_l13_c13-18_0013
              (first _internal_cdr_l13_c13-18_0012))
            (= _internal_cdr_l13_c13-18_0012
              (second _internal_dup_l13_c7-10_0011))
            (= _internal_dup_l13_c7-10_0011 _internal_param_stor_0010)
            (= _internal_param_stor_0010 intial_0004)
            (not
              (= (first (first (second intial_0004)))
                (first (second (second intial_0004)))))
            (=
              (contract_ty_opt
                ((as mk-pair (Pair String String)) (first self_addr)
                  "%default"))
              ((as some (Option String)) "(option key_hash)")))
          (and
            (= _fresh_binding_0079
              ((as mk-pair
                 (Pair (Pair (Pair String String) (Option String))
                   (Pair (Pair String String) (Option String))))
                _internal_pair_l20_c27-35_0045 _internal_cdr_l20_c27-35_0046))
            (= _internal_cdr_l20_c27-35_0046
              (second _internal_dup_l20_c27-35_0040))
            (= _internal_pair_l20_c27-35_0045
              ((as mk-pair (Pair (Pair String String) (Option String)))
                _internal_car_l20_c27-35_0044
                _internal_unpair_l20_c11-17_0038))
            (= _internal_car_l20_c27-35_0044
              (first _internal_car_l20_c27-35_0041))
            (= _internal_cdr_l20_c27-35_0043
              (second _internal_dup_l20_c27-35_0042))
            (= _internal_dup_l20_c27-35_0042 _internal_car_l20_c27-35_0041)
            (= _internal_car_l20_c27-35_0041
              (first _internal_unpair_l20_c11-17_0039))
            (= _internal_dup_l20_c27-35_0040
              _internal_unpair_l20_c11-17_0039)
            (= _internal_param_stor_0010
              ((as mk-pair
                 (Pair (Option String)
                   (Pair (Pair (Pair String String) (Option String))
                     (Pair (Pair String String) (Option String)))))
                _internal_unpair_l20_c11-17_0038
                _internal_unpair_l20_c11-17_0039))
            (= _internal_eq_l14_c7-60_0017 false)
            (= _internal_eq_l14_c7-60_0017
              (= _internal_compare_l14_c7-60_0016 0))
            (ite
              (= (first _internal_sender_l13_c29-35_0015)
                (first _internal_car_l13_c13-18_0014))
              (ite
                (= (second _internal_sender_l13_c29-35_0015)
                  (second _internal_car_l13_c13-18_0014))
                (= _internal_compare_l14_c7-60_0016 0)
                (ite
                  (str.< (second _internal_car_l13_c13-18_0014)
                    (second _internal_sender_l13_c29-35_0015))
                  (= _internal_compare_l14_c7-60_0016 1)
                  (= _internal_compare_l14_c7-60_0016 (- 1))))
              (ite
                (str.< (first _internal_car_l13_c13-18_0014)
                  (first _internal_sender_l13_c29-35_0015))
                (= _internal_compare_l14_c7-60_0016 1)
                (= _internal_compare_l14_c7-60_0016 (- 1))))
            (= _internal_sender_l13_c29-35_0015 sender)
            (= _internal_car_l13_c13-18_0014
              (first _internal_car_l13_c13-18_0013))
            (= _internal_car_l13_c13-18_0013
              (first _internal_cdr_l13_c13-18_0012))
            (= _internal_cdr_l13_c13-18_0012
              (second _internal_dup_l13_c7-10_0011))
            (= _internal_dup_l13_c7-10_0011 _internal_param_stor_0010)
            (= _internal_param_stor_0010 intial_0004)
            (not
              (= (first (first (second intial_0004)))
                (first (second (second intial_0004)))))
            (=
              (contract_ty_opt
                ((as mk-pair (Pair String String)) (first self_addr)
                  "%default"))
              ((as some (Option String)) "(option key_hash)")))))
      (and (= _fresh_binding_0078 (as seq.empty (Seq Operation)))
        (= _internal_cdr_l23_c19-23_0054 (as none (Option String)))
        (= _internal_cdr_l22_c13-17_0051
          ((as some (Option String)) _internal_if_none_l24_c7-38_0055))
        (= _internal_cdr_l23_c19-23_0054
          (second _internal_cdr_l23_c19-23_0053))
        (= _internal_cdr_l23_c19-23_0053
          (second _internal_dup_l23_c13-16_0052))
        (= _internal_dup_l23_c13-16_0052 _fresh_binding_0079)
        (= _internal_cdr_l22_c13-17_0051
          (second _internal_car_l22_c13-17_0050))
        (= _internal_car_l22_c13-17_0050
          (first _internal_dup_l22_c7-10_0049))
        (= _internal_dup_l22_c7-10_0049 _fresh_binding_0079)
        (or
          (and
            (= _fresh_binding_0079
              ((as mk-pair
                 (Pair (Pair (Pair String String) (Option String))
                   (Pair (Pair String String) (Option String))))
                _internal_car_l18_c31-39_0033 _internal_pair_l18_c31-39_0032))
            (= _internal_car_l18_c31-39_0033
              (first _internal_dup_l18_c31-39_0027))
            (= _internal_pair_l18_c31-39_0032
              ((as mk-pair (Pair (Pair String String) (Option String)))
                _internal_car_l18_c31-39_0031
                _internal_unpair_l18_c15-21_0025))
            (= _internal_car_l18_c31-39_0031
              (first _internal_cdr_l18_c31-39_0028))
            (= _internal_cdr_l18_c31-39_0030
              (second _internal_dup_l18_c31-39_0029))
            (= _internal_dup_l18_c31-39_0029 _internal_cdr_l18_c31-39_0028)
            (= _internal_cdr_l18_c31-39_0028
              (second _internal_unpair_l18_c15-21_0026))
            (= _internal_dup_l18_c31-39_0027
              _internal_unpair_l18_c15-21_0026)
            (= _internal_param_stor_0010
              ((as mk-pair
                 (Pair (Option String)
                   (Pair (Pair (Pair String String) (Option String))
                     (Pair (Pair String String) (Option String)))))
                _internal_unpair_l18_c15-21_0025
                _internal_unpair_l18_c15-21_0026))
            (= _internal_eq_l17_c11-21_0024 true)
            (= _internal_eq_l17_c11-21_0024
              (= _internal_compare_l17_c11-21_0023 0))
            (ite
              (= (first _internal_sender_l16_c25-31_0022)
                (first _internal_car_l16_c17-22_0021))
              (ite
                (= (second _internal_sender_l16_c25-31_0022)
                  (second _internal_car_l16_c17-22_0021))
                (= _internal_compare_l17_c11-21_0023 0)
                (ite
                  (str.< (second _internal_car_l16_c17-22_0021)
                    (second _internal_sender_l16_c25-31_0022))
                  (= _internal_compare_l17_c11-21_0023 1)
                  (= _internal_compare_l17_c11-21_0023 (- 1))))
              (ite
                (str.< (first _internal_car_l16_c17-22_0021)
                  (first _internal_sender_l16_c25-31_0022))
                (= _internal_compare_l17_c11-21_0023 1)
                (= _internal_compare_l17_c11-21_0023 (- 1))))
            (= _internal_sender_l16_c25-31_0022 sender)
            (= _internal_car_l16_c17-22_0021
              (first _internal_cdr_l16_c17-22_0020))
            (= _internal_cdr_l16_c17-22_0020
              (second _internal_cdr_l16_c17-22_0019))
            (= _internal_cdr_l16_c17-22_0019
              (second _internal_dup_l16_c11-14_0018))
            (= _internal_dup_l16_c11-14_0018 _internal_param_stor_0010)
            (= _internal_eq_l14_c7-60_0017 true)
            (= _internal_eq_l14_c7-60_0017
              (= _internal_compare_l14_c7-60_0016 0))
            (ite
              (= (first _internal_sender_l13_c29-35_0015)
                (first _internal_car_l13_c13-18_0014))
              (ite
                (= (second _internal_sender_l13_c29-35_0015)
                  (second _internal_car_l13_c13-18_0014))
                (= _internal_compare_l14_c7-60_0016 0)
                (ite
                  (str.< (second _internal_car_l13_c13-18_0014)
                    (second _internal_sender_l13_c29-35_0015))
                  (= _internal_compare_l14_c7-60_0016 1)
                  (= _internal_compare_l14_c7-60_0016 (- 1))))
              (ite
                (str.< (first _internal_car_l13_c13-18_0014)
                  (first _internal_sender_l13_c29-35_0015))
                (= _internal_compare_l14_c7-60_0016 1)
                (= _internal_compare_l14_c7-60_0016 (- 1))))
            (= _internal_sender_l13_c29-35_0015 sender)
            (= _internal_car_l13_c13-18_0014
              (first _internal_car_l13_c13-18_0013))
            (= _internal_car_l13_c13-18_0013
              (first _internal_cdr_l13_c13-18_0012))
            (= _internal_cdr_l13_c13-18_0012
              (second _internal_dup_l13_c7-10_0011))
            (= _internal_dup_l13_c7-10_0011 _internal_param_stor_0010)
            (= _internal_param_stor_0010 intial_0004)
            (not
              (= (first (first (second intial_0004)))
                (first (second (second intial_0004)))))
            (=
              (contract_ty_opt
                ((as mk-pair (Pair String String)) (first self_addr)
                  "%default"))
              ((as some (Option String)) "(option key_hash)")))
          (and
            (= _fresh_binding_0079
              ((as mk-pair
                 (Pair (Pair (Pair String String) (Option String))
                   (Pair (Pair String String) (Option String))))
                _internal_pair_l20_c27-35_0045 _internal_cdr_l20_c27-35_0046))
            (= _internal_cdr_l20_c27-35_0046
              (second _internal_dup_l20_c27-35_0040))
            (= _internal_pair_l20_c27-35_0045
              ((as mk-pair (Pair (Pair String String) (Option String)))
                _internal_car_l20_c27-35_0044
                _internal_unpair_l20_c11-17_0038))
            (= _internal_car_l20_c27-35_0044
              (first _internal_car_l20_c27-35_0041))
            (= _internal_cdr_l20_c27-35_0043
              (second _internal_dup_l20_c27-35_0042))
            (= _internal_dup_l20_c27-35_0042 _internal_car_l20_c27-35_0041)
            (= _internal_car_l20_c27-35_0041
              (first _internal_unpair_l20_c11-17_0039))
            (= _internal_dup_l20_c27-35_0040
              _internal_unpair_l20_c11-17_0039)
            (= _internal_param_stor_0010
              ((as mk-pair
                 (Pair (Option String)
                   (Pair (Pair (Pair String String) (Option String))
                     (Pair (Pair String String) (Option String)))))
                _internal_unpair_l20_c11-17_0038
                _internal_unpair_l20_c11-17_0039))
            (= _internal_eq_l14_c7-60_0017 false)
            (= _internal_eq_l14_c7-60_0017
              (= _internal_compare_l14_c7-60_0016 0))
            (ite
              (= (first _internal_sender_l13_c29-35_0015)
                (first _internal_car_l13_c13-18_0014))
              (ite
                (= (second _internal_sender_l13_c29-35_0015)
                  (second _internal_car_l13_c13-18_0014))
                (= _internal_compare_l14_c7-60_0016 0)
                (ite
                  (str.< (second _internal_car_l13_c13-18_0014)
                    (second _internal_sender_l13_c29-35_0015))
                  (= _internal_compare_l14_c7-60_0016 1)
                  (= _internal_compare_l14_c7-60_0016 (- 1))))
              (ite
                (str.< (first _internal_car_l13_c13-18_0014)
                  (first _internal_sender_l13_c29-35_0015))
                (= _internal_compare_l14_c7-60_0016 1)
                (= _internal_compare_l14_c7-60_0016 (- 1))))
            (= _internal_sender_l13_c29-35_0015 sender)
            (= _internal_car_l13_c13-18_0014
              (first _internal_car_l13_c13-18_0013))
            (= _internal_car_l13_c13-18_0013
              (first _internal_cdr_l13_c13-18_0012))
            (= _internal_cdr_l13_c13-18_0012
              (second _internal_dup_l13_c7-10_0011))
            (= _internal_dup_l13_c7-10_0011 _internal_param_stor_0010)
            (= _internal_param_stor_0010 intial_0004)
            (not
              (= (first (first (second intial_0004)))
                (first (second (second intial_0004)))))
            (=
              (contract_ty_opt
                ((as mk-pair (Pair String String)) (first self_addr)
                  "%default"))
              ((as some (Option String)) "(option key_hash)")))))
      (and
        (= _fresh_binding_0078
          (seq.++ (seq.unit _internal_set_delegate_l34_c19-31_0070)
            _internal_nil_l34_c34-47_0071))
        (= _internal_nil_l34_c34-47_0071 (as seq.empty (Seq Operation)))
        (= ((as setDelegate Operation) _internal_some_l33_c19-23_0069)
          _internal_set_delegate_l34_c19-31_0070)
        (= _internal_some_l33_c19-23_0069
          ((as some (Option String)) _internal_if_none_l24_c7-38_0055))
        (= _internal_eq_l32_c15-34_0068 true)
        (= _internal_eq_l32_c15-34_0068
          (= _internal_compare_l32_c15-34_0067 0))
        (ite
          (= _internal_if_none_l30_c11-37_0064 _internal_dup_l31_c21-24_0066)
          (= _internal_compare_l32_c15-34_0067 0)
          (ite
            (str.< _internal_dup_l31_c21-24_0066
              _internal_if_none_l30_c11-37_0064)
            (= _internal_compare_l32_c15-34_0067 1)
            (= _internal_compare_l32_c15-34_0067 (- 1))))
        (= _internal_dup_l31_c21-24_0066 _internal_if_none_l24_c7-38_0055)
        (= _internal_cdr_l23_c19-23_0054
          ((as some (Option String)) _internal_if_none_l30_c11-37_0064))
        (= _internal_cdr_l22_c13-17_0051
          ((as some (Option String)) _internal_if_none_l24_c7-38_0055))
        (= _internal_cdr_l23_c19-23_0054
          (second _internal_cdr_l23_c19-23_0053))
        (= _internal_cdr_l23_c19-23_0053
          (second _internal_dup_l23_c13-16_0052))
        (= _internal_dup_l23_c13-16_0052 _fresh_binding_0079)
        (= _internal_cdr_l22_c13-17_0051
          (second _internal_car_l22_c13-17_0050))
        (= _internal_car_l22_c13-17_0050
          (first _internal_dup_l22_c7-10_0049))
        (= _internal_dup_l22_c7-10_0049 _fresh_binding_0079)
        (or
          (and
            (= _fresh_binding_0079
              ((as mk-pair
                 (Pair (Pair (Pair String String) (Option String))
                   (Pair (Pair String String) (Option String))))
                _internal_car_l18_c31-39_0033 _internal_pair_l18_c31-39_0032))
            (= _internal_car_l18_c31-39_0033
              (first _internal_dup_l18_c31-39_0027))
            (= _internal_pair_l18_c31-39_0032
              ((as mk-pair (Pair (Pair String String) (Option String)))
                _internal_car_l18_c31-39_0031
                _internal_unpair_l18_c15-21_0025))
            (= _internal_car_l18_c31-39_0031
              (first _internal_cdr_l18_c31-39_0028))
            (= _internal_cdr_l18_c31-39_0030
              (second _internal_dup_l18_c31-39_0029))
            (= _internal_dup_l18_c31-39_0029 _internal_cdr_l18_c31-39_0028)
            (= _internal_cdr_l18_c31-39_0028
              (second _internal_unpair_l18_c15-21_0026))
            (= _internal_dup_l18_c31-39_0027
              _internal_unpair_l18_c15-21_0026)
            (= _internal_param_stor_0010
              ((as mk-pair
                 (Pair (Option String)
                   (Pair (Pair (Pair String String) (Option String))
                     (Pair (Pair String String) (Option String)))))
                _internal_unpair_l18_c15-21_0025
                _internal_unpair_l18_c15-21_0026))
            (= _internal_eq_l17_c11-21_0024 true)
            (= _internal_eq_l17_c11-21_0024
              (= _internal_compare_l17_c11-21_0023 0))
            (ite
              (= (first _internal_sender_l16_c25-31_0022)
                (first _internal_car_l16_c17-22_0021))
              (ite
                (= (second _internal_sender_l16_c25-31_0022)
                  (second _internal_car_l16_c17-22_0021))
                (= _internal_compare_l17_c11-21_0023 0)
                (ite
                  (str.< (second _internal_car_l16_c17-22_0021)
                    (second _internal_sender_l16_c25-31_0022))
                  (= _internal_compare_l17_c11-21_0023 1)
                  (= _internal_compare_l17_c11-21_0023 (- 1))))
              (ite
                (str.< (first _internal_car_l16_c17-22_0021)
                  (first _internal_sender_l16_c25-31_0022))
                (= _internal_compare_l17_c11-21_0023 1)
                (= _internal_compare_l17_c11-21_0023 (- 1))))
            (= _internal_sender_l16_c25-31_0022 sender)
            (= _internal_car_l16_c17-22_0021
              (first _internal_cdr_l16_c17-22_0020))
            (= _internal_cdr_l16_c17-22_0020
              (second _internal_cdr_l16_c17-22_0019))
            (= _internal_cdr_l16_c17-22_0019
              (second _internal_dup_l16_c11-14_0018))
            (= _internal_dup_l16_c11-14_0018 _internal_param_stor_0010)
            (= _internal_eq_l14_c7-60_0017 true)
            (= _internal_eq_l14_c7-60_0017
              (= _internal_compare_l14_c7-60_0016 0))
            (ite
              (= (first _internal_sender_l13_c29-35_0015)
                (first _internal_car_l13_c13-18_0014))
              (ite
                (= (second _internal_sender_l13_c29-35_0015)
                  (second _internal_car_l13_c13-18_0014))
                (= _internal_compare_l14_c7-60_0016 0)
                (ite
                  (str.< (second _internal_car_l13_c13-18_0014)
                    (second _internal_sender_l13_c29-35_0015))
                  (= _internal_compare_l14_c7-60_0016 1)
                  (= _internal_compare_l14_c7-60_0016 (- 1))))
              (ite
                (str.< (first _internal_car_l13_c13-18_0014)
                  (first _internal_sender_l13_c29-35_0015))
                (= _internal_compare_l14_c7-60_0016 1)
                (= _internal_compare_l14_c7-60_0016 (- 1))))
            (= _internal_sender_l13_c29-35_0015 sender)
            (= _internal_car_l13_c13-18_0014
              (first _internal_car_l13_c13-18_0013))
            (= _internal_car_l13_c13-18_0013
              (first _internal_cdr_l13_c13-18_0012))
            (= _internal_cdr_l13_c13-18_0012
              (second _internal_dup_l13_c7-10_0011))
            (= _internal_dup_l13_c7-10_0011 _internal_param_stor_0010)
            (= _internal_param_stor_0010 intial_0004)
            (not
              (= (first (first (second intial_0004)))
                (first (second (second intial_0004)))))
            (=
              (contract_ty_opt
                ((as mk-pair (Pair String String)) (first self_addr)
                  "%default"))
              ((as some (Option String)) "(option key_hash)")))
          (and
            (= _fresh_binding_0079
              ((as mk-pair
                 (Pair (Pair (Pair String String) (Option String))
                   (Pair (Pair String String) (Option String))))
                _internal_pair_l20_c27-35_0045 _internal_cdr_l20_c27-35_0046))
            (= _internal_cdr_l20_c27-35_0046
              (second _internal_dup_l20_c27-35_0040))
            (= _internal_pair_l20_c27-35_0045
              ((as mk-pair (Pair (Pair String String) (Option String)))
                _internal_car_l20_c27-35_0044
                _internal_unpair_l20_c11-17_0038))
            (= _internal_car_l20_c27-35_0044
              (first _internal_car_l20_c27-35_0041))
            (= _internal_cdr_l20_c27-35_0043
              (second _internal_dup_l20_c27-35_0042))
            (= _internal_dup_l20_c27-35_0042 _internal_car_l20_c27-35_0041)
            (= _internal_car_l20_c27-35_0041
              (first _internal_unpair_l20_c11-17_0039))
            (= _internal_dup_l20_c27-35_0040
              _internal_unpair_l20_c11-17_0039)
            (= _internal_param_stor_0010
              ((as mk-pair
                 (Pair (Option String)
                   (Pair (Pair (Pair String String) (Option String))
                     (Pair (Pair String String) (Option String)))))
                _internal_unpair_l20_c11-17_0038
                _internal_unpair_l20_c11-17_0039))
            (= _internal_eq_l14_c7-60_0017 false)
            (= _internal_eq_l14_c7-60_0017
              (= _internal_compare_l14_c7-60_0016 0))
            (ite
              (= (first _internal_sender_l13_c29-35_0015)
                (first _internal_car_l13_c13-18_0014))
              (ite
                (= (second _internal_sender_l13_c29-35_0015)
                  (second _internal_car_l13_c13-18_0014))
                (= _internal_compare_l14_c7-60_0016 0)
                (ite
                  (str.< (second _internal_car_l13_c13-18_0014)
                    (second _internal_sender_l13_c29-35_0015))
                  (= _internal_compare_l14_c7-60_0016 1)
                  (= _internal_compare_l14_c7-60_0016 (- 1))))
              (ite
                (str.< (first _internal_car_l13_c13-18_0014)
                  (first _internal_sender_l13_c29-35_0015))
                (= _internal_compare_l14_c7-60_0016 1)
                (= _internal_compare_l14_c7-60_0016 (- 1))))
            (= _internal_sender_l13_c29-35_0015 sender)
            (= _internal_car_l13_c13-18_0014
              (first _internal_car_l13_c13-18_0013))
            (= _internal_car_l13_c13-18_0013
              (first _internal_cdr_l13_c13-18_0012))
            (= _internal_cdr_l13_c13-18_0012
              (second _internal_dup_l13_c7-10_0011))
            (= _internal_dup_l13_c7-10_0011 _internal_param_stor_0010)
            (= _internal_param_stor_0010 intial_0004)
            (not
              (= (first (first (second intial_0004)))
                (first (second (second intial_0004)))))
            (=
              (contract_ty_opt
                ((as mk-pair (Pair String String)) (first self_addr)
                  "%default"))
              ((as some (Option String)) "(option key_hash)")))))
      (and (= _fresh_binding_0078 (as seq.empty (Seq Operation)))
        (= _internal_eq_l32_c15-34_0068 false)
        (= _internal_eq_l32_c15-34_0068
          (= _internal_compare_l32_c15-34_0067 0))
        (ite
          (= _internal_if_none_l30_c11-37_0064 _internal_dup_l31_c21-24_0066)
          (= _internal_compare_l32_c15-34_0067 0)
          (ite
            (str.< _internal_dup_l31_c21-24_0066
              _internal_if_none_l30_c11-37_0064)
            (= _internal_compare_l32_c15-34_0067 1)
            (= _internal_compare_l32_c15-34_0067 (- 1))))
        (= _internal_dup_l31_c21-24_0066 _internal_if_none_l24_c7-38_0055)
        (= _internal_cdr_l23_c19-23_0054
          ((as some (Option String)) _internal_if_none_l30_c11-37_0064))
        (= _internal_cdr_l22_c13-17_0051
          ((as some (Option String)) _internal_if_none_l24_c7-38_0055))
        (= _internal_cdr_l23_c19-23_0054
          (second _internal_cdr_l23_c19-23_0053))
        (= _internal_cdr_l23_c19-23_0053
          (second _internal_dup_l23_c13-16_0052))
        (= _internal_dup_l23_c13-16_0052 _fresh_binding_0079)
        (= _internal_cdr_l22_c13-17_0051
          (second _internal_car_l22_c13-17_0050))
        (= _internal_car_l22_c13-17_0050
          (first _internal_dup_l22_c7-10_0049))
        (= _internal_dup_l22_c7-10_0049 _fresh_binding_0079)
        (or
          (and
            (= _fresh_binding_0079
              ((as mk-pair
                 (Pair (Pair (Pair String String) (Option String))
                   (Pair (Pair String String) (Option String))))
                _internal_car_l18_c31-39_0033 _internal_pair_l18_c31-39_0032))
            (= _internal_car_l18_c31-39_0033
              (first _internal_dup_l18_c31-39_0027))
            (= _internal_pair_l18_c31-39_0032
              ((as mk-pair (Pair (Pair String String) (Option String)))
                _internal_car_l18_c31-39_0031
                _internal_unpair_l18_c15-21_0025))
            (= _internal_car_l18_c31-39_0031
              (first _internal_cdr_l18_c31-39_0028))
            (= _internal_cdr_l18_c31-39_0030
              (second _internal_dup_l18_c31-39_0029))
            (= _internal_dup_l18_c31-39_0029 _internal_cdr_l18_c31-39_0028)
            (= _internal_cdr_l18_c31-39_0028
              (second _internal_unpair_l18_c15-21_0026))
            (= _internal_dup_l18_c31-39_0027
              _internal_unpair_l18_c15-21_0026)
            (= _internal_param_stor_0010
              ((as mk-pair
                 (Pair (Option String)
                   (Pair (Pair (Pair String String) (Option String))
                     (Pair (Pair String String) (Option String)))))
                _internal_unpair_l18_c15-21_0025
                _internal_unpair_l18_c15-21_0026))
            (= _internal_eq_l17_c11-21_0024 true)
            (= _internal_eq_l17_c11-21_0024
              (= _internal_compare_l17_c11-21_0023 0))
            (ite
              (= (first _internal_sender_l16_c25-31_0022)
                (first _internal_car_l16_c17-22_0021))
              (ite
                (= (second _internal_sender_l16_c25-31_0022)
                  (second _internal_car_l16_c17-22_0021))
                (= _internal_compare_l17_c11-21_0023 0)
                (ite
                  (str.< (second _internal_car_l16_c17-22_0021)
                    (second _internal_sender_l16_c25-31_0022))
                  (= _internal_compare_l17_c11-21_0023 1)
                  (= _internal_compare_l17_c11-21_0023 (- 1))))
              (ite
                (str.< (first _internal_car_l16_c17-22_0021)
                  (first _internal_sender_l16_c25-31_0022))
                (= _internal_compare_l17_c11-21_0023 1)
                (= _internal_compare_l17_c11-21_0023 (- 1))))
            (= _internal_sender_l16_c25-31_0022 sender)
            (= _internal_car_l16_c17-22_0021
              (first _internal_cdr_l16_c17-22_0020))
            (= _internal_cdr_l16_c17-22_0020
              (second _internal_cdr_l16_c17-22_0019))
            (= _internal_cdr_l16_c17-22_0019
              (second _internal_dup_l16_c11-14_0018))
            (= _internal_dup_l16_c11-14_0018 _internal_param_stor_0010)
            (= _internal_eq_l14_c7-60_0017 true)
            (= _internal_eq_l14_c7-60_0017
              (= _internal_compare_l14_c7-60_0016 0))
            (ite
              (= (first _internal_sender_l13_c29-35_0015)
                (first _internal_car_l13_c13-18_0014))
              (ite
                (= (second _internal_sender_l13_c29-35_0015)
                  (second _internal_car_l13_c13-18_0014))
                (= _internal_compare_l14_c7-60_0016 0)
                (ite
                  (str.< (second _internal_car_l13_c13-18_0014)
                    (second _internal_sender_l13_c29-35_0015))
                  (= _internal_compare_l14_c7-60_0016 1)
                  (= _internal_compare_l14_c7-60_0016 (- 1))))
              (ite
                (str.< (first _internal_car_l13_c13-18_0014)
                  (first _internal_sender_l13_c29-35_0015))
                (= _internal_compare_l14_c7-60_0016 1)
                (= _internal_compare_l14_c7-60_0016 (- 1))))
            (= _internal_sender_l13_c29-35_0015 sender)
            (= _internal_car_l13_c13-18_0014
              (first _internal_car_l13_c13-18_0013))
            (= _internal_car_l13_c13-18_0013
              (first _internal_cdr_l13_c13-18_0012))
            (= _internal_cdr_l13_c13-18_0012
              (second _internal_dup_l13_c7-10_0011))
            (= _internal_dup_l13_c7-10_0011 _internal_param_stor_0010)
            (= _internal_param_stor_0010 intial_0004)
            (not
              (= (first (first (second intial_0004)))
                (first (second (second intial_0004)))))
            (=
              (contract_ty_opt
                ((as mk-pair (Pair String String)) (first self_addr)
                  "%default"))
              ((as some (Option String)) "(option key_hash)")))
          (and
            (= _fresh_binding_0079
              ((as mk-pair
                 (Pair (Pair (Pair String String) (Option String))
                   (Pair (Pair String String) (Option String))))
                _internal_pair_l20_c27-35_0045 _internal_cdr_l20_c27-35_0046))
            (= _internal_cdr_l20_c27-35_0046
              (second _internal_dup_l20_c27-35_0040))
            (= _internal_pair_l20_c27-35_0045
              ((as mk-pair (Pair (Pair String String) (Option String)))
                _internal_car_l20_c27-35_0044
                _internal_unpair_l20_c11-17_0038))
            (= _internal_car_l20_c27-35_0044
              (first _internal_car_l20_c27-35_0041))
            (= _internal_cdr_l20_c27-35_0043
              (second _internal_dup_l20_c27-35_0042))
            (= _internal_dup_l20_c27-35_0042 _internal_car_l20_c27-35_0041)
            (= _internal_car_l20_c27-35_0041
              (first _internal_unpair_l20_c11-17_0039))
            (= _internal_dup_l20_c27-35_0040
              _internal_unpair_l20_c11-17_0039)
            (= _internal_param_stor_0010
              ((as mk-pair
                 (Pair (Option String)
                   (Pair (Pair (Pair String String) (Option String))
                     (Pair (Pair String String) (Option String)))))
                _internal_unpair_l20_c11-17_0038
                _internal_unpair_l20_c11-17_0039))
            (= _internal_eq_l14_c7-60_0017 false)
            (= _internal_eq_l14_c7-60_0017
              (= _internal_compare_l14_c7-60_0016 0))
            (ite
              (= (first _internal_sender_l13_c29-35_0015)
                (first _internal_car_l13_c13-18_0014))
              (ite
                (= (second _internal_sender_l13_c29-35_0015)
                  (second _internal_car_l13_c13-18_0014))
                (= _internal_compare_l14_c7-60_0016 0)
                (ite
                  (str.< (second _internal_car_l13_c13-18_0014)
                    (second _internal_sender_l13_c29-35_0015))
                  (= _internal_compare_l14_c7-60_0016 1)
                  (= _internal_compare_l14_c7-60_0016 (- 1))))
              (ite
                (str.< (first _internal_car_l13_c13-18_0014)
                  (first _internal_sender_l13_c29-35_0015))
                (= _internal_compare_l14_c7-60_0016 1)
                (= _internal_compare_l14_c7-60_0016 (- 1))))
            (= _internal_sender_l13_c29-35_0015 sender)
            (= _internal_car_l13_c13-18_0014
              (first _internal_car_l13_c13-18_0013))
            (= _internal_car_l13_c13-18_0013
              (first _internal_cdr_l13_c13-18_0012))
            (= _internal_cdr_l13_c13-18_0012
              (second _internal_dup_l13_c7-10_0011))
            (= _internal_dup_l13_c7-10_0011 _internal_param_stor_0010)
            (= _internal_param_stor_0010 intial_0004)
            (not
              (= (first (first (second intial_0004)))
                (first (second (second intial_0004)))))
            (=
              (contract_ty_opt
                ((as mk-pair (Pair String String)) (first self_addr)
                  "%default"))
              ((as some (Option String)) "(option key_hash)"))))))
    (not
      (=>
        (not
          (= (first (first (second intial_0004)))
            (first (second (second intial_0004)))))
        (and
          (or (not (= sender (first (first (second _internal_result_0082)))))
            (= (second (first (second _internal_result_0082)))
              (first intial_0004)))
          (or
            (not (= sender (first (second (second _internal_result_0082)))))
            (= (second (second (second _internal_result_0082)))
              (first intial_0004)))
          (= (first _internal_result_0082)
            (ite
              (=
                (= (second (first (second _internal_result_0082)))
                  (second (second (second _internal_result_0082)))) true)
              (seq.++
                (seq.unit
                  ((as setDelegate Operation)
                    (second (first (second _internal_result_0082)))))
                (as seq.empty (Seq Operation)))
              (as seq.empty (Seq Operation)))))))))
(echo
  "Finding counter-example for postcondition of ContractAnnot at line 0, characters -1--1")
(check-sat)
(echo "START_MODEL")
(get-model)
(echo "END_MODEL")
(pop 1)


