(set-logic ALL)
(declare-datatypes ((Pair 2))
  ((par (T1 T2) ((mk-pair (first T1) (second T2))))))
(declare-datatypes ((Unit 0)) (((unit))))
(declare-datatypes ((Or 2))
  ((par (T1 T2) ((left (extractLeft T1)) (right (extractRight T2))))))
(declare-datatypes ((Option 1)) ((par (T) ((none) (some (extractSome T))))))
(declare-datatypes ((Contract 0))
  (((contract (extractContract (Pair String String))))))
(define-sort Fun () Int)
(declare-datatypes ((Operation 0))
  (((setDelegate (delegateAddress (Option String)))
     (transferTokens (transferData String) (transferMutez Int)
       (transferContract Contract))
     (createContract (createContractDelegateAddress (Option String))
       (createContractMutez Int) (createContractInitStorage String)
       (createContractAddress (Pair String String))))))
(declare-datatypes ((Exception 0))
  (((overflow) (error (extractError String)))))
(declare-fun amount () Int)
(declare-fun balance () Int)
(declare-fun contract_ty_opt ((Pair String String)) (Option String))
(declare-fun pack!unit (Unit) String)
(declare-fun self_addr () (Pair String String))
(declare-fun sender () (Pair String String))
(declare-fun source () (Pair String String))
(declare-fun unpack_opt!int (String) (Option Int))
(declare-fun unpack_opt!unit (String) (Option Unit))
(assert (or (= amount balance) (> balance amount)))
(assert (= (contract_ty_opt source) ((as some (Option String)) "unit")))
(assert (= (second source) "%default"))
(assert (= (second sender) "%default"))
(assert (= (second self_addr) "%default"))
(push 1)
(declare-const _internal_car_l8_c9-12_0011 (Pair String String))
(declare-const _internal_contract_l9_c9-22_0012 (Option Contract))
(declare-const _internal_param_stor_0010 (Pair (Pair String String) Unit))
(declare-const _internal_result_0024 Exception)
(declare-const _internal_unit_l11_c13-17_0014 Unit)
(declare-const intial_0004 (Pair (Pair String String) Unit))
(assert
  (and
    (= (unpack_opt!unit (pack!unit _internal_unit_l11_c13-17_0014))
      ((as some (Option Unit)) _internal_unit_l11_c13-17_0014))
    (= (unpack_opt!unit (pack!unit unit)) ((as some (Option Unit)) unit))
    (ite (is-some _internal_contract_l9_c9-22_0012)
      (=
        (contract_ty_opt
          (extractContract (extractSome _internal_contract_l9_c9-22_0012)))
        ((as some (Option String)) "unit")) true)
    (= _internal_result_0024
      ((as error Exception) (pack!unit _internal_unit_l11_c13-17_0014)))
    (= _internal_unit_l11_c13-17_0014 unit)
    (= _internal_contract_l9_c9-22_0012 (as none (Option Contract)))
    (= _internal_contract_l9_c9-22_0012
      (ite
        (= (contract_ty_opt _internal_car_l8_c9-12_0011)
          ((as some (Option String)) "unit"))
        ((as some (Option Contract))
          ((as contract Contract) _internal_car_l8_c9-12_0011))
        (as none (Option Contract))))
    (= _internal_car_l8_c9-12_0011 (first _internal_param_stor_0010))
    (= _internal_param_stor_0010 intial_0004)
    (=
      (contract_ty_opt
        ((as mk-pair (Pair String String)) (first self_addr) "%default"))
      ((as some (Option String)) "address"))
    (not (= _internal_result_0024 ((as error Exception) (pack!unit unit))))))
(echo
  "Finding counter-example for ab-postcondition of ContractAnnot at line 0, characters -1--1")
(check-sat)
(echo "START_MODEL")
(get-model)
(echo "END_MODEL")
(pop 1)
(push 1)
(declare-const _internal_car_l8_c9-12_0011 (Pair String String))
(declare-const _internal_cons_l18_c12-16_0020 (Seq Operation))
(declare-const _internal_contract_l9_c9-22_0012 (Option Contract))
(declare-const _internal_if_none_l10_c9-12_0013 Contract)
(declare-const _internal_nil_l16_c12-25_0019 (Seq Operation))
(declare-const _internal_param_stor_0010 (Pair (Pair String String) Unit))
(declare-const _internal_push_l13_c12-24_0016 Int)
(declare-const _internal_result_0025 (Pair (Seq Operation) Unit))
(declare-const _internal_transfer_tokens_l15_c12-27_0018 Operation)
(declare-const _internal_unit_l14_c12-16_0017 Unit)
(declare-const _internal_unit_l19_c12-16_0021 Unit)
(declare-const intial_0004 (Pair (Pair String String) Unit))
(assert
  (and
    (= (unpack_opt!unit (pack!unit _internal_unit_l14_c12-16_0017))
      ((as some (Option Unit)) _internal_unit_l14_c12-16_0017))
    (or (= 0 _internal_push_l13_c12-24_0016)
      (> _internal_push_l13_c12-24_0016 0))
    (or (= _internal_push_l13_c12-24_0016 9223372036854775807)
      (> 9223372036854775807 _internal_push_l13_c12-24_0016))
    (= (contract_ty_opt (extractContract _internal_if_none_l10_c9-12_0013))
      ((as some (Option String)) "unit"))
    (ite (is-some _internal_contract_l9_c9-22_0012)
      (=
        (contract_ty_opt
          (extractContract (extractSome _internal_contract_l9_c9-22_0012)))
        ((as some (Option String)) "unit")) true)
    (= _internal_result_0025
      ((as mk-pair (Pair (Seq Operation) Unit))
        _internal_cons_l18_c12-16_0020 _internal_unit_l19_c12-16_0021))
    (= _internal_unit_l19_c12-16_0021 unit)
    (= _internal_cons_l18_c12-16_0020
      (seq.++ (seq.unit _internal_transfer_tokens_l15_c12-27_0018)
        _internal_nil_l16_c12-25_0019))
    (= _internal_nil_l16_c12-25_0019 (as seq.empty (Seq Operation)))
    (=
      ((as transferTokens Operation)
        (pack!unit _internal_unit_l14_c12-16_0017)
        _internal_push_l13_c12-24_0016 _internal_if_none_l10_c9-12_0013)
      _internal_transfer_tokens_l15_c12-27_0018)
    (= _internal_unit_l14_c12-16_0017 unit)
    (= _internal_push_l13_c12-24_0016 1)
    (= _internal_contract_l9_c9-22_0012
      ((as some (Option Contract)) _internal_if_none_l10_c9-12_0013))
    (= _internal_contract_l9_c9-22_0012
      (ite
        (= (contract_ty_opt _internal_car_l8_c9-12_0011)
          ((as some (Option String)) "unit"))
        ((as some (Option Contract))
          ((as contract Contract) _internal_car_l8_c9-12_0011))
        (as none (Option Contract))))
    (= _internal_car_l8_c9-12_0011 (first _internal_param_stor_0010))
    (= _internal_param_stor_0010 intial_0004)
    (=
      (contract_ty_opt
        ((as mk-pair (Pair String String)) (first self_addr) "%default"))
      ((as some (Option String)) "address"))
    (not
      (or
        (not
          (ite
            (is-some
              (ite
                (= (contract_ty_opt (first intial_0004))
                  ((as some (Option String)) "unit"))
                ((as some (Option Contract))
                  ((as contract Contract) (first intial_0004)))
                (as none (Option Contract)))) true false))
        (ite
          (and (< 0 (seq.len (first _internal_result_0025)))
            (is-transferTokens (seq.nth (first _internal_result_0025) 0))
            (is-some
              (unpack_opt!int
                (transferData (seq.nth (first _internal_result_0025) 0))))
            (= 0
              (seq.len
                (seq.extract (first _internal_result_0025) 1
                  (- (seq.len (first _internal_result_0025)) 1)))))
          (=
            (extractContract
              (transferContract (seq.nth (first _internal_result_0025) 0)))
            (first intial_0004)) false)))))
(echo
  "Finding counter-example for postcondition of ContractAnnot at line 0, characters -1--1")
(check-sat)
(echo "START_MODEL")
(get-model)
(echo "END_MODEL")
(pop 1)


