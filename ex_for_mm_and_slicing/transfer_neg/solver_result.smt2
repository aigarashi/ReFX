Finding counter-example for ab-postcondition of ContractAnnot at line 0, characters -1--1
unsat
START_MODEL
(error "line 73 column 10: model is not available")
END_MODEL
Finding counter-example for postcondition of ContractAnnot at line 0, characters -1--1
sat
START_MODEL
(
  (define-fun _internal_result_0025 () (Pair (Seq Operation) Unit)
    (let ((a!1 (seq.unit (transferTokens "t" 1 (contract (mk-pair "it" "nit"))))))
  (mk-pair a!1 unit)))
  (define-fun _internal_cons_l18_c12-16_0020 () (Seq Operation)
    (seq.unit (transferTokens "t" 1 (contract (mk-pair "it" "nit")))))
  (define-fun _internal_push_l13_c12-24_0016 () Int
    1)
  (define-fun intial_0004 () (Pair (Pair String String) Unit)
    (mk-pair (mk-pair "it" "nit") unit))
  (define-fun _internal_transfer_tokens_l15_c12-27_0018 () Operation
    (transferTokens "t" 1 (contract (mk-pair "it" "nit"))))
  (define-fun _internal_unit_l19_c12-16_0021 () Unit
    unit)
  (define-fun self_addr () (Pair String String)
    (mk-pair "i" "%default"))
  (define-fun _internal_unit_l14_c12-16_0017 () Unit
    unit)
  (define-fun _internal_car_l8_c9-12_0011 () (Pair String String)
    (mk-pair "it" "nit"))
  (define-fun source () (Pair String String)
    (mk-pair "u" "%default"))
  (define-fun _internal_contract_l9_c9-22_0012 () (Option Contract)
    (some (contract (mk-pair "it" "nit"))))
  (define-fun sender () (Pair String String)
    (mk-pair "n" "%default"))
  (define-fun _internal_if_none_l10_c9-12_0013 () Contract
    (contract (mk-pair "it" "nit")))
  (define-fun amount () Int
    0)
  (define-fun balance () Int
    1)
  (define-fun _internal_nil_l16_c12-25_0019 () (Seq Operation)
    (as seq.empty (Seq Operation)))
  (define-fun _internal_param_stor_0010 () (Pair (Pair String String) Unit)
    (mk-pair (mk-pair "it" "nit") unit))
  (define-fun unpack_opt!unit ((x!0 String)) (Option Unit)
    (some unit))
  (define-fun seq.nth_u ((x!0 (Seq Operation)) (x!1 Int)) Operation
    (setDelegate (as none (Option String))))
  (define-fun unpack_opt!int ((x!0 String)) (Option Int)
    (as none (Option Int)))
  (define-fun contract_ty_opt ((x!0 (Pair String String))) (Option String)
    (ite (= x!0
            (mk-pair "u"
                     (str.++ (seq.unit (_ Char 37))
                             (seq.unit (_ Char 100))
                             (seq.unit (_ Char 101))
                             (seq.unit (_ Char 102))
                             (seq.unit (_ Char 97))
                             (seq.unit (_ Char 117))
                             (seq.unit (_ Char 108))
                             (seq.unit (_ Char 116)))))
      (some "unit")
    (ite (= x!0
            (mk-pair "i"
                     (str.++ (seq.unit (_ Char 37))
                             (seq.unit (_ Char 100))
                             (seq.unit (_ Char 101))
                             (seq.unit (_ Char 102))
                             (seq.unit (_ Char 97))
                             (seq.unit (_ Char 117))
                             (seq.unit (_ Char 108))
                             (seq.unit (_ Char 116)))))
      (some "address")
    (ite (= x!0 (mk-pair "it" "nit")) (some "unit")
      (some "unit")))))
  (define-fun pack!unit ((x!0 Unit)) String
    "t")
)
END_MODEL