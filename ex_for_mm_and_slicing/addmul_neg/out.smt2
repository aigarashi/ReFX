(set-logic ALL)
(declare-datatypes ((Pair 2))
  ((par (T1 T2) ((mk-pair (first T1) (second T2))))))
(declare-datatypes ((Unit 0)) (((unit))))
(declare-datatypes ((Or 2))
  ((par (T1 T2) ((left (extractLeft T1)) (right (extractRight T2))))))
(declare-datatypes ((Option 1)) ((par (T) ((none) (some (extractSome T))))))
(declare-datatypes ((Contract 0))
  (((contract (extractContract (Pair String String))))))
(define-sort Fun () Int)
(declare-datatypes ((Operation 0))
  (((setDelegate (delegateAddress (Option String)))
     (transferTokens (transferData String) (transferMutez Int)
       (transferContract Contract))
     (createContract (createContractDelegateAddress (Option String))
       (createContractMutez Int) (createContractInitStorage String)
       (createContractAddress (Pair String String))))))
(declare-datatypes ((Exception 0))
  (((overflow) (error (extractError String)))))
(declare-fun amount () Int)
(declare-fun balance () Int)
(declare-fun contract_ty_opt ((Pair String String)) (Option String))
(declare-fun self_addr () (Pair String String))
(declare-fun sender () (Pair String String))
(declare-fun source () (Pair String String))
(assert (or (= amount balance) (> balance amount)))
(assert (= (contract_ty_opt source) ((as some (Option String)) "unit")))
(assert (= (second source) "%default"))
(assert (= (second sender) "%default"))
(assert (= (second self_addr) "%default"))
(push 1)
(declare-const _internal_result_0023 Exception)
(declare-const intial_0004 (Pair (Pair (Pair Int Int) (Pair Int Int)) Int))
(assert (and false (not false)))
(echo
  "Finding counter-example for ab-postcondition of ContractAnnot at line 0, characters -1--1")
(check-sat)
(echo "START_MODEL")
(get-model)
(echo "END_MODEL")
(pop 1)
(push 1)
(declare-const _internal_add_l8_c9-12_0016 Int)
(declare-const _internal_add_l9_c23-26_0019 Int)
(declare-const _internal_car_l5_c9-12_0011
  (Pair (Pair Int Int) (Pair Int Int)))
(declare-const _internal_mul_l10_c9-12_0020 Int)
(declare-const _internal_nil_l11_c9-22_0021 (Seq Operation))
(declare-const _internal_param_stor_0010
  (Pair (Pair (Pair Int Int) (Pair Int Int)) Int))
(declare-const _internal_result_0024 (Pair (Seq Operation) Int))
(declare-const _internal_unpair_l6_c9-15_0012 (Pair Int Int))
(declare-const _internal_unpair_l6_c9-15_0013 (Pair Int Int))
(declare-const _internal_unpair_l7_c9-15_0014 Int)
(declare-const _internal_unpair_l7_c9-15_0015 Int)
(declare-const _internal_unpair_l9_c15-21_0017 Int)
(declare-const _internal_unpair_l9_c15-21_0018 Int)
(declare-const intial_0004 (Pair (Pair (Pair Int Int) (Pair Int Int)) Int))
(assert
  (and
    (= _internal_result_0024
      ((as mk-pair (Pair (Seq Operation) Int)) _internal_nil_l11_c9-22_0021
        _internal_mul_l10_c9-12_0020))
    (= _internal_nil_l11_c9-22_0021 (as seq.empty (Seq Operation)))
    (= _internal_mul_l10_c9-12_0020
      (* _internal_add_l8_c9-12_0016 _internal_add_l9_c23-26_0019))
    (= _internal_add_l9_c23-26_0019
      (+ _internal_unpair_l9_c15-21_0017 _internal_unpair_l9_c15-21_0018))
    (= _internal_unpair_l6_c9-15_0013
      ((as mk-pair (Pair Int Int)) _internal_unpair_l9_c15-21_0017
        _internal_unpair_l9_c15-21_0018))
    (= _internal_add_l8_c9-12_0016
      (+ _internal_unpair_l7_c9-15_0014 _internal_unpair_l7_c9-15_0015))
    (= _internal_unpair_l6_c9-15_0012
      ((as mk-pair (Pair Int Int)) _internal_unpair_l7_c9-15_0014
        _internal_unpair_l7_c9-15_0015))
    (= _internal_car_l5_c9-12_0011
      ((as mk-pair (Pair (Pair Int Int) (Pair Int Int)))
        _internal_unpair_l6_c9-15_0012 _internal_unpair_l6_c9-15_0013))
    (= _internal_car_l5_c9-12_0011 (first _internal_param_stor_0010))
    (= _internal_param_stor_0010 intial_0004)
    (=
      (contract_ty_opt
        ((as mk-pair (Pair String String)) (first self_addr) "%default"))
      ((as some (Option String)) "(pair (pair int int) (pair int int))"))
    (not
      (= (second _internal_result_0024)
        (+
          (* (first (first (first intial_0004)))
            (second (first (first intial_0004))))
          (* (first (second (first intial_0004)))
            (second (second (first intial_0004)))))))))
(echo
  "Finding counter-example for postcondition of ContractAnnot at line 0, characters -1--1")
(check-sat)
(echo "START_MODEL")
(get-model)
(echo "END_MODEL")
(pop 1)


