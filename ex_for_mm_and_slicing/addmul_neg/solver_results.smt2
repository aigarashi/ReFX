Finding counter-example for ab-postcondition of ContractAnnot at line 0, characters -1--1
unsat
START_MODEL
(error "line 39 column 10: model is not available")
END_MODEL
Finding counter-example for postcondition of ContractAnnot at line 0, characters -1--1
sat
START_MODEL
(
  (define-fun _internal_add_l9_c23-26_0019 () Int
    2)
  (define-fun _internal_mul_l10_c9-12_0020 () Int
    0)
  (define-fun _internal_result_0024 () (Pair (Seq Operation) Int)
    (mk-pair (as seq.empty (Seq Operation)) 0))
  (define-fun intial_0004 () (Pair (Pair (Pair Int Int) (Pair Int Int)) Int)
    (mk-pair (mk-pair (mk-pair 0 0) (mk-pair 1 1)) 9))
  (define-fun _internal_car_l5_c9-12_0011 () (Pair (Pair Int Int) (Pair Int Int))
    (mk-pair (mk-pair 0 0) (mk-pair 1 1)))
  (define-fun self_addr () (Pair String String)
    (mk-pair "i" "%default"))
  (define-fun _internal_unpair_l6_c9-15_0013 () (Pair Int Int)
    (mk-pair 1 1))
  (define-fun _internal_unpair_l7_c9-15_0015 () Int
    0)
  (define-fun _internal_unpair_l9_c15-21_0017 () Int
    1)
  (define-fun _internal_add_l8_c9-12_0016 () Int
    0)
  (define-fun _internal_unpair_l7_c9-15_0014 () Int
    0)
  (define-fun source () (Pair String String)
    (mk-pair "u" "%default"))
  (define-fun sender () (Pair String String)
    (mk-pair "n" "%default"))
  (define-fun _internal_param_stor_0010 () (Pair (Pair (Pair Int Int) (Pair Int Int)) Int)
    (mk-pair (mk-pair (mk-pair 0 0) (mk-pair 1 1)) 9))
  (define-fun _internal_unpair_l6_c9-15_0012 () (Pair Int Int)
    (mk-pair 0 0))
  (define-fun amount () Int
    (- 1))
  (define-fun balance () Int
    0)
  (define-fun _internal_unpair_l9_c15-21_0018 () Int
    1)
  (define-fun _internal_nil_l11_c9-22_0021 () (Seq Operation)
    (as seq.empty (Seq Operation)))
  (define-fun contract_ty_opt ((x!0 (Pair String String))) (Option String)
    (ite (= x!0
            (mk-pair "u"
                     (str.++ (seq.unit (_ Char 37))
                             (seq.unit (_ Char 100))
                             (seq.unit (_ Char 101))
                             (seq.unit (_ Char 102))
                             (seq.unit (_ Char 97))
                             (seq.unit (_ Char 117))
                             (seq.unit (_ Char 108))
                             (seq.unit (_ Char 116)))))
      (some "unit")
    (ite (= x!0
            (mk-pair "i"
                     (str.++ (seq.unit (_ Char 37))
                             (seq.unit (_ Char 100))
                             (seq.unit (_ Char 101))
                             (seq.unit (_ Char 102))
                             (seq.unit (_ Char 97))
                             (seq.unit (_ Char 117))
                             (seq.unit (_ Char 108))
                             (seq.unit (_ Char 116)))))
      (some "(pair (pair int int) (pair int int))")
      (some "unit"))))
)
END_MODEL