(set-logic ALL)
(declare-datatypes ((Pair 2))
  ((par (T1 T2) ((mk-pair (first T1) (second T2))))))
(declare-datatypes ((Unit 0)) (((unit))))
(declare-datatypes ((Or 2))
  ((par (T1 T2) ((left (extractLeft T1)) (right (extractRight T2))))))
(declare-datatypes ((Option 1)) ((par (T) ((none) (some (extractSome T))))))
(declare-datatypes ((Contract 0))
  (((contract (extractContract (Pair String String))))))
(define-sort Fun () Int)
(declare-datatypes ((Operation 0))
  (((setDelegate (delegateAddress (Option String)))
     (transferTokens (transferData String) (transferMutez Int)
       (transferContract Contract))
     (createContract (createContractDelegateAddress (Option String))
       (createContractMutez Int) (createContractInitStorage String)
       (createContractAddress (Pair String String))))))
(declare-datatypes ((Exception 0))
  (((overflow) (error (extractError String)))))
(declare-fun amount () Int)
(declare-fun balance () Int)
(declare-fun contract_ty_opt ((Pair String String)) (Option String))
(declare-fun pack!unit (Unit) String)
(declare-fun self_addr () (Pair String String))
(declare-fun sender () (Pair String String))
(declare-fun source () (Pair String String))
(declare-fun unpack_opt!unit (String) (Option Unit))
(assert (or (= amount balance) (> balance amount)))
(assert (= (contract_ty_opt source) ((as some (Option String)) "unit")))
(assert (= (second source) "%default"))
(assert (= (second sender) "%default"))
(assert (= (second self_addr) "%default"))
(push 1)
(declare-const _internal_cdr_l8_c10-13_0011 Unit)
(declare-const _internal_contract_l11_c10-23_0014 (Option Contract))
(declare-const _internal_nil_l9_c10-23_0012 (Seq Operation))
(declare-const _internal_param_stor_0010 (Pair Unit Unit))
(declare-const _internal_result_0028 Exception)
(declare-const _internal_source_l10_c10-16_0013 (Pair String String))
(declare-const _internal_unit_l12_c10-21_0016 Unit)
(declare-const intial_0004 (Pair Unit Unit))
(assert
  (and
    (= (unpack_opt!unit (pack!unit _internal_unit_l12_c10-21_0016))
      ((as some (Option Unit)) _internal_unit_l12_c10-21_0016))
    (ite (is-some _internal_contract_l11_c10-23_0014)
      (=
        (contract_ty_opt
          (extractContract (extractSome _internal_contract_l11_c10-23_0014)))
        ((as some (Option String)) "unit")) true)
    (= _internal_result_0028
      ((as error Exception) (pack!unit _internal_unit_l12_c10-21_0016)))
    (= _internal_unit_l12_c10-21_0016 unit)
    (= _internal_contract_l11_c10-23_0014 (as none (Option Contract)))
    (= _internal_contract_l11_c10-23_0014
      (ite
        (= (contract_ty_opt _internal_source_l10_c10-16_0013)
          ((as some (Option String)) "unit"))
        ((as some (Option Contract))
          ((as contract Contract) _internal_source_l10_c10-16_0013))
        (as none (Option Contract))))
    (= _internal_source_l10_c10-16_0013 source)
    (= _internal_nil_l9_c10-23_0012 (as seq.empty (Seq Operation)))
    (= _internal_cdr_l8_c10-13_0011 (second _internal_param_stor_0010))
    (= _internal_param_stor_0010 intial_0004)
    (=
      (contract_ty_opt
        ((as mk-pair (Pair String String)) (first self_addr) "%default"))
      ((as some (Option String)) "unit")) (not false)))
(echo
  "Finding counter-example for ab-postcondition of ContractAnnot at line 0, characters -1--1")
(check-sat)
(echo "START_MODEL")
(get-model)
(echo "END_MODEL")
(pop 1)
(push 1)
(declare-const _fresh_binding_0020 Contract)
(declare-const _fresh_binding_0021 (Seq Operation))
(declare-const _fresh_binding_0022 Unit)
(declare-const _internal_amount_l13_c10-16_0023 Int)
(declare-const _internal_cons_l16_c10-14_0026 (Seq Operation))
(declare-const _internal_contract_l11_c10-23_0014 (Option Contract))
(declare-const _internal_param_stor_0010 (Pair Unit Unit))
(declare-const _internal_result_0029 (Pair (Seq Operation) Unit))
(declare-const _internal_source_l10_c10-16_0013 (Pair String String))
(declare-const _internal_transfer_tokens_l15_c10-25_0025 Operation)
(declare-const _internal_unit_l14_c10-14_0024 Unit)
(declare-const intial_0004 (Pair Unit Unit))
(assert
  (and (or (= 0 amount) (> amount 0))
    (or (= amount 9223372036854775807) (> 9223372036854775807 amount))
    (= (unpack_opt!unit (pack!unit _internal_unit_l14_c10-14_0024))
      ((as some (Option Unit)) _internal_unit_l14_c10-14_0024))
    (or (= 0 _internal_amount_l13_c10-16_0023)
      (> _internal_amount_l13_c10-16_0023 0))
    (or (= _internal_amount_l13_c10-16_0023 9223372036854775807)
      (> 9223372036854775807 _internal_amount_l13_c10-16_0023))
    (= (contract_ty_opt (extractContract _fresh_binding_0020))
      ((as some (Option String)) "unit"))
    (ite (is-some _internal_contract_l11_c10-23_0014)
      (=
        (contract_ty_opt
          (extractContract (extractSome _internal_contract_l11_c10-23_0014)))
        ((as some (Option String)) "unit")) true)
    (= _internal_result_0029
      ((as mk-pair (Pair (Seq Operation) Unit))
        _internal_cons_l16_c10-14_0026 _fresh_binding_0022))
    (= _internal_cons_l16_c10-14_0026
      (seq.++ (seq.unit _internal_transfer_tokens_l15_c10-25_0025)
        _fresh_binding_0021))
    (=
      ((as transferTokens Operation)
        (pack!unit _internal_unit_l14_c10-14_0024)
        _internal_amount_l13_c10-16_0023 _fresh_binding_0020)
      _internal_transfer_tokens_l15_c10-25_0025)
    (= _internal_unit_l14_c10-14_0024 unit)
    (= _internal_amount_l13_c10-16_0023 amount)
    (= _internal_contract_l11_c10-23_0014
      ((as some (Option Contract)) _fresh_binding_0020))
    (= _internal_contract_l11_c10-23_0014
      (ite
        (= (contract_ty_opt _internal_source_l10_c10-16_0013)
          ((as some (Option String)) "unit"))
        ((as some (Option Contract))
          ((as contract Contract) _internal_source_l10_c10-16_0013))
        (as none (Option Contract))))
    (= _internal_source_l10_c10-16_0013 source)
    (= _fresh_binding_0021 (as seq.empty (Seq Operation)))
    (= _fresh_binding_0022 (second _internal_param_stor_0010))
    (= _internal_param_stor_0010 intial_0004)
    (=
      (contract_ty_opt
        ((as mk-pair (Pair String String)) (first self_addr) "%default"))
      ((as some (Option String)) "unit"))
    (not
      (ite
        (and (< 0 (seq.len (first _internal_result_0029)))
          (is-transferTokens (seq.nth (first _internal_result_0029) 0))
          (is-some
            (unpack_opt!unit
              (transferData (seq.nth (first _internal_result_0029) 0))))
          (= 0
            (seq.len
              (seq.extract (first _internal_result_0029) 1
                (- (seq.len (first _internal_result_0029)) 1)))))
        (and
          (=
            (extractContract
              (transferContract (seq.nth (first _internal_result_0029) 0)))
            sender)
          (= (transferMutez (seq.nth (first _internal_result_0029) 0))
            amount)) false))))
(echo
  "Finding counter-example for postcondition of ContractAnnot at line 0, characters -1--1")
(check-sat)
(echo "START_MODEL")
(get-model)
(echo "END_MODEL")
(pop 1)


