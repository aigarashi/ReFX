Finding counter-example for ab-postcondition of ContractAnnot at line 0, characters -1--1
unsat
START_MODEL
(error "line 74 column 10: model is not available")
END_MODEL
Finding counter-example for postcondition of ContractAnnot at line 0, characters -1--1
sat
START_MODEL
(
  (define-fun _internal_transfer_tokens_l15_c10-25_0025 () Operation
    (transferTokens "t" 39 (contract (mk-pair "i" "%default"))))
  (define-fun _internal_unit_l14_c10-14_0024 () Unit
    unit)
  (define-fun intial_0004 () (Pair Unit Unit)
    (mk-pair unit unit))
  (define-fun self_addr () (Pair String String)
    (mk-pair "n" "%default"))
  (define-fun _internal_contract_l11_c10-23_0014 () (Option Contract)
    (some (contract (mk-pair "i" "%default"))))
  (define-fun _fresh_binding_0020 () Contract
    (contract (mk-pair "i" "%default")))
  (define-fun _fresh_binding_0022 () Unit
    unit)
  (define-fun _internal_cons_l16_c10-14_0026 () (Seq Operation)
    (seq.unit (transferTokens "t" 39 (contract (mk-pair "i" "%default")))))
  (define-fun _internal_result_0029 () (Pair (Seq Operation) Unit)
    (let ((a!1 (seq.unit (transferTokens "t" 39 (contract (mk-pair "i" "%default"))))))
  (mk-pair a!1 unit)))
  (define-fun _internal_source_l10_c10-16_0013 () (Pair String String)
    (mk-pair "i" "%default"))
  (define-fun source () (Pair String String)
    (mk-pair "i" "%default"))
  (define-fun _internal_amount_l13_c10-16_0023 () Int
    39)
  (define-fun sender () (Pair String String)
    (mk-pair "u" "%default"))
  (define-fun amount () Int
    39)
  (define-fun balance () Int
    40)
  (define-fun _internal_param_stor_0010 () (Pair Unit Unit)
    (mk-pair unit unit))
  (define-fun _fresh_binding_0021 () (Seq Operation)
    (as seq.empty (Seq Operation)))
  (define-fun unpack_opt!unit ((x!0 String)) (Option Unit)
    (some unit))
  (define-fun seq.nth_u ((x!0 (Seq Operation)) (x!1 Int)) Operation
    (setDelegate (as none (Option String))))
  (define-fun contract_ty_opt ((x!0 (Pair String String))) (Option String)
    (ite (= x!0
            (mk-pair "i"
                     (str.++ (seq.unit (_ Char 37))
                             (seq.unit (_ Char 100))
                             (seq.unit (_ Char 101))
                             (seq.unit (_ Char 102))
                             (seq.unit (_ Char 97))
                             (seq.unit (_ Char 117))
                             (seq.unit (_ Char 108))
                             (seq.unit (_ Char 116)))))
      (some "unit")
    (ite (= x!0
            (mk-pair "n"
                     (str.++ (seq.unit (_ Char 37))
                             (seq.unit (_ Char 100))
                             (seq.unit (_ Char 101))
                             (seq.unit (_ Char 102))
                             (seq.unit (_ Char 97))
                             (seq.unit (_ Char 117))
                             (seq.unit (_ Char 108))
                             (seq.unit (_ Char 116)))))
      (some "unit")
      (some "unit"))))
  (define-fun pack!unit ((x!0 Unit)) String
    "t")
)
END_MODEL