import React from 'react';
import clsx from 'clsx';
import styles from './HomepageFeatures.module.css';

const FeatureList = [
  {
    title: 'Verification for Michelson',
    Svg: require('../../static/img/Helmholtz.svg').default,
    description: (
      <>
            Helmholtz is designed to be a static verifier for Michelson, the smart contract language running on the Tezos blockchain platform.  (It is named after Hermann von Helmholtz, who was a thesis advisor of Albert Michelson.)
      </>
    ),
  },
  {
    title: 'Fully Automated',
    Svg: require('../../static/img/aha.svg').default,
    description: (
      <>
            All you have to do is to annotate your contract with its functional specification (such as pre- and post-conditions, loop invariants); Helmholtz will automatically generate verification conditions and discharge them by using an SMT solver such as Z3.
      </>
    ),
  },
  {
    title: 'Powered by Refinement Types',
    Svg: require('../../static/img/refinement_type.svg').default,
    description: (
      <>
            Helmholtz is based on a theory of refinement types, which can describe relationship between elements on the operand stack.
      </>
    ),
  },
];

function Feature({Svg, title, description}) {
  return (
    <div className={clsx('col col--4')}>
      <div className="text--center">
        <Svg className={styles.featureSvg} alt={title} />
      </div>
      <div className="text--center padding-horiz--md">
        <h3>{title}</h3>
        <p>{description}</p>
      </div>
    </div>
  );
}

export default function HomepageFeatures() {
  return (
    <section className={styles.features}>
      <div className="container">
        <div className="row">
          {FeatureList.map((props, idx) => (
            <Feature key={idx} {...props} />
          ))}
        </div>
      </div>
    </section>
  );
}
