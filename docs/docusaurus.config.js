const lightCodeTheme = require('prism-react-renderer/themes/github');
const darkCodeTheme = require('prism-react-renderer/themes/dracula');
const math = require('remark-math');
const katex = require('rehype-katex');

/** @type {import('@docusaurus/types').DocusaurusConfig} */
module.exports = {
  title: 'Helmholtz',
  tagline: 'Verifier for Tezos Smart Contracts',
  url: 'https://www.fos.kuis.kyoto-u.ac.jp/',
  baseUrl: '/projects/Helmholtz/',
  onBrokenLinks: 'throw',
  onBrokenMarkdownLinks: 'warn',
  favicon: 'img/favicon.ico',
  organizationName: 'aigarashi', // Usually your GitHub org/user name.
  projectName: 'ReFX', // Usually your repo name.
  themeConfig: {
    navbar: {
      title: 'Top',
      logo: {
        alt: 'Top',
        src: 'img/Helmholtz_face.jpeg',
      },
      items: [
        {
          type: 'doc',
          docId: 'intro',
          position: 'left',
          label: 'Reference Manual',
        },
          //{to: '/blog', label: 'Blog', position: 'left'},
        {
          to: '/papers', label: 'Papers', position: 'left'
        },
        {
          to: 'https://www.fos.kuis.kyoto-u.ac.jp/trylang/Helmholtz', label: 'Try Online', position: 'left'
        },
        {
          href: 'https://gitlab.com/aigarashi/ReFX',
          label: 'GitLab',
          position: 'right',
        },
      ],
    },
    footer: {
      style: 'dark',
      links: [
        {
          title: 'Docs',
          items: [
            {
              label: 'Reference Manual',
              to: '/docs/intro',
            },
            {
              to: '/papers',
              label: 'Papers',
            },
          ],
        },
//        {
//          title: 'Community',
//          items: [
//            {
//              label: 'Stack Overflow',
//              href: 'https://stackoverflow.com/questions/tagged/docusaurus',
//            },
//            {
//              label: 'Discord',
//              href: 'https://discordapp.com/invite/docusaurus',
//            },
//            {
//              label: 'Twitter',
//              href: 'https://twitter.com/docusaurus',
//            },
//          ],
//        },
        {
          title: 'More',
          items: [
//            {
//              label: 'Blog',
//              to: '/blog',
//            },
            {
              label: 'GitLab',
              href: 'https://gitlab.com/aigarashi/ReFX',
            },
          ],
        },
      ],
      copyright: `Copyright (c) ${new Date().getFullYear()} ReFX Project. Built with Docusaurus.`,
    },
    prism: {
      theme: lightCodeTheme,
      darkTheme: darkCodeTheme,
    },
  },
  presets: [
    [
      '@docusaurus/preset-classic',
      {
        docs: {
          sidebarPath: require.resolve('./sidebars.js'),
          remarkPlugins: [math],
          rehypePlugins: [katex],
          // Please change this to your repo.
          editUrl:
            'https://github.com/facebook/docusaurus/edit/master/website/',
        },
        blog: {
          showReadingTime: true,
          // Please change this to your repo.
          editUrl:
            'https://github.com/facebook/docusaurus/edit/master/website/blog/',
        },
        theme: {
          customCss: require.resolve('./src/css/custom.css'),
        },
      },
    ],
  ],
  stylesheets: [
    {
      href: 'https://cdn.jsdelivr.net/npm/katex@0.13.11/dist/katex.min.css',
      integrity:
        'sha384-Um5gpz1odJg5Z4HAmzPtgZKdTBHZdw8S29IecapCSB31ligYPhHQZMIlWLYQGVoc',
      crossorigin: 'anonymous',
    },
  ],
};
