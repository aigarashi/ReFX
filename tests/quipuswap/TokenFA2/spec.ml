let last_token_id = 10

let approved_operator account_info tx =
  let (_, allowances) = get_account tx.from_ account_info in
  from = sender || mem sender allowances

let rec approved_operator_all account_info txs =
  match txs with
  | [] -> True
  | h :: t -> approved_operator account_info h && approved_operator_all account_info t

let rec defined_token tx =
  match tx.txs with
  | [] -> True
  | h :: t -> h.token_id < last_token_id && defined_token t

let rec defined_token_all txs =
  match txs with
  | [] -> True
  | h :: t -> defined_token h && defined_token_all t

let valid_transaction account_info param =
  defined_token_all param && 

invariant store.last_token_id = last_token_id

Spec1:
requires param = [];
ensures store = old(store);

Spec2:
requires param = h :: t;
requires approved_operator_all store.account_info param;
requires valid_transaction store.account_info param
ensures store = { old(store) with account_info = iterate_transfer old(store.account_info) param };

Spec3:...
