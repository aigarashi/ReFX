# Usage

Install pytest.  This is only required at first time.

```
% poetry install
```

Run tests.

```
% poetry run pytest
```
