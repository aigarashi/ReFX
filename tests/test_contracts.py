import glob
import re
import subprocess
import sys
import shutil
import os
import pytest

helmholtz = "../_build/install/default/bin/helmholtz"
matcher = re.compile("^(VERIFIED)|(UNVERIFIED)|(TIMEOUT)|(UNKNOWN)", re.MULTILINE)

@pytest.mark.skipif(not os.path.exists(helmholtz), reason="helmholtz is not built yet")
@pytest.mark.parametrize("solver",
    [ pytest.param(s, marks=pytest.mark.skipif(shutil.which(s) is None, reason=s+" is not installed")) for s in ["z3", "cvc5"] ])
@pytest.mark.parametrize("expect,filename",
    [ (e, f) for e in ["pos", "neg"] for f in sorted(glob.glob("./"+e+"/*.tz")) ]
)
def test_pos_z3(solver, expect, filename):
    res = subprocess.run([helmholtz, "--solver", solver, filename],
                         encoding="utf-8",
                         stdout=subprocess.PIPE,
                         stderr=subprocess.DEVNULL)
    m = matcher.search(res.stdout)
    if m:
        assert m.group(0) == "VERIFIED" if expect == "pos" else "UNVERIFIED", m.group(0)
    else:
        assert False, "internal error"
