import glob
import re
import subprocess
import sys
import pytest

matcher = re.compile("^(VERIFIED)|(UNVERIFIED)|(TIMEOUT)|(UNKNOWN)", re.MULTILINE)

@pytest.mark.parametrize(
    "filename",
    sorted(glob.glob("./*.tz"))
)
def test_pos(filename):
    res = subprocess.run(["tezos-client", "--mode", "mockup", "refinement", filename],
                         encoding="utf-8",
                         stdout=subprocess.PIPE,
                         stderr=subprocess.DEVNULL)
    m = matcher.search(res.stdout)
    if m:
        assert m.group(0) == "VERIFIED", res.stdout
    else:
        assert False, res.stdout
