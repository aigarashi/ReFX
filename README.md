# ReFX

Verifier for Tezos Michelson

## Pre-requirement

- opam

## Installation

Clone repository:
```
$ git clone https://gitlab.com/aigarashi/ReFX.git
$ cd ReFX
```

Create a local switch:
```
$ opam switch create . 4.12.1 --no-install
```
Note depending libraries do not support newer OCaml.

Install dependencies:
```
$ opam install . --deps-only
```
Note some packages will depend on extra system package.  Follow opam message in that case.

Build binaries:
```
$ dune build
```

Try examples:
```
$ dune exec helmholtz tests/pos/boomerang.tz
```

## Usage

The following command verifies `<FILENAME>` Michelson code annotated with specification.

```
$ dune exec helmholtz <FILENAME>
```

You can find example code in `./tests/{pos,neg}`.

Syntax of specification is found in [manual](https://www.fos.kuis.kyoto-u.ac.jp/projects/Helmholtz/docs/intro).
