# 実装メモ書き
## 登場する言語
### Michelson言語
- node_info.ml
- decoration_def.ml
### ロジック用Sort言語
論理式の型，アサーション言語とロジック言語で共用
- sort.ml
### アサーション言語
コード中に書くアノテーション言語
- syntax.ml
言語の定義
- syntax_error.ml
パースエラーの定義
- decoration_parser.mly
パーサ
- decoration_lexer.mll
レキサー
- ty_env.ml
組み込み関数の型とかの型環境の定義
- typing.ml
型推論の実装
- transpile.ml
ロジック言語への変換の実装
- transpile_helper.ml
パターンマッチの網羅性チェックの実装
### ロジック言語
検証中に論理式を表現するための言語
- logic.ml
言語の定義とか型検査の実装とかもろもろ
- simplify.ml
SMT solver に投げる前の前処理の実装，式の簡略化や仮定の追加など
### SMT-Lib 言語
SMT solver に投げる S 式
- z3_gen.ml
定義とロジック言語から SMT-Lib format への変換の実装
## データ構造
- constraint.ml
検証時に集める制約情報の定義
- measure.ml
組み込み Measure の定義
## データ処理
- ../lib_client_command/client_proto_programs_commands.ml
    - Refx.print_typecheck_result の呼び出し
- decoration.ml
    - 入力ファイルをアノテーション部分と Michelson コード部分に分離
    - アノテーション部分をロジック言語に変換して Michelson AST に再結合
- refx.ml
    - typing_program
    Michelson AST を巡回して検証条件の生成
    - print_typecheck_result
    検証全体の処理を記述
- z3.ml
SMT solver の呼び出し実装
- printer.ml
Michelson AST のプリンタ

## 小野実装分
### モデル極小化
- ce.ml
    - minimize_ce：検証失敗時にZ3から得られるモデルを極小化する関数
- results.ml
    - ce.mlで用いる諸関数を記述
### 仕様スライシング
- Refx.search_unverified_context
    - 検証失敗時にPostconditionの条件式をスライシング
- zipper.ml
    - 条件式に対応するZipper構造