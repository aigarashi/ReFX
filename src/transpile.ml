open Syntax
include Transpile_helper

type e_subst = (variable * Logic.exp) list

let rec transpile_rtype (subst : (variable * Logic.exp) list)
    (rtype : typed_rtype) :
    (Logic.variable list * Logic.exp * e_subst * Logic.exp list) tzresult Lwt.t
    =
  let stack, exp = rtype.body in
  transpile_stack stack >>=? fun (stack_binds, cond, subst_s, nobinds) ->
  (* subst_s comes first for shadowing *)
  transpile_exp (subst_s @ subst) exp >>=? fun exp ->
  (* If stack pattern does not match stack, refinement type becomes empty type. *)
  return (stack_binds, Logic.Lang.(cond && exp), subst_s, nobinds)

and transpile_stack (stack : typed_stack) :
    (Logic.variable list * Logic.exp * e_subst * Logic.exp list) tzresult Lwt.t
    =
  match stack.body with
  | T_stack_nil -> return ([], Logic.Lang.c_bool true, [], [])
  | T_stack_cons (p, tl) ->
      let x = Logic.fresh_var p.ty in
      transpile_pattern Logic.Lang.(?/x) p
      >>=? fun (cond_p, subst_p, nobinds_p) ->
      transpile_stack tl
      >>=? fun (stack_binds, cond_tl, subst_tl, nobinds_tl) ->
      return
        ( x :: stack_binds,
          Logic.Lang.(cond_p && cond_tl),
          subst_p @ subst_tl,
          nobinds_p @ nobinds_tl )

(** Transpile a pattern.  [transpile_pattern match_exp pattern] returns a tuple
   [(cond, subst, nobindss)], where [cond] is a condition that expresses whether
   [match_exp] matches [pattern], and [subst] expresses a binding for the
   variables in [pattern] to matched expressions when [cond] is true.  We
   replace bound variables with the matched expressions, for instance, in
   matching clause bodies because of technical reason.  Note that [subst] is
   well-specified when [cond] is true.  [nobindss] is a set of matched
   expressions which are not bounded by any variables because matched by
   any-pattern.  *)
and transpile_pattern (match_exp : Logic.exp) (pattern : typed_pattern) :
    (Logic.exp * e_subst * Logic.exp list) tzresult Lwt.t =
  match pattern.body with
  | T_pattern_constr (c, pl) -> (
      match (c, pl) with
      | "True", [] -> return (Logic.Lang.(match_exp = c_bool true), [], [])
      | "False", [] -> return (Logic.Lang.(match_exp = c_bool false), [], [])
      | "Unit", [] -> return (Logic.Lang.c_bool true, [], [])
      | "Nil", [] -> return (Logic.Lang.(is_nil match_exp), [], [])
      | "Cons", [ p1; p2 ] ->
          transpile_pattern Logic.Lang.(extract_hd match_exp) p1
          >>=? fun (cond1, subst1, nobinds1) ->
          transpile_pattern Logic.Lang.(extract_tl match_exp) p2
          >>=? fun (cond2, subst2, nobinds2) ->
          return
            ( Logic.Lang.(is_cons match_exp && cond1 && cond2),
              subst1 @ subst2,
              nobinds1 @ nobinds2 )
      | "Pair", [ p1; p2 ] ->
          transpile_pattern Logic.Lang.(fst match_exp) p1
          >>=? fun (cond1, subst1, nobinds1) ->
          transpile_pattern Logic.Lang.(snd match_exp) p2
          >>=? fun (cond2, subst2, nobinds2) ->
          return
            (Logic.Lang.(cond1 && cond2), subst1 @ subst2, nobinds1 @ nobinds2)
      | "None", [] -> return (Logic.Lang.(is_none match_exp), [], [])
      | "Some", [ p ] ->
          transpile_pattern Logic.Lang.(extract_some match_exp) p
          >>=? fun (cond, subst, nobinds) ->
          return (Logic.Lang.(is_some match_exp && cond), subst, nobinds)
      | "Left", [ p ] ->
          transpile_pattern Logic.Lang.(extract_left match_exp) p
          >>=? fun (cond, subst, nobinds) ->
          return (Logic.Lang.(is_left match_exp && cond), subst, nobinds)
      | "Right", [ p ] ->
          transpile_pattern Logic.Lang.(extract_right match_exp) p
          >>=? fun (cond, subst, nobinds) ->
          return (Logic.Lang.(is_right match_exp && cond), subst, nobinds)
      | "Contract", [ p ] ->
          transpile_pattern Logic.Lang.(extract_contract match_exp) p
          >>=? fun (cond, subst, nobinds) -> return (cond, subst, nobinds)
      | "SetDelegate", [ p ] ->
          transpile_pattern Logic.Lang.(extract_set_delegate match_exp) p
          >>=? fun (cond, subst, nobinds) ->
          return (Logic.Lang.(is_set_delegate match_exp && cond), subst, nobinds)
      | ("Transfer" | "TransferTokens"), [ p1; p2; p3 ] ->
          let payload_ty = p1.ty in
          let e1 =
            Logic.Lang.(
              extract_some
                (unpack_opt payload_ty (extract_transfer_tokens_data match_exp)))
          in
          let e2 = Logic.Lang.(extract_transfer_tokens_mutez match_exp) in
          let e3 =
            Logic.Lang.(extract_transfer_tokens_contract payload_ty match_exp)
          in
          transpile_pattern e1 p1 >>=? fun (cond1, subst1, nobinds1) ->
          transpile_pattern e2 p2 >>=? fun (cond2, subst2, nobinds2) ->
          transpile_pattern e3 p3 >>=? fun (cond3, subst3, nobinds3) ->
          return
            ( Logic.Lang.(
                is_transfer match_exp
                && is_some
                     (unpack_opt payload_ty
                        (extract_transfer_tokens_data match_exp))
                && cond1 && cond2 && cond3),
              subst1 @ subst2 @ subst3,
              nobinds1 @ nobinds2 @ nobinds3 )
      | "CreateContract", [ p1; p2; p3; p4 ] ->
          let payload_ty = p3.ty in
          let e1 = Logic.Lang.(extract_create_contract_keyhash match_exp) in
          let e2 = Logic.Lang.(extract_create_contract_mutez match_exp) in
          let e3 =
            Logic.Lang.(
              extract_some
                (unpack_opt payload_ty (extract_create_contract_data match_exp)))
          in
          let e4 = Logic.Lang.(extract_create_contract_address match_exp) in
          transpile_pattern e1 p1 >>=? fun (cond1, subst1, nobinds1) ->
          transpile_pattern e2 p2 >>=? fun (cond2, subst2, nobinds2) ->
          transpile_pattern e3 p3 >>=? fun (cond3, subst3, nobinds3) ->
          transpile_pattern e4 p4 >>=? fun (cond4, subst4, nobinds4) ->
          return
            ( Logic.Lang.(
                is_create_contract match_exp
                && is_some
                     (unpack_opt payload_ty
                        (extract_create_contract_data match_exp))
                && cond1 && cond2 && cond3 && cond4),
              subst1 @ subst2 @ subst3 @ subst4,
              nobinds1 @ nobinds2 @ nobinds3 @ nobinds4 )
      | "Error", [ p ] ->
          let payload_ty = p.ty in
          let e =
            Logic.Lang.(
              extract_some (unpack_opt payload_ty (extract_error match_exp)))
          in
          transpile_pattern e p >>=? fun (cond, subst, nobinds) ->
          return
            ( Logic.Lang.(
                is_error match_exp
                && is_some (unpack_opt payload_ty (extract_error match_exp))
                && cond),
              subst,
              nobinds )
      | "Overflow", [] -> return (Logic.Lang.(is_overflow match_exp), [], [])
      | ("Add" | "Bind"), _ ->
          failwith "Add and Bind pattern can only occur in measure annotations"
      | _ -> failwith "Unexpected error: %s" __LOC__)
  | T_pattern_var x -> return (Logic.Lang.c_bool true, [ (x, match_exp) ], [])
  | T_pattern_any -> return (Logic.Lang.c_bool true, [], [ match_exp ])

and transpile_exp (subst : e_subst) (exp : typed_exp) : Logic.exp tzresult Lwt.t
    =
  let ety = exp.ty in
  match exp.body with
  | T_exp_match (me, pc) ->
      transpile_exp subst me >>=? fun me ->
      let rec aux = function
        | [ (p, e) ] ->
            transpile_pattern me p >>=? fun (_, subst_p, _) ->
            (* This is the last clause.  So, match condition must be
               true by the exhaustiveness. *)
            transpile_exp (subst_p @ subst) e
        | (p, e) :: tl ->
            transpile_pattern me p >>=? fun (cond, subst_p, _) ->
            transpile_exp (subst_p @ subst) e >>=? fun e ->
            aux tl >>=? fun tl -> return Logic.Lang.(ite cond e tl)
        | _ ->
            (* There is no sort which has no constructors.  So, there
               is at least one clause by the exhaustiveness. *)
            failwith "Unexpected error: %s" __LOC__
      in
      if exhaustive_pattern_check (List.map fst pc) then aux pc
      else
        failwith "This pattern match is not exhaustive: %a" pp_location exp.loc
  | T_exp_capp (c, el) -> (
      List.map_es (transpile_exp subst) el >>=? fun el' ->
      match (c, el', List.map (fun e -> e.ty) el, ety) with
      | "True", [], _, _ -> return Logic.Lang.(c_bool true)
      | "False", [], _, _ -> return Logic.Lang.(c_bool false)
      | "Unit", [], _, _ -> return Logic.Lang.c_unit
      | "Nil", [], _, Sort.S_list ty -> return Logic.Lang.(nil ty)
      | "Cons", [ e1; e2 ], _, _ -> return Logic.Lang.(cons e1 e2)
      | "Pair", [ e1; e2 ], _, _ -> return Logic.Lang.(pair e1 e2)
      | "None", [], _, Sort.S_option ty -> return Logic.Lang.(none ty)
      | "Some", [ e ], _, _ -> return Logic.Lang.(some e)
      | "Left", [ e ], _, Sort.S_or (_, rty) -> return Logic.Lang.(left rty e)
      | "Right", [ e ], _, Sort.S_or (lty, _) -> return Logic.Lang.(right lty e)
      | "SetDelegate", [ e ], _, _ -> return Logic.Lang.(set_delegate e)
      | ("Transfer" | "TransferTokens"), [ e1; e2; e3 ], [ ty1; _; _ ], _ ->
          return Logic.Lang.(transfer_tokens ty1 e1 e2 e3)
      | "CreateContract", [ e1; e2; e3; e4 ], [ _; _; ty3; _ ], _ ->
          return Logic.Lang.(create_contract ty3 e1 e2 e3 e4)
      | "Error", [ e ], [ ty ], _ -> return Logic.Lang.(error ty e)
      | "Overflow", [], [], _ -> return Logic.Lang.(overflow)
      | _ -> failwith "Unexpected error: %s" __LOC__)
  | T_exp_fapp (f, annot, el) -> (
      List.map_es (transpile_exp subst) el >>=? fun el' ->
      match (f, el', List.map (fun e -> e.ty) el, ety) with
      | "-", [ e ], _, Sort.S_int -> return Logic.Lang.(c_int 0 - e)
      | "-", [ e ], _, Sort.S_timestamp -> (
          match e.desc with
          | E_const (K_timestamp n) -> return Logic.Lang.(c_timestamp (-n))
          | _ ->
              failwith "Negation of timestamp is only valid for constants on %a"
                pp_location exp.loc)
      | "!", [ e ], _, _ -> return Logic.Lang.(neg e)
      | "+", [ e1; e2 ], _, _ -> return Logic.Lang.(e1 + e2)
      | "-", [ e1; e2 ], _, _ -> return Logic.Lang.(e1 - e2)
      | "*", [ e1; e2 ], _, _ -> return Logic.Lang.(e1 * e2)
      | "/", [ e1; e2 ], _, _ -> return Logic.Lang.(e1 / e2)
      | "mod", [ e1; e2 ], _, _ -> return Logic.Lang.(e1 % e2)
      | "::", [ e1; e2 ], _, _ -> return Logic.Lang.(cons e1 e2)
      | "@", [ e1; e2 ], _, _ -> return Logic.Lang.(append e1 e2)
      | "^", [ e1; e2 ], _, _ -> return Logic.Lang.(concat_str e1 e2)
      | "=", [ e1; e2 ], _, _ -> return Logic.Lang.(e1 = e2)
      | "<>", [ e1; e2 ], _, _ -> return Logic.Lang.(e1 <> e2)
      | "<", [ e1; e2 ], _, _ -> return Logic.Lang.(e1 < e2)
      | "<=", [ e1; e2 ], _, _ -> return Logic.Lang.(e1 <= e2)
      | ">", [ e1; e2 ], _, _ -> return Logic.Lang.(e1 > e2)
      | ">=", [ e1; e2 ], _, _ -> return Logic.Lang.(e1 >= e2)
      | "&&", [ e1; e2 ], _, _ -> return Logic.Lang.(e1 && e2)
      | "||", [ e1; e2 ], _, _ -> return Logic.Lang.(e1 || e2)
      | "not", [ e ], _, _ -> return Logic.Lang.(neg e)
      | "get_str_opt", [ e1; e2 ], _, _ -> return Logic.Lang.(at_str_opt e1 e2)
      | "sub_str_opt", [ e1; e2; e3 ], _, _ ->
          return Logic.Lang.(substr_str_opt e1 e2 e3)
      | "len_str", [ e ], _, _ -> return Logic.Lang.(len_str e)
      | "concat_str", [ e1; e2 ], _, _ -> return Logic.Lang.(concat_str e1 e2)
      | "get_bytes_opt", [ e1; e2 ], _, _ ->
          return Logic.Lang.(at_bytes_opt e1 e2)
      | "sub_bytes_opt", [ e1; e2; e3 ], _, _ ->
          return Logic.Lang.(substr_bytes_opt e1 e2 e3)
      | "len_bytes", [ e ], _, _ -> return Logic.Lang.(len_bytes e)
      | "size_list", [ e ], _, _ -> return Logic.Lang.(size_list e)
      | "concat_bytes", [ e1; e2 ], _, _ ->
          return Logic.Lang.(concat_bytes e1 e2)
      | "first", [ e ], _, _ -> return Logic.Lang.(fst e)
      | "second", [ e ], _, _ -> return Logic.Lang.(snd e)
      | "find_opt", [ e1; e2 ], _, _ -> return Logic.Lang.(find_opt_map e1 e2)
      | "update", [ e1; e2; e3 ], _, _ ->
          return Logic.Lang.(update_map e1 e2 e3)
      | "binding_opt", [ e ], _, _ -> return Logic.Lang.(choose_opt_map e)
      | "empty_map", [], _, Sort.S_map (ty1, ty2) ->
          return Logic.Lang.(empty_map ty1 ty2)
      | "find_opt_b", [ e1; e2 ], _, _ ->
          return Logic.Lang.(find_opt_big_map e1 e2)
      | "update_b", [ e1; e2; e3 ], _, _ ->
          return Logic.Lang.(update_big_map e1 e2 e3)
      | "empty_big_map", [], _, Sort.S_big_map (ty1, ty2) ->
          return Logic.Lang.(empty_big_map ty1 ty2)
      | "mem", [ e1; e2 ], _, _ -> return Logic.Lang.(mem_set e1 e2)
      | "add", [ e1; e2 ], _, _ -> return Logic.Lang.(add_set e1 e2)
      | "remove", [ e1; e2 ], _, _ -> return Logic.Lang.(remove_set e1 e2)
      | "choose_opt", [ e ], _, _ -> return Logic.Lang.(choose_opt_set e)
      | "empty_set", [], _, Sort.S_set ty -> return Logic.Lang.(empty_set ty)
      | "max_mutez", [], _, _ -> return Logic.Lang.c_mutez_max
      | "source", [], _, _ -> return Logic.Lang.source
      | "sender", [], _, _ -> return Logic.Lang.sender
      | "now", [], _, _ -> return Logic.Lang.now
      | "self", [], _, Sort.(S_contract ty) ->
          return Logic.Lang.(self ?entrypoint:annot ty)
      | "self_addr", [], _, _ -> return Logic.Lang.self_addr
      | "balance", [], _, _ -> return Logic.Lang.balance
      | "amount", [], _, _ -> return Logic.Lang.amount
      | "chain_id", [], _, _ -> return Logic.Lang.chain_id
      | "level", [], _, _ -> return Logic.Lang.level
      | "voting_power", [ e ], _, _ -> return Logic.Lang.(voting_power e)
      | "total_voting_power", [], _, _ -> return Logic.Lang.total_voting_power
      | "pack", [ e ], [ ty ], _ -> return Logic.Lang.(pack ty e)
      | "unpack_opt", [ e ], _, Sort.S_option ty ->
          return Logic.Lang.(unpack_opt ty e)
      | "contract_opt", [ e ], _, Sort.(S_option (S_contract ty)) ->
          return Logic.Lang.(contract_opt ?entrypoint:annot ty e)
      | "implicit_account", [ e ], _, _ ->
          return Logic.Lang.(implicit_account e)
      | "call", [ e1; e2; e3 ], _, _ -> return Logic.Lang.(call e1 e2 e3)
      | "hash", [ e ], _, _ -> return Logic.Lang.(b58check e)
      | "blake2b", [ e ], _, _ -> return Logic.Lang.(blake2b e)
      | "keccak", [ e ], _, _ -> return Logic.Lang.(keccak e)
      | "sha256", [ e ], _, _ -> return Logic.Lang.(sha256 e)
      | "sha512", [ e ], _, _ -> return Logic.Lang.(sha512 e)
      | "sha3", [ e ], _, _ -> return Logic.Lang.(sha3 e)
      | "sig", [ e1; e2; e3 ], _, _ -> return Logic.Lang.(signature e1 e3 = e2)
      | "abs", [ e ], _, _ -> return Logic.Lang.(abs e)
      | _ -> failwith "Unexpected error: %s" __LOC__)
  | T_exp_mapp ((ufid, t_vars, t_params, t_ret), sl, el) ->
      List.map_es (transpile_exp subst) el >>=? fun el ->
      let f = Logic.{ ufid; t_vars; t_params; t_ret } in
      return Logic.Lang.(mapp f sl el)
  | T_exp_var v -> (
      match List.assoc ~equal:( = ) v subst with
      | Some e -> return e
      | None -> failwith "Unexpected error %s: %s" v __LOC__)
  | T_exp_number n -> (
      match ety with
      | Sort.S_int -> return Logic.Lang.(c_int n)
      | Sort.S_nat -> return Logic.Lang.(c_nat n)
      | Sort.S_timestamp -> return Logic.Lang.(c_timestamp n)
      | Sort.S_mutez -> return Logic.Lang.(c_mutez n)
      | _ -> failwith "Unexpected error: %s" __LOC__)
  | T_exp_string s -> (
      match ety with
      | Sort.S_string -> return Logic.Lang.(c_string s)
      | Sort.S_address -> (
          let addr, entry =
            match String.index_opt s '%' with
            | None -> (s, "%default")
            | Some pos ->
                let len = String.length s - pos in
                let addr = String.sub s 0 pos in
                let entry =
                  match String.sub s pos len with "%" -> "%default" | s -> s
                in
                (addr, entry)
          in
          match Protocol.Alpha_context.Contract.of_b58check addr with
          | Ok _ ->
              return
                Logic.Lang.(create_address (c_string addr) (c_string entry))
          | Error _ -> failwith "Invalid address %s on %a" s pp_location exp.loc
          )
      | Sort.S_key -> (
          match Signature.Public_key.of_b58check_opt s with
          | Some _ -> return Logic.Lang.(c_key s)
          | None -> failwith "Invalid key %s on %a" s pp_location exp.loc)
      | Sort.S_key_hash -> (
          match Signature.Public_key_hash.of_b58check_opt s with
          | Some _ -> return Logic.Lang.(c_key_hash s)
          | None -> failwith "Invalid key_hash %s on %a" s pp_location exp.loc)
      | Sort.S_chain_id -> (
          match Chain_id.of_b58check_opt s with
          | Some _ -> return Logic.Lang.(c_chain_id s)
          | None -> failwith "Invalid chain_id %s on %a" s pp_location exp.loc)
      | Sort.S_signature -> (
          match Signature.of_b58check_opt s with
          | Some s ->
              Syntax.normalize_signature s >>=? fun s ->
              return Logic.Lang.(c_signature s)
          | None -> failwith "Invalid signature %s on %a" s pp_location exp.loc)
      | Sort.S_timestamp -> (
          match Protocol.Script_timestamp.of_string s with
          | Some v ->
              Protocol.Script_timestamp.to_zint v |> Z.to_int |> fun n ->
              return Logic.Lang.(c_timestamp n)
          | None -> failwith "Invalid timestamp %s on %a" s pp_location exp.loc)
      | _ -> failwith "Unexpected error: %s" __LOC__)
  | T_exp_bytes b -> (
      match ety with
      | Sort.S_bytes -> return Logic.Lang.(c_bytes b)
      | Sort.S_key -> (
          match
            `Hex (Bytes.to_string b)
            |> Hex.to_bytes_exn
            |> Data_encoding.Binary.of_bytes_opt Signature.Public_key.encoding
          with
          | Some v ->
              let s = Signature.Public_key.to_b58check v in
              return Logic.Lang.(c_key s)
          | None ->
              failwith "Invalid key %a on %a" pp_bytes b pp_location exp.loc)
      | Sort.S_key_hash -> (
          match
            `Hex (Bytes.to_string b)
            |> Hex.to_bytes_exn
            |> Data_encoding.Binary.of_bytes_opt
                 Signature.Public_key_hash.encoding
          with
          | Some v ->
              let s = Signature.Public_key_hash.to_b58check v in
              return Logic.Lang.(c_key_hash s)
          | None ->
              failwith "Invalid key %a on %a" pp_bytes b pp_location exp.loc)
      | Sort.S_address -> (
          match
            `Hex (Bytes.to_string b)
            |> Hex.to_bytes_exn
            |> Data_encoding.Binary.of_bytes_opt
                 Data_encoding.(
                   tup2 Protocol.Alpha_context.Contract.encoding Variable.string)
          with
          | Some (v, entry) ->
              let s = Protocol.Alpha_context.Contract.to_b58check v in
              let entry = match entry with "" -> "%default" | s -> "%" ^ s in
              return Logic.Lang.(create_address (c_string s) (c_string entry))
          | None ->
              failwith "Invalid address %a on %a" pp_bytes b pp_location exp.loc
          )
      | Sort.S_signature -> (
          match
            `Hex (Bytes.to_string b)
            |> Hex.to_bytes_exn
            |> Data_encoding.Binary.of_bytes_opt Signature.encoding
          with
          | Some v ->
              let s = Signature.to_b58check v in
              return Logic.Lang.(c_signature s)
          | None ->
              failwith "Invalid signature %a on %a" pp_bytes b pp_location
                exp.loc)
      | Sort.S_chain_id -> (
          match
            `Hex (Bytes.to_string b)
            |> Hex.to_bytes_exn
            |> Data_encoding.Binary.of_bytes_opt Chain_id.encoding
          with
          | Some v ->
              let s = Chain_id.to_b58check v in
              return Logic.Lang.(c_chain_id s)
          | None ->
              failwith "Invalid chain_id %a on %a" pp_bytes b pp_location
                exp.loc)
      | _ -> failwith "Unexpected error: %s" __LOC__)
  | T_exp_lambda_annot (f, (rty1, rty2, rty3)) ->
      transpile_rtype subst rty1 >>=? fun (stackbind1, pred1, subst_arg, _) ->
      transpile_rtype (subst_arg @ subst) rty2
      >>=? fun (stackbind2, pred2, _, _) ->
      transpile_rtype (subst_arg @ subst) rty3
      >>=? fun (stackbind3, pred3, _, _) ->
      (match (stackbind1, stackbind2, stackbind3) with
      | [ arg ], [ res ], [ err ] -> return (arg, res, err)
      | _ -> failwith "Unexpected error: %s" __LOC__)
      >>=? fun (arg, res, er) ->
      (match List.assoc ~equal:( = ) f subst with
      | Some e -> return e
      | None -> failwith "Unexpected error: %s" __LOC__)
      >>=? fun f -> return Logic.Lang.(lambda f arg res er pred1 pred2 pred3)

let transpile_condition (subst : (Syntax.variable * Logic.exp) list)
    (condition : typed_condition) =
  transpile_rtype subst condition >>=? fun (params, body, _, nobinds) ->
  return
    ({ loc = condition.loc; params; body; nobinds }
      : Decoration_def.rtype_annot)

let transpile_lambda_annot (annot : typed_lambda_annot) :
    (Decoration_def.lambda_annot * (Syntax.variable * Logic.exp) list) tzresult
    Lwt.t =
  let { arg = rty1; res = rty2; exc = rty3; annot_env } = annot in
  transpile_rtype [] rty1 >>=? fun (stackbind1, pred1, subst_arg, _) ->
  transpile_rtype subst_arg rty2 >>=? fun (stackbind2, pred2, _, _) ->
  transpile_rtype subst_arg rty3 >>=? fun (stackbind3, pred3, _, _) ->
  List.map_es
    (fun (v, t) ->
      let x = Logic.fresh_var t in
      return (x, (v, Logic.Lang.(?/x))))
    annot_env
  >>=? fun l ->
  List.split l |> fun (env, subst_annot) ->
  (match (stackbind1, stackbind2, stackbind3) with
  | [ arg ], [ res ], [ err ] -> return (arg, res, err)
  | _ -> failwith "Unexpected error: %s" __LOC__)
  >>=? fun (arg, res, err) ->
  return
    ( ({
         arg = (rty1.loc, arg, pred1);
         res = (rty2.loc, res, pred2);
         err = (rty3.loc, err, pred3);
         env;
       }
        : Decoration_def.lambda_annot),
      subst_annot @ subst_arg )

let transpile_measure ({ ufid; tyvars; args; ret_ty; body } : typed_measure) :
    Decoration_def.measure_annot tzresult Lwt.t =
  let fresh_vars = List.map (fun (x, ty) -> (x, Logic.fresh_var ty)) args in
  let params = List.map snd fresh_vars in
  let s_args = List.map (fun (x, x') -> (x, Logic.Lang.(?/x'))) fresh_vars in
  transpile_exp s_args body >>=? fun body ->
  return
    ({ ufid; tyvars; args = params; ret_ty; body }
      : Decoration_def.measure_annot)

(* let arg = fst arg in *)
(* let ret = fst ret in *)
(* (match carg.body with *)
(* | T_pattern_constr (("Cons" | "Add"), [ph; pt]) -> return (ph, pt) *)
(* | T_pattern_constr ("Bind", [pk; pv; pt]) -> *)
(*     return *)
(*       ( { *)
(*           body = T_pattern_constr ("Pair", [pk; pv]); *)
(*           ty = S_pair (pk.ty, pv.ty); *)
(*           loc = dummy_loc; *)
(*         }, *)
(*         pt ) *)
(* | _ -> failwith "Unexpected error: %s" __LOC__) *)
(* >>=? fun (ph, pt) -> *)
(* let s_args = *)
(*   List.map (fun (x, ty) -> (x, Logic.Lang.(?/(Logic.fresh_var ty)))) args *)
(* in *)
(* let hd_ty = ph.ty in *)
(* let tl_ty = pt.ty in *)
(* let hd = Logic.fresh_var hd_ty in *)
(* let tl = Logic.fresh_var tl_ty in *)
(* transpile_pattern Logic.Lang.(?/hd) ph >>=? fun (_, s_hd) -> *)
(* transpile_pattern Logic.Lang.(?/tl) pt >>=? fun (_, s_tl) -> *)
(* transpile_exp s_args nilexp >>=? fun nilexp -> *)
(* transpile_exp (s_args @ s_hd @ s_tl) cexp >>=? fun cexp -> *)
(* let measure_app x = Logic.E_mapp (ufid, [], List.map snd s_args @ [x]) in *)
(* (match arg with *)
(* | Sort.S_list ty -> *)
(*     return *)
(*       ( Logic.Lang.(measure_app (nil ty) = nilexp), *)
(*         Logic.Lang.(measure_app (cons ?/hd ?/tl) = cexp) ) *)
(* | Sort.S_set ty -> *)
(*     return *)
(*       ( Logic.Lang.(measure_app (empty_set ty) = nilexp), *)
(*         Logic.Lang.(measure_app (add_set ?/hd ?/tl) = cexp) ) *)
(* | Sort.S_map (tk, tv) -> *)
(*     return *)
(*       ( Logic.Lang.(measure_app (empty_map tk tv) = nilexp), *)
(*         Logic.Lang.( *)
(*           measure_app (update_map (fst ?/hd) (some (snd ?/hd)) ?/tl) = cexp) *)
(*       ) *)
(* | _ -> failwith "Unexpected error: %s" __LOC__) *)
(* >>=? fun (nilexp, cexp) -> *)
(* return *)
(*   ({ *)
(*      ufid; *)
(*      tyvars; *)
(*      arg; *)
(*      ret; *)
(*      nilexp; *)
(*      consexp = (fst hd, fst tl, cexp); *)
(*      nilid = ufid ^ "nil"; *)
(*      consid = ufid ^ "cons"; *)
(*    } *)
(*     : Decoration_def.measure_annot) *)
