open Syntax

(* We use a list monad for type inference with overloaded functions. *)

module LM : sig
  type 'a result (* hide implementation to prevent the monad used as list *)

  val lift : 'a tzresult Lwt.t -> 'a result
  val return_m : 'a -> 'a result
  val choose : 'a result -> 'a tzresult Lwt.t
  val ( <+> ) : 'a result -> 'a result -> 'a result
  val ( >>=|? ) : 'a result -> ('a -> 'b result) -> 'b result
  val fold_right_m : ('a -> 'b -> 'b result) -> 'a list -> 'b -> 'b result
  val map_m : ('a -> 'b result) -> 'a list -> 'b list result
  val failwith_m : ('a, Format.formatter, unit, 'b result) format4 -> 'a
end = struct
  type 'a result = 'a list tzresult Lwt.t

  let lift (x : 'a tzresult Lwt.t) : 'a result = x >>=? fun x -> return [ x ]
  let mzero loc : 'a result = failwith "Unexpected error: %s" loc

  let mplus (x : 'a result) (y : 'a result) : 'a result =
    Lwt.both x y >>= function
    | Error _, (Error _ as err) -> Lwt.return err
    | Ok l, Error _ | Error _, Ok l -> Lwt.return (Ok l)
    | Ok l1, Ok l2 -> Lwt.return (Ok (l1 @ l2))

  let return_m (x : 'a) : 'a result = return [ x ]

  let bind (x : 'a result) (f : 'a -> 'b result) : 'b result =
    x >>=? List.fold_left (fun a c -> mplus a (f c)) (mzero __LOC__)

  let choose (x : 'a result) : 'a tzresult Lwt.t =
    x >>=? function
    | [] -> failwith "Unexpected error: %s" __LOC__
    | h :: _ -> Lwt.return_ok h

  let ( <+> ) = mplus
  let ( >>=|? ) = bind

  let rec fold_right_m (f : 'a -> 'b -> 'b result) (l : 'a list) (a : 'b) :
      'b result =
    match l with
    | [] -> return_m a
    | h :: t -> fold_right_m f t a >>=|? fun a' -> f h a'

  let map_m (f : 'a -> 'b result) (l : 'a list) : 'b list result =
    fold_right_m (fun x xs -> f x >>=|? fun x -> return_m @@ (x :: xs)) l []

  let failwith_m = failwith
end

open LM

let fresh_id =
  let ctr = ref 0 in
  fun c ->
    incr ctr;
    Format.sprintf "%s%04d" c !ctr

let fresh_ty () = Sort.S_var (fresh_id "t")

type subst = (string * Sort.t) list

let rec eqs_of_subst = function
  | (str, ty) :: t -> (Sort.S_var str, ty) :: eqs_of_subst t
  | [] -> []

let rec subst_eqs s = function
  | (ty1, ty2) :: t ->
      (Sort.subst_sort s ty1, Sort.subst_sort s ty2) :: subst_eqs s t
  | [] -> []

let occur_check tyvar ty =
  let rec aux = function
    | Sort.S_var str ->
        if str = tyvar then failwith "Unification Error" else return_unit
    | Sort.S_list ty | Sort.S_option ty | Sort.S_set ty | Sort.S_contract ty ->
        aux ty
    | Sort.S_pair (ty1, ty2)
    | Sort.S_or (ty1, ty2)
    | Sort.S_map (ty1, ty2)
    | Sort.S_big_map (ty1, ty2)
    | Sort.S_lambda (ty1, ty2) ->
        Lwt_result_syntax.tzjoin [ aux ty1; aux ty2 ]
    | Sort.S_string | Sort.S_nat | Sort.S_int | Sort.S_bytes | Sort.S_bool
    | Sort.S_unit | Sort.S_timestamp | Sort.S_mutez | Sort.S_address
    | Sort.S_operation | Sort.S_key | Sort.S_key_hash | Sort.S_signature
    | Sort.S_chain_id | Sort.S_exception ->
        return_unit
  in
  aux ty

let rec unify eqs =
  match eqs with
  | [] -> return []
  | (ty1, ty2) :: t when ty1 = ty2 -> unify t
  | (Sort.S_var str, ty) :: t | (ty, Sort.S_var str) :: t ->
      occur_check str ty >>=? fun () ->
      unify @@ subst_eqs [ (str, ty) ] t >>=? fun s -> return @@ ((str, ty) :: s)
  | (Sort.S_list ty1, Sort.S_list ty2) :: t
  | (Sort.S_option ty1, Sort.S_option ty2) :: t
  | (Sort.S_set ty1, Sort.S_set ty2) :: t
  | (Sort.S_contract ty1, Sort.S_contract ty2) :: t ->
      unify ((ty1, ty2) :: t)
  | (Sort.S_pair (ty11, ty12), Sort.S_pair (ty21, ty22)) :: t
  | (Sort.S_or (ty11, ty12), Sort.S_or (ty21, ty22)) :: t
  | (Sort.S_map (ty11, ty12), Sort.S_map (ty21, ty22)) :: t
  | (Sort.S_big_map (ty11, ty12), Sort.S_big_map (ty21, ty22)) :: t
  | (Sort.S_lambda (ty11, ty12), Sort.S_lambda (ty21, ty22)) :: t ->
      unify ((ty11, ty21) :: (ty12, ty22) :: t)
  | (ty1, ty2) :: _ ->
      failwith "Type error : %a is not equal to %a" Sort.pp_sort ty1
        Sort.pp_sort ty2

let add_loc_to_failwith loc = function
  | Ok res -> Ok res
  | Error [ Exn (Failure e) ] ->
      let e = Format.asprintf "%s on %a" e pp_location loc in
      Error [ Exn (Failure e) ]
  | Error e -> Error e

let unify_with_loc loc s = unify s >|= add_loc_to_failwith loc

(** [typecheck_rtype rtype stack_ty var_env measure_env] checks
   wellformedness of the refinement stack type [rtype] under [var_env]
   and [measure_env].  It also checks if [rtype] is a refinement of
   the simple stack type [stack_ty].  A result is [(typed_rtype,
   subst, tyenv)], where [typed_rtype] is typechecked rtype, [subst]
   is a type substitution which keepes the type inference result, and
   [tyenv] is a typing environment for variables in the stack binder
   of [rtype].  Note that no type variables in [typed_rtype] are
   instantiated at this point.  So [subst] is required to be applied
   for before examining the types in [typed_rtype].  [tyenv] is used
   to bind the variables, for instance, in the post-condition of
   lambda annotation. *)
let rec typecheck_rtype (rtype : parsed_rtype) (stack_ty : Sort.t list)
    (self_entries : (string * Sort.t) list) (var_env : Ty_env.var_env)
    (measure_env : Ty_env.measure_env) :
    (typed_rtype * subst * Ty_env.var_env) result =
  let stack, exp = rtype.body in
  typecheck_stack stack stack_ty >>=|? fun (stack, s1, arg_env) ->
  let arg_env = Ty_env.map_env (Sort.subst_sort s1) arg_env in
  typecheck_exp exp self_entries (Ty_env.append_env arg_env var_env) measure_env
  >>=|? fun (exp, s2) ->
  let eqs = (exp.ty, Sort.S_bool) :: eqs_of_subst (s1 @ s2) in
  lift (unify_with_loc exp.loc eqs) >>=|? fun s ->
  return_m ({ body = (stack, exp); loc = rtype.loc }, s, arg_env)

and typecheck_stack (stack : parsed_stack) (stack_ty : Sort.t list) :
    (typed_stack * subst * Ty_env.var_env) result =
  let unify eqs = lift (unify_with_loc stack.loc eqs) in
  let loc = stack.loc in
  match (stack.body, stack_ty) with
  | P_stack_bottom { body = P_pattern_constr ("Nil", _, _); _ }, [] ->
      return_m ({ body = T_stack_nil; loc }, [], Ty_env.empty_var_env)
  | P_stack_bottom { body = P_pattern_any; _ }, stack_ty ->
      let rec aux = function
        | [] -> { body = T_stack_nil; loc }
        | ty :: t ->
            {
              body = T_stack_cons ({ body = T_pattern_any; ty; loc }, aux t);
              loc;
            }
      in
      return_m (aux stack_ty, [], Ty_env.empty_var_env)
  | P_stack_bottom p, [ ty ] ->
      typecheck_pattern p >>=|? fun (p, s, env) ->
      let eqs = (p.ty, ty) :: eqs_of_subst s in
      unify eqs >>=|? fun s ->
      return_m
        ({ body = T_stack_cons (p, { body = T_stack_nil; loc }); loc }, s, env)
  | P_stack_cons (p, t), hty :: tty ->
      typecheck_pattern p >>=|? fun (p, s1, env1) ->
      typecheck_stack t tty >>=|? fun (t, s2, env2) ->
      let eqs = (p.ty, hty) :: eqs_of_subst (s1 @ s2) in
      unify eqs >>=|? fun s ->
      lift @@ Ty_env.disjoint_env env1 env2 ~loc >>=|? fun env ->
      return_m ({ body = T_stack_cons (p, t); loc }, s, env)
  | _, [] ->
      failwith_m "Stack typecheck fails : Stack %a is empty on %a"
        pp_parsed_stack stack pp_location stack.loc
  | _ ->
      failwith_m
        "Stack typecheck fails : Stack %a is not be equivalent to [%a] on %a"
        pp_parsed_stack stack
        (Format.pp_print_list
           ~pp_sep:(fun fmt () -> Format.fprintf fmt " ::@ ")
           Sort.pp_sort)
        stack_ty pp_location loc

and typecheck_pattern (pattern : parsed_pattern) :
    (typed_pattern * subst * Ty_env.var_env) result =
  let unify eqs = lift (unify_with_loc pattern.loc eqs) in
  let make_pattern body ty = { body; ty; loc = pattern.loc } in
  match pattern.body with
  | P_pattern_constr (c, sl, pl) ->
      fold_right_m
        (fun p (pl, s, env) ->
          typecheck_pattern p >>=|? fun (p', s', env') ->
          unify (eqs_of_subst (s @ s')) >>=|? fun s ->
          lift @@ Ty_env.disjoint_env env' env ~loc:pattern.loc >>=|? fun env ->
          return_m (p' :: pl, s, env))
        pl
        ([], [], Ty_env.empty_var_env)
      >>=|? fun (pl, s, env) ->
      (match (c, sl, List.map (fun p -> p.ty) pl) with
      | "True", [], [] -> return_m ([], Sort.S_bool)
      | "False", [], [] -> return_m ([], Sort.S_bool)
      | "Unit", [], [] -> return_m ([], Sort.S_unit)
      | "Nil", [], [] ->
          let ty = fresh_ty () in
          return_m ([], Sort.S_list ty)
      | "Cons", [], [ ty1; ty2 ] -> return_m ([ (Sort.S_list ty1, ty2) ], ty2)
      | "Pair", [], [ ty1; ty2 ] -> return_m ([], Sort.S_pair (ty1, ty2))
      | "None", [], [] ->
          let ty = fresh_ty () in
          return_m ([], Sort.S_option ty)
      | "Some", [], [ ty ] -> return_m ([], Sort.S_option ty)
      | "Left", [], [ ty ] ->
          let rty = fresh_ty () in
          return_m ([], Sort.S_or (ty, rty))
      | "Right", [], [ ty ] ->
          let lty = fresh_ty () in
          return_m ([], Sort.S_or (lty, ty))
      | "EmptySet", [], [] ->
          let ty = fresh_ty () in
          return_m ([], Sort.S_set ty)
      | "Add", [], [ ty1; ty2 ] -> return_m ([ (Sort.S_set ty1, ty2) ], ty2)
      | "EmptyMap", [], [] ->
          let ty1 = fresh_ty () in
          let ty2 = fresh_ty () in
          return_m ([], Sort.S_map (ty1, ty2))
      | "Bind", [], [ ty1; ty2; ty3 ] ->
          return_m ([ (Sort.S_map (ty1, ty2), ty3) ], ty3)
      | "Contract", [], [ ty ] ->
          let sty = fresh_ty () in
          return_m ([ (ty, Sort.S_address) ], Sort.S_contract sty)
      | "Contract", [ tyv ], [ ty ] ->
          return_m ([ (ty, Sort.S_address) ], Sort.S_contract tyv)
      | "SetDelegate", [], [ ty ] ->
          return_m ([ (ty, Sort.S_option Sort.S_key_hash) ], Sort.S_operation)
      | ("Transfer" | "TransferTokens"), [], [ ty1; ty2; ty3 ] ->
          let ty = fresh_ty () in
          return_m
            ( [ (ty1, ty); (ty2, Sort.S_mutez); (ty3, Sort.S_contract ty) ],
              Sort.S_operation )
      | ("Transfer" | "TransferTokens"), [ tyv ], [ ty1; ty2; ty3 ] ->
          let ty = tyv in
          return_m
            ( [ (ty1, ty); (ty2, Sort.S_mutez); (ty3, Sort.S_contract ty) ],
              Sort.S_operation )
      | "CreateContract", [], [ ty1; ty2; ty3; ty4 ] ->
          let ty = fresh_ty () in
          return_m
            ( [
                (ty1, Sort.S_option Sort.S_key_hash);
                (ty2, Sort.S_mutez);
                (ty3, ty);
                (ty4, Sort.S_address);
              ],
              Sort.S_operation )
      | "CreateContract", [ tyv ], [ ty1; ty2; ty3; ty4 ] ->
          let ty = tyv in
          return_m
            ( [
                (ty1, Sort.S_option Sort.S_key_hash);
                (ty2, Sort.S_mutez);
                (ty3, ty);
                (ty4, Sort.S_address);
              ],
              Sort.S_operation )
      | "Error", [], [ _ ] -> return_m ([], Sort.S_exception)
      | "Error", [ tyv ], [ ty ] -> return_m ([ (ty, tyv) ], Sort.S_exception)
      | "Overflow", [], [] -> return_m ([], Sort.S_exception)
      | _ ->
          failwith_m "Pattern %s does not match any patterns on %a" c
            pp_location pattern.loc)
      >>=|? fun (eqs, ty) ->
      let eqs = eqs @ eqs_of_subst s in
      unify eqs >>=|? fun s ->
      return_m (make_pattern (T_pattern_constr (c, pl)) ty, s, env)
  | P_pattern_var var ->
      let ty = fresh_ty () in
      return_m
        (make_pattern (T_pattern_var var) ty, [], Ty_env.singleton_var var ty)
  | P_pattern_any ->
      let ty = fresh_ty () in
      return_m (make_pattern T_pattern_any ty, [], Ty_env.empty_var_env)

and typecheck_exp (exp : parsed_exp) (self_entries : (string * Sort.t) list)
    (var_env : Ty_env.var_env) (measure_env : Ty_env.measure_env) :
    (typed_exp * subst) result =
  (* self_entries and measure_env is immutable during type checking. *)
  let typecheck_exp exp var_env =
    typecheck_exp exp self_entries var_env measure_env
  in
  let unify eqs = lift (unify_with_loc exp.loc eqs) in
  let make_exp body ty = { body; ty; loc = exp.loc } in
  match exp.body with
  | P_exp_if (e1, e2, e3) ->
      typecheck_exp e1 var_env >>=|? fun (e1, s1) ->
      typecheck_exp e2 var_env >>=|? fun (e2, s2) ->
      typecheck_exp e3 var_env >>=|? fun (e3, s3) ->
      let eqs =
        (e2.ty, e3.ty) :: (e1.ty, Sort.S_bool) :: eqs_of_subst (s1 @ s2 @ s3)
      in
      unify eqs >>=|? fun s ->
      let ty = e2.ty in
      (* de-sugaring into: match e1 with true -> e2 | false -> e3 *)
      return_m
        ( make_exp
            (T_exp_match
               ( e1,
                 [
                   ( {
                       body = T_pattern_constr ("True", []);
                       ty = Sort.S_bool;
                       loc = e1.loc;
                     },
                     e2 );
                   ( {
                       body = T_pattern_constr ("True", []);
                       ty = Sort.S_bool;
                       loc = e1.loc;
                     },
                     e3 );
                 ] ))
            ty,
          s )
  | P_exp_match (e, pl) ->
      typecheck_exp e var_env >>=|? fun (me, ms) ->
      let ty = fresh_ty () in
      let rec patterns eqs = function
        | (p, e) :: t ->
            typecheck_pattern p >>=|? fun (p, s1, env_p) ->
            typecheck_exp e @@ Ty_env.append_env env_p var_env
            >>=|? fun (e, s2) ->
            let eqs =
              ((ty, e.ty) :: (me.ty, p.ty) :: eqs_of_subst (s1 @ s2)) @ eqs
            in
            patterns eqs t >>=|? fun (t, eqs) -> return_m ((p, e) :: t, eqs)
        | [] -> return_m ([], eqs)
      in
      let eqs = eqs_of_subst ms in
      patterns eqs pl >>=|? fun (pl, eqs) ->
      unify eqs >>=|? fun s -> return_m (make_exp (T_exp_match (me, pl)) ty, s)
  | P_exp_bop (op, e1, e2) ->
      typecheck_exp e1 var_env >>=|? fun (e1, s1) ->
      typecheck_exp e2 var_env >>=|? fun (e2, s2) ->
      (match op with
      | "+" ->
          return_m ([ (e1.ty, Sort.S_int); (e2.ty, Sort.S_int) ], Sort.S_int)
          <+> return_m ([ (e1.ty, Sort.S_int); (e2.ty, Sort.S_nat) ], Sort.S_int)
          <+> return_m ([ (e1.ty, Sort.S_nat); (e2.ty, Sort.S_int) ], Sort.S_int)
          <+> return_m ([ (e1.ty, Sort.S_nat); (e2.ty, Sort.S_nat) ], Sort.S_nat)
          <+> return_m
                ( [ (e1.ty, Sort.S_timestamp); (e2.ty, Sort.S_int) ],
                  Sort.S_timestamp )
          <+> return_m
                ( [ (e1.ty, Sort.S_int); (e2.ty, Sort.S_timestamp) ],
                  Sort.S_timestamp )
          <+> return_m
                ([ (e1.ty, Sort.S_mutez); (e2.ty, Sort.S_mutez) ], Sort.S_mutez)
      | "-" ->
          return_m ([ (e1.ty, Sort.S_int); (e2.ty, Sort.S_int) ], Sort.S_int)
          <+> return_m ([ (e1.ty, Sort.S_int); (e2.ty, Sort.S_nat) ], Sort.S_int)
          <+> return_m ([ (e1.ty, Sort.S_nat); (e2.ty, Sort.S_int) ], Sort.S_int)
          <+> return_m ([ (e1.ty, Sort.S_nat); (e2.ty, Sort.S_nat) ], Sort.S_int)
          <+> return_m
                ( [ (e1.ty, Sort.S_timestamp); (e2.ty, Sort.S_int) ],
                  Sort.S_timestamp )
          <+> return_m
                ( [ (e1.ty, Sort.S_timestamp); (e2.ty, Sort.S_timestamp) ],
                  Sort.S_int )
          <+> return_m
                ([ (e1.ty, Sort.S_mutez); (e2.ty, Sort.S_mutez) ], Sort.S_mutez)
      | "*" ->
          return_m ([ (e1.ty, Sort.S_int); (e2.ty, Sort.S_int) ], Sort.S_int)
          <+> return_m ([ (e1.ty, Sort.S_int); (e2.ty, Sort.S_nat) ], Sort.S_int)
          <+> return_m ([ (e1.ty, Sort.S_nat); (e2.ty, Sort.S_int) ], Sort.S_int)
          <+> return_m ([ (e1.ty, Sort.S_nat); (e2.ty, Sort.S_nat) ], Sort.S_nat)
          <+> return_m
                ([ (e1.ty, Sort.S_mutez); (e2.ty, Sort.S_nat) ], Sort.S_mutez)
          <+> return_m
                ([ (e1.ty, Sort.S_nat); (e2.ty, Sort.S_mutez) ], Sort.S_mutez)
      | "/" ->
          return_m ([ (e1.ty, Sort.S_int); (e2.ty, Sort.S_int) ], Sort.S_int)
          <+> return_m ([ (e1.ty, Sort.S_int); (e2.ty, Sort.S_nat) ], Sort.S_int)
          <+> return_m ([ (e1.ty, Sort.S_nat); (e2.ty, Sort.S_int) ], Sort.S_int)
          <+> return_m ([ (e1.ty, Sort.S_nat); (e2.ty, Sort.S_nat) ], Sort.S_nat)
          <+> return_m
                ([ (e1.ty, Sort.S_mutez); (e2.ty, Sort.S_nat) ], Sort.S_mutez)
          <+> return_m
                ([ (e1.ty, Sort.S_mutez); (e2.ty, Sort.S_mutez) ], Sort.S_nat)
      | "mod" ->
          return_m ([ (e1.ty, Sort.S_int); (e2.ty, Sort.S_int) ], Sort.S_nat)
          <+> return_m ([ (e1.ty, Sort.S_int); (e2.ty, Sort.S_nat) ], Sort.S_nat)
          <+> return_m ([ (e1.ty, Sort.S_nat); (e2.ty, Sort.S_int) ], Sort.S_nat)
          <+> return_m ([ (e1.ty, Sort.S_nat); (e2.ty, Sort.S_nat) ], Sort.S_nat)
          <+> return_m
                ([ (e1.ty, Sort.S_mutez); (e2.ty, Sort.S_nat) ], Sort.S_mutez)
          <+> return_m
                ([ (e1.ty, Sort.S_mutez); (e2.ty, Sort.S_mutez) ], Sort.S_mutez)
      | "::" -> return_m ([ (Sort.S_list e1.ty, e2.ty) ], e2.ty)
      | "@" ->
          let ty = fresh_ty () in
          return_m ([ (Sort.S_list ty, e1.ty); (Sort.S_list ty, e2.ty) ], e1.ty)
      | "^" ->
          return_m
            ([ (e1.ty, Sort.S_string); (e2.ty, Sort.S_string) ], Sort.S_string)
      | "=" | "<>" -> return_m ([ (e1.ty, e2.ty) ], Sort.S_bool)
      | "<" | "<=" | ">" | ">=" -> return_m ([ (e1.ty, e2.ty) ], Sort.S_bool)
      | "&&" | "||" ->
          return_m ([ (e1.ty, Sort.S_bool); (e2.ty, Sort.S_bool) ], Sort.S_bool)
      | _ ->
          failwith_m "Unknown binary operator %s on %a" op pp_location exp.loc)
      >>=|? fun (eqs, ty) ->
      let eqs = eqs @ eqs_of_subst (s1 @ s2) in
      unify eqs >>=|? fun s ->
      return_m (make_exp (T_exp_fapp (op, None, [ e1; e2 ])) ty, s)
  | P_exp_uop (op, e) ->
      typecheck_exp e var_env >>=|? fun (e, s) ->
      (match op with
      | "-" ->
          return_m ([ (e.ty, Sort.S_int) ], Sort.S_int)
          <+> return_m ([ (e.ty, Sort.S_nat) ], Sort.S_int)
          <+> return_m ([ (e.ty, Sort.S_timestamp) ], Sort.S_timestamp)
      | "!" -> return_m ([ (e.ty, Sort.S_bool) ], Sort.S_bool)
      | _ -> failwith_m "Unknown unary operator %s on %a" op pp_location exp.loc)
      >>=|? fun (eqs, ty) ->
      let eqs = eqs @ eqs_of_subst s in
      unify eqs >>=|? fun s ->
      return_m (make_exp (T_exp_fapp (op, None, [ e ])) ty, s)
  | P_exp_capp (c, el) ->
      map_m (fun e -> typecheck_exp e var_env) el >>=|? fun l ->
      let el, sl = List.split l in
      let s = List.flatten sl in
      (match (c, List.map (fun e -> e.ty) el) with
      | "True", [] -> return_m ([], Sort.S_bool)
      | "False", [] -> return_m ([], Sort.S_bool)
      | "Unit", [] -> return_m ([], Sort.S_unit)
      | "Nil", [] ->
          let ty = fresh_ty () in
          return_m ([], Sort.S_list ty)
      | "Cons", [ ty1; ty2 ] -> return_m ([ (Sort.S_list ty1, ty2) ], ty2)
      | "Pair", [ ty1; ty2 ] -> return_m ([], Sort.S_pair (ty1, ty2))
      | "None", [] ->
          let ty = fresh_ty () in
          return_m ([], Sort.S_option ty)
      | "Some", [ ty ] -> return_m ([], Sort.S_option ty)
      | "Left", [ ty ] ->
          let rty = fresh_ty () in
          return_m ([], Sort.S_or (ty, rty))
      | "Right", [ ty ] ->
          let lty = fresh_ty () in
          return_m ([], Sort.S_or (lty, ty))
      | "SetDelegate", [ ty ] ->
          return_m ([ (ty, Sort.S_option Sort.S_key_hash) ], Sort.S_operation)
      | ("Transfer" | "TransferTokens"), [ ty1; ty2; ty3 ] ->
          let ty = fresh_ty () in
          return_m
            ( [ (ty1, ty); (ty2, Sort.S_mutez); (ty3, Sort.S_contract ty) ],
              Sort.S_operation )
      | "CreateContract", [ ty1; ty2; ty3; ty4 ] ->
          let ty = fresh_ty () in
          return_m
            ( [
                (ty1, Sort.S_option Sort.S_key_hash);
                (ty2, Sort.S_mutez);
                (ty3, ty);
                (ty4, Sort.S_address);
              ],
              Sort.S_operation )
      | "Error", [ _ ] -> return_m ([], Sort.S_exception)
      | "Overflow", [] -> return_m ([], Sort.S_exception)
      | _ -> failwith_m "Not found constructor %s on %a" c pp_location exp.loc)
      >>=|? fun (eqs, ty) ->
      let eqs = eqs @ eqs_of_subst s in
      unify eqs >>=|? fun s -> return_m (make_exp (T_exp_capp (c, el)) ty, s)
  | P_exp_fapp (f, annot, el) -> (
      if f = "self" then
        match
          List.assoc_opt ~equal:( = )
            Option.(value ~default:"%default" annot)
            self_entries
        with
        | Some ty ->
            return_m
              ( make_exp (T_exp_fapp ("self", annot, [])) Sort.(S_contract ty),
                [] )
        | None ->
            failwith_m "%a: self %a cannot be use here" pp_location exp.loc
              Format.(pp_print_option pp_print_string)
              annot
      else
        match Ty_env.find f var_env measure_env with
        | B_fsymbol (vars, param_tys, ty) ->
            let s = List.map (fun tv -> (tv, fresh_ty ())) vars in
            let param_tys = List.map (Sort.subst_sort s) param_tys in
            let ty = Sort.subst_sort s ty in
            let rec aux = function
              | [], [] -> return_m ([], [])
              | ty :: tyl, e :: el ->
                  typecheck_exp e var_env >>=|? fun (e, s) ->
                  aux (tyl, el) >>=|? fun (el, eqs) ->
                  return_m (e :: el, ((e.ty, ty) :: eqs_of_subst s) @ eqs)
              | _ ->
                  failwith_m "Symbol %s expects %d parameters on %a" f
                    (List.length param_tys) pp_location exp.loc
            in
            aux (param_tys, el) >>=|? fun (el, eqs) ->
            unify eqs >>=|? fun s ->
            return_m (make_exp (T_exp_fapp (f, annot, el)) ty, s)
        | B_measure (ufid, vars, param_tys, ty) ->
            let f = (ufid, vars, param_tys, ty) in
            let s = List.map (fun tv -> (tv, fresh_ty ())) vars in
            let vars = List.map snd s in
            let param_tys = List.map (Sort.subst_sort s) param_tys in
            let ty = Sort.subst_sort s ty in
            let rec aux = function
              | [], [] -> return_m ([], [])
              | ty :: tyl, e :: el ->
                  typecheck_exp e var_env >>=|? fun (e, s) ->
                  aux (tyl, el) >>=|? fun (el, eqs) ->
                  return_m (e :: el, ((e.ty, ty) :: eqs_of_subst s) @ eqs)
              | _ ->
                  failwith_m "Symbol %s expects %d parameters on %a" ufid
                    (List.length param_tys) pp_location exp.loc
            in
            aux (param_tys, el) >>=|? fun (el, eqs) ->
            unify eqs >>=|? fun s ->
            return_m (make_exp (T_exp_mapp (f, vars, el)) ty, s)
        | _ ->
            failwith_m "Not found function symbol %s on %a" f pp_location
              exp.loc)
  | P_exp_var str -> (
      if str = "self" then
        match List.assoc_opt ~equal:( = ) "%default" self_entries with
        | Some ty ->
            return_m
              (make_exp (T_exp_fapp ("self", None, [])) Sort.(S_contract ty), [])
        | None -> failwith_m "%a: self cannot be use here" pp_location exp.loc
      else
        match Ty_env.find str var_env measure_env with
        | B_fsymbol (vars, [], ty) ->
            let s = List.map (fun tv -> (tv, fresh_ty ())) vars in
            let ty = Sort.subst_sort s ty in
            return_m (make_exp (T_exp_fapp (str, None, [])) ty, [])
        | B_fsymbol (_, param_tys, _) ->
            failwith_m "%a: Symbol %s expects %d parameters" pp_location exp.loc
              str (List.length param_tys)
        | B_var ty -> return_m (make_exp (T_exp_var str) ty, [])
        | _ -> failwith_m "Not found symbol %s on %a" str pp_location exp.loc)
  | P_exp_number n ->
      return_m (make_exp (T_exp_number n) Sort.S_int, [])
      <+> return_m (make_exp (T_exp_number n) Sort.S_nat, [])
      <+> return_m (make_exp (T_exp_number n) Sort.S_timestamp, [])
      <+> return_m (make_exp (T_exp_number n) Sort.S_mutez, [])
  | P_exp_string s ->
      return_m (make_exp (T_exp_string s) Sort.S_string, [])
      (* Note: sanity check is done at transpile phase. *)
      <+> return_m (make_exp (T_exp_string s) Sort.S_address, [])
      <+> return_m (make_exp (T_exp_string s) Sort.S_key, [])
      <+> return_m (make_exp (T_exp_string s) Sort.S_key_hash, [])
      <+> return_m (make_exp (T_exp_string s) Sort.S_chain_id, [])
      <+> return_m (make_exp (T_exp_string s) Sort.S_signature, [])
      <+> return_m (make_exp (T_exp_string s) Sort.S_timestamp, [])
  | P_exp_bytes b ->
      return_m (make_exp (T_exp_bytes b) Sort.S_bytes, [])
      (* Note: sanity check is done at transpile phase. *)
      <+> return_m (make_exp (T_exp_bytes b) Sort.S_key, [])
      <+> return_m (make_exp (T_exp_bytes b) Sort.S_key_hash, [])
      <+> return_m (make_exp (T_exp_bytes b) Sort.S_address, [])
      <+> return_m (make_exp (T_exp_bytes b) Sort.S_signature, [])
      <+> return_m (make_exp (T_exp_bytes b) Sort.S_chain_id, [])
  | P_exp_type_annot (e, ty) ->
      typecheck_exp e var_env >>=|? fun (e, s) ->
      let eqs = (e.ty, ty) :: eqs_of_subst s in
      unify eqs >>=|? fun s -> return_m (e, s)
  | P_exp_lambda_annot (v, (rty1, rty2, rty3)) ->
      (match Ty_env.find v var_env measure_env with
      | B_var fty -> return_m fty
      | _ -> failwith_m "Function %s is not found" v)
      >>=|? fun fty ->
      let arg_ty = fresh_ty () in
      let ret_ty = fresh_ty () in
      typecheck_rtype rty1 [ arg_ty ] self_entries var_env measure_env
      >>=|? fun (rty1, s1, arg_env) ->
      let env' = Ty_env.append_env arg_env var_env in
      typecheck_rtype rty2 [ ret_ty ] self_entries env' measure_env
      >>=|? fun (rty2, s2, _) ->
      typecheck_rtype rty3 [ Sort.S_exception ] self_entries env' measure_env
      >>=|? fun (rty3, s3, _) ->
      let eqs =
        (fty, Sort.S_lambda (arg_ty, ret_ty)) :: eqs_of_subst (s1 @ s2 @ s3)
      in
      unify eqs >>=|? fun s ->
      return_m
        (make_exp (T_exp_lambda_annot (v, (rty1, rty2, rty3))) Sort.S_bool, s)
  | P_exp_masked_true ->
      typecheck_exp (Zipper.change_desc exp (P_exp_capp ("True", []))) var_env
  | P_exp_masked_false ->
      typecheck_exp (Zipper.change_desc exp (P_exp_capp ("False", []))) var_env

(** [subst_type_fv_check s ty] applies the substitution [s] to the
   sort [ty].  After applying the substitution, if there are type
   variables in the result, this function raises failure.  That is
   because we suppose that all types are revealed after type checking.
   *)
let subst_type_fv_check ?loc s ty =
  let ty = Sort.subst_sort s ty in
  if Sort.Tyvars.is_empty @@ Sort.ftv ty then return ty
  else
    match loc with
    | Some loc ->
        failwith "Unidentified type variables exists on %a" pp_location loc
    | None -> failwith "Unidentified type variables exists"

(* lift [subst_type_fv_check] to typed AST. *)

let rec subst_rtype s (rtype : typed_rtype) =
  let stack, exp = rtype.body in
  subst_stack s stack >>=? fun stack ->
  subst_exp s exp >>=? fun exp -> return { rtype with body = (stack, exp) }

and subst_stack s (stack : typed_stack) =
  (match stack.body with
  | T_stack_nil -> return T_stack_nil
  | T_stack_cons (p, t) ->
      subst_pattern s p >>=? fun p ->
      subst_stack s t >>=? fun t -> return @@ T_stack_cons (p, t))
  >>=? fun body -> return { stack with body }

and subst_pattern s pat =
  subst_type_fv_check ~loc:pat.loc s pat.ty >>=? fun ty ->
  (match pat.body with
  | T_pattern_constr (c, pl) ->
      List.map_es (subst_pattern s) pl >>=? fun pl ->
      return @@ T_pattern_constr (c, pl)
  | T_pattern_var var -> return @@ T_pattern_var var
  | T_pattern_any -> return @@ T_pattern_any)
  >>=? fun body -> return { pat with body; ty }

and subst_exp s exp =
  subst_type_fv_check ~loc:exp.loc s exp.ty >>=? fun ty ->
  (match exp.body with
  | T_exp_match (e, pel) ->
      subst_exp s e >>=? fun e ->
      List.map_es
        (fun (p, e) ->
          subst_pattern s p >>=? fun p ->
          subst_exp s e >>=? fun e -> return (p, e))
        pel
      >>=? fun pel -> return @@ T_exp_match (e, pel)
  | T_exp_capp (c, el) ->
      List.map_es (subst_exp s) el >>=? fun el -> return @@ T_exp_capp (c, el)
  | T_exp_fapp (f, annot, el) ->
      List.map_es (subst_exp s) el >>=? fun el ->
      return @@ T_exp_fapp (f, annot, el)
  | T_exp_mapp (f, sl, el) ->
      List.map_es (subst_type_fv_check s) sl >>=? fun sl ->
      List.map_es (subst_exp s) el >>=? fun el ->
      return @@ T_exp_mapp (f, sl, el)
  | T_exp_lambda_annot (v, (rty1, rty2, rty3)) ->
      subst_rtype s rty1 >>=? fun rty1 ->
      subst_rtype s rty2 >>=? fun rty2 ->
      subst_rtype s rty3 >>=? fun rty3 ->
      return @@ T_exp_lambda_annot (v, (rty1, rty2, rty3))
  | T_exp_var _ | T_exp_number _ | T_exp_string _ | T_exp_bytes _ ->
      return exp.body)
  >>=? fun body -> return { exp with body; ty }

let typecheck_condition (condition : parsed_condition) (expected : Sort.t list)
    (self_entries : (string * Sort.t) list) (var_env : Ty_env.var_env)
    (measure_env : Ty_env.measure_env) : typed_condition tzresult Lwt.t =
  choose
    ( typecheck_rtype condition expected self_entries var_env measure_env
    >>=|? fun (rtype, s, _) -> lift @@ subst_rtype s rtype )

let typecheck_lambda_annot (annot : parsed_lambda_annot) (arg_ty : Sort.t)
    (ret_ty : Sort.t) (self_entries : (string * Sort.t) list)
    (measure_env : Ty_env.measure_env) :
    (typed_lambda_annot * Ty_env.var_env) tzresult Lwt.t =
  let ({ arg = rty1; res = rty2; exc = rty3; env = annot_env }
        : parsed_lambda_annot) =
    annot
  in
  List.map_es
    (fun (x, ty, loc) ->
      subst_type_fv_check ~loc [] ty >>=? fun _ -> return (x, ty))
    annot_env
  >>=? fun annot_env ->
  choose
    ( typecheck_rtype rty1 [ arg_ty ] self_entries Ty_env.empty_var_env
        measure_env
    >>=|? fun (rty1, s1, arg_env) ->
      let env' = arg_env in
      typecheck_rtype rty2 [ ret_ty ] self_entries env' measure_env
      >>=|? fun (rty2, s2, _) ->
      typecheck_rtype rty3 [ Sort.S_exception ] self_entries env' measure_env
      >>=|? fun (rty3, s3, _) ->
      let eqs = eqs_of_subst (s1 @ s2 @ s3) in
      lift (unify eqs) >>=|? fun s ->
      lift @@ subst_rtype s rty1 >>=|? fun arg ->
      lift @@ subst_rtype s rty2 >>=|? fun res ->
      lift @@ subst_rtype s rty3 >>=|? fun exc ->
      lift
      @@ Ty_env.StringMap.fold_es
           (fun k ty ty_env ->
             subst_type_fv_check s ty >>=? fun ty ->
             return @@ Ty_env.StringMap.add k ty ty_env)
           env' Ty_env.empty_var_env
      >>=|? fun arg_env ->
      lift
      @@ Ty_env.disjoint_env arg_env
      @@ List.fold_left
           (fun env (x, ty) -> Ty_env.add_var x ty env)
           Ty_env.empty_var_env annot_env
      >>=|? fun body_env ->
      return_m (({ arg; res; exc; annot_env } : typed_lambda_annot), body_env)
    )

let typecheck_measure ({ name; args; ret_ty; body } : parsed_measure)
    (measure_env : Ty_env.measure_env) : typed_measure tzresult Lwt.t =
  let ufid = fresh_id name in
  List.fold_left_es
    (fun e (x, t) -> Ty_env.disjoint_env e (Ty_env.singleton_var x t))
    Ty_env.empty_var_env args
  >>=? fun args_env ->
  let measure_env =
    Ty_env.add_measure name ufid [] (List.map snd args) ret_ty measure_env
  in
  choose
    ( typecheck_exp body [] args_env measure_env >>=|? fun (body, s) ->
      let eqs = (body.ty, ret_ty) :: eqs_of_subst s in
      lift @@ unify_with_loc body.loc eqs >>=|? fun s ->
      lift
      @@ List.map_es
           (fun (x, ty) ->
             subst_type_fv_check s ty >>=? fun ty -> return (x, ty))
           args
      >>=|? fun args ->
      lift @@ subst_type_fv_check s ret_ty >>=|? fun ret_ty ->
      lift @@ subst_exp s body >>=|? fun body ->
      return_m { ufid; tyvars = []; args; ret_ty; body } )
