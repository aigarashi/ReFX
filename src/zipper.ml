open Syntax

exception Unexpected_value

(* search_unverified_contextのための条件式に対応するZipper構造 *)

type if_branch = If | Then | Else
type bop_branch = Left | Right

type parsed_exp_path =
  | Top
  | If of if_branch * parsed_exp * parsed_exp * parsed_exp_path
  | Match of
      parsed_exp
      * parsed_pattern
      * (parsed_pattern * parsed_exp) list
      * (parsed_pattern * parsed_exp) list
      * parsed_exp_path
  | Bop of string * bop_branch * parsed_exp * parsed_exp_path
  | Not of string option * parsed_exp_path

(* Zipper構造の位置構造の構成要素
   注目している式，親のパス，not下にいるかどうかの情報(not下ならばfalse) *)
type parsed_exp_loc = parsed_exp * parsed_exp_path * bool

let translate_to_zipper (exp : parsed_exp) : parsed_exp_loc = (exp, Top, true)
let translate_from_zipper ((exp, _, _) : parsed_exp_loc) = exp

let change_desc (exp : parsed_exp) (desc : parsed_exp_desc) =
  { exp with body = desc }

let exp_with_dummy (desc : parsed_exp_desc) = { body = desc; loc = dummy_loc }

type zipper_error =
  | Can'tGoRight
  | Can'tGoDown
  | Can'tGoUp
  | Complete of parsed_exp_loc

exception ZipperError of zipper_error

let go_right ((exp, p, b) : parsed_exp_loc) : parsed_exp_loc =
  match p with
  | Top | Not (_, _) -> raise (ZipperError Can'tGoRight)
  | If (br, exp1, exp2, p') -> (
      match br with
      | If -> (exp1, If (Then, exp, exp2, p'), b)
      | Then -> (exp2, If (Else, exp1, exp, p'), b)
      | Else -> raise (ZipperError Can'tGoRight))
  | Match (exp_match, patt, left, right, p') -> (
      match right with
      | [] -> raise (ZipperError Can'tGoRight)
      | (patt', exp') :: right' ->
          (exp', Match (exp_match, patt', (patt, exp) :: left, right', p'), b))
  | Bop (str, br, exp', p') -> (
      match br with
      | Left -> (exp', Bop (str, Right, exp, p'), b)
      | Right -> raise (ZipperError Can'tGoRight))

let go_down ((exp, p, b) : parsed_exp_loc) : parsed_exp_loc =
  match exp.body with
  | P_exp_if (exp_if, exp_then, exp_else) ->
      (exp_then, If (Then, exp_if, exp_else, p), b)
  | P_exp_match (exp_match, l) -> (
      match l with
      | [] -> raise Unexpected_value
      | (patt, exp') :: l' -> (exp', Match (exp_match, patt, [], l', p), b))
  | P_exp_bop (str, exp1, exp2) ->
      if str = "||" || str = "&&" then (exp1, Bop (str, Left, exp2, p), b)
      else raise (ZipperError Can'tGoDown)
  | P_exp_fapp (str, annot, l) -> (
      match l with
      | [] -> raise (ZipperError Can'tGoDown)
      | [ exp' ] ->
          if str = "not" then (exp', Not (annot, p), not b)
          else raise (ZipperError Can'tGoDown)
      | _ -> raise (ZipperError Can'tGoDown))
  | _ -> raise (ZipperError Can'tGoDown)

let go_up ((exp, p, b) : parsed_exp_loc) : parsed_exp_loc =
  match p with
  | Top -> raise (ZipperError Can'tGoUp)
  | If (br, exp1, exp2, p') -> (
      match br with
      | If -> (change_desc exp (P_exp_if (exp, exp1, exp2)), p', b)
      | Then -> (change_desc exp (P_exp_if (exp1, exp, exp2)), p', b)
      | Else -> (change_desc exp (P_exp_if (exp1, exp2, exp)), p', b))
  | Match (exp_match, patt, left, right, p') ->
      ( change_desc exp
          (P_exp_match (exp_match, List.rev left @ [ (patt, exp) ] @ right)),
        p',
        b )
  | Bop (str, br, exp', p') -> (
      match br with
      | Left -> (change_desc exp (P_exp_bop (str, exp, exp')), p', b)
      | Right -> (change_desc exp (P_exp_bop (str, exp', exp)), p', b))
  | Not (annot, p') ->
      (change_desc exp (P_exp_fapp ("not", annot, [ exp ])), p', not b)

let change_value ((exp, p, b) : parsed_exp_loc) (desc1 : parsed_exp_desc)
    (desc2 : parsed_exp_desc) : parsed_exp_loc =
  if b then (change_desc exp desc1, p, b) else (change_desc exp desc2, p, b)

let rec go_down_as_possible (zipper : parsed_exp_loc) =
  try
    let zipper = go_down zipper in
    go_down_as_possible zipper
  with
  | ZipperError Can'tGoDown -> zipper
  | _ -> raise Unexpected_value

let rec go_up_as_possible (zipper : parsed_exp_loc) =
  try
    let zipper = go_up zipper in
    go_up_as_possible zipper
  with
  | ZipperError Can'tGoUp -> zipper
  | _ -> raise Unexpected_value

let rec go_next_branch (zipper : parsed_exp_loc) =
  try
    let zipper = go_right zipper in
    go_down_as_possible zipper
  with
  | ZipperError Can'tGoRight -> (
      try
        let zipper = go_up zipper in
        go_next_branch zipper
      with
      | ZipperError Can'tGoUp -> raise (ZipperError (Complete zipper))
      | ZipperError (Complete zipper') -> raise (ZipperError (Complete zipper'))
      | _ -> raise Unexpected_value)
  | _ -> raise Unexpected_value

let rec if_expand (exp : parsed_exp) : parsed_exp =
  match exp.body with
  | P_exp_if (exp_if, exp_then, exp_else) ->
      let exp_if' = if_expand exp_if in
      let exp_then' = if_expand exp_then in
      let exp_else' = if_expand exp_else in
      let desc =
        P_exp_bop
          ( "&&",
            exp_with_dummy
              (P_exp_bop
                 ( "||",
                   exp_with_dummy (P_exp_fapp ("not", None, [ exp_if' ])),
                   exp_then' )),
            exp_with_dummy (P_exp_bop ("||", exp_if', exp_else')) )
      in
      change_desc exp desc
  | _ -> exp

let search_and_change_first (zipper : parsed_exp_loc) =
  (*let (exp, p, b) = zipper in
    let zipper = (if_expand exp, p, b) in*)
  let zipper = go_down_as_possible zipper in
  let zipper' = change_value zipper P_exp_masked_true P_exp_masked_false in
  (zipper, zipper')

let search_and_change_continue (zipper : parsed_exp_loc) =
  let zipper = go_next_branch zipper in
  let zipper' = change_value zipper P_exp_masked_true P_exp_masked_false in
  (zipper, zipper')

let change_onestep (zipper : parsed_exp_loc)
    (f : parsed_exp_loc -> parsed_exp_loc * parsed_exp_loc) =
  let zipper, zipper' = f zipper in
  let exp, _, _ = go_up_as_possible zipper' in
  (zipper, zipper', exp)

let rec contractannot_to_zipper (decs : (int * int * parsed_decoration) list) :
    parsed_exp_loc =
  match decs with
  | [] -> raise Unexpected_value
  | (_, _, dec) :: tl -> (
      match dec with
      | P_contract_annot (spe, _) ->
          let rtype = spe.res in
          let _, exp = rtype.body in
          translate_to_zipper exp
      | _ -> contractannot_to_zipper tl)

let rec apply_to_decs (decs : (int * int * parsed_decoration) list)
    (exp : parsed_exp) =
  match decs with
  | [] -> raise Unexpected_value
  | (i1, i2, dec) :: tl -> (
      match dec with
      | P_contract_annot (spe, annot) ->
          let rtype = spe.res in
          let stack, _ = rtype.body in
          let rtype' = { rtype with body = (stack, exp) } in
          let spe' = { spe with res = rtype' } in
          let dec' = P_contract_annot (spe', annot) in
          (i1, i2, dec') :: tl
      | _ -> (i1, i2, dec) :: apply_to_decs tl exp)
