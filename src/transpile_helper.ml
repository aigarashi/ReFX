open Syntax

let exhaustive_pattern_check (pl : typed_pattern list) =
  let destruct (p : typed_pattern) (pl : typed_pattern list) =
    match (p, pl) with
    | ( { body = T_pattern_constr (c1, _); _ },
        { body = T_pattern_constr (c2, pl); _ } :: t ) ->
        if c1 = c2 then Some (pl @ t) else None
    | { body = T_pattern_constr (_, pl); _ }, { body = T_pattern_var _; _ } :: t
    | { body = T_pattern_constr (_, pl); _ }, { body = T_pattern_any; _ } :: t
      ->
        let pl =
          List.map
            (fun (pat : typed_pattern) -> { pat with body = T_pattern_any })
            pl
        in
        Some (pl @ t)
    | _ -> assert false
  in
  let rec check_any (p : typed_pattern) (pl : typed_pattern list) =
    match (p, pl) with
    | ( { body = T_pattern_constr (c1, _); _ },
        { body = T_pattern_constr (c2, _); _ } :: t ) ->
        c1 = c2 || check_any p t
    | _, { body = T_pattern_any; _ } :: _ -> true
    | _, { body = T_pattern_var _; _ } :: _ -> true
    | _, [] -> false
    | _ -> assert false
  in
  let is_any (p : typed_pattern) =
    match p.body with
    | T_pattern_any -> true
    | T_pattern_var _ -> true
    | _ -> false
  in
  let make_pattern pattern ty = { body = pattern; ty; loc = dummy_loc } in
  let rec is_useful (pl : typed_pattern list) pll =
    match pl with
    | p :: pt -> (
        let hpl, tpl =
          List.map (function h :: t -> (h, t) | [] -> assert false) pll
          |> List.split
        in
        if List.for_all is_any hpl then is_useful pt tpl
        else
          match p with
          | { body = T_pattern_constr ("SetDelegate", _); _ }
          | { body = T_pattern_constr ("Transfer", _); _ }
          | { body = T_pattern_constr ("TransferTokens", _); _ }
          | { body = T_pattern_constr ("CreateContract", _); _ }
          | { body = T_pattern_constr ("Overflow", _); _ }
          | { body = T_pattern_constr ("Error", _); _ } ->
              List.exists is_any hpl && is_useful pt tpl
          (*| T_pattern_constr (_, []), _ ->
              let pll = List.fold_left (fun s pl -> let l = destruct p pl in
                                         match l with Some l -> l :: s | None -> s) [] pll in
              is_useful pt pll*)
          | { body = T_pattern_constr (_, l); _ } ->
              let pll =
                List.fold_left
                  (fun s pl ->
                    let l = destruct p pl in
                    match l with Some l -> l :: s | None -> s)
                  [] pll
              in
              is_useful (l @ pt) pll
          | {
           body = T_pattern_var _ | T_pattern_any;
           ty = Sort.S_bool | Sort.S_unit;
           _;
          } ->
              is_useful pt pll
          | {
           body = T_pattern_var _ | T_pattern_any;
           ty = Sort.S_list ta as ty;
           _;
          } ->
              let pth = make_pattern T_pattern_any ta in
              let ptt = make_pattern T_pattern_any ty in
              let pa =
                make_pattern (T_pattern_constr ("Cons", [ pth; ptt ])) ty
              in
              let pb = make_pattern (T_pattern_constr ("Nil", [])) ty in
              check_any pa hpl && check_any pb hpl
              && is_useful (pa :: pt) pll
              && is_useful (pb :: pt) pll
          | {
           body = T_pattern_var _ | T_pattern_any;
           ty = Sort.S_pair (ta, tb) as ty;
           _;
          } ->
              let pa = make_pattern T_pattern_any ta in
              let pb = make_pattern T_pattern_any tb in
              let p = make_pattern (T_pattern_constr ("Pair", [ pa; pb ])) ty in
              check_any p hpl && is_useful (p :: pt) pll
          | {
           body = T_pattern_var _ | T_pattern_any;
           ty = Sort.S_or (ta, tb) as ty;
           _;
          } ->
              let pta = make_pattern T_pattern_any ta in
              let ptb = make_pattern T_pattern_any tb in
              let pa = make_pattern (T_pattern_constr ("Left", [ pta ])) ty in
              let pb = make_pattern (T_pattern_constr ("Right", [ ptb ])) ty in
              check_any pa hpl && check_any pb hpl
              && is_useful (pa :: pt) pll
              && is_useful (pb :: pt) pll
          | {
           body = T_pattern_var _ | T_pattern_any;
           ty = Sort.S_option ta as ty;
           _;
          } ->
              let pta = make_pattern T_pattern_any ta in
              let pa = make_pattern (T_pattern_constr ("Some", [ pta ])) ty in
              let pb = make_pattern (T_pattern_constr ("None", [])) ty in
              check_any pa hpl && check_any pb hpl
              && is_useful (pa :: pt) pll
              && is_useful (pb :: pt) pll
          | {
           body = T_pattern_var _ | T_pattern_any;
           ty = Sort.S_contract ta as ty;
           _;
          } ->
              let pta = make_pattern T_pattern_any ta in
              let p =
                make_pattern (T_pattern_constr ("Contract", [ pta ])) ty
              in
              check_any p hpl && is_useful (p :: pt) pll
          | { body = T_pattern_var _ | T_pattern_any; _ } ->
              List.exists is_any hpl && is_useful pt tpl
          (* 型引数が必要な連中はany型引数を実装するときにこのへんもいじる *))
    | [] -> true
  in
  is_useful
    [ make_pattern T_pattern_any (Stdlib.List.hd pl).ty ]
    (List.map (fun p -> [ p ]) pl)
