let read_file filename =
  let lines = ref [] in
  let chan = open_in filename in
  try
    while true do
      lines := input_line chan :: !lines
    done;
    !lines
  with End_of_file ->
    close_in chan;
    List.rev !lines

let execute_z3 solver f =
  let tmpin = Filename.temp_file "tezos-refinement-tmpin-" ".smt2" in
  let tmpout = Filename.temp_file "tezos-refinement-tmpout-" ".smt2" in
  let oc = open_out tmpin in
  Printf.fprintf oc "%s" f;
  close_out oc;
  (* timeout は 60s *)
  (match solver with
  | "z3" ->
      return @@ Unix.system ("z3 -t:60000 " ^ tmpin ^ " > " ^ tmpout ^ " 2>&1")
  | "cvc5" ->
      return
      @@ Unix.system
           ("cvc5 --incremental --strings-exp --tlimit=60000 " ^ tmpin ^ " > "
          ^ tmpout ^ " 2>&1")
  | _ -> failwith "Unsupported solver: %s" solver)
  >>=? fun _ ->
  let out = read_file tmpout in
  (*Sys.remove tmpin ;*)
  (*Sys.remove tmpout ;*)
  if out = [] then
    failwith "Solver returns no output (%s would not be installed)" solver
  else return @@ String.concat "\n" out ^ "\n"

let judge_validity results =
  let strs = String.split '\n' results in
  let open Re in
  let verified =
    alt
      [
        seq [ bol; str "unsat"; eol ]
        (* seq [bol; opt @@ str {|"|}; str "Finding counter-example"]; *);
      ]
    |> compile
  in
  let unverified =
    alt
      [
        seq [ bol; str "sat"; eol ]
        (* seq [bol; str "unsat"; eol]; *)
        (* seq [bol; opt @@ str {|"|}; str "Finding counter-example"]; *);
      ]
    |> compile
  in
  let unknown =
    alt
      [
        seq [ bol; str "unknown"; eol ]
        (* seq [bol; str "sat"; eol]; *)
        (* seq [bol; str "unsat"; eol]; *)
        (* seq [bol; opt @@ str {|"|}; str "Finding counter-example"]; *);
      ]
    |> compile
  in
  if List.exists (execp unknown) strs then "UNKNOWN"
  else if List.exists (execp unverified) strs then "UNVERIFIED"
  else if List.exists (execp verified) strs then "VERIFIED"
  else
    Format.sprintf "UNKNOWN: %s"
      (List.rev strs |> List.hd |> Option.value ~default:"")
