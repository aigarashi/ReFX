{
  let loc lexbuf =
    Lexing.(lexeme_start_p lexbuf, lexeme_end_p lexbuf)

  let char_of_escape c =
    match c with
    | 'n' -> '\n'
    | 't' -> '\t'
    | 'b' -> '\b'
    | 'r' -> '\r'
    | _ -> c

  let char_of_decimal d =
    Char.chr @@ int_of_string d

  let char_of_octal d =
    Char.chr @@ int_of_string @@ "0o" ^ d

  let char_of_hex d =
    Char.chr @@ int_of_string @@ "0x" ^ d
}

let space = ['\t' '\r' ' ']
let symbol_char = ['a'-'z' 'A'-'Z' '0'-'9' '_' '\'']
let symbols = ['!' '$' '%' '&' '*' '+' '-' '.' '/' '<' '=' '>' '?' '@' '^' '_' '~']
let upper_cases = [ 'A'-'Z' ]
let hex_char = ['0'-'9' 'a'-'f' 'A'-'F']
let number_char = ['0'-'9']

rule comment start_p = parse
 "(*" { comment (loc lexbuf :: start_p) lexbuf }
| "*)"
    { match start_p with [_] -> token lexbuf | _ -> comment (Stdlib.List.tl start_p) lexbuf }
| eof
    { Syntax_error.unclosed "(*" (Stdlib.List.hd start_p) "*)" (loc lexbuf) }
| _ { comment start_p lexbuf }
and string start_p s = parse
  '\"'
    { Decoration_parser.STRING s }
| '\\' (['\\' '\"' 'n' 't' 'b' 'r'] as c)
    { string start_p Format.(sprintf "%s%c" s @@ char_of_escape c) lexbuf }
| '\\' ((number_char number_char number_char) as d)
    { string start_p Format.(sprintf "%s%c" s @@ char_of_decimal d) lexbuf }
| "\\o" ((['0'-'7'] ['0'-'7'] ['0'-'7']) as d)
    { string start_p Format.(sprintf "%s%c" s @@ char_of_octal d) lexbuf }
| "\\x" ((hex_char hex_char) as d)
    { string start_p Format.(sprintf "%s%c" s @@ char_of_hex d) lexbuf }
| '\\' (_ as c)
    { Syntax_error.invalid_escape_sequence (loc lexbuf) c }
| eof
    { Syntax_error.unclosed "\"" start_p "\"" (loc lexbuf) }
| _ as c
    { string start_p Format.(sprintf "%s%c" s c) lexbuf }
and token = parse
| "(*" { comment [loc lexbuf] lexbuf }
| '\n' { Lexing.new_line lexbuf; token lexbuf }
| space+ { token lexbuf (* use recursion to ignore *) }
| "bool" { Decoration_parser.BOOL }
| "int" { Decoration_parser.INT }
| "string" { Decoration_parser.STRINGSORT }
| "bytes" { Decoration_parser.BYTESSORT }
| "unit" { Decoration_parser.UNITSORT }
| "nat" { Decoration_parser.NAT }
| "mutez" { Decoration_parser.MUTEZSORT }
| "timestamp" { Decoration_parser.TIMESTAMPSORT }
| "chain_id" { Decoration_parser.CHAINIDSORT }
| "pair" { Decoration_parser.PAIR }
| "or" { Decoration_parser.OR }
| "option" { Decoration_parser.OPTION }
| "list" { Decoration_parser.LIST }
| "operation" { Decoration_parser.OPERATION }
| "key" { Decoration_parser.KEYSORT }
| "signature" { Decoration_parser.SIGNATURESORT }
| "address" { Decoration_parser.ADDRESSSORT }
| "key_hash" { Decoration_parser.KEYHASHSORT }
| "contract" { Decoration_parser.CONTRACTSORT }
| "lambda" { Decoration_parser.FUN }
| "set" { Decoration_parser.SET }
| "map" { Decoration_parser.MAP }
| "big_map" { Decoration_parser.BIGMAP }
| "exception" { Decoration_parser.EXCEPTION }
| "first" { Decoration_parser.FIRST }
| "second" { Decoration_parser.SECOND }
| "Assert" { Decoration_parser.ASSERT }
| "LambdaAnnot" { Decoration_parser.LAMBDAANNOT }
| "ContractAnnot" { Decoration_parser.CONTRACTANNOT }
| "LoopInv" { Decoration_parser.LOOPINV }
| "MapInv" { Decoration_parser.MAPINV }
| "Assume" { Decoration_parser.ASSUME }
| "Measure" { Decoration_parser.MEASURE }

| "if" { Decoration_parser.IF }
| "then" { Decoration_parser.THEN }
| "else" { Decoration_parser.ELSE }
| "match" { Decoration_parser.MATCH }
| "with" { Decoration_parser.WITH }
| "where" { Decoration_parser.WHERE }
| "let" { Decoration_parser.LET }
| "in" { Decoration_parser.IN }
| '(' { Decoration_parser.LPAREN }
| ')' { Decoration_parser.RPAREN }
| '{' { Decoration_parser.LBRACE }
| '}' { Decoration_parser.RBRACE }
| '[' { Decoration_parser.LBRACKET }
| ']' { Decoration_parser.RBRACKET }
| '<' { Decoration_parser.LAPAREN }
| '>' { Decoration_parser.RAPAREN }
| "&&" { Decoration_parser.LAND }
| '&' { Decoration_parser.AND }
| "||" { Decoration_parser.LOR }
| '|' { Decoration_parser.PIPE }
| "::" { Decoration_parser.COLONCOLON }
| ":>" { Decoration_parser.COLONGT }
| ':' { Decoration_parser.COLON }
| ';' { Decoration_parser.SEMI }
| ',' { Decoration_parser.COMMA }
| '.' { Decoration_parser.PERIOD }
| '=' { Decoration_parser.EQ }
| '-' { Decoration_parser.MINUS }
| '_' { Decoration_parser.UNDERBAR }
| "->" { Decoration_parser.RARROW }
| "mod" { Decoration_parser.MOD }
| "land" { Decoration_parser.BAND }
| "lor" { Decoration_parser.BOR }
| "lxor" { Decoration_parser.BXOR }
| "lsl" { Decoration_parser.LSL }
| "lsr" { Decoration_parser.LSR }
| "asr" { Decoration_parser.ASR }
| ['!' '?' '~'] symbols*
    {
      let id = Lexing.lexeme lexbuf in
      Decoration_parser.PrefixOp id
    }
| ['&' '|' '<' '>' '$' '='] symbols*
    {
      let id = Lexing.lexeme lexbuf in
      Decoration_parser.LogicOp id
    }
| ['@' '^'] symbols*
    {
      let id = Lexing.lexeme lexbuf in
      Decoration_parser.ListOp id
    }
| ['+' '-'] symbols*
    {
      let id = Lexing.lexeme lexbuf in
      Decoration_parser.WeakArithOp id
    }
| "**" symbols*
    {
      let id = Lexing.lexeme lexbuf in
      Decoration_parser.PowOp id
    }
| ['*' '/' ] symbols*
    {
      let id = Lexing.lexeme lexbuf in
      Decoration_parser.StrongArithOp id
    }
| '%' ['_' '0'-'9' 'a'-'z' 'A'-'Z'] ['_' '0'-'9' 'a'-'z' 'A'-'Z' '.' '%' '@']* as annot
    {
      Decoration_parser.MichelsonAnnot annot
    }
| "0x" hex_char+ as lexeme {
    let b = Bytes.of_string lexeme in
    Decoration_parser.BYTES (Bytes.sub b 2 (Bytes.length b - 2))
  }
| upper_cases symbol_char* as lexeme {
    Decoration_parser.CONSTRUCTOR lexeme
  }
| symbol_char+ as lexeme {
    match int_of_string_opt lexeme with
    | Some i ->
        Decoration_parser.NUMBER i
    | None ->
        Decoration_parser.SYMBOL lexeme
  }
| '\"'
    { string (loc lexbuf) "" lexbuf }
| eof { Decoration_parser.EOF }

{
  (* This part is inserted into the end of the generated file. *)
}
