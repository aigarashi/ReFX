open Protocol
open Tezos_micheline
open Micheline_parser

type stack_ty

type loc_info = {
  loc : location;  (** コード中の[Micheline.node]の位置 *)
  bef : Script_tc_errors.unparsed_stack_ty option;
      (** [Micheline.node] の前での stack の型 *)
  aft : Script_tc_errors.unparsed_stack_ty option;
      (** [Micheline.node] の後での stack の型 *)
}
(** [Refx/inject_types] で [Micheline.node] に付与する stack の型情報を表すデータ *)

type 'a dec_info = {
  loc : location;
  bef : Script_tc_errors.unparsed_stack_ty;
      (** [Micheline.node] の前での stack の型 *)
  aft : Script_tc_errors.unparsed_stack_ty;
      (** [Micheline.node] の後での stack の型 *)
  bef_dec : 'a list;  (** [Micheline.node] の前での decoration *)
  aft_dec : 'a list;  (** [Micheline.node] の後での decoration *)
}
(** [Refx/inject_decorations] で [Micheline.node] に付与する stack の型と decoration の情報を表すデータ *)
