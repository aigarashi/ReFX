%{
  open Z3_gen
%}

%token <string> SYMBOL
%token <string> STRING
%token LPAREN RPAREN EOF

%start z3exp
%type <Z3_gen.Z3exp.t> z3exp

%%
z3exp:
    Z3exp_elm EOF { $1 }

Z3exp_elm:
    SYMBOL { Z3exp.Symbol $1 }
  | STRING { Z3exp.String $1 }
  | LPAREN RPAREN { Z3exp.List [] }
  | LPAREN Z3exp_list RPAREN { Z3exp.List $2 }

Z3exp_list:
    Z3exp_elm Z3exp_list { $1 :: $2 }
  | Z3exp_elm { $1 :: [] }
