{
}

let space = [' ' '\t' '\n' '\r']

rule main = parse
  space+ { main lexbuf }
| "(" { Parser.LPAREN }
| ")" { Parser.RPAREN }
| '"' [^ '"']* '"' as str { Parser.STRING str }
| [^ '(' ')' '"' ' ' '\t' '\n' '\r']+ as sym { Parser.SYMBOL sym }
| eof { Parser.EOF }
