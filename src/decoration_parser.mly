%{
  open Syntax
  open Syntax_error
  open Sort
  (* This part is inserted into the generated file *)
  (*let failwith = Pervasives.failwith*)

  let make_sort loc body = ignore loc; body
  let make_rtype loc e = { body=e; loc=loc }
  let make_stack loc e = { body=e; loc=loc }
  let make_pattern loc e = { body=e; loc=loc }
  let make_exp loc e = { body=e; loc=loc }
  let dummy_loc = Lexing.dummy_pos, Lexing.dummy_pos

  let mk_measure_def arg_ty p1 e1 p2 e2 : (variable * Sort.t) list * parsed_exp =
    let x = "x" in
    ([(x, arg_ty)], mk_parsed_exp @@ P_exp_match (mk_parsed_exp @@ P_exp_var x, [(p1, e1); (p2, e2)]))
%}

%token COLONCOLON "::"
%token LPAREN "("
%token RPAREN ")"
%token LBRACE "{"
%token RBRACE "}"
%token LBRACKET "["
%token RBRACKET "]"
%token UNDERBAR "_"

%token <string> SYMBOL
%token <string> CONSTRUCTOR
%token <string> STRING
%token <string> MichelsonAnnot
%token <bytes> BYTES
%token <int> NUMBER
%token ASSERT LOOPINV MAPINV LAMBDAANNOT CONTRACTANNOT ASSUME MEASURE
%token LAPAREN RAPAREN RARROW PIPE COLON COLONGT SEMI COMMA PERIOD LOR LAND AND EQ MINUS
%token IF THEN ELSE MATCH WITH WHERE LET IN MOD BAND BOR BXOR LSL LSR ASR
%token FIRST SECOND EOF
%token BOOL INT OPERATION FUN PAIR LIST UNITSORT OR OPTION STRINGSORT BYTESSORT NAT MUTEZSORT TIMESTAMPSORT CHAINIDSORT KEYSORT SIGNATURESORT KEYHASHSORT ADDRESSSORT CONTRACTSORT SET MAP BIGMAP EXCEPTION
%type < Syntax.parsed_decoration > decoration
%start decoration

%token <string> LogicOp
%token <string> ListOp
%token <string> WeakArithOp
%token <string> StrongArithOp
%token <string> PowOp
%token <string> PrefixOp

%nonassoc IN
%nonassoc WITH
%nonassoc ELSE
%left     PIPE
%left     COLON
%right    COMMA
%right    RARROW
%right    LOR
%right    LAND
%left     LogicOp EQ RAPAREN LAPAREN
%right    ListOp
%right    COLONCOLON
%left     WeakArithOp MINUS
%left     StrongArithOp MOD BAND BOR BXOR
%right    PowOp LSL LSR ASR
%nonassoc prec_uminus
%nonassoc PrefixOp

%%

decoration:
  | ASSERT r=rType EOF
    { P_assert r }
  | ASSUME r=rType EOF
    { P_assume r }
  | LOOPINV r=rType EOF
    { P_loop_inv r }
  | MAPINV LPAREN v=SYMBOL COLON s=sort RPAREN r=rType EOF
    { P_map_inv ((v, s, $loc(v)), r) }
  | LAMBDAANNOT arg=rType RARROW res=rType AND exc=rType env=loption(tVars) EOF
    { P_lambda_annot { arg; res; exc; env } }
  | CONTRACTANNOT annot=MichelsonAnnot? arg=rType RARROW res=rType AND exc=rType env=loption(tVars) EOF
    { P_contract_annot ({ arg; res; exc; env }, annot) }
  | MEASURE name=SYMBOL COLON arg_ty=sort RARROW ret_ty=sort WHERE nilarg=pattern EQ nilexp=exp PIPE carg=pattern EQ cexp=exp EOF
    { let (args, body) = mk_measure_def arg_ty nilarg nilexp carg cexp in
      P_measure { name; args; ret_ty; body } }
  | MEASURE SYMBOL COLON sort RARROW sort WHERE error
    { expecting $loc($8) "pattern = exp | pattern = exp" }
  | MEASURE SYMBOL COLON sort RARROW sort error
    { expecting $loc($7) "where" }
  | MEASURE error
    { syntax_error () }
  | MEASURE name=SYMBOL args=binding+ COLON ret_ty=sort EQ body=exp EOF
    { P_measure { name; args; ret_ty; body } }
  | error
    { unknown_annotation () }

binding:
  | LPAREN x=SYMBOL COLON t=sort RPAREN
    { (x, t) }

rType:
  | LBRACE s=stack PIPE e=exp RBRACE
    { make_rtype $loc (s, e) }
  | LBRACE stack PIPE exp error
    { unclosed "{" $loc($1) "}" $loc($5) }
  | error
    { expecting $loc($1) "refinement type { ... }" }

stack:
  | p=pattern COLON s=stack
    { make_stack $loc (P_stack_cons (p, s)) }
  | p=pattern
    { make_stack $loc (P_stack_bottom p) }

tVars:
  | LPAREN RPAREN
    { [] }
  | LPAREN ts=tVarList RPAREN
    { ts }
  | LPAREN tVarList error
    { unclosed "(" $loc($1) ")" $loc($3) }
  | LPAREN error
    { expecting $loc($2) "typed variable list" }

tVarList:
  | v=SYMBOL COLON s=sort
    { [(v, s, $loc(s))] }
  | v=SYMBOL COLON s=sort COMMA tl=tVarList
    { (v, s, $loc(s)) :: tl }
  | SYMBOL COLON error { expecting $loc($3) "type" }

pattern:
  | p1=pattern COMMA p2=pattern
    { make_pattern $loc (P_pattern_constr ("Pair", [], [p1; p2])) }
  | ph=pattern COLONCOLON pt=pattern
    { make_pattern $loc (P_pattern_constr ("Cons", [], [ph; pt])) }
  | c=CONSTRUCTOR s=loption(typeParameter) ps=patternExpr+
    { make_pattern $loc (P_pattern_constr (c, s, ps)) }
  | p=patternExpr
    { p }

patternExpr:
  | v=SYMBOL
    { make_pattern $loc (P_pattern_var v) }
  | c=CONSTRUCTOR s=loption(typeParameter)
    { make_pattern $loc (P_pattern_constr (c, s, [])) }
  | LBRACKET RBRACKET
    { make_pattern $loc (P_pattern_constr ("Nil", [], [])) }
  | LBRACKET l=patternList RBRACKET
    { l }
  | LPAREN p=pattern RPAREN
    { p }
  | UNDERBAR
    { make_pattern $loc P_pattern_any }
  | LBRACKET patternList error
    { unclosed "[" $loc($1) "]" $loc($3) }
  | LPAREN pattern error
    { unclosed "(" $loc($1) ")" $loc($3) }
  | LBRACKET error
    { unclosed "[" $loc($1) "]" $loc($2) }

typeParameter:
  | LAPAREN s=sort RAPAREN
    { [s] }
  | LAPAREN sort error
    { unclosed "<" $loc($1) ">" $loc($3) }
  | LAPAREN error
    { expecting $loc($2) "type" }

patternList:
  | ph=pattern SEMI pt=patternList
    { make_pattern $loc (P_pattern_constr ("Cons", [], [ph; pt])) }
  | p=pattern
    { make_pattern $loc (P_pattern_constr ("Cons", [], [p; make_pattern dummy_loc (P_pattern_constr ("Nil", [], []))])) }

sort:
  | CONTRACTSORT s=sort
    { make_sort $loc (S_contract s) }
  | PAIR s1=sort s2=sort
    { make_sort $loc (S_pair (s1, s2)) }
  | OR s1=sort s2=sort
    { make_sort $loc (S_or (s1, s2)) }
  | OPTION s=sort
    { make_sort $loc (S_option s) }
  | FUN arg_ty=sort ret_ty=sort
    { make_sort $loc (S_lambda (arg_ty, ret_ty)) }
  | LIST s=sort
    { make_sort $loc (S_list s) }
  | SET s=sort
    { make_sort $loc (S_set s) }
  | MAP s1=sort s2=sort
    { make_sort $loc (S_map (s1, s2)) }
  | BIGMAP s1=sort s2=sort
    { make_sort $loc (S_big_map (s1, s2)) }
  | s=atomSort
    { s }

atomSort:
  | BOOL
    { make_sort $loc S_bool }
  | INT
    { make_sort $loc S_int }
  | NAT
    { make_sort $loc S_nat }
  | STRINGSORT
    { make_sort $loc S_string }
  | BYTESSORT
    { make_sort $loc S_bytes }
  | UNITSORT
    { make_sort $loc S_unit }
  | MUTEZSORT
    { make_sort $loc S_mutez }
  | TIMESTAMPSORT
    { make_sort $loc S_timestamp }
  | OPERATION
    { make_sort $loc S_operation }
  | KEYSORT
    { make_sort $loc S_key }
  | KEYHASHSORT
    { make_sort $loc S_key_hash }
  | SIGNATURESORT
    { make_sort $loc S_signature }
  | ADDRESSSORT
    { make_sort $loc S_address }
  | CHAINIDSORT
    { make_sort $loc S_chain_id }
  | EXCEPTION
    { make_sort $loc S_exception }
  | LPAREN s=sort RPAREN
    { s }
  | LPAREN sort error
    { unclosed "(" $loc($1) ")" $loc($3) }


patterns:
| p=pattern RARROW e=exp { [(p, e)] }
| ps=patterns PIPE p=pattern RARROW e=exp { (p, e)::ps }


exp:
  | IF e1=exp THEN e2=exp ELSE e3=exp
    { make_exp $loc (P_exp_if (e1, e2, e3)) }
  | MATCH me=exp WITH option(PIPE) ps=patterns
    { make_exp $loc (P_exp_match (me, List.rev ps)) }
  | LET p=pattern EQ e1=exp IN e2=exp
    { make_exp $loc (P_exp_match (e1, [(p, e2)])) }
  | e1=exp COMMA e2=exp
    { make_exp $loc (P_exp_capp ("Pair", [e1; e2])) }
  | e1=exp op=binop e2=exp
    { make_exp $loc (P_exp_bop (op, e1, e2)) }
  | l=exp COLON s=sort
    { make_exp $loc (P_exp_type_annot (l, s)) }
  | v=SYMBOL COLONGT arg=rType RARROW res=rType AND exc=rType
    { make_exp $loc (P_exp_lambda_annot (v, (arg, res, exc))) }
  | l=exp COLONCOLON r=exp
    { make_exp $loc (P_exp_capp ("Cons", [l; r])) }
  | MINUS e=exp %prec prec_uminus
    { make_exp $loc (P_exp_uop ("-", e)) }
  | op=PrefixOp e=exp
    { make_exp $loc (P_exp_uop (op, e)) }
  | c=CONSTRUCTOR el=atomExp+
    { make_exp $loc (P_exp_capp (c, el)) }
  | f=SYMBOL annot=MichelsonAnnot? el=atomExp+
    { make_exp $loc (P_exp_fapp (f, annot, el)) }
  | f=accessor el=atomExp+
    { make_exp $loc (P_exp_fapp (f, None, el)) }
  | e=atomExp
    { e }

%inline binop:
  | LOR { "||" }
  | LAND { "&&" }
  | LogicOp { $1 }
  | EQ { "=" }
  | LAPAREN { "<" }
  | RAPAREN { ">" }
  | ListOp { $1 }
  | WeakArithOp { $1 }
  | MINUS { "-" }
  | StrongArithOp { $1 }
  | MOD { "mod" }
  | BAND { "land" }
  | BOR { "lor" }
  | BXOR { "lxor" }
  | PowOp { $1 }
  | LSL { "lsl" }
  | LSR { "lsr" }
  | ASR { "asr" }

accessor:
| FIRST { "first" }
| SECOND { "second" }

atomExp:
| e=atomExp PERIOD a=accessor { make_exp $loc (P_exp_fapp (a, None, [e])) }
| CONSTRUCTOR { make_exp $loc (P_exp_capp ($1, [])) }
| SYMBOL { make_exp $loc (P_exp_var ($1)) }
| SYMBOL annot=MichelsonAnnot { make_exp $loc (P_exp_fapp ($1, Some annot, [])) }
| NUMBER { make_exp $loc (P_exp_number ($1)) }
| STRING { make_exp $loc (P_exp_string ($1)) }
| BYTES { make_exp $loc (P_exp_bytes ($1)) }
| LBRACKET RBRACKET { make_exp $loc (P_exp_capp ("Nil", [])) }
| LBRACKET l=listExp RBRACKET { l }
| LPAREN e=exp RPAREN { e }
| LPAREN exp error { unclosed "(" $loc($1) ")" $loc($2) }
| LBRACKET listExp error { unclosed "[" $loc($1) "]" $loc($3) }
| LBRACKET error { unclosed "[" $loc($1) "]" $loc($2) }
| UNDERBAR { not_expecting $loc($1) "_" }

listExp:
| e=exp SEMI l=listExp { make_exp $loc (P_exp_capp ("Cons", [e; l])) }
| e=exp { make_exp $loc (P_exp_capp ("Cons", [e; make_exp dummy_loc (P_exp_capp ("Nil", []))])) }
