let use_z3_tester = true

module Z3exp = struct
  type t = Symbol of string | String of string | List of t list

  let compare = compare

  let rec pp_print fmt (e : t) =
    let open Format in
    match e with
    | Symbol s -> fprintf fmt "%s" s
    | String s -> fprintf fmt "%s" s
    | List l ->
        fprintf fmt "@[<hov 2>(%a)@]"
          (pp_print_list ~pp_sep:pp_print_space pp_print)
          l

  let declare_datatypes name param cstr =
    assert (List.length cstr > 0);
    List
      [
        Symbol "declare-datatypes";
        List
          [ List [ Symbol name; Symbol (List.length param |> string_of_int) ] ];
        List
          [
            (if List.length param > 0 then
               List
                 [
                   Symbol "par";
                   List param;
                   List
                     (List.map
                        (fun (csname, params) ->
                          List
                            (Symbol csname
                            :: List.map
                                 (fun (param, t) -> List [ Symbol param; t ])
                                 params))
                        cstr);
                 ]
             else
               List
                 (List.map
                    (fun (csname, params) ->
                      List
                        (Symbol csname
                        :: List.map
                             (fun (param, t) -> List [ Symbol param; t ])
                             params))
                    cstr));
          ];
      ]

  let call f el = match el with [] -> Symbol f | _ -> List (Symbol f :: el)
end

type env = (string * (string list * Sort.t list * Sort.t)) list

let symbol_of_sort (t : Sort.t) : string =
  [ ("(", "<"); (")", ">"); (" ", "/") ]
  |> List.fold_left
       (fun s (from, by) -> Re.(replace_string (str from |> compile) ~by s))
       (Sort.string_of_sort t)

let of_string (s : string) : Z3exp.t =
  Re.(
    replace (compile any)
      ~f:(fun g ->
        let c = Char.code (Group.get g 0).[0] in
        if c = Char.code '\"' then {|""|}
        else if 0x20 <= c && c <= 0x7e then Format.sprintf "%c" @@ Char.chr c
        else Format.sprintf "\\u00%02x" c)
      s)
  |> fun s -> Z3exp.String ({|"|} ^ s ^ {|"|})

(** [of_sort ty] converts the Logic.Sort [ty] into SMT-LIB sort.  Note that the
   resulted sort often loses information.  So, properly compensate the lost
   information by, for example, using the [is_of_sort] function. *)
let rec of_sort (t : Sort.t) : Z3exp.t =
  match t with
  | S_var _ -> assert false
  | S_unit -> Z3exp.Symbol "Unit"
  | S_bool -> Z3exp.Symbol "Bool"
  | S_int -> Z3exp.Symbol "Int"
  | S_nat -> Z3exp.Symbol "Int"
  | S_string -> Z3exp.Symbol "String"
  | S_chain_id -> Z3exp.Symbol "String"
  | S_bytes -> Z3exp.Symbol "String"
  | S_mutez -> Z3exp.Symbol "Int"
  | S_key_hash -> Z3exp.Symbol "String"
  | S_key -> Z3exp.Symbol "String"
  | S_signature -> Z3exp.Symbol "String"
  | S_timestamp -> Z3exp.Symbol "Int"
  | S_address ->
      Z3exp.(List [ Symbol "Pair"; Symbol "String"; Symbol "String" ])
  | S_option t ->
      let t = of_sort t in
      Z3exp.(List [ Symbol "Option"; t ])
  | S_or (t1, t2) ->
      let t1 = of_sort t1 in
      let t2 = of_sort t2 in
      Z3exp.(List [ Symbol "Or"; t1; t2 ])
  | S_pair (t1, t2) ->
      let t1 = of_sort t1 in
      let t2 = of_sort t2 in
      Z3exp.(List [ Symbol "Pair"; t1; t2 ])
  | S_list t ->
      let t = of_sort t in
      Z3exp.(List [ Symbol "Seq"; t ])
  | S_set t ->
      let t = of_sort t in
      Z3exp.(List [ Symbol "Array"; t; Symbol "Bool" ])
  | S_operation -> Z3exp.Symbol "Operation"
  | S_contract _ ->
      Z3exp.Symbol "Contract"
      (* TODO: should we prepare more precise z3 sort? *)
  | S_lambda _ -> Z3exp.Symbol "Fun"
  | S_map (tk, tv) ->
      let tk = of_sort tk in
      let tv = of_sort tv in
      Z3exp.(List [ Symbol "Array"; tk; List [ Symbol "Option"; tv ] ])
  | S_big_map (tk, tv) ->
      let tk = of_sort tk in
      let tv = of_sort tv in
      Z3exp.(List [ Symbol "Array"; tk; List [ Symbol "Option"; tv ] ])
  | S_exception -> Z3exp.Symbol "Exception"

let of_binding (b : Logic.binding) : Z3exp.t =
  Z3exp.(List (List.map (fun (x, t) -> List [ Symbol x; of_sort t ]) b))

let of_const (c : Logic.const) : Z3exp.t =
  match c with
  | K_sort s -> of_string (Sort.string_of_sort s)
  | K_unit -> Z3exp.Symbol "unit"
  | K_bool b -> Z3exp.Symbol (string_of_bool b)
  | K_int n ->
      if n < 0 then Z3exp.(List [ Symbol "-"; Symbol (string_of_int (-n)) ])
      else Z3exp.Symbol (string_of_int n)
  | K_nat n -> Z3exp.Symbol (string_of_int n)
  | K_string s -> of_string s
  | K_chain_id s -> of_string s
  | K_bytes b ->
      let len = Bytes.length b in
      let rec loop i =
        if i < len then
          "\\u00"
          ^ String.make 1 (Bytes.get b i)
          ^ String.make 1 (Bytes.get b (i + 1))
          ^ loop (i + 2)
        else "\""
      in
      "\"" ^ loop 0 |> fun s -> Z3exp.String s
  | K_mutez n -> Z3exp.Symbol (string_of_int n)
  | K_mutez_max -> Z3exp.Symbol "9223372036854775807" (* 2^63 - 1 *)
  | K_key_hash k -> of_string k
  | K_key k -> of_string k
  | K_signature s -> of_string s
  | K_timestamp n ->
      if n < 0 then Z3exp.List [ Symbol "-"; Symbol (string_of_int (-n)) ]
      else Z3exp.Symbol (string_of_int n)
  | K_empty_set t ->
      let t_set = Sort.S_set t in
      Z3exp.(
        List
          [
            List [ Symbol "as"; Symbol "const"; of_sort t_set ]; Symbol "false";
          ])
  | K_empty_map (tk, tv) ->
      Z3exp.(
        List
          [
            List
              [
                Symbol "as";
                Symbol "const";
                List [ Symbol "Array"; of_sort tk; of_sort (S_option tv) ];
              ];
            List [ Symbol "as"; Symbol "none"; of_sort (S_option tv) ];
          ])
  | K_empty_big_map (tk, tv) ->
      Z3exp.(
        List
          [
            List
              [
                Symbol "as";
                Symbol "const";
                List [ Symbol "Array"; of_sort tk; of_sort (S_option tv) ];
              ];
            List [ Symbol "as"; Symbol "none"; of_sort (S_option tv) ];
          ])

module Z3expSet = Set.Make (Z3exp)

type 'a e_result = ('a * Z3expSet.t) tzresult Lwt.t

let e_return ?(poly_dec : (string * Sort.t list * Sort.t) option) (x : 'a) :
    'a e_result =
  let declare_fun f t_args t_ret =
    Z3exp.(
      List
        [
          Symbol "declare-fun";
          Symbol f;
          List (List.map of_sort t_args);
          of_sort t_ret;
        ])
  in
  let pd =
    match poly_dec with
    | Some (f, t_args, t_ret) -> Z3expSet.singleton (declare_fun f t_args t_ret)
    | None -> Z3expSet.empty
  in
  return (x, pd)

let bind (r : 'a e_result) (f : 'a -> 'b e_result) : 'b e_result =
  r >>=? fun (e, d) ->
  f e >>=? fun (e', d') -> return (e', Z3expSet.union d d')

let ( >>= ) = bind

let map_m (f : 'a -> 'b e_result) (l : 'a list) : 'b list e_result =
  let rec aux l =
    match l with
    | [] -> e_return []
    | h :: t ->
        f h >>= fun h ->
        aux t >>= fun t -> e_return @@ (h :: t)
  in
  aux l

let rec of_exp (exp : Logic.exp) : Z3exp.t e_result =
  match exp.desc with
  | E_symbol (x, _) -> e_return @@ Z3exp.Symbol x
  | E_const c -> e_return @@ of_const c
  | E_data_constr (C_cons, _, el) -> (
      map_m of_exp el >>= function
      | [ e_hd; e_tl ] ->
          e_return
            Z3exp.(
              List [ Symbol "seq.++"; List [ Symbol "seq.unit"; e_hd ]; e_tl ])
      | _ -> assert false)
  | E_data_constr (c, _, el) -> (
      map_m of_exp el >>= fun el ->
      (match c with
      | C_none -> e_return "none"
      | C_some -> e_return "some"
      | C_left -> e_return "left"
      | C_right -> e_return "right"
      | C_pair -> e_return "mk-pair"
      | C_nil -> e_return "seq.empty"
      | C_set_delegate -> e_return "setDelegate"
      | C_transfer -> e_return "transferTokens"
      | C_create_contract -> e_return "createContract"
      | C_contract -> e_return "contract"
      | C_address -> e_return "mk-pair"
      | C_error -> e_return "error"
      | C_overflow -> e_return "overflow"
      | _ -> assert false)
      >>= fun c ->
      let c = Z3exp.(List [ Symbol "as"; Symbol c; of_sort exp.ty ]) in
      match el with [] -> e_return c | _ -> e_return Z3exp.(List (c :: el)))
  | E_data_destr (C_cons, _, e, n) -> (
      of_exp e >>= fun e ->
      match n with
      | 0 -> e_return Z3exp.(List [ Symbol "seq.nth"; e; Symbol "0" ])
      | 1 ->
          e_return
            Z3exp.(
              List
                [
                  Symbol "seq.extract";
                  e;
                  Symbol "1";
                  List [ Symbol "-"; List [ Symbol "seq.len"; e ]; Symbol "1" ];
                ])
      | _ -> assert false)
  | E_data_destr (c, _, e, n) ->
      of_exp e >>= fun e ->
      (match (c, n) with
      | C_some, 0 -> e_return "extractSome"
      | C_left, 0 -> e_return "extractLeft"
      | C_right, 0 -> e_return "extractRight"
      | C_pair, 0 -> e_return "first"
      | C_pair, 1 -> e_return "second"
      | C_set_delegate, 0 -> e_return "delegateAddress"
      | C_transfer, 0 -> e_return "transferData"
      | C_transfer, 1 -> e_return "transferMutez"
      | C_transfer, 2 -> e_return "transferContract"
      | C_create_contract, 0 -> e_return "createContractDelegateAddress"
      | C_create_contract, 1 -> e_return "createContractMutez"
      | C_create_contract, 2 -> e_return "createContractInitStorage"
      | C_create_contract, 3 -> e_return "createContractAddress"
      | C_contract, 0 -> e_return "extractContract"
      | C_address, 0 -> e_return "first"
      | C_address, 1 -> e_return "second"
      | C_error, 0 -> e_return "extractError"
      | _ -> assert false)
      >>= fun c -> e_return Z3exp.(List [ Symbol c; e ])
  | E_data_tester (c, e) ->
      of_exp e >>= fun e ->
      let test x =
        if use_z3_tester then Z3exp.(List [ Symbol ("is-" ^ x); e ])
        else Z3exp.(List [ List [ Symbol "_"; Symbol "is"; Symbol x ]; e ])
      in
      (match c with
      | C_none -> e_return @@ test "none"
      | C_some -> e_return @@ test "some"
      | C_left -> e_return @@ test "left"
      | C_right -> e_return @@ test "right"
      | C_nil ->
          e_return
          @@ Z3exp.(
               List [ Symbol "="; Symbol "0"; List [ Symbol "seq.len"; e ] ])
      | C_cons ->
          e_return
          @@ Z3exp.(
               List [ Symbol "<"; Symbol "0"; List [ Symbol "seq.len"; e ] ])
      | C_set_delegate -> e_return @@ test "setDelegate"
      | C_transfer -> e_return @@ test "transferTokens"
      | C_create_contract -> e_return @@ test "createContract"
      | C_error -> e_return @@ test "error"
      | C_overflow -> e_return @@ test "overflow"
      | _ -> assert false)
      >>= fun e -> e_return e
  | E_forall (b, e) -> (
      let b' = of_binding b in
      of_exp e >>= fun e ->
      match b with
      | [] -> e_return e
      | _ -> e_return Z3exp.(List [ Symbol "forall"; b'; e ]))
  | E_exists (b, e) -> (
      let b' = of_binding b in
      of_exp e >>= fun e ->
      match b with
      | [] -> e_return e
      | _ -> e_return Z3exp.(List [ Symbol "exists"; b'; e ]))
  | E_fapp (f, _, el) -> (
      map_m (fun e -> of_exp e >>= fun e' -> e_return (e', e.ty)) el
      >>= fun etl ->
      match (f, etl) with
      | F_imply, etl -> (
          match List.map fst etl with
          | [ Z3exp.Symbol "true"; e2 ] -> e_return e2
          | [ e1; e2 ] -> e_return Z3exp.(List [ Symbol "=>"; e1; e2 ])
          | _ -> assert false)
      | F_conj, etl -> (
          List.map fst etl
          |> List.filter_map (function
               | Z3exp.Symbol "true" -> None
               | Z3exp.List (Z3exp.Symbol "and" :: el) -> Some el
               | e -> Some [ e ])
          |> List.flatten
          |> fun el ->
          match el with
          | [] -> e_return (Z3exp.Symbol "true")
          | [ e ] -> e_return e
          | _ -> e_return (Z3exp.call "and" el))
      | F_disj, etl -> (
          List.map fst etl
          |> List.filter_map (function
               | Z3exp.Symbol "false" -> None
               | Z3exp.List (Z3exp.Symbol "or" :: el) -> Some el
               | e -> Some [ e ])
          |> List.flatten
          |> fun el ->
          match el with
          | [] -> e_return (Z3exp.Symbol "false")
          | [ e ] -> e_return e
          | _ -> e_return (Z3exp.call "or" el))
      | F_set_add, [ (elt, _); (set, _) ] ->
          e_return Z3exp.(List [ Symbol "store"; set; elt; Symbol "true" ])
      | F_set_remove, [ (elt, _); (set, _) ] ->
          e_return Z3exp.(List [ Symbol "store"; set; elt; Symbol "false" ])
      | F_set_find, [ (elt, _); (set, _) ] ->
          e_return Z3exp.(List [ Symbol "select"; set; elt ])
      | F_map_update, [ (key, _); (uelt, _); (map, _) ] ->
          e_return Z3exp.(List [ Symbol "store"; map; key; uelt ])
      | F_map_find, [ (key, _); (map, _) ] ->
          e_return Z3exp.(List [ Symbol "select"; map; key ])
      | F_big_map_update, [ (key, _); (uelt, _); (map, _) ] ->
          e_return Z3exp.(List [ Symbol "store"; map; key; uelt ])
      | F_big_map_find, [ (key, _); (map, _) ] ->
          e_return Z3exp.(List [ Symbol "select"; map; key ])
      | F_str_substr_opt, [ (s, t_s); (offset, _); (length, _) ] ->
          e_return
            Z3exp.(
              List
                [
                  Symbol "ite";
                  List
                    [
                      Symbol "<=";
                      List [ Symbol "+"; offset; length ];
                      List [ Symbol "str.len"; s ];
                    ];
                  List
                    [
                      Symbol "some";
                      List [ Symbol "str.substr"; s; offset; length ];
                    ];
                  List
                    [ Symbol "as"; Symbol "none"; of_sort Sort.(S_option t_s) ];
                ])
      | F_str_at_opt, [ (s, t_s); (offset, _) ] ->
          e_return
            Z3exp.(
              List
                [
                  Symbol "ite";
                  List [ Symbol "<="; offset; List [ Symbol "str.len"; s ] ];
                  List [ Symbol "some"; List [ Symbol "str.at"; s; offset ] ];
                  List
                    [ Symbol "as"; Symbol "none"; of_sort Sort.(S_option t_s) ];
                ])
      | F_address_of_key_hash, [ (e_key_hash, _) ] ->
          e_return
            Z3exp.(List [ Symbol "mk-pair"; e_key_hash; of_string "%default" ])
      | _ ->
          List.split etl |> fun (el', tl) ->
          let uf f = e_return ~poly_dec:(f, tl, exp.ty) f in
          (match f with
          | F_ite -> e_return "ite"
          | F_neg -> e_return "not"
          | F_eq -> e_return "="
          | F_gt -> e_return ">"
          | F_add -> e_return "+"
          | F_sub -> e_return "-"
          | F_mul -> e_return "*"
          | F_div -> e_return "div"
          | F_mod -> e_return "mod"
          | F_abs -> e_return "abs"
          | F_str_concat -> e_return "str.++"
          | F_str_len -> e_return "str.len"
          | F_str_lt -> e_return "str.<"
          | F_append -> e_return "seq.++"
          | F_size_list -> e_return "seq.len"
          | F_pack -> (
              match tl with
              | [ t ] ->
                  let f = "pack!" ^ symbol_of_sort t in
                  uf f
              | _ -> assert false)
          | F_unpack_opt -> (
              match exp.ty with
              | Sort.S_option t ->
                  let f = "unpack_opt!" ^ symbol_of_sort t in
                  uf f
              | _ -> assert false)
          | F_self_addr -> uf "self_addr"
          | F_balance -> uf "balance"
          | F_amount -> uf "amount"
          | F_now -> uf "now"
          | F_source -> uf "source"
          | F_sender -> uf "sender"
          | F_current_chain_id -> uf "current_chain_id"
          | F_level -> uf "level"
          | F_voting_power -> uf "voting_power"
          | F_total_voting_power -> uf "total_voting_power"
          | F_b58check -> uf "b58check"
          | F_blake2b -> uf "blake2b"
          | F_keccak -> uf "keccak"
          | F_sha256 -> uf "sha256"
          | F_sha512 -> uf "sha512"
          | F_sha3 -> uf "sha3"
          | F_sig -> uf "sig"
          | F_contract_ty_opt -> uf "contract_ty_opt"
          | F_set_choose_opt -> (
              match tl with
              | [ Sort.S_set t_elt ] ->
                  let f = "set_choose_opt!" ^ symbol_of_sort t_elt in
                  uf f
              | _ -> assert false)
          | F_map_choose_opt -> (
              match tl with
              | [ Sort.S_map (t_key, t_val) ] ->
                  let f =
                    "map_choose_opt!" ^ symbol_of_sort t_key ^ "!"
                    ^ symbol_of_sort t_val
                  in
                  uf f
              | _ -> assert false)
          | _ -> assert false)
          >>= fun f -> e_return (Z3exp.call f el'))
  | E_mapp (f, sl, el) ->
      map_m (fun e -> of_exp e >>= fun e' -> e_return (e', e.ty)) el
      >>= fun etl ->
      List.split etl |> fun (el, tl) ->
      let f =
        "|" ^ String.concat "!" (f.ufid :: List.map symbol_of_sort sl) ^ "|"
      in
      e_return ~poly_dec:(f, tl, exp.ty) (Z3exp.call f el)
  | E_lambda (e_f, v_arg, v_ret, v_err, e_pre, e_post, e_excpt) ->
      let q =
        Logic.QLang.(
          forall [ v_arg; v_ret ] (call e_f ?/v_arg ?/v_ret := e_pre := e_post)
          && forall [ v_arg; v_err ]
               (call_err e_f ?/v_arg ?/v_err := e_pre := e_excpt))
      in
      Logic.type_of q >>=? fun q_ty ->
      assert (q_ty = Sort.S_bool);
      of_exp q
  | E_call (e1, e2, e3) -> (
      map_m (fun e -> of_exp e >>= fun e' -> e_return (e', e.ty)) [ e1; e2; e3 ]
      >>= fun etl ->
      List.split etl |> fun (el, tl) ->
      match tl with
      | [ _; t_arg; t_ret ] ->
          let f = "call!" ^ symbol_of_sort t_arg ^ "!" ^ symbol_of_sort t_ret in
          e_return ~poly_dec:(f, tl, Sort.S_bool) Z3exp.(call f el)
      | _ -> assert false)
  | E_call_err (e1, e2, e3) -> (
      map_m (fun e -> of_exp e >>= fun e' -> e_return (e', e.ty)) [ e1; e2; e3 ]
      >>= fun etl ->
      List.split etl |> fun (el, tl) ->
      match tl with
      | [ _; t_arg; _ ] ->
          let f = "call_err!" ^ symbol_of_sort t_arg in
          e_return ~poly_dec:(f, tl, Sort.S_bool) Z3exp.(call f el)
      | _ -> assert false)
  | E_unfold_measure (f, sl, el, e) ->
      map_m (fun e -> of_exp e >>= fun e' -> e_return (e', e.ty)) el
      >>= fun etl ->
      List.split etl |> fun (el, tl) ->
      of_exp e >>= fun e' ->
      let f =
        "|" ^ String.concat "!" (f.ufid :: List.map symbol_of_sort sl) ^ "|"
      in
      e_return ~poly_dec:(f, tl, e.ty)
        Z3exp.(List [ Symbol "="; call f el; e' ])

let preambles =
  let t = Z3exp.Symbol "T" in
  let t1 = Z3exp.Symbol "T1" in
  let t2 = Z3exp.Symbol "T2" in
  Z3exp.
    [
      declare_datatypes "Pair" [ t1; t2 ]
        [ ("mk-pair", [ ("first", t1); ("second", t2) ]) ];
      declare_datatypes "Unit" [] [ ("unit", []) ];
      declare_datatypes "Or" [ t1; t2 ]
        [
          ("left", [ ("extractLeft", t1) ]); ("right", [ ("extractRight", t2) ]);
        ];
      declare_datatypes "Option" [ t ]
        [ ("none", []); ("some", [ ("extractSome", t) ]) ];
      declare_datatypes "Contract" []
        [ ("contract", [ ("extractContract", of_sort Sort.S_address) ]) ];
      List [ Symbol "define-sort"; Symbol "Fun"; List []; Symbol "Int" ];
      declare_datatypes "Operation" []
        [
          ( "setDelegate",
            [ ("delegateAddress", of_sort Sort.(S_option S_key_hash)) ] );
          ( "transferTokens",
            [
              ("transferData", of_sort Sort.S_bytes);
              ("transferMutez", of_sort Sort.S_mutez);
              ( "transferContract",
                of_sort Sort.(S_contract S_unit)
                (* S_unit is a phony argument since [of_sort] ignore it. *) );
            ] );
          ( "createContract",
            [
              ( "createContractDelegateAddress",
                of_sort Sort.(S_option S_key_hash) );
              ("createContractMutez", of_sort Sort.S_mutez);
              ("createContractInitStorage", of_sort Sort.S_bytes);
              ("createContractAddress", of_sort Sort.S_address);
            ] );
        ];
      declare_datatypes "Exception" []
        [
          ("overflow", []); ("error", [ ("extractError", of_sort Sort.S_bytes) ]);
        ];
    ]

let of_constraint (coll_info : Constraint.t) (*: string tzresult Lwt.t*) =
  map_m
    (fun
      ( loc,
        msg_opt,
        env,
        (dec1 : Constraint.condition),
        (dec2 : Constraint.condition) )
    ->
      Format.printf "  Compiling VC...@.";
      assert (List.length dec1.params = List.length dec2.params);
      let new_params =
        List.map
          (fun (_, ty) -> Logic.internal_fresh_var ~x:"result_" ty)
          dec1.params
      in
      Constraint.subst_body
        ~new_params:(List.map Logic.Lang.( ?/ ) new_params)
        dec1
      >>=? fun dec1_body ->
      Constraint.subst_body
        ~new_params:(List.map Logic.Lang.( ?/ ) new_params)
        dec2
      >>=? fun dec2_body ->
      let binding = env @ new_params @ dec1.exists in
      (* We assume asserted conditions have no existential because it comes from
         annotations. *)
      assert (List.length dec2.exists = 0);
      let bound_preds =
        List.map (fun x -> Simplify.is_of_sort (snd x) Logic.Lang.(?/x)) binding
      in
      let vc = Logic.Lang.(conj bound_preds && dec1_body && neg dec2_body) in
      let inst_hyp = Simplify.inst_axiom vc in
      let unfold_measures =
        Simplify.unfold_measures coll_info.measures Logic.Lang.(inst_hyp && vc)
      in
      let vc = Logic.Lang.(conj unfold_measures && inst_hyp && vc) in
      Format.printf "    Type check formula...@.";
      Logic.type_of vc (* >|=? Simplify.inst_lambda *) >>=? fun vc_ty ->
      assert (vc_ty = Sort.S_bool);
      Format.printf "    Translate formula...@.";
      of_exp vc >>= fun c ->
      let binding =
        let module S = Set.Make (struct
          type t = Logic.variable

          let compare = compare
        end) in
        S.of_list binding |> S.elements
      in
      e_return
        Z3exp.(
          List [ Symbol "push"; Symbol "1" ]
          :: List.map
               (fun (x, t) ->
                 List [ Symbol "declare-const"; Symbol x; of_sort t ])
               binding
          @ [
              List [ Symbol "assert"; c ];
              List
                [
                  Symbol "echo";
                  Format.asprintf "@[<h>Finding counter-example%a at %a@]"
                    (Format.pp_print_option (fun fmt s ->
                         Format.fprintf fmt " for %s" s))
                    msg_opt Syntax.pp_location loc
                  |> of_string;
                ];
              List [ Symbol "check-sat" ];
              List [ Symbol "echo"; of_string "START_MODEL" ];
              List [ Symbol "get-model" ];
              List [ Symbol "echo"; of_string "END_MODEL" ];
              List [ Symbol "pop"; Symbol "1" ];
            ]))
  @@ List.sort
       (fun ((l1, _), _, _, _, _) ((l2, _), _, _, _, _) ->
         compare l1.Lexing.pos_lnum l2.Lexing.pos_lnum)
       coll_info.constraints

let of_constraint2 (constraints : Z3exp.t list list) (d1 : Z3expSet.t) :
    string tzresult Lwt.t =
  map_m
    (fun e ->
      Logic.type_of e >>=? fun e_ty ->
      assert (e_ty = Sort.S_bool);
      of_exp e >>= fun e -> e_return @@ Z3exp.(List [ Symbol "assert"; e ]))
    Logic.axiom
  >>=? fun (axioms, d2) ->
  ignore @@ Format.flush_str_formatter ();
  List.iter
    (Format.fprintf Format.str_formatter "@[%a@]@." Z3exp.pp_print)
    ([
       Z3exp.(
         List [ Symbol "set-info"; Symbol ":smt-lib-version"; Symbol "2.6" ]);
       Z3exp.(List [ Symbol "set-logic"; Symbol "ALL" ]);
       Z3exp.(
         List [ Symbol "set-option"; Symbol ":produce-models"; Symbol "true" ]);
       (* Z3exp.( *)
       (*   List *)
       (*     [ *)
       (*       Symbol "set-option"; Symbol ":smtlib2_compliant"; Symbol "true"; *)
       (*     ]); *)
     ]
    @ preambles
    @ Z3expSet.elements (Z3expSet.union d1 d2)
    @ axioms @ List.flatten constraints);
  return @@ Format.flush_str_formatter ()
