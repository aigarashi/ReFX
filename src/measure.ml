open Decoration_def
(* module MapStr = Stdlib.Map.Make (String) *)

type t = measure_annot list

let empty : t = []
let add_measure (m : measure_annot) (t : t) : t = m :: t

let concat_str_list : Logic.msymbol =
  {
    ufid = "concat_str_list";
    t_vars = [];
    t_params = [ Sort.(S_list S_string) ];
    t_ret = Sort.S_string;
  }

let concat_bytes_list : Logic.msymbol =
  {
    ufid = "concat_bytes_list";
    t_vars = [];
    t_params = [ Sort.(S_list S_bytes) ];
    t_ret = Sort.S_bytes;
  }

let size_set : Logic.msymbol =
  {
    ufid = "size_set";
    t_vars = [ "a" ];
    t_params = [ Sort.(S_set (S_var "a")) ];
    t_ret = Sort.S_nat;
  }

let size_map : Logic.msymbol =
  {
    ufid = "size_map";
    t_vars = [ "a"; "b" ];
    t_params = [ Sort.(S_map (S_var "a", S_var "b")) ];
    t_ret = Sort.S_nat;
  }

let init_measures : t =
  let open Logic in
  let open Sort in
  [
    (let ufid = "concat_str_list" in
     let x = fresh_var @@ S_list S_string in
     let h = Lang.(extract_hd ?/x) in
     let t = Lang.(extract_tl ?/x) in
     let f = { ufid; t_vars = []; t_params = [ snd x ]; t_ret = S_string } in
     {
       ufid;
       tyvars = [];
       args = [ x ];
       ret_ty = S_string;
       body =
         Lang.(ite (is_nil ?/x) (c_string "") (concat_str h (mapp f [] [ t ])));
     });
    (let ufid = "concat_bytes_list" in
     let x = fresh_var @@ S_list S_bytes in
     let h = Lang.(extract_hd ?/x) in
     let t = Lang.(extract_tl ?/x) in
     let f = { ufid; t_vars = []; t_params = [ snd x ]; t_ret = S_bytes } in
     {
       ufid;
       tyvars = [];
       args = [ x ];
       ret_ty = S_bytes;
       body =
         Lang.(
           ite (is_nil ?/x) (c_string "") (concat_bytes h (mapp f [] [ t ])));
     });
    (let ufid = "size_set" in
     let a = "a" in
     let x = fresh_var @@ S_set (S_var a) in
     let f = { ufid; t_vars = [ a ]; t_params = [ snd x ]; t_ret = S_nat } in
     {
       ufid;
       tyvars = [ a ];
       args = [ x ];
       ret_ty = S_nat;
       body =
         Lang.(
           ite
             (is_some (choose_opt_set ?/x))
             (c_nat 1
             + mapp f [ S_var a ]
                 [ remove_set (extract_some (choose_opt_set ?/x)) ?/x ])
             (c_nat 0));
     });
    (let ufid = "size_map" in
     let a = "a" and b = "b" in
     let x = fresh_var @@ S_map (S_var a, S_var b) in
     let f = { ufid; t_vars = [ a; b ]; t_params = [ snd x ]; t_ret = S_nat } in
     {
       ufid;
       tyvars = [ a; b ];
       args = [ x ];
       ret_ty = S_nat;
       body =
         Lang.(
           ite
             (is_some (choose_opt_map ?/x))
             (c_nat 1
             + mapp f [ S_var a; S_var b ]
                 [
                   update_map
                     (fst (extract_some (choose_opt_map ?/x)))
                     (none (S_var b)) ?/x;
                 ])
             (c_nat 0));
     });
  ]

(* type t = { *)
(*   list_measures : measure_annot list; *)
(*   set_measures : measure_annot list; *)
(*   map_measures : measure_annot list; *)
(* } *)

(* let empty = {list_measures = []; set_measures = []; map_measures = []} *)

(* let get_subst s1 s2 tyvars = *)
(*   let open Sort in *)
(*   let rec f s1 s2 mp = *)
(*     match (s1, s2) with *)
(*     | (S_pair (sf1, sf2), S_pair (ss1, ss2)) *)
(*     | (S_map (sf1, sf2), S_map (ss1, ss2)) *)
(*     | (S_or (sf1, sf2), S_or (ss1, ss2)) *)
(*     | (S_lambda (sf1, sf2), S_lambda (ss1, ss2)) -> *)
(*         f sf1 ss1 mp >>? fun mp -> f sf2 ss2 mp *)
(*     | (S_option s1, S_option s2) | (S_list s1, S_list s2) | (S_set s1, S_set s2) *)
(*       -> *)
(*         f s1 s2 mp *)
(*     | (S_var tyvar, e) -> ( *)
(*         match MapStr.find_opt tyvar mp with *)
(*         | Some ty when e = ty -> ok mp *)
(*         | Some _ -> error_with "%s is bound by different sorts" tyvar *)
(*         | None -> MapStr.add tyvar e mp |> ok) *)
(*     | (e1, e2) when e1 = e2 -> ok mp *)
(*     | _ -> error_with "error in get_subst" *)
(*   in *)
(*   match f s1 s2 MapStr.empty with *)
(*   | Ok mp -> *)
(*       Some *)
(*         (List.map *)
(*            (fun tyvar -> *)
(*              match MapStr.find_opt tyvar mp with Some ty -> ty | None -> S_int) *)
(*            (\* なんでも良い? *\) *)
(*            tyvars) *)
(*   | Error _ -> None *)

(* let add_measure (m : measure_annot) (measures : t) : t = *)
(*   match m with *)
(*   | {arg = Sort.S_list _; _} -> *)
(*       {measures with list_measures = m :: measures.list_measures} *)
(*   | {arg = Sort.S_set _; _} -> *)
(*       {measures with set_measures = m :: measures.set_measures} *)
(*   | {arg = Sort.S_map _; _} -> *)
(*       {measures with map_measures = m :: measures.map_measures} *)
(*   | _ -> assert false *)

(* let get_domain (ty : Sort.t) (measure : t) : measure_annot list = *)
(*   match ty with *)
(*   | Sort.S_list _ -> measure.list_measures *)
(*   | Sort.S_set _ -> measure.set_measures *)
(*   | Sort.S_map _ -> measure.map_measures *)
(*   | _ -> [] *)

(* let get_measures (ty : Sort.t) (measures : t) : *)
(*     (Sort.t list * measure_annot) list = *)
(*   get_domain ty measures *)
(*   |> List.fold_left *)
(*        (fun s ({tyvars; arg; _} as m) -> *)
(*          match get_subst arg ty tyvars with *)
(*          | Some sorts -> (sorts, m) :: s *)
(*          | None -> s) *)
(*        [] *)

(* let init_measures : measure_annot list = *)
(*   let open Logic in *)
(*   let open Sort in *)
(*   [ *)
(*     (let h = ("h", S_string) and t = ("t", S_list S_string) in *)
(*      { *)
(*        ufid = "concat_str_list"; *)
(*        tyvars = []; *)
(*        arg = S_list S_string; *)
(*        ret = S_string; *)
(*        nilexp = Lang.(mapp "concat_str_list" [] [nil S_string] = E_string ""); *)
(*        consexp = *)
(*          ( "h", *)
(*            "t", *)
(*            Lang.( *)
(*              mapp "concat_str_list" [] [cons ?/h ?/t] *)
(*              = concat_str ?/h (mapp "concat_str_list" [] [?/t])) ); *)
(*        nilid = "fconcat_str_list_nil"; *)
(*        consid = "fconcat_str_list_cons"; *)
(*      }); *)
(*     (let h = ("h", S_bytes) and t = ("t", S_list S_bytes) in *)
(*      { *)
(*        ufid = "concat_bytes_list"; *)
(*        tyvars = []; *)
(*        arg = S_list S_bytes; *)
(*        ret = S_bytes; *)
(*        nilexp = Lang.(mapp "concat_bytes_list" [] [nil S_bytes] = E_string ""); *)
(*        consexp = *)
(*          ( "h", *)
(*            "t", *)
(*            Lang.( *)
(*              mapp "concat_bytes_list" [] [cons ?/h ?/t] *)
(*              = concat_bytes ?/h (mapp "concat_bytes_list" [] [?/t])) ); *)
(*        nilid = "fconcat_bytes_list_nil"; *)
(*        consid = "fconcat_bytes_list_cons"; *)
(*      }); *)
(*     (let a = S_var "a" in *)
(*      let h = ("h", a) and t = ("t", S_set a) in *)
(*      { *)
(*        ufid = "size_set"; *)
(*        tyvars = ["a"]; *)
(*        arg = S_set a; *)
(*        ret = S_nat; *)
(*        nilexp = Lang.(mapp "size_set" [a] [empty_set a] = c_nat 0); *)
(*        consexp = *)
(*          ( "h", *)
(*            "t", *)
(*            Lang.( *)
(*              mapp "size_set" [a] [add_set ?/h ?/t] *)
(*              = c_nat 1 + mapp "size_set" [a] [?/t]) ); *)
(*        nilid = "fsize_set_nil"; *)
(*        consid = "fsize_set_cons"; *)
(*      }); *)
(*     (let a = S_var "a" and b = S_var "b" in *)
(*      let h = ("h", S_pair (a, b)) and t = ("t", S_map (a, b)) in *)
(*      { *)
(*        ufid = "size_map"; *)
(*        tyvars = ["a"; "b"]; *)
(*        arg = S_map (a, b); *)
(*        ret = S_nat; *)
(*        nilexp = Lang.(mapp "size_map" [a; b] [empty_map a b] = c_nat 0); *)
(*        consexp = *)
(*          ( "h", *)
(*            "t", *)
(*            Lang.( *)
(*              mapp "size_map" [a; b] [update_map (fst ?/h) (some (snd ?/h)) ?/t] *)
(*              = c_nat 1 + mapp "size_map" [a; b] [?/t]) ); *)
(*        nilid = "fsize_map_nil"; *)
(*        consid = "fsize_map_cons"; *)
(*      }); *)
(*   ] *)
