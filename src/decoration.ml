open Protocol
open Tezos_micheline
open Micheline
open Node_info
open Decoration_def

let parse_decoration input lc =
  let is_z3str str =
    let rec lparen n =
      if not (n < String.length str) then false
      else
        let c = str.[n] in
        if c = '(' then true
        else if c = ' ' || c = '\n' || c = '\t' || c = '\r' then lparen (n + 1)
        else false
    in
    let rec rparen n =
      if not (n >= 0) then false
      else
        let c = str.[n] in
        if c = ')' then true
        else if c = ' ' || c = '\n' || c = '\t' || c = '\r' then rparen (n - 1)
        else false
    in
    lparen 0 && rparen (String.length str - 1)
  in
  if is_z3str input then return @@ Syntax.Z3Str input
  else
    let lexbuf = Lexing.from_string input in
    let line, col = lc in
    let init_pos =
      { Lexing.pos_lnum = line; pos_cnum = col; pos_bol = 0; pos_fname = "a" }
    in
    let lexbuf =
      { { lexbuf with lex_curr_p = init_pos } with lex_abs_pos = col }
    in
    let lexloc lexbuf = (lexbuf.Lexing.lex_start_p, lexbuf.Lexing.lex_curr_p) in
    try
      return @@ Decoration_parser.decoration Decoration_lexer.token lexbuf
    with
    | Syntax_error.Unclosed (open_pos, open_str, _, close_str) ->
        failwith "Syntax error: %s is expeced. %s on %a might be unmatched"
          close_str open_str Syntax.pp_location open_pos
    | Syntax_error.UnknownAnnotation ->
        let loc = lexloc lexbuf in
        failwith
          "Syntax error: ContractAnnot, LambdaAnnot, Assert, Assume, LoopInv, \
           or Measure are expected on %a"
          Syntax.pp_location loc
    | Syntax_error.Expecting (pos, expecting_str) ->
        failwith "Syntax error: %s is expeced on %a" expecting_str
          Syntax.pp_location pos
    | Syntax_error.NotExpecting (pos, expecting_str) ->
        failwith "Syntax error: %s is not expeced on %a" expecting_str
          Syntax.pp_location pos
    | Syntax_error.Invalid_escape_sequence (l, c) ->
        failwith "Syntax error: invalid escape sequence \"%c\" on %a" c
          Syntax.pp_location l
    | _ ->
        let loc = lexloc lexbuf in
        failwith "Syntax error on %a" Syntax.pp_location loc

let split_decoration :
    string ->
    (string * (int * int * Syntax.parsed_decoration) list) tzresult Lwt.t =
 fun source ->
  (* 文字列中で'<<'と'>>'にマッチしたら、その中を decoration として split して、空白で埋める
      (int * int * decoration) は文字列中の'<<'と'>>'の位置をintとして保存している *)
  let src = Bytes.of_string source in
  let length = Bytes.length src in
  let rec fill_except_newline l r =
    if l < r then (
      if Bytes.get src l = '\n' then () else Bytes.set src l ' ';
      fill_except_newline (l + 1) r)
    else ()
  in
  let rec expr1 (index, line, col) decs =
    if index >= length then return decs
    else if Bytes.get src index = '<' && Bytes.get src (index + 1) = '<' then (
      expr2 (index + 2, line, col + 2)
      >>=? fun (((next_index, _, _) as next_loc), dec) ->
      fill_except_newline index next_index;
      expr1 next_loc (dec :: decs))
    else if Bytes.get src index = '\n' then expr1 (index + 1, line + 1, 0) decs
    else expr1 (index + 1, line, col + 1) decs
  and expr2 (l, line, col) =
    let rec find_right (i, line, col) =
      if Bytes.get src i = '>' && Bytes.get src (i + 1) = '>' then (i, line, col)
      else if Bytes.get src i = '\n' then find_right (i + 1, line + 1, 0)
      else find_right (i + 1, line, col + 1)
    in
    let r, new_line, new_col = find_right (l, line, col) in
    parse_decoration (Bytes.sub_string src l (r - l)) (line, col)
    >>=? fun dec -> return ((r + 2, new_line, new_col + 2), (l, r - l + 4, dec))
  in
  expr1 (0, 1, 0) [] >>=? fun decs -> return (Bytes.to_string src, decs)

let fill_stack_ty program parameter_ty storage_ty =
  let mk_T_operation () =
    strip_locations (Prim (-1, Michelson_v1_primitives.T_operation, [], []))
  in
  let mk_T_pair ty1 ty2 =
    strip_locations
      (Prim (-1, Michelson_v1_primitives.T_pair, [ ty1; ty2 ], []))
  in
  let mk_T_list ty =
    strip_locations (Prim (-1, Michelson_v1_primitives.T_list, [ ty ], []))
  in
  let join ty1 ty2 =
    match (ty1, ty2) with
    | [], [] -> []
    | [], _ -> ty2
    | _, [] -> ty1
    | _ -> ty1
    (* it's better to check if ty1 = ty2 in the last case *)
  in
  let rec aft_ty_of_node
      (node :
        (Syntax.parsed_decoration dec_info, Michelson_v1_primitives.prim) node)
      =
    match node with
    | Seq (info, items) -> (
        match List.rev items with
        | [] -> info.aft
        | node :: _ -> join info.aft (aft_ty_of_node node))
    | Prim (info, _, items, _) ->
        List.map aft_ty_of_node items |> List.fold_left join info.aft
    | Int _ | String _ | Bytes _ -> []
  in
  let restore orig supp =
    match (orig, supp) with [], [] -> [] | [], _ -> supp | _ -> orig
    (* it's better to check if orig = supp in the last case *)
  in
  let rec fill_seq bef aft = function
    | h1 :: h2 :: t ->
        (* Suppose we can get an after stack type in non-tail position *)
        let mid = aft_ty_of_node h1 in
        fill bef mid h1 >>=? fun h ->
        fill_seq mid aft (h2 :: t) >>=? fun t -> return (h :: t)
    | [ h ] -> fill bef aft h >>=? fun node -> return [ node ]
    | [] -> return []
  and fill bef aft
      (node :
        (Syntax.parsed_decoration dec_info, Michelson_v1_primitives.prim) node)
      =
    match node with
    | Seq (info, items) ->
        let bef = restore info.bef bef in
        let aft = restore info.aft aft in
        let info = { info with bef; aft } in
        fill_seq bef aft items >>=? fun items -> return @@ Seq (info, items)
    | Prim (info, name, items, annot) -> (
        let bef = restore info.bef bef in
        let aft = restore info.aft aft in
        let info = { info with bef; aft } in
        let rec pop n l =
          if n > 0 then
            match l with
            | [] -> failwith "Unexpected Error: Fail to fill stack_ty"
            | _ :: tl -> pop (n - 1) tl
          else return l
        in
        let prim bef aft =
          List.map_es (fill bef aft) items >>=? fun items ->
          return @@ Prim (info, name, items, annot)
        in
        let branch_prim bef1 bef2 aft =
          match items with
          | [ br1; br2 ] ->
              fill bef1 aft br1 >>=? fun br1 ->
              fill bef2 aft br2 >>=? fun br2 ->
              return @@ Prim (info, name, [ br1; br2 ], annot)
          | _ -> failwith "Unexpecte Error : Fila to fill stack_ty"
        in
        let get_top_subtype (tys : Script_tc_errors.unparsed_stack_ty) =
          match tys with
          | p :: tl -> (
              let p = root p in
              match p with
              | Prim (_, tag, items, _) -> return (tag, items, tl)
              | _ -> failwith "Unable to get stacktop tyarg")
          | _ -> failwith "Unable to get stacktop type"
        in
        match name with
        | I_DIP ->
            (match items with
            | [ _ ] -> return 1
            | [ Int (_, n); _ ] -> return @@ Z.to_int n
            | _ -> failwith "Unexpected Error : Fail to fill stack_ty for DIP")
            >>=? fun n ->
            pop n bef >>=? fun bef ->
            pop n aft >>=? fun aft -> prim bef aft
        | I_LOOP -> prim aft bef
        | I_ITER -> (
            get_top_subtype bef >>=? function
            | T_set, [ elt_ty ], bef -> prim (strip_locations elt_ty :: bef) aft
            | T_map, [ key_ty; val_ty ], bef ->
                prim (mk_T_pair key_ty val_ty :: bef) aft
            | T_list, [ elt_ty ], bef ->
                prim (strip_locations elt_ty :: bef) aft
            | _ -> failwith "Unexpected Error : Fail to fill stack_ty for ITER")
        | I_IF -> (
            get_top_subtype bef >>=? function
            | T_bool, [], bef -> branch_prim bef bef aft
            | _ -> failwith "Unexpected Error : Fail to fill stack_ty for IF")
        | I_IF_CONS -> (
            get_top_subtype bef >>=? function
            | T_list, [ elt_ty ], bef ->
                branch_prim
                  (strip_locations elt_ty :: mk_T_list elt_ty :: bef)
                  bef aft
            | _ ->
                failwith "Unexpected Error : Fail to fill stack_ty for IF_CONS")
        | I_LOOP_LEFT -> (
            get_top_subtype bef >>=? function
            | T_or, [ tyl; _ ], tl -> prim (strip_locations tyl :: tl) bef
            | _ ->
                failwith
                  "Unexpected Error : Fail to fill stack_ty for LOOP_LEFT")
        | I_IF_LEFT -> (
            get_top_subtype bef >>=? function
            | T_or, [ l_ty; r_ty ], bef ->
                branch_prim
                  (strip_locations l_ty :: bef)
                  (strip_locations r_ty :: bef)
                  aft
            | _ ->
                failwith "Unexpected Error : Fail to fill stack_ty for IF_LEFT")
        | I_IF_NONE -> (
            get_top_subtype bef >>=? function
            | T_option, [ elt_ty ], bef ->
                branch_prim bef (strip_locations elt_ty :: bef) aft
            | _ ->
                failwith "Unexpected Error : Fail to fill stack_ty for IF_NONE")
        | I_LAMBDA -> (
            match items with
            | [ arg; ret; _ ] ->
                prim [ strip_locations arg ] [ strip_locations ret ]
            | _ ->
                failwith "Unexpected Error : Fail to fill stack_ty for LAMBDA")
        | I_MAP -> (
            Lwt_result_syntax.tzboth (get_top_subtype bef) (get_top_subtype aft)
            >>=? function
            | (T_map, [ key_ty; val_ty ], bef), (T_map, [ _; acc_ty ], aft) ->
                prim
                  (mk_T_pair key_ty val_ty :: bef)
                  (strip_locations acc_ty :: aft)
            | (T_list, [ elt_ty ], bef), (T_list, [ acc_ty ], aft) ->
                prim
                  (strip_locations elt_ty :: bef)
                  (strip_locations acc_ty :: aft)
            | _ -> failwith "Unexpected Error : Fail to fill stack_ty for MAP")
        | I_CREATE_CONTRACT -> prim bef aft (* TODO? *)
        | _ -> prim bef aft)
    | Int _ | String _ | Bytes _ -> return node
  in
  let bef =
    [
      mk_T_pair
        (parameter_ty |> strip_locations |> root)
        (storage_ty |> strip_locations |> root);
    ]
  in
  let aft =
    [
      mk_T_pair
        (mk_T_list (mk_T_operation () |> root) |> root)
        (storage_ty |> strip_locations |> root);
    ]
  in
  fill bef aft program

let sort_of_micheline_ty_with_ep ty :
    (Sort.t * (string * Sort.t) list) tzresult Lwt.t =
  let rec aux ty =
    let open Michelson_v1_primitives in
    (match ty with
    | Prim (_, T_int, [], _) -> return (Sort.S_int, [])
    | Prim (_, T_nat, [], _) -> return (Sort.S_nat, [])
    | Prim (_, T_unit, [], _) -> return (Sort.S_unit, [])
    | Prim (_, T_string, [], _) -> return (Sort.S_string, [])
    | Prim (_, T_bytes, [], _) -> return (Sort.S_bytes, [])
    | Prim (_, T_mutez, [], _) -> return (Sort.S_mutez, [])
    | Prim (_, T_bool, [], _) -> return (Sort.S_bool, [])
    | Prim (_, T_key, [], _) -> return (Sort.S_key, [])
    | Prim (_, T_key_hash, [], _) -> return (Sort.S_key_hash, [])
    | Prim (_, T_timestamp, [], _) -> return (Sort.S_timestamp, [])
    | Prim (_, T_address, [], _) -> return (Sort.S_address, [])
    | Prim (_, T_signature, [], _) -> return (Sort.S_signature, [])
    | Prim (_, T_chain_id, [], _) -> return (Sort.S_chain_id, [])
    | Prim (_, T_operation, [], _) -> return (Sort.S_operation, [])
    | Prim (_, T_contract, [ ut ], _) ->
        aux ut >>=? fun (sort, _, ep) -> return (Sort.S_contract sort, ep)
    | Prim (loc, T_pair, utl :: utr, annot) ->
        aux utl >>=? fun (lsort, _, ep1) ->
        (match utr with
        | [ utr ] -> aux utr
        | _ -> aux (Prim (loc, T_pair, utr, annot)))
        >>=? fun (rsort, _, ep2) ->
        return (Sort.S_pair (lsort, rsort), ep1 @ ep2)
    | Prim (_, T_or, [ utl; utr ], _) ->
        aux utl >>=? fun (lsort, e1, ep1) ->
        aux utr >>=? fun (rsort, e2, ep2) ->
        return
          ( Sort.S_or (lsort, rsort),
            Option.to_list e1 @ Option.to_list e2 @ ep1 @ ep2 )
    | Prim (_, T_lambda, [ uta; utr ], _) ->
        aux uta >>=? fun (asort, _, ep1) ->
        aux utr >>=? fun (rsort, _, ep2) ->
        return (Sort.S_lambda (asort, rsort), ep1 @ ep2)
    | Prim (_, T_option, [ ut ], _) ->
        aux ut >>=? fun (sort, _, ep) -> return (Sort.S_option sort, ep)
    | Prim (_, T_list, [ ut ], _) ->
        aux ut >>=? fun (sort, _, ep) -> return (Sort.S_list sort, ep)
    | Prim (_, T_set, [ ut ], _) ->
        aux ut >>=? fun (sort, _, ep) -> return (Sort.S_set sort, ep)
    | Prim (_, T_map, [ uta; utr ], _) ->
        aux uta >>=? fun (asort, _, ep1) ->
        aux utr >>=? fun (rsort, _, ep2) ->
        return (Sort.S_map (asort, rsort), ep1 @ ep2)
    | Prim (_, T_big_map, [ uta; utr ], _) ->
        aux uta >>=? fun (asort, _, ep1) ->
        aux utr >>=? fun (rsort, _, ep2) ->
        return (Sort.S_big_map (asort, rsort), ep1 @ ep2)
    | Prim (_, ty, _, _) ->
        failwith "%s : %s" "t_expr match failure" (string_of_prim ty)
    | _ -> failwith "%s" "t_expr match failure")
    >>=? fun (sort, ep) ->
    let entry_opt =
      match ty with
      | Prim (_, _, _, annots) ->
          annots
          |> List.filter (fun s -> s.[0] = '%')
          |> List.hd
          |> Option.map (fun e -> (e, sort))
      | _ -> None
    in
    return (sort, entry_opt, ep)
  in
  aux ty >>=? fun (sort, _, ep) ->
  if List.exists (fun (e, _) -> e = "%default") ep then return (sort, ep)
  else return (sort, ("%default", sort) :: ep)

let sort_of_micheline_ty ty =
  sort_of_micheline_ty_with_ep ty >>=? fun (sort, _) -> return sort

let rec enum_entrypoints ty =
  let pick_entrypoint annots =
    annots |> List.filter (fun s -> s.[0] = '%') |> List.hd
  in
  let open Michelson_v1_primitives in
  match ty with
  | Prim (_, T_or, [ utl; utr ], annots) -> (
      enum_entrypoints utl
      >>=? List.map_es (fun (ep, f) ->
               return
                 ( ep,
                   fun p ->
                     Syntax.mk_parsed_pattern
                     @@ P_pattern_constr ("Left", [], [ f p ]) ))
      >>=? fun epl ->
      enum_entrypoints utr
      >>=? List.map_es (fun (ep, f) ->
               return
                 ( ep,
                   fun p ->
                     Syntax.mk_parsed_pattern
                     @@ P_pattern_constr ("Right", [], [ f p ]) ))
      >>=? fun epr ->
      match pick_entrypoint annots with
      | Some ep -> return @@ ((ep, Fun.id) :: epl) @ epr
      | None -> return @@ epl @ epr)
  | Prim
      ( _,
        ( T_bool | T_contract | T_int | T_key | T_key_hash | T_lambda | T_list
        | T_map | T_big_map | T_nat | T_option | T_pair | T_set | T_signature
        | T_string | T_bytes | T_mutez | T_timestamp | T_unit | T_operation
        | T_address | T_sapling_transaction | T_sapling_state | T_chain_id
        | T_never | T_bls12_381_g1 | T_bls12_381_g2 | T_bls12_381_fr | T_ticket
          ),
        _,
        annots ) -> (
      match pick_entrypoint annots with
      | Some ep -> return [ (ep, Fun.id) ]
      | None -> return [])
  | _ -> failwith "Invalid form of nodes for types"

(** [transpile_measure_annots decoration] transpile measure
   annotations in [decoration].  A result is a triple, [(rest_decs,
   m_decs, measure_env)], where [rest_decs] is other annotations,
   [m_decs] is translated measure annotations, and [measure_env] is
   the typing environment of the translated measures.  *)
let transpile_measure_annots (decoration : Syntax.parsed_decoration list) :
    (Syntax.parsed_decoration list * measure_annot list * Ty_env.measure_env)
    tzresult
    Lwt.t =
  let aux (rest_decs, m_decs, m_env) = function
    | Syntax.P_measure (d : Syntax.parsed_measure) ->
        Typing.typecheck_measure d m_env >>=? fun (d' : Syntax.typed_measure) ->
        Transpile.transpile_measure d' >>=? fun (d'' : measure_annot) ->
        return
          ( rest_decs,
            d'' :: m_decs,
            Ty_env.add_measure d.name d'.ufid d'.tyvars (List.map snd d'.args)
              d'.ret_ty m_env )
    | d -> return (d :: rest_decs, m_decs, m_env)
  in
  let m_env =
    List.fold_left
      (fun measure_env { ufid; tyvars; args; ret_ty; body = _ } ->
        Ty_env.add_measure ufid ufid tyvars (List.map snd args) ret_ty
          measure_env)
      Ty_env.empty_measure_env Measure.init_measures
  in
  List.fold_left_es aux ([], [], m_env) decoration

let transpile_contract_annots parameter_ty storage_ty measure_env
    (decoration : Syntax.parsed_decoration list) =
  enum_entrypoints parameter_ty >>=? fun entrypoints ->
  sort_of_micheline_ty_with_ep parameter_ty
  >>=? fun (parameter_ty, self_entries) ->
  sort_of_micheline_ty storage_ty >>=? fun storage_ty ->
  let arg_ty = Sort.S_pair (parameter_ty, storage_ty) in
  let res_ty = Sort.(S_pair (S_list S_operation, storage_ty)) in
  let aux
      ( rest_decs,
        ({
           arg = _, w_arg_v, w_arg_p;
           res = _, w_res_v, w_res_p;
           err = _, w_err_v, w_err_p;
           env = w_annot_env;
         } as w_dec),
        w_env,
        w_subst ) = function
    | Syntax.P_contract_annot (d, ep_opt) ->
        (match
           List.assoc ~equal:( = )
             (Option.value ~default:"" ep_opt)
             (("", Fun.id) :: entrypoints)
         with
        | Some mk_ep_pat -> return mk_ep_pat
        | _ ->
            failwith "no entrypoint: %s"
              (Option.value ~default:"%default" ep_opt))
        >>=? fun mk_ep_pat ->
        let arg_stack_pat = fst d.arg.body in
        let arg_pred = snd d.arg.body in
        (match arg_stack_pat.body with
        | Syntax.P_stack_bottom
            {
              body = Syntax.P_pattern_constr ("Pair", [], [ p_param; p_store ]);
              loc;
            } ->
            return
            @@ Syntax.mk_parsed_stack ~loc:arg_stack_pat.loc
            @@ P_stack_bottom
                 (Syntax.mk_parsed_pattern ~loc
                    (P_pattern_constr
                       ("Pair", [], [ mk_ep_pat p_param; p_store ])))
        | Syntax.P_stack_bottom { body = Syntax.P_pattern_any; loc } ->
            return
            @@ Syntax.mk_parsed_stack ~loc:arg_stack_pat.loc
            @@ P_stack_bottom
                 (Syntax.mk_parsed_pattern ~loc
                    (P_pattern_constr
                       ( "Pair",
                         [],
                         [
                           mk_ep_pat @@ Syntax.mk_parsed_pattern P_pattern_any;
                           Syntax.mk_parsed_pattern ~loc P_pattern_any;
                         ] )))
        | _ ->
            failwith
              "%a: Stack pattern for the precondition of ContractAnnot must be \
               of the form '(parameter_pattern, store_pattern)' or '_'"
              Syntax.pp_location d.arg.loc)
        >>=? fun arg_stack_pat ->
        let d =
          { d with arg = { d.arg with body = (arg_stack_pat, arg_pred) } }
        in
        Typing.typecheck_lambda_annot d arg_ty res_ty self_entries measure_env
        >>=? fun (d, env) ->
        Ty_env.disjoint_env w_env env >>=? fun w_env ->
        Transpile.transpile_lambda_annot d
        >>=? fun ( {
                     arg = _, arg_v, arg_p;
                     res = _, res_v, res_p;
                     err = _, err_v, err_p;
                     env = annot_env;
                   },
                   subst ) ->
        let rename =
          Logic.subst_exp
            [
              (arg_v, Logic.Lang.(?/w_arg_v));
              (res_v, Logic.Lang.(?/w_res_v));
              (err_v, Logic.Lang.(?/w_err_v));
            ]
        in
        let arg_p = rename arg_p in
        let res_p = rename res_p in
        let err_p = rename err_p in
        let subst = List.map (fun (v, e) -> (v, rename e)) subst in
        let w_dec =
          {
            arg = (Syntax.dummy_loc, w_arg_v, Logic.Lang.(w_arg_p || arg_p));
            res =
              ( Syntax.dummy_loc,
                w_res_v,
                Logic.Lang.(w_res_p && (arg_p := res_p)) );
            err =
              ( Syntax.dummy_loc,
                w_err_v,
                Logic.Lang.(w_err_p && (arg_p := err_p)) );
            env = w_annot_env @ annot_env;
          }
        in
        return (rest_decs, w_dec, w_env, w_subst @ subst)
    | d -> return (d :: rest_decs, w_dec, w_env, w_subst)
  in
  let arg_v = Logic.fresh_var ~x:"initial_" arg_ty in
  let res_v = Logic.fresh_var ~x:"res_v_" res_ty in
  let err_v = Logic.fresh_var ~x:"err_v_" Sort.S_exception in
  let dec =
    {
      arg = (Syntax.dummy_loc, arg_v, Logic.Lang.c_bool false);
      res = (Syntax.dummy_loc, res_v, Logic.Lang.c_bool true);
      err = (Syntax.dummy_loc, err_v, Logic.Lang.c_bool true);
      env = [];
    }
  in
  List.fold_left_es aux ([], dec, Ty_env.empty_var_env, []) decoration

let transpile_decorations
    (code :
      (Syntax.parsed_decoration dec_info, Michelson_v1_primitives.prim) node)
    parameter_ty storage_ty =
  let sorts_of_stack_ty (stack_ty : Script_tc_errors.unparsed_stack_ty) =
    List.map_es (fun ty -> sort_of_micheline_ty (root ty)) stack_ty
  in
  (match code with
  | Seq (info, items) ->
      transpile_measure_annots info.bef_dec >>=? fun (ds, mds, measure_env) ->
      List.fold_left
        (fun measure_env { ufid; tyvars; args; ret_ty; body = _ } ->
          Ty_env.add_measure ufid ufid tyvars (List.map snd args) ret_ty
            measure_env)
        measure_env Measure.init_measures
      |> fun measure_env ->
      transpile_contract_annots parameter_ty storage_ty measure_env ds
      >>=? fun (ds, cd, var_env, subst) ->
      return
        ( Seq ({ info with bef_dec = ds }, items),
          mds,
          measure_env,
          cd,
          var_env,
          subst )
  | _ -> failwith "Unexpected error: %s" __LOC__)
  >>=? fun (program, mds, measure_env, cd, var_env, subst) ->
  let rec transpile_annots stack ?lambda_arg_ty ?lambda_ret_ty
      (self_entries, env, subst) decorations =
    match decorations with
    | [] -> return ([], None)
    | decoration :: tl ->
        transpile_annots stack ?lambda_arg_ty ?lambda_ret_ty
          (self_entries, env, subst) tl
        >>=? fun (tl, body_env_subst_tl) ->
        (match decoration with
        | Syntax.P_lambda_annot d ->
            (match (lambda_arg_ty, lambda_ret_ty) with
            | Some arg_ty, Some ret_ty ->
                Typing.typecheck_lambda_annot d arg_ty ret_ty [] measure_env
            | _ ->
                failwith
                  "LambdaAnnot must be placed just before LAMBDA instruction")
            >>=? fun (d, env') ->
            Transpile.transpile_lambda_annot d >>=? fun (d, subst) ->
            return (LambdaAnnot d, Some ([], env', subst))
        | Syntax.P_loop_inv d ->
            Typing.typecheck_condition d stack self_entries env measure_env
            >>=? Transpile.transpile_condition subst
            >>=? fun d -> return (LoopInv d, None)
        | Syntax.P_map_inv ((v, vs, loc), ({ body = s, e; _ } as d)) ->
            let s =
              Syntax.(
                mk_parsed_stack
                  (P_stack_cons (mk_parsed_pattern ~loc (P_pattern_var v), s)))
            in
            let body = (s, e) in
            let stack = vs :: stack in
            Typing.typecheck_condition { d with body } stack self_entries env
              measure_env
            >>=? Transpile.transpile_condition subst
            >>=? fun d -> return (MapInv d, None)
        | Syntax.P_assert d ->
            Typing.typecheck_condition d stack self_entries env measure_env
            >>=? Transpile.transpile_condition subst
            >>=? fun d -> return (Assert d, None)
        | Syntax.P_assume d ->
            Typing.typecheck_condition d stack self_entries env measure_env
            >>=? Transpile.transpile_condition subst
            >>=? fun d -> return (Assume d, None)
        | Syntax.P_contract_annot _ ->
            failwith "ContractAnnot must be placed just before code section"
        | Syntax.P_measure _ ->
            failwith "Measure must be placed just before code section"
        | Syntax.Z3Str _ -> failwith "Unexpected error: %s" __LOC__)
        >>=? fun (d, body_env_subst) ->
        (match (body_env_subst, body_env_subst_tl) with
        | None, None -> return None
        | Some x, None -> return @@ Some x
        | None, Some x -> return @@ Some x
        | Some _, Some _ -> failwith "Multiple LambdaAnnot is disallowed")
        >>=? fun body_env_subst -> return (d :: tl, body_env_subst)
  in
  let rec transpile (self_entries, env, subst) node =
    (match node with
    | Prim
        ( _,
          Michelson_v1_primitives.I_LAMBDA,
          [ lambda_arg_ty; lambda_ret_ty; _ ],
          _ ) ->
        sort_of_micheline_ty lambda_arg_ty >>=? fun lambda_arg_ty ->
        sort_of_micheline_ty lambda_ret_ty >>=? fun lambda_ret_ty ->
        return (Some lambda_arg_ty, Some lambda_ret_ty)
    | _ -> return (None, None))
    >>=? fun (lambda_arg_ty, lambda_ret_ty) ->
    let info = location node in
    sorts_of_stack_ty info.bef >>=? fun bef ->
    sorts_of_stack_ty info.aft >>=? fun aft ->
    transpile_annots bef ?lambda_arg_ty ?lambda_ret_ty
      (self_entries, env, subst) info.bef_dec
    >>=? fun (bef_dec, lambda_es) ->
    transpile_annots aft (self_entries, env, subst) info.aft_dec
    >>=? fun (aft_dec, _) ->
    let info = { info with bef_dec; aft_dec } in
    Option.value ~default:(self_entries, env, subst) lambda_es
    |> fun lambda_es ->
    match node with
    | Seq (_, items) ->
        List.map_es (transpile lambda_es) items >>=? fun items ->
        return @@ Seq (info, items)
    | Prim (_, name, items, annot) ->
        List.map_es (transpile lambda_es) items >>=? fun items ->
        return @@ Prim (info, name, items, annot)
    | Int (_, n) -> return @@ Int (info, n)
    | String (_, s) -> return @@ String (info, s)
    | Bytes (_, b) -> return @@ Bytes (info, b)
  in
  sort_of_micheline_ty_with_ep parameter_ty >>=? fun (_, self_entries) ->
  transpile (self_entries, var_env, subst) program >>=? function
  | Seq (info, items) ->
      let mds = List.map (fun m -> Decoration_def.Measure m) mds in
      return
      @@ Seq
           ( { info with bef_dec = (ContractAnnot cd :: mds) @ info.bef_dec },
             items )
  | _ -> failwith "Unexpected error: %s" __LOC__
