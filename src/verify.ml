(** Entrypoint for verification *)

open Tezos_micheline
open Protocol
open Micheline
open Michelson_v1_primitives
open Node_info

let typing_program
    (program :
      ( Decoration_def.decoration dec_info,
        Michelson_v1_primitives.prim )
      Micheline.node) param_ty : Constraint.t tzresult Lwt.t =
  let decs, code = ((Micheline.location program).bef_dec, program) in
  Decoration.sort_of_micheline_ty_with_ep param_ty >>=? fun (_, self_entries) ->
  let coll_info = Constraint.empty self_entries in
  let coll_info =
    List.fold_left Constraint.add_measure coll_info Measure.init_measures
  in
  List.fold_left
    (fun (coll_info, c_annot) -> function
      | Decoration_def.ContractAnnot
          {
            arg = _, argp, arg_body;
            res = res_loc, resp, res_body;
            err = err_loc, errp, err_body;
            env;
          } ->
          let p = Logic.internal_fresh_var ~x:"param_stor_" @@ snd argp in
          let env = argp :: env in
          let params = [ p ] in
          let coll_info =
            {
              coll_info with
              Constraint.axiom =
                Logic.Lang.(
                  conj
                  @@ List.map
                       (fun (e, s) ->
                         contract_ty_opt
                           (create_address
                              (extract_address self_addr)
                              (c_string e))
                         = some (c_sort s))
                       self_entries);
            }
          in
          let body = Logic.Lang.(?/p = ?/argp && arg_body && coll_info.axiom) in
          let coll_info, bef_dec =
            Constraint.mk_condition coll_info env params [] body
          in
          let coll_info, aft_dec =
            Constraint.mk_condition coll_info env [ resp ] [] res_body
          in
          let coll_info, excpt =
            Constraint.mk_exception coll_info env ~params:[ errp ] [] err_body
          in
          (coll_info, Some (env, bef_dec, (aft_dec, res_loc), (excpt, err_loc)))
      | Decoration_def.Measure m -> (Constraint.add_measure coll_info m, c_annot)
      | _ -> (coll_info, c_annot))
    (coll_info, None) decs
  |> function
  | coll_info, Some (env, start_dec, (stop_dec, res_loc), (excpt_annot, err_loc))
    -> (
      Refx.collect env (coll_info, start_dec) code
      >>=? fun (coll_info, j, excpt_opt) ->
      let coll_info =
        match excpt_opt with
        | None ->
            let coll_info, bottom_excpt =
              Constraint.mk_exception coll_info env [] Logic.Lang.(c_bool false)
            in
            Constraint.add_subsume ~msg:"ab-postcondition of ContractAnnot"
              coll_info err_loc env bottom_excpt excpt_annot
        | Some excpt ->
            Constraint.add_subsume ~msg:"ab-postcondition of ContractAnnot"
              coll_info err_loc env excpt excpt_annot
      in
      match j with
      | Typed aft_dec ->
          let coll_info =
            Constraint.add_subsume ~msg:"postcondition of ContractAnnot"
              coll_info res_loc env aft_dec stop_dec
          in
          return coll_info
      | Failed -> return coll_info)
  | _ -> failwith "Contract_Annot is not exists"

let match_program =
  let f (param, store, code) = function
    | Prim (_, K_parameter, [ ty ], _) -> return (Some ty, store, code)
    | Prim (_, K_storage, [ ty ], _) -> return (param, Some ty, code)
    | Prim (info, K_code, [ Seq (_, items) ], _) ->
        return (param, store, Some (Seq (info, items)))
    | _ -> failwith "parse program fail1"
  in
  function
  | Seq (_, items) -> (
      List.fold_left_es f (None, None, None) items >>=? function
      | Some param, Some store, Some code -> return (param, store, code)
      | _ -> failwith "parse program fail")
  | _ -> failwith "parse program fail"

(** 篩型付きのプログラムを、 z3 を用いて検査する *)
let verify_program solver ?logfile_path ?(show_ce = false) prog
    (decs : (int * int * Syntax.parsed_decoration) list) type_map =
  let z3strs, decs =
    List.fold_left
      (fun (strs, decs) -> function
        | _, _, Syntax.Z3Str str -> (str :: strs, decs)
        | dec -> (strs, dec :: decs))
      ([], []) decs
  in
  Format.printf "Injecting decorations...@.";
  Decorator.inject_decorations decs type_map prog >>=? match_program
  >>=? fun (param_ty, storage_ty, code) ->
  Format.printf "Transpiling decorations...@.";
  Decoration.transpile_decorations code param_ty storage_ty >>=? fun code ->
  Format.printf "Generating constraints...@.";
  typing_program code param_ty >>=? fun cstr ->
  Format.printf "Generating SMT inputs...@.";
  Z3_gen.of_constraint cstr >>=? fun (constraints, d1) ->
  Z3_gen.of_constraint2 constraints d1 >>=? fun z3exp ->
  Format.printf "Calling SMT solver %s...@." solver;
  let z3file =
    Format.sprintf "%s%s\n"
      (List.map (fun s -> s ^ "\n") z3strs |> String.concat "")
      z3exp
  in
  Option.iter_s
    (fun logfile_path ->
      (* let z3file = Z3.zip_z3file_z3out z3file results in *)
      Lwt_utils_unix.create_dir logfile_path >>= fun () ->
      (* Z3 file *)
      let smt_filename = logfile_path ^ "/out.smt2" in
      Lwt_utils_unix.create_file smt_filename
        (Printf.sprintf "%s\n" (* (Z3.string_of_z3file z3file) *) z3file))
    logfile_path
  >>= fun _ ->
  Z3.execute_z3 solver z3file >>=? fun results ->
  (* let dump_filename = logfile_path ^ "/dump.txt" in *)
  (* Lwt_utils_unix.create_file dump_filename *)
  (*   (Format.asprintf "%a\n" Printer.print_expr program) *)
  (* >>= fun _ -> *)
  Format.print_string "=========== solver results ===========\n";
  Format.print_string results;
  let decision = Z3.judge_validity results in
  if show_ce && decision = "UNVERIFIED" then
    (* TODO: don't use exception *)
    (try
       Ce.minimize_ce results constraints d1 solver z3strs >>=? fun ce ->
       return (Some ce)
     with Results.Error _ -> return None)
    >>=? fun ce -> return (decision, ce)
  else return (decision, None)

let rec search_unverified_context_continue solver program type_map decs zipper =
  try
    let zipper, zipper', exp =
      Zipper.change_onestep zipper Zipper.search_and_change_continue
    in
    let decs' = Zipper.apply_to_decs decs exp in
    verify_program ~show_ce:false solver program decs' type_map
    >>=? fun (decision, _) ->
    let zipper = if decision = "VERIFIED" then zipper else zipper' in
    Format.printf "\n";
    search_unverified_context_continue solver program type_map decs zipper
  with Zipper.ZipperError (Complete zipper') ->
    Format.print_string "=========== searching completed ===========\n";
    let exp, _, _ = zipper' in
    let decs' = Zipper.apply_to_decs decs exp in
    Format.printf "decoration =@.%a@."
      (Format.pp_print_list Syntax.pp_parsed_decoration)
      (List.map (fun (_, _, d) -> d) decs');
    return_unit

(* 仕様の条件式のスライシング *)
let search_unverified_context solver program type_map
    (decs : (int * int * Syntax.parsed_decoration) list) =
  Format.print_string "=========== searching unverified context ===========\n";
  let zipper = Zipper.contractannot_to_zipper decs in
  let zipper, zipper', exp =
    Zipper.change_onestep zipper Zipper.search_and_change_first
  in
  let decs' = Zipper.apply_to_decs decs exp in
  (* 更新したdecs'でprint_typecheck_resultに相当する操作を行い検証する *)
  verify_program ~show_ce:false solver program decs' type_map
  >>=? fun (decision, _) ->
  (* 検証結果がVERIFIEDならば，変更部分がUNVERIFIEDとなる要因なので，
     zipperの変更部分を更新前に戻す *)
  let zipper = if decision = "VERIFIED" then zipper else zipper' in
  Format.printf "\n";
  search_unverified_context_continue solver program type_map decs zipper

let print_typecheck_result ~solver ?(logfile_path = ".refx") ?(show_ce = false)
    ?(show_slice = false) prog
    (decs : (int * int * Syntax.parsed_decoration) list) type_map =
  verify_program solver ~logfile_path ~show_ce prog decs type_map
  >>=? fun (decision, ce_opt) ->
  (if show_slice && decision = "UNVERIFIED" then
     search_unverified_context solver prog type_map decs
   else return_unit)
  >>=? fun () ->
  Option.iter
    (Format.printf "@.========== counter example ==========@.@[%a@]@."
       Z3_gen.Z3exp.pp_print)
    ce_opt
  |> fun () ->
  Format.printf "@.======== verification result ========@.@[%s@]@." decision;
  return_unit
