type rtype_annot = {
  loc : Syntax.location;
  params : Logic.binding;
  body : Logic.exp;
  nobinds : Logic.exp list;
}

type lambda_annot = {
  arg : Syntax.location * Logic.variable * Logic.exp;
  res : Syntax.location * Logic.variable * Logic.exp;
  err : Syntax.location * Logic.variable * Logic.exp;
  env : Logic.binding;
}

type measure_annot = {
  ufid : string;
  tyvars : string list;
  args : Logic.binding;
  ret_ty : Sort.t;
  body : Logic.exp;
}

(* type measure_condition = { *)
(*   id : string; *)
(*   tyvars : string list; *)
(*   params : Logic.binding; *)
(*   body : Logic.exp; *)
(* } *)

type decoration =
  | Assert of rtype_annot
  | Assume of rtype_annot
  | LoopInv of rtype_annot
  | MapInv of rtype_annot
      (** MapInv records a refinement type of the form `{l :. xTs | exists l'. p}`
          as `params = l' :: l :: xTs; body = p` *)
  | LambdaAnnot of lambda_annot
  | ContractAnnot of lambda_annot
  | Measure of measure_annot
  | Z3Str of string

let pp_rtype_annot fmt (d : rtype_annot) =
  Format.fprintf fmt "@[{loc: %a;@ params: %a;@ body: %a}@]" Syntax.pp_location
    d.loc Logic.pp_binding d.params Logic.pp_exp d.body

let pp_lambda_annot fmt (annot : lambda_annot) =
  let pp_cond fmt (loc, var, pred) =
    Format.fprintf fmt "@[(%a:@ %a,@ %a)@]" Syntax.pp_location loc
      Logic.pp_variable var Logic.pp_exp pred
  in
  Format.fprintf fmt "@[{arg: %a;@ res: %a;@ err: %a}@]" pp_cond annot.arg
    pp_cond annot.res pp_cond annot.err

let pp_measure_annot fmt (annot : measure_annot) =
  Format.fprintf fmt
    "@[{ufid: %s;@ tyvars: %a;@ args: %a;@ ret_ty: %a;@ body: %a}@]" annot.ufid
    (Format.pp_print_list ~pp_sep:Format.pp_print_space Format.pp_print_string)
    annot.tyvars Logic.pp_binding annot.args Sort.pp_sort annot.ret_ty
    Logic.pp_exp annot.body

let pp_decoration fmt = function
  | Assert d -> Format.fprintf fmt "Assert %a" pp_rtype_annot d
  | Assume d -> Format.fprintf fmt "Assume %a" pp_rtype_annot d
  | LoopInv d -> Format.fprintf fmt "LoopInv %a" pp_rtype_annot d
  | MapInv d -> Format.fprintf fmt "MapInv %a" pp_rtype_annot d
  | LambdaAnnot d -> Format.fprintf fmt "@[LambdaAnnot %a@]" pp_lambda_annot d
  | ContractAnnot d ->
      Format.fprintf fmt "@[ContractAnnot %a@]" pp_lambda_annot d
  | Measure d -> Format.fprintf fmt "@[Measure %a@]" pp_measure_annot d
  | Z3Str str -> Format.fprintf fmt "@[Z3Str %s@]" str
