(* モデルの極小化 *)
let minimize_ce results constraints d1 solver z3strs :
    Z3_gen.Z3exp.t tzresult Lwt.t =
  Format.print_string "=========== model minimization ===========\n";
  let assertion =
    "Finding counter-example for postcondition of ContractAnnot at line 0, \
     characters -1--1"
    (*let assertion = "Finding counter-example for Assert at line 7, characters 20-122"*)
  in
  let model = Results.get_model ~ass:assertion results in
  (* 文字列をZ3exp.t型に変換 *)
  let model_z3 = Parser.z3exp Lexer.main (Lexing.from_string model) in
  let c_z3 = Results.to_c model_z3 in
  (* 等式制約集合のソート *)
  let sort_c_z3 c_z3 =
    let compare c1 c2 =
      let is_internal s =
        try String.sub s 0 10 = "_internal_" with Invalid_argument _ -> false
      in
      let is_fresh_binding s =
        try String.sub s 0 15 = "_fresh_binding_"
        with Invalid_argument _ -> false
      in
      let open Z3_gen.Z3exp in
      match (c1, c2) with
      | _, List (Symbol "forall" :: _) -> 1
      | _, List (Symbol "=" :: Symbol s2 :: _) when is_fresh_binding s2 -> 1
      | _, List (Symbol "=" :: Symbol s2 :: _) when is_internal s2 -> (
          match c1 with
          | List (Symbol "=" :: Symbol s1 :: _) when is_fresh_binding s1 -> -1
          | _ -> 1)
      | _, _ -> -1
    in
    List.sort compare c_z3
  in
  let c_z3 = sort_c_z3 c_z3 in
  (*Z3_gen.Z3exp.pp_print Format.std_formatter (Z3_gen.Z3exp.List c_z3) ;
    Format.print_string "\n" ;*)
  let vc = Results.find_vc ~ass:assertion constraints in
  (* ax ∧ constr ∧ spe の充足性を判定 *)
  let ax_constr_spe = Results.ax_constr_spe vc in
  Z3_gen.of_constraint2 [ ax_constr_spe ] d1 >>=? fun z3exp ->
  let z3file =
    Format.sprintf "%s%s\n"
      (List.map (fun s -> s ^ "\n") z3strs |> String.concat "")
      z3exp
  in
  Z3.execute_z3 solver z3file >>=? fun results ->
  (* ケースの分岐 *)
  if Results.check_unsat ~ass:assertion results then (
    (* ケース2: c ∧ not (ax ∧ constr) の充足性を判定することによる極小化 *)
    Format.print_string "ax ∧ constr ∧ spe is unsat";
    let rec minimize2 c minimized =
      match c with
      | [] -> return minimized
      | hd :: tl ->
          let cp = Results.c_not_ax_constr vc (minimized @ tl) in
          Z3_gen.of_constraint2 [ cp ] d1 >>=? fun z3exp ->
          let z3file =
            Format.sprintf "%s%s\n"
              (List.map (fun s -> s ^ "\n") z3strs |> String.concat "")
              z3exp
          in
          Z3.execute_z3 solver z3file >>=? fun results ->
          if Results.check_unsat ~ass:assertion results then
            minimize2 tl minimized
          else
            let minimized = hd :: minimized in
            minimize2 tl minimized
    in
    minimize2 c_z3 [] >>=? fun minimized ->
    let rev_minimized = List.rev minimized in
    return @@ Z3_gen.Z3exp.List rev_minimized)
  else
    (* ケース2: c ∧ ax ∧ constr ∧ spe の充足性を判定することによる極小化 *)
    let rec minimize c minimized =
      match c with
      | [] -> return minimized
      | hd :: tl ->
          let cp = Results.c_ax_constr_spe vc (minimized @ tl) in
          Z3_gen.of_constraint2 [ cp ] d1 >>=? fun z3exp ->
          let z3file =
            Format.sprintf "%s%s\n"
              (List.map (fun s -> s ^ "\n") z3strs |> String.concat "")
              z3exp
          in
          Z3.execute_z3 solver z3file >>=? fun results ->
          if Results.check_unsat ~ass:assertion results then
            minimize tl minimized
          else
            let minimized = hd :: minimized in
            minimize tl minimized
    in
    (* 極小化後のモデルの要素数が1個の場合に，その候補を全て出力する用 *)
    (* let rec minimize_single c minimized =
         match c with
         | [] -> return minimized
         | hd :: tl ->
             let cp = Results.c_ax_constr_spe vc [ hd ] in
             Z3_gen.of_constraint2 [ cp ] d1 >>=? fun z3exp ->
             let z3file =
               Format.sprintf "%s%s\n"
                 (List.map (fun s -> s ^ "\n") z3strs |> String.concat "")
                 z3exp
             in
             Z3.execute_z3 solver z3file >>=? fun results ->
             Format.print_string "z3 " ;
             if Results.check_unsat ~ass:assertion results then
               let minimized = hd :: minimized in
               minimize_single tl minimized
             else minimize_single tl minimized
       in *)
    minimize c_z3 [] >>=? fun minimized ->
    (* (if List.length minimized = 1 then (
        Format.print_string "\nsingle" ;
        minimize_single c_z3 [])
       else return minimized)
       >>=? fun minimized -> *)
    (* 結果を出力 *)
    let rev_minimized = List.rev minimized in
    return @@ Z3_gen.Z3exp.List rev_minimized
