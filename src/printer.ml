open Tezos_micheline
open Micheline
open Protocol
open Node_info

let print_decoration = Syntax.pp_parsed_decoration
let print_ty = Michelson_v1_printer.print_expr_unwrapped

let rec print_stack_ty ppf (s : Script_tc_errors.unparsed_stack_ty) =
  let rec loop ppf = function
    | [] -> ()
    | [ last ] -> Format.fprintf ppf "%a" print_ty last
    | last :: rest -> Format.fprintf ppf "%a : %a" print_ty last loop rest
  in
  match s with
  | [] -> Format.fprintf ppf "[]"
  | sty -> Format.fprintf ppf "[ %a ]" loop sty

(** [(dec_info option, Michelson_v1_primitives.prim) Micheline.node] を print する
    stack の型と decoration が前後両方とも出力される *)
and print_expr ppf = function
  | Prim ({ loc = _; bef; aft; bef_dec; aft_dec }, name, args, _) ->
      Format.fprintf ppf "(<<%a>> %a, %s %a, <<%a>> %a)"
        (Format.pp_print_list
           ~pp_sep:(fun ppf () -> Format.fprintf ppf " ")
           print_decoration)
        bef_dec print_stack_ty bef
        (Michelson_v1_primitives.string_of_prim name)
        (Format.pp_print_list
           ~pp_sep:(fun ppf () -> Format.fprintf ppf " ")
           print_expr)
        args
        (Format.pp_print_list
           ~pp_sep:(fun ppf () -> Format.fprintf ppf " ")
           print_decoration)
        aft_dec print_stack_ty aft
  | Int (_, value) -> Format.fprintf ppf "%s" (Z.to_string value)
  | String (_, value) -> Format.fprintf ppf "%s" value
  | Bytes (_, value) -> Format.fprintf ppf "0x%a" Hex.pp (Hex.of_bytes value)
  | Seq ({ loc = _; bef; aft; bef_dec; aft_dec }, items) ->
      Format.fprintf ppf "(<<%a>> %a, {%a}, <<%a>> %a)"
        (Format.pp_print_list
           ~pp_sep:(fun ppf () -> Format.fprintf ppf " ")
           print_decoration)
        bef_dec print_stack_ty bef
        (Format.pp_print_list
           ~pp_sep:(fun ppf () -> Format.fprintf ppf " ; ")
           print_expr)
        items
        (Format.pp_print_list
           ~pp_sep:(fun ppf () -> Format.fprintf ppf " ")
           print_decoration)
        aft_dec print_stack_ty aft
