open Syntax
module StringMap = Map.Make (String)

type var_env = Sort.t StringMap.t
type measure_env = (string * tyschema) StringMap.t

let builtin_env =
  let open Sort in
  List.fold_left
    (fun env (f, typaram, dom, cod) ->
      StringMap.add f (TySchema (typaram, dom, cod)) env)
    StringMap.empty
    [
      ("not", [], [ S_bool ], S_bool);
      ("get_str_opt", [], [ S_string; S_nat ], S_option S_string);
      ("sub_str_opt", [], [ S_string; S_nat; S_nat ], S_option S_string);
      ("len_str", [], [ S_string ], S_nat);
      ("concat_str", [], [ S_string; S_string ], S_string);
      ("get_bytes_opt", [], [ S_bytes; S_nat ], S_option S_bytes);
      ("sub_bytes_opt", [], [ S_bytes; S_nat; S_nat ], S_option S_bytes);
      ("len_bytes", [], [ S_bytes ], S_nat);
      ("concat_bytes", [], [ S_bytes; S_bytes ], S_bytes);
      ("size_list", [ "T1" ], [ S_list (S_var "T1") ], S_nat);
      ("first", [ "T1"; "T2" ], [ S_pair (S_var "T1", S_var "T2") ], S_var "T1");
      ("second", [ "T1"; "T2" ], [ S_pair (S_var "T1", S_var "T2") ], S_var "T2");
      ( "find_opt",
        [ "T1"; "T2" ],
        [ S_var "T1"; S_map (S_var "T1", S_var "T2") ],
        S_option (S_var "T2") );
      ( "update",
        [ "T1"; "T2" ],
        [ S_var "T1"; S_option (S_var "T2"); S_map (S_var "T1", S_var "T2") ],
        S_map (S_var "T1", S_var "T2") );
      ( "binding_opt",
        [ "T1"; "T2" ],
        [ S_map (S_var "T1", S_var "T2") ],
        S_option (S_pair (S_var "T1", S_var "T2")) );
      ("empty_map", [ "T1"; "T2" ], [], S_map (S_var "T1", S_var "T2"));
      ( "find_opt_b",
        [ "T1"; "T2" ],
        [ S_var "T1"; S_big_map (S_var "T1", S_var "T2") ],
        S_option (S_var "T2") );
      ( "update_b",
        [ "T1"; "T2" ],
        [
          S_var "T1"; S_option (S_var "T2"); S_big_map (S_var "T1", S_var "T2");
        ],
        S_big_map (S_var "T1", S_var "T2") );
      ("empty_big_map", [ "T1"; "T2" ], [], S_big_map (S_var "T1", S_var "T2"));
      ("mem", [ "T" ], [ S_var "T"; S_set (S_var "T") ], S_bool);
      ("add", [ "T" ], [ S_var "T"; S_set (S_var "T") ], S_set (S_var "T"));
      ("remove", [ "T" ], [ S_var "T"; S_set (S_var "T") ], S_set (S_var "T"));
      ("choose_opt", [ "T" ], [ S_set (S_var "T") ], S_option (S_var "T"));
      ("empty_set", [ "T" ], [], S_set (S_var "T"));
      ("max_mutez", [], [], S_mutez);
      ("source", [], [], S_address);
      ("sender", [], [], S_address);
      ("now", [], [], S_timestamp);
      ("self_addr", [], [], S_address);
      ("balance", [], [], S_mutez);
      ("amount", [], [], S_mutez);
      ("chain_id", [], [], S_chain_id);
      ("level", [], [], S_nat);
      ("voting_power", [], [ S_key_hash ], S_nat);
      ("total_voting_power", [], [], S_nat);
      ("pack", [ "T" ], [ S_var "T" ], S_bytes);
      ("unpack_opt", [ "T" ], [ S_bytes ], S_option (S_var "T"));
      ("contract_opt", [ "T" ], [ S_address ], S_option (S_contract (S_var "T")));
      ("implicit_account", [], [ S_key_hash ], S_contract S_unit);
      ( "call",
        [ "T1"; "T2" ],
        [ S_lambda (S_var "T1", S_var "T2"); S_var "T1"; S_var "T2" ],
        S_bool );
      ("hash", [], [ S_key ], S_key_hash);
      ("blake2b", [], [ S_bytes ], S_bytes);
      ("keccak", [], [ S_bytes ], S_bytes);
      ("sha256", [], [ S_bytes ], S_bytes);
      ("sha512", [], [ S_bytes ], S_bytes);
      ("sha3", [], [ S_bytes ], S_bytes);
      ("sig", [], [ S_key; S_signature; S_bytes ], S_bool);
      ("abs", [], [ S_int ], S_nat);
    ]

let empty_var_env = StringMap.empty
let empty_measure_env = StringMap.empty

let singleton_var (var : variable) (t : Sort.t) : var_env =
  StringMap.singleton var t

let singleton_measure (var : variable) (uid : variable) (tyvars : string list)
    (arg_tys : Sort.t list) (ret_ty : Sort.t) : measure_env =
  StringMap.singleton var (uid, TySchema (tyvars, arg_tys, ret_ty))

let add_var (var : variable) (t : Sort.t) (env : var_env) : var_env =
  StringMap.add var t env

let remove_var (var : variable) (env : var_env) : var_env =
  StringMap.remove var env

let add_measure (var : variable) (uid : variable) (tyvars : string list)
    (arg_tys : Sort.t list) (ret_ty : Sort.t) (env : measure_env) : measure_env
    =
  StringMap.add var (uid, TySchema (tyvars, arg_tys, ret_ty)) env

type binding =
  | B_none
  | B_var of Sort.t
  | B_measure of variable * string list * Sort.t list * Sort.t
  | B_fsymbol of string list * Sort.t list * Sort.t

let find (x : variable) (var_env : var_env) (measure_env : measure_env) :
    binding =
  match StringMap.find x var_env with
  | Some t -> B_var t
  | None -> (
      match StringMap.find x measure_env with
      | Some (f, TySchema (vars, param_tys, ty)) ->
          B_measure (f, vars, param_tys, ty)
      | None -> (
          match StringMap.find x builtin_env with
          | Some (TySchema (vars, param_tys, ty)) ->
              B_fsymbol (vars, param_tys, ty)
          | None ->
              B_none
              (* (
                 * match (x, env.self) with ("self", Some ty) -> B_var ty | _ -> B_none ) *)
          ))

(** [disjoint_env env1 env2] merges the two environments [env1] [env2].
   If binding variable occurs in the both environemnts, operation
   fails. *)
let disjoint_env ?loc (env1 : var_env) (env2 : var_env) : var_env tzresult Lwt.t
    =
  let merge m1 m2 =
    StringMap.union
      (fun k s _ ->
        if k = "self" then Some s
        else
          Stdlib.failwith
          @@
          match loc with
          | Some loc ->
              Format.asprintf "Variable %s is bound several times in %a" k
                pp_location loc
          | None -> Format.asprintf "Variable %s is bound several times" k)
      m1 m2
  in
  try return @@ merge env1 env2 with Failure s -> failwith "%s" s

(** [append_env env1 env2] appends the environment [env1] to [env2].
   If binding variable occurs in the both environments.  One of [env1]
   shadows [env2].  *)
let append_env (env1 : var_env) (env2 : var_env) : var_env =
  let append m1 m2 = StringMap.union (fun _ v _ -> Some v) m1 m2 in
  append env1 env2

let map_env (f : Sort.t -> Sort.t) (env : var_env) = StringMap.map f env
