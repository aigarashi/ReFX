exception Unexpected_value
exception Error of string

(* モデルの極小化のための諸関数 *)

let hd_tl l = match l with [] -> raise Unexpected_value | hd :: tl -> (hd, tl)
let get_hd l = match l with [] -> raise Unexpected_value | hd :: _ -> hd
let skip_hd l = match l with [] -> raise Unexpected_value | _ :: tl -> tl
let split res = String.split '\n' res

let rec collect (spl : string list) : string list =
  match spl with
  | "START_MODEL" :: tl ->
      let rec collect_aux tl l =
        match tl with
        | "END_MODEL" :: _ -> List.rev l
        | hd :: tl -> collect_aux tl (hd :: l)
        | _ -> raise Unexpected_value
      in
      collect_aux tl []
  | _ :: tl -> collect tl
  | _ -> raise Unexpected_value

let rec get_model_list
    ?(ass : string =
      "Finding counter-example for postcondition of ContractAnnot at line 0, \
       characters -1--1") (spl : string list) : string list =
  match spl with
  | [] -> raise (Error "The assertion was not found")
  | hd :: tl ->
      if hd = ass then
        match tl with
        | "sat" :: tl' -> collect tl'
        | "unsat" :: _ -> raise (Error "Solver results is unsat")
        | _ -> raise Unexpected_value
      else get_model_list ~ass tl

(* resに含まれるモデルの文字列を抽出 *)
let get_model
    ?(ass : string =
      "Finding counter-example for postcondition of ContractAnnot at line 0, \
       characters -1--1") (res : string) : string =
  let spl = split res in
  let model_list = get_model_list ~ass spl in
  String.concat "" model_list

let append_z3exp (aexp : Z3_gen.Z3exp.t) (bexp : Z3_gen.Z3exp.t) :
    Z3_gen.Z3exp.t =
  match (aexp, bexp) with
  | Symbol _, Symbol _
  | Symbol _, String _
  | String _, Symbol _
  | String _, String _ ->
      List [ aexp; bexp ]
  | Symbol _, List lb | String _, List lb -> List (aexp :: lb)
  | List la, Symbol _ | List la, String _ -> List (List.append la [ bexp ])
  | List la, List lb -> List (List.append la lb)

(* 変数束縛を等式制約に変換 *)
let to_eq (l : Z3_gen.Z3exp.t list) : Z3_gen.Z3exp.t list =
  let remove_df =
    match l with Symbol "define-fun" :: tl -> tl | _ -> raise Unexpected_value
  in
  let vn, remove_vn = hd_tl remove_df in
  let it, remove_it = hd_tl remove_vn in
  match it with
  | Symbol _ | String _ -> raise Unexpected_value
  | List [] ->
      let remove_ot = skip_hd remove_it in
      let ov = get_hd remove_ot in
      [ Symbol "="; vn; ov ]
  | List l ->
      let rec extract_x (l : Z3_gen.Z3exp.t list) (exp : Z3_gen.Z3exp.t) =
        match l with
        | [] -> exp
        | List (ix :: _) :: tl -> extract_x tl (append_z3exp exp ix)
        | _ -> raise Unexpected_value
      in
      let ix = extract_x l (List []) in
      let remove_ot = skip_hd remove_it in
      let ov = get_hd remove_ot in
      [ Symbol "forall"; it; List [ Symbol "="; append_z3exp vn ix; ov ] ]

(* モデルを等式制約集合に変換 *)
let to_c (exp : Z3_gen.Z3exp.t) : Z3_gen.Z3exp.t list =
  match exp with
  | Symbol _ | String _ -> raise Unexpected_value
  | List l ->
      let rec to_c_aux (l : Z3_gen.Z3exp.t list) : Z3_gen.Z3exp.t list =
        match l with
        | [] -> []
        | Symbol _ :: _ | String _ :: _ -> raise Unexpected_value
        | List l :: tl -> List (to_eq l) :: to_c_aux tl
      in
      to_c_aux l

(* 指定された注釈に対応する検証条件 *)
let find_vc
    ?(ass =
      "Finding counter-example for postcondition of ContractAnnot at line 0, \
       characters -1--1") (l : Z3_gen.Z3exp.t trace trace) :
    Z3_gen.Z3exp.t trace =
  Option.value
    (List.find
       (fun l ->
         List.mem ~equal:( = )
           (Z3_gen.Z3exp.List [ Symbol "echo"; Z3_gen.of_string ass ])
           l)
       l)
    ~default:[]

(* ax ∧ constr ∧ spe を生成 *)
let ax_constr_spe (vc : Z3_gen.Z3exp.t list) : Z3_gen.Z3exp.t list =
  let rec ax_constr_spe_aux = function
    | [] -> raise Unexpected_value
    | [ last ] -> (
        match last with
        | Z3_gen.Z3exp.List [ Symbol "not"; tl ] -> [ tl ]
        | _ -> raise Unexpected_value)
    | eq :: tl -> eq :: ax_constr_spe_aux tl
  in
  List.map
    (function
      | Z3_gen.Z3exp.List [ Symbol "assert"; List (Symbol "and" :: conj) ] ->
          Z3_gen.Z3exp.List
            [ Symbol "assert"; List (Symbol "and" :: ax_constr_spe_aux conj) ]
      | Z3_gen.Z3exp.List (Symbol "assert" :: _) -> raise Unexpected_value
      | Z3_gen.Z3exp.List _ as exp -> exp
      | _ -> raise Unexpected_value)
    vc

(*  c ∧ ax ∧ constr ∧ spe を生成 *)
let c_ax_constr_spe (vc : Z3_gen.Z3exp.t list) (c : Z3_gen.Z3exp.t list) :
    Z3_gen.Z3exp.t list =
  let rec c_ax_constr_spe_aux = function
    | [] -> raise Unexpected_value
    | [ last ] -> (
        match last with
        | Z3_gen.Z3exp.List [ Symbol "not"; tl ] -> tl :: c
        | _ -> raise Unexpected_value)
    | eq :: tl -> eq :: c_ax_constr_spe_aux tl
  in
  List.map
    (function
      | Z3_gen.Z3exp.List [ Symbol "assert"; List (Symbol "and" :: conj) ] ->
          Z3_gen.Z3exp.List
            [ Symbol "assert"; List (Symbol "and" :: c_ax_constr_spe_aux conj) ]
      | Z3_gen.Z3exp.List (Symbol "assert" :: _) -> raise Unexpected_value
      | Z3_gen.Z3exp.List _ as exp -> exp
      | _ -> raise Unexpected_value)
    vc

(* c ∧ not (ax ∧ constr) を生成 *)
let c_not_ax_constr (vc : Z3_gen.Z3exp.t list) (c : Z3_gen.Z3exp.t list) :
    Z3_gen.Z3exp.t list =
  let rec c_not_ax_constr_aux = function
    | [] -> raise Unexpected_value
    | [ last ] -> (
        match last with
        | Z3_gen.Z3exp.List [ Symbol "not"; _ ] -> []
        | _ -> raise Unexpected_value)
    | eq :: tl -> eq :: c_not_ax_constr_aux tl
  in
  List.map
    (function
      | Z3_gen.Z3exp.List [ Symbol "assert"; List (Symbol "and" :: conj) ] ->
          Z3_gen.Z3exp.List
            [
              Symbol "assert";
              List
                (Symbol "and"
                :: List
                     [
                       Symbol "not";
                       List (Symbol "and" :: c_not_ax_constr_aux conj);
                     ]
                :: c);
            ]
      | Z3_gen.Z3exp.List (Symbol "assert" :: _) -> raise Unexpected_value
      | Z3_gen.Z3exp.List _ as exp -> exp
      | _ -> raise Unexpected_value)
    vc

(* res中のZ3による判定結果を返す *)
let check_unsat
    ?(ass : string =
      "Finding counter-example for postcondition of ContractAnnot at line 0, \
       characters -1--1") (res : string) : bool =
  let spl = split res in
  let rec check_unsat_aux = function
    | [] -> raise Unexpected_value
    | hd :: tl ->
        if hd = ass then
          match tl with
          | "unsat" :: _ -> true
          | "sat" :: _ -> false
          | _ -> raise Unexpected_value
        else check_unsat_aux tl
  in
  check_unsat_aux spl

(* Z3exp.t型の出力用 *)
let rec print_z3explist l =
  match l with
  | [] -> ()
  | hd :: tl ->
      Z3_gen.Z3exp.pp_print Format.std_formatter hd;
      Format.print_string "\n";
      print_z3explist tl
