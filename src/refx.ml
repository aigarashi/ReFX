(** Inference of Refinement Type *)

open Protocol
open Alpha_context
open Tezos_micheline
open Micheline
open Michelson_v1_primitives
open Node_info

let parse_int = function
  | Int (_, n') -> return @@ Z.to_int n'
  | _ -> failwith "%s" "head fail in parse_int function"

let match_cons = function
  | h :: t -> return (h, t)
  | _ -> failwith "%s" "head fail in match_cons function"

let match_singleton_list = function
  | [ h ] -> return h
  | _ -> failwith "%s" "match fail in match_singleton_list function"

let pop_n_stack (n : Z.t) (l : 'a list) : ('a list * 'a list) tzresult Lwt.t =
  let rec iter l1 l2 z =
    match Z.(compare z zero) with
    | 0 -> return (List.rev l1, l2)
    | 1 -> match_cons l2 >>=? fun (h, t) -> iter (h :: l1) t Z.(pred z)
    | _ -> failwith "n must be a natural number for pop_n_stack"
  in
  iter [] l n

let body (c : Constraint.condition) : Logic.exp = c.body
let sort_of_micheline_ty s = Decoration.sort_of_micheline_ty s

let rec micheline_value ty n : Logic.exp tzresult Lwt.t =
  match (ty, n) with
  | Sort.S_unit, Prim (_, D_Unit, _, _) -> return Logic.Lang.c_unit
  | Sort.S_bool, Prim (_, D_True, _, _) -> return Logic.Lang.(c_bool true)
  | Sort.S_bool, Prim (_, D_False, _, _) -> return Logic.Lang.(c_bool false)
  | Sort.S_int, Int (_, value) -> return @@ Logic.Lang.(c_int (Z.to_int value))
  | Sort.S_nat, Int (_, value) -> return @@ Logic.Lang.(c_nat (Z.to_int value))
  | Sort.S_string, String (_, value) -> return @@ Logic.Lang.(c_string value)
  | Sort.S_bytes, Bytes (_, value) ->
      return
      @@ Logic.Lang.(
           c_bytes
             (let (`Hex s) = Hex.of_bytes value in
              Bytes.of_string s))
  | Sort.S_mutez, Int (_, value) ->
      return @@ Logic.Lang.(c_mutez (Z.to_int value))
  | Sort.S_timestamp, Int (_, value) ->
      return Logic.Lang.(c_timestamp (Z.to_int value))
  | Sort.S_timestamp, String (_, value) -> (
      match Script_timestamp.of_string value with
      | Some v ->
          return
            Logic.Lang.(c_timestamp (Script_timestamp.to_zint v |> Z.to_int))
      | None -> failwith "timestamp parse error in PUSH")
  | Sort.S_address, String (_, value) ->
      let addr, entry =
        match String.index_opt value '%' with
        | None -> (value, "%default")
        | Some pos ->
            let len = String.length value - pos in
            let addr = String.sub value 0 pos in
            let entry =
              match String.sub value pos len with "%" -> "%default" | s -> s
            in
            (addr, entry)
      in
      return Logic.Lang.(create_address (c_string addr) (c_string entry))
  | Sort.S_address, Bytes (_, value) -> (
      match
        Data_encoding.Binary.of_bytes_opt
          Data_encoding.(tup2 Contract.encoding Variable.string)
          value
      with
      | Some (v, entry) ->
          let str = Contract.to_b58check v in
          let entry = match entry with "" -> "%default" | s -> "%" ^ s in
          return Logic.Lang.(create_address (c_string str) (c_string entry))
      | None -> failwith "address parse error in PUSH")
  | Sort.S_key_hash, String (_, value) -> return Logic.Lang.(c_key_hash value)
  | Sort.S_key_hash, Bytes (_, value) -> (
      match Data_encoding.Binary.of_bytes_opt Contract.encoding value with
      | Some v ->
          let str = Contract.to_b58check v in
          return Logic.Lang.(c_key_hash str)
      | None -> failwith "address parse error in PUSH")
  | Sort.S_key, String (_, value) -> return Logic.Lang.(c_key value)
  | Sort.S_key, Bytes (_, value) -> (
      match
        Data_encoding.Binary.of_bytes_opt Signature.Public_key.encoding value
      with
      | Some v ->
          let str = Signature.Public_key.to_b58check v in
          return Logic.Lang.(c_key str)
      | None -> failwith "key parse error in PUSH")
  | Sort.S_signature, String (_, value) -> (
      match Signature.of_b58check_opt value with
      | Some v ->
          Syntax.normalize_signature v >>=? fun str ->
          return Logic.Lang.(c_signature str)
      | None -> failwith "signature parse error in PUSH")
  | Sort.S_signature, Bytes (_, value) -> (
      match Data_encoding.Binary.of_bytes_opt Signature.encoding value with
      | Some v ->
          let str = Signature.to_b58check v in
          return Logic.Lang.(c_signature str)
      | None -> failwith "signature parse error in PUSH")
  | Sort.S_chain_id, String (_, value) -> return Logic.Lang.(c_chain_id value)
  | Sort.S_chain_id, Bytes (_, value) -> (
      match Data_encoding.Binary.of_bytes_opt Chain_id.encoding value with
      | Some v ->
          let str = Chain_id.to_b58check v in
          print_string str;
          return Logic.Lang.(c_chain_id str)
      | None -> failwith "chain_id parse error in PUSH")
  | Sort.S_pair (ty1, ty2), Prim (_, D_Pair, [ v1; v2 ], _) ->
      micheline_value ty1 v1 >>=? fun r1 ->
      micheline_value ty2 v2 >>=? fun r2 -> return @@ Logic.Lang.(pair r1 r2)
  | Sort.S_or (tyl, tyr), Prim (_, D_Left, [ v ], _) ->
      micheline_value tyl v >>=? fun r -> return @@ Logic.Lang.(left tyr r)
  | Sort.S_or (tyl, tyr), Prim (_, D_Right, [ v ], _) ->
      micheline_value tyr v >>=? fun r -> return @@ Logic.Lang.(right tyl r)
  | Sort.S_option ty, Prim (_, D_Some, [ v ], _) ->
      micheline_value ty v >>=? fun r -> return @@ Logic.Lang.(some r)
  | Sort.S_option ty, Prim (_, D_None, [], _) -> return Logic.Lang.(none ty)
  | Sort.S_list ty, Seq (_, items) ->
      List.fold_right_es
        (fun v r_tl ->
          micheline_value ty v >>=? fun r_hd ->
          return Logic.Lang.(cons r_hd r_tl))
        items
        Logic.Lang.(nil ty)
  | Sort.S_set ty, Seq (_, items) ->
      List.fold_left_es
        (fun v_set m_elt ->
          micheline_value ty m_elt >>=? fun v_elt ->
          return Logic.Lang.(add_set v_elt v_set))
        Logic.Lang.(empty_set ty)
        items
  | Sort.S_map (ty_k, ty_v), Seq (_, items) ->
      List.fold_left_es
        (fun v_map m_elt ->
          match m_elt with
          | Prim (_, D_Elt, [ m_k; m_v ], _) ->
              micheline_value ty_k m_k >>=? fun v_k ->
              micheline_value ty_v m_v >>=? fun v_v ->
              return Logic.Lang.(update_map v_k (some v_v) v_map)
          | _ -> failwith "map parse error in PUSH")
        Logic.Lang.(empty_map ty_k ty_v)
        items
  (* remaining sorts: lambda, big_map, contract, operation *)
  (* lambda are pushable *)
  (* big_map, contract, operation are unpushable *)
  | _ -> failwith "%s" "in PUSH"

let retrieve_lambda_annot info =
  List.find_opt
    (function Decoration_def.LambdaAnnot _ -> true | _ -> false)
    info.bef_dec
  |> function
  | Some (Decoration_def.LambdaAnnot d) -> return d
  | _ -> failwith "%s" "LAMBDA needs LAMBDA-ANNOT"

let retrieve_loop_inv_annot info =
  List.find_opt
    (function Decoration_def.LoopInv _ -> true | _ -> false)
    info.bef_dec
  |> function
  | Some (Decoration_def.LoopInv d) -> return d
  | _ -> failwith "%s" "lack of LOOP-INV"

let retrieve_map_inv_annot info =
  List.find_map
    (function Decoration_def.MapInv d -> Some d | _ -> None)
    info.bef_dec
  |> function
  | Some d -> return d
  | None -> failwith "lack of MAP-INV"

let constraint_dec (coll_info : Constraint.t) (env : Logic.binding)
    (dec : Decoration_def.decoration list) (aft : Constraint.condition) :
    (Constraint.t * Constraint.condition) tzresult Lwt.t =
  List.fold_left_es
    (fun (coll_info, aft) -> function
      | Decoration_def.Assert c ->
          let coll_info, dec =
            Constraint.mk_condition coll_info env c.params [] c.body
          in
          (* 与えられた条件から assert (loop inv) の条件を帰結する制約を追加 *)
          return
            ( Constraint.add_subsume ~msg:"Assert" coll_info c.loc env aft dec,
              aft )
      | Decoration_def.LoopInv c ->
          let coll_info, dec =
            Constraint.mk_condition coll_info env c.params [] c.body
          in
          (* 与えられた条件から assert (loop inv) の条件を帰結する制約を追加 *)
          return
            ( Constraint.add_subsume ~msg:"LoopInv before entering loop"
                coll_info c.loc env aft dec,
              aft )
      | Decoration_def.MapInv c ->
          let coll_info, d =
            Constraint.mk_condition coll_info env c.params [] c.body
          in
          (match c.params with
          | (_, Sort.S_list ty) :: xs -> (Logic.Lang.nil ty, xs)
          | (_, Sort.S_map (kt, vt)) :: xs -> (Logic.Lang.empty_map kt vt, xs)
          | _ -> assert false)
          |> fun (empty, params) ->
          Constraint.subst_body d
            ~new_params:(empty :: List.map Logic.Lang.( ?/ ) params)
          >>=? fun new_body ->
          let coll_info, dec =
            Constraint.mk_condition coll_info env params [] new_body
          in
          return
            ( Constraint.add_subsume ~msg:"MapInv before entering loop"
                coll_info c.loc env aft dec,
              aft )
      | Decoration_def.Assume c ->
          (* 与えられた条件を assume した式で置き換え *)
          let coll_info, assume_dec =
            Constraint.mk_condition coll_info env c.params [] c.body
          in
          let new_params = aft.params in
          let new_assume_exists = Logic.fresh_binding assume_dec.exists in
          let new_exists = aft.exists @ new_assume_exists in
          Constraint.subst_body assume_dec
            ~new_params:(List.map Logic.Lang.( ?/ ) aft.params)
            ~new_exists:(List.map Logic.Lang.( ?/ ) new_assume_exists)
          >>=? fun assume_body ->
          let new_body = Logic.Lang.(aft.body && assume_body) in
          let coll_info, aft =
            Constraint.mk_condition coll_info env new_params new_exists new_body
          in
          return (coll_info, aft)
      | _ -> return (coll_info, aft))
    (coll_info, aft) dec

type judgment = Typed of Constraint.condition | Failed

let rec collect (env : Logic.binding)
    ((coll_info, bef) : Constraint.t * Constraint.condition)
    (node :
      ( Decoration_def.decoration dec_info,
        Michelson_v1_primitives.prim )
      Micheline.node) :
    (Constraint.t * judgment * Constraint.condition option) tzresult Lwt.t =
  typing_node env coll_info bef node >>=? fun (coll_info, j, excpt_opt) ->
  match j with
  | Typed aft ->
      (* node の後のアノテーションから制約生成・事後条件の変更 *)
      constraint_dec coll_info env (location node).aft_dec aft
      >>=? fun (coll_info, aft) -> return (coll_info, Typed aft, excpt_opt)
  | Failed -> return (coll_info, Failed, excpt_opt)

(** [typing_loop env coll_info inv_annot bef continue_cond exit_cond
   extra_bef_param extra_aft_param replacer body] infers postcondition of a
   loop-like instruction of which body is [body].  [inv_annot] is a user-given
   loop invariant annotation.  [bef] is a precondition of the loop.
   [continue_cond] is a condition under which the loop goes on.  [exit_cond] is
   a condition when the loop finishes.  [extra_bef_param] is an optional
   parameter pushed on the stack before executing the body.  [extra_aft_param]
   is an optional parameter pushed on the stack after executing the loop.
   [replacer] is an optional parameter pushed on the stack before executing the
   next iteration.  *)
and typing_loop env coll_info (inv_annot : Decoration_def.rtype_annot)
    (bef : Constraint.condition) ~continue_cond ~exit_cond ?extra_bef_param
    ?extra_aft_param ?replacer body :
    (Constraint.t * judgment * Constraint.condition option) tzresult Lwt.t =
  let coll_info, inv_dec =
    Constraint.mk_condition coll_info env inv_annot.params [] inv_annot.body
  in
  let fresh_params = Logic.fresh_binding bef.params in
  let mk_immune_cond params =
    List.map_es
      (fun nobind ->
        List.map2_es
          ~when_different_lengths:[ error_of_fmt "Unexpected error %s" __LOC__ ]
          (fun v v' -> return (v, Logic.Lang.(?/v')))
          inv_dec.params params
        >>=? fun s1 ->
        List.map2_es
          ~when_different_lengths:[ error_of_fmt "Unexpected error %s" __LOC__ ]
          (fun v v' -> return (v, Logic.Lang.(?/v')))
          inv_dec.params fresh_params
        >>=? fun s2 ->
        let l = Logic.subst_exp s1 nobind in
        let r = Logic.subst_exp s2 nobind in
        return Logic.Lang.(l = r))
      inv_annot.nobinds
    >|=? Logic.Lang.conj
  in
  match_cons bef.params >>=? fun (p, bef_params) ->
  (* preserve condition before loop by binding fresh parameters *)
  Constraint.subst_body
    ~new_params:(List.map Logic.Lang.( ?/ ) fresh_params)
    bef
  >>=? fun bef_prsv_body ->
  Constraint.subst_body
    ~new_params:(List.map Logic.Lang.( ?/ ) bef.params)
    inv_dec
  >>=? fun inv_bef_body ->
  mk_immune_cond bef.params >>=? fun immune_cond ->
  let body_env = Option.to_list replacer @ fresh_params @ env in
  let body_params = Option.to_list extra_bef_param @ bef_params in
  let body_exists = p :: bef.exists in
  let body_cond =
    Logic.Lang.(continue_cond p && inv_bef_body && bef_prsv_body && immune_cond)
  in
  let coll_info, body_bef =
    Constraint.mk_condition coll_info body_env body_params body_exists body_cond
  in
  collect body_env (coll_info, body_bef) body
  >>=? fun (coll_info, j, body_excpt_opt) ->
  (match j with
  | Typed body_aft ->
      Constraint.subst_body
        ~new_params:
          (List.map Logic.Lang.( ?/ ) @@ Option.to_list replacer
         @ body_aft.params)
        inv_dec
      >>=? fun inv_aft_body ->
      let coll_info, inv_aft =
        Constraint.mk_condition coll_info body_env body_aft.params []
          inv_aft_body
      in
      let coll_info =
        Constraint.add_subsume ~msg:"LoopInv after loop iteration" coll_info
          inv_annot.loc body_env body_aft inv_aft
      in
      mk_immune_cond (Option.to_list replacer @ body_aft.params)
      >>=? fun immune_chk_body ->
      let coll_info, immune_chk_cond =
        Constraint.mk_condition coll_info body_env body_aft.params []
          immune_chk_body
      in
      return
      @@ Constraint.add_subsume ~msg:"immutable stack spec. during iteration"
           coll_info inv_annot.loc body_env body_aft immune_chk_cond
  | Failed -> return @@ coll_info)
  >>=? fun coll_info ->
  let new_params = Option.to_list extra_aft_param @ bef_params in
  let new_exists = (p :: fresh_params) @ bef.exists in
  let new_body =
    Logic.Lang.(exit_cond p && inv_bef_body && bef_prsv_body && immune_cond)
  in
  let coll_info, aft =
    Constraint.mk_condition coll_info env new_params new_exists new_body
  in
  match body_excpt_opt with
  | None -> return (coll_info, Typed aft, None)
  | Some excpt ->
      let coll_info, excpt =
        Constraint.mk_exception coll_info env
          (Option.to_list replacer @ fresh_params @ excpt.exists)
          excpt.body
      in
      return (coll_info, Typed aft, Some excpt)

(** [[typing env coll_info bef node]] で環境 [[env]] と事前条件
    [[bef]] のもとでコード [[node]] から得られる事後条件を推論する *)
and typing_node env coll_info bef node :
    (Constraint.t * judgment * Constraint.condition option) tzresult Lwt.t =
  let ret ?excpt (coll_info, aft) = return (coll_info, Typed aft, excpt) in
  let merge_excpt_opt env coll_info (excpt1_opt : Constraint.condition option)
      (excpt2_opt : Constraint.condition option) =
    match (excpt1_opt, excpt2_opt) with
    | None, None -> return (coll_info, None)
    | Some excpt, None | None, Some excpt -> return (coll_info, Some excpt)
    | Some excpt1, Some excpt2 ->
        (* Suppose param is same [err].  Note only error conditions from
           annotations use a different parameter name, but those are never
           merged. *)
        assert (
          excpt1.params = [ Logic.Lang.err ]
          && excpt2.params = [ Logic.Lang.err ]);
        let new_excpt_body = Logic.Lang.(excpt1.body || excpt2.body) in
        let coll_info, excpt =
          Constraint.mk_exception coll_info env
            (excpt1.exists @ excpt2.exists)
            new_excpt_body
        in
        return (coll_info, Some excpt)
  in
  let typing_branch env coll_info bef_br1 br1 bef_br2 br2 =
    collect env (coll_info, bef_br1) br1 >>=? fun (coll_info, j1, excpt1_opt) ->
    collect env (coll_info, bef_br2) br2 >>=? fun (coll_info, j2, excpt2_opt) ->
    merge_excpt_opt env coll_info excpt1_opt excpt2_opt
    >>=? fun (coll_info, excpt_opt) ->
    match (j1, j2) with
    | Typed aft_dec_br1, Typed aft_dec_br2 ->
        (* stack の長さは一致してるはずなのでどっちとっても良い *)
        assert (List.length aft_dec_br1.params = List.length aft_dec_br2.params);
        let new_params = Logic.fresh_binding aft_dec_br1.params in
        let new_exists = aft_dec_br1.exists @ aft_dec_br2.exists in
        Constraint.subst_body aft_dec_br1
          ~new_params:(List.map Logic.Lang.( ?/ ) new_params)
        >>=? fun aft_dec_br1_body ->
        Constraint.subst_body aft_dec_br2
          ~new_params:(List.map Logic.Lang.( ?/ ) new_params)
        >>=? fun aft_dec_br2_body ->
        let new_body = Logic.Lang.(aft_dec_br1_body || aft_dec_br2_body) in
        let coll_info, aft =
          Constraint.mk_condition coll_info env new_params new_exists new_body
        in
        ret ?excpt:excpt_opt (coll_info, aft)
    | Failed, Typed aft | Typed aft, Failed ->
        ret ?excpt:excpt_opt (coll_info, aft)
    | Failed, Failed -> return (coll_info, Failed, excpt_opt)
  in
  let var_name dec_info =
    let loc = dec_info.loc in
    (*let start_point = string_of_int(loc.start.point) in
      let start_byte = string_of_int(loc.start.byte) in*)
    let start_line = string_of_int loc.start.line in
    let start_column = string_of_int loc.start.column in
    (*let stop_point = string_of_int(loc.stop.point) in
      let stop_byte = string_of_int(loc.stop.byte) in
      let stop_line = string_of_int(loc.stop.line) in*)
    let stop_column = string_of_int loc.stop.column in
    "_l" ^ start_line ^ "_c" ^ start_column ^ "-" ^ stop_column ^ "_"
  in
  match node with
  | Seq (_, items) -> (
      (* シーケンスの先頭のアノテーションから制約生成 *)
      (* TODO nishida: { << ... >> } って書くとこのアノテーションのこるのか? EDIT: 残らんらしい． *)
      match items with
      | [] -> ret (coll_info, bef)
      | h :: _ ->
          let dec = (location h).bef_dec in
          constraint_dec coll_info env dec bef >>=? fun (coll_info, bef) ->
          List.fold_left_es
            (fun (coll_info, bef, w_excpt_opt) node ->
              match bef with
              | Typed bef -> (
                  collect env (coll_info, bef) node
                  >>=? fun (coll_info, aft, excpt_opt) ->
                  merge_excpt_opt env coll_info excpt_opt w_excpt_opt
                  >>=? fun (coll_info, w_excpt_opt) ->
                  match aft with
                  | Typed aft -> ret ?excpt:w_excpt_opt (coll_info, aft)
                  | Failed -> return (coll_info, Failed, w_excpt_opt))
              | Failed -> failwith "internal: %s" __LOC__)
            (coll_info, Typed bef, None)
            items)
  | Prim (dec_info, I_UNIT, _, _) ->
      let p =
        Logic.(internal_fresh_var ~x:("unit" ^ var_name dec_info) S_unit)
      in
      let new_params = p :: bef.params in
      let new_exists = bef.exists in
      let new_body = Logic.Lang.(?/p = c_unit && body bef) in
      Constraint.mk_condition coll_info env new_params new_exists new_body
      |> ret
  | Prim (dec_info, I_LEFT, args, _) ->
      match_singleton_list args >>=? sort_of_micheline_ty >>=? fun right_t ->
      match_cons bef.params >>=? fun (((_, left_t) as left_p), bef_params) ->
      let p =
        Logic.(
          internal_fresh_var
            ~x:("left" ^ var_name dec_info)
            (S_or (left_t, right_t)))
      in
      let new_params = p :: bef_params in
      let new_exists = left_p :: bef.exists in
      let new_body = Logic.Lang.(?/p = left right_t ?/left_p && body bef) in
      Constraint.mk_condition coll_info env new_params new_exists new_body
      |> ret
  | Prim (dec_info, I_RIGHT, args, _) ->
      match_singleton_list args >>=? sort_of_micheline_ty >>=? fun left_t ->
      match_cons bef.params >>=? fun (((_, right_t) as right_p), bef_params) ->
      let p =
        Logic.(
          internal_fresh_var
            ~x:("right" ^ var_name dec_info)
            (S_or (left_t, right_t)))
      in
      let new_params = p :: bef_params in
      let new_exists = right_p :: bef.exists in
      let new_body = Logic.Lang.(?/p = right left_t ?/right_p && body bef) in
      Constraint.mk_condition coll_info env new_params new_exists new_body
      |> ret
  | Prim (dec_info, I_SOME, _, _) ->
      match_cons bef.params >>=? fun (((_, some_t) as some_p), bef_params) ->
      let p =
        Logic.(
          internal_fresh_var ~x:("some" ^ var_name dec_info) (S_option some_t))
      in
      let new_params = p :: bef_params in
      let new_exists = some_p :: bef.exists in
      let new_body = Logic.Lang.(?/p = some ?/some_p && body bef) in
      Constraint.mk_condition coll_info env new_params new_exists new_body
      |> ret
  | Prim (dec_info, I_NONE, args, _) ->
      match_singleton_list args >>=? sort_of_micheline_ty >>=? fun none_t ->
      let p =
        Logic.(
          internal_fresh_var ~x:("none" ^ var_name dec_info) (S_option none_t))
      in
      let new_params = p :: bef.params in
      let new_exists = bef.exists in
      let new_body = Logic.Lang.(?/p = none none_t && body bef) in
      Constraint.mk_condition coll_info env new_params new_exists new_body
      |> ret
  | Prim (dec_info, I_PUSH, args, _) ->
      (match args with
      | [ ty; value ] -> return (ty, value)
      | _ -> failwith "Invalid argument: I_PUSH")
      >>=? fun (ty, value) ->
      sort_of_micheline_ty ty >>=? fun ty ->
      micheline_value ty value >>=? fun value ->
      let p = Logic.internal_fresh_var ~x:("push" ^ var_name dec_info) ty in
      let new_params = p :: bef.params in
      let new_exists = bef.exists in
      let new_body = Logic.Lang.(?/p = value && body bef) in
      Constraint.mk_condition coll_info env new_params new_exists new_body
      |> ret
  | Prim (dec_info, I_NIL, args, _) ->
      match_singleton_list args >>=? sort_of_micheline_ty >>=? fun ty ->
      let p =
        Logic.(internal_fresh_var ~x:("nil" ^ var_name dec_info) (S_list ty))
      in
      let new_params = p :: bef.params in
      let new_exists = bef.exists in
      let new_body = Logic.Lang.(?/p = nil ty && body bef) in
      Constraint.mk_condition coll_info env new_params new_exists new_body
      |> ret
  | Prim (dec_info, I_CONS, _, _) ->
      match_cons bef.params >>=? fun (h, bef_params) ->
      match_cons bef_params >>=? fun (l, bef_params) ->
      let ty = snd h in
      let new_param =
        Logic.(internal_fresh_var ~x:("cons" ^ var_name dec_info) (S_list ty))
      in
      let new_params = new_param :: bef_params in
      let new_exists = h :: l :: bef.exists in
      let new_body = Logic.Lang.(?/new_param = cons ?/h ?/l && body bef) in
      Constraint.mk_condition coll_info env new_params new_exists new_body
      |> ret
  | Prim (dec_info, I_CAR, _, _) ->
      match_cons bef.params >>=? fun (p, bef_params) ->
      (match p with
      | _, Sort.S_pair (ls, _) -> return ls
      | _ -> failwith "%s" "in CAR")
      >>=? fun ls ->
      let q = Logic.(internal_fresh_var ~x:("car" ^ var_name dec_info) ls) in
      let new_params = q :: bef_params in
      let new_exists = p :: bef.exists in
      let new_body = Logic.Lang.(?/q = fst ?/p && body bef) in
      Constraint.mk_condition coll_info env new_params new_exists new_body
      |> ret
  | Prim (dec_info, I_CDR, _, _) ->
      match_cons bef.params >>=? fun (p, bef_params) ->
      (match p with
      | _, Sort.S_pair (_, rs) -> return rs
      | _ -> failwith "%s" "in CDR")
      >>=? fun rs ->
      let q = Logic.(internal_fresh_var ~x:("cdr" ^ var_name dec_info) rs) in
      let new_params = q :: bef_params in
      let new_exists = p :: bef.exists in
      let new_body = Logic.Lang.(?/q = snd ?/p && body bef) in
      Constraint.mk_condition coll_info env new_params new_exists new_body
      |> ret
  | Prim (dec_info, I_PAIR, args, _) ->
      (match args with
      | [] -> return @@ Z.of_int 2
      | [ Int (_, n) ] -> return n
      | _ -> failwith "error in PAIR")
      >>=? fun n ->
      pop_n_stack n bef.params >>=? fun (hds, params) ->
      let rec f hds =
        match hds with
        | [ ((_, ls) as l); ((_, rs) as r) ] ->
            let exp = Logic.Lang.(pair ?/l ?/r) in
            let s = Sort.S_pair (ls, rs) in
            return (exp, s)
        | ((_, ls) as l) :: t ->
            f t >>=? fun (exp, s) ->
            let exp = Logic.Lang.(pair ?/l exp) in
            let s = Sort.S_pair (ls, s) in
            return (exp, s)
        | _ -> failwith "Unexpected Error"
      in
      f hds >>=? fun (exp, s) ->
      let p = Logic.(internal_fresh_var ~x:("pair" ^ var_name dec_info) s) in
      let new_params = p :: params in
      let new_exists = hds @ bef.exists in
      let new_body = Logic.Lang.(?/p = exp && body bef) in
      Constraint.mk_condition coll_info env new_params new_exists new_body
      |> ret
  | Prim (dec_info, I_UNPAIR, args, _) ->
      (match args with
      | [] -> return 2
      | [ Int (_, n) ] -> return @@ Z.to_int n
      | _ -> failwith "error in UNPAIR")
      >>=? fun n ->
      match_cons bef.params >>=? fun (((_, ps) as p), bef_params) ->
      let rec f n ps =
        match (n, ps) with
        | 2, Sort.S_pair (ls, rs) ->
            let p1 =
              Logic.(internal_fresh_var ~x:("unpair" ^ var_name dec_info) ls)
            in
            let p2 =
              Logic.(internal_fresh_var ~x:("unpair" ^ var_name dec_info) rs)
            in
            let new_params = p1 :: p2 :: bef_params in
            let exp = Logic.Lang.(pair ?/p1 ?/p2) in
            return (exp, new_params)
        | n, Sort.S_pair (ls, rs) when n > 2 ->
            let p1 =
              Logic.(internal_fresh_var ~x:("unpair" ^ var_name dec_info) ls)
            in
            f (n - 1) rs >>=? fun (exp, params) ->
            let new_params = p1 :: params in
            let exp = Logic.Lang.(pair ?/p1 exp) in
            return (exp, new_params)
        | _ -> failwith "Unexpected Error in UNPAIR"
      in
      f n ps >>=? fun (exp, new_params) ->
      let new_exists = p :: bef.exists in
      let new_body = Logic.Lang.(?/p = exp && body bef) in
      Constraint.mk_condition coll_info env new_params new_exists new_body
      |> ret
  | Prim (dec_info, I_EMPTY_SET, args, _) ->
      match_singleton_list args >>=? sort_of_micheline_ty >>=? fun ty ->
      let t_set = Sort.S_set ty in
      let p =
        Logic.internal_fresh_var ~x:("empty_set" ^ var_name dec_info) t_set
      in
      let new_params = p :: bef.params in
      let new_exists = bef.exists in
      let new_body = Logic.Lang.(?/p = empty_set ty && body bef) in
      Constraint.mk_condition coll_info env new_params new_exists new_body
      |> ret
  | Prim (dec_info, I_MEM, _, _) ->
      match_cons bef.params >>=? fun (e, bef_params) ->
      match_cons bef_params >>=? fun (((_, ty) as s), bef_params) ->
      let p =
        Logic.(internal_fresh_var ~x:("mem" ^ var_name dec_info) S_bool)
      in
      let new_params = p :: bef_params in
      let new_exists = e :: s :: bef.exists in
      (match ty with
      | S_set _ -> return Logic.Lang.(?/p = mem_set ?/e ?/s && body bef)
      | S_map _ ->
          return Logic.Lang.(?/p = is_some (find_opt_map ?/e ?/s) && body bef)
      | S_big_map _ ->
          return
            Logic.Lang.(?/p = is_some (find_opt_big_map ?/e ?/s) && body bef)
      | _ -> failwith "error in MEM")
      >>=? fun new_body ->
      Constraint.mk_condition coll_info env new_params new_exists new_body
      |> ret
  | Prim (dec_info, I_UPDATE, args, _) -> (
      match args with
      | [] ->
          (* 偶然 Set と Map が同じコードで行ける *)
          match_cons bef.params >>=? fun (k, bef_params) ->
          match_cons bef_params >>=? fun (b, bef_params) ->
          match_cons bef_params >>=? fun (((_, ty) as s), bef_params) ->
          let p =
            Logic.(internal_fresh_var ~x:("update" ^ var_name dec_info) ty)
          in
          let new_params = p :: bef_params in
          let new_exists = k :: b :: s :: bef.exists in
          (match ty with
          | Sort.S_set _ ->
              return
                Logic.Lang.(
                  ite ?/b (?/p = add_set ?/k ?/s) (?/p = remove_set ?/k ?/s)
                  && body bef)
          | Sort.S_map _ ->
              return Logic.Lang.(?/p = update_map ?/k ?/b ?/s && body bef)
          | Sort.S_big_map _ ->
              return Logic.Lang.(?/p = update_big_map ?/k ?/b ?/s && body bef)
          | _ -> failwith "error in UPDATE")
          >>=? fun new_body ->
          Constraint.mk_condition coll_info env new_params new_exists new_body
          |> ret
      | [ n ] ->
          parse_int n >>=? fun n ->
          match_cons bef.params >>=? fun (((_, qs) as q), bef_params) ->
          match_cons bef_params >>=? fun (p, bef_params) ->
          let rec f n p =
            match (n, p) with
            | 0, _ -> return ([], q, [])
            | 1, (_, Sort.S_pair (ls, rs)) ->
                let p1 =
                  Logic.(
                    internal_fresh_var ~x:("update" ^ var_name dec_info) ls)
                in
                let p2 =
                  Logic.(
                    internal_fresh_var ~x:("update" ^ var_name dec_info) rs)
                in
                let r =
                  Logic.(
                    internal_fresh_var
                      ~x:("update" ^ var_name dec_info)
                      (S_pair (qs, rs)))
                in
                let cond1 = Logic.Lang.(?/p = pair ?/p1 ?/p2) in
                let cond2 = Logic.Lang.(?/r = pair ?/q ?/p2) in
                return ([ cond1; cond2 ], r, [ p1; p2 ])
            | n, (_, Sort.S_pair (ls, rs)) when n >= 2 ->
                let p1 =
                  Logic.(
                    internal_fresh_var ~x:("update" ^ var_name dec_info) ls)
                in
                let p2 =
                  Logic.(
                    internal_fresh_var ~x:("update" ^ var_name dec_info) rs)
                in
                f (n - 2) p2 >>=? fun (conds, ((_, prms) as prm), vars) ->
                let r =
                  Logic.(
                    internal_fresh_var
                      ~x:("update" ^ var_name dec_info)
                      (S_pair (ls, prms)))
                in
                let cond1 = Logic.Lang.(?/p = pair ?/p1 ?/p2) in
                let cond2 = Logic.Lang.(?/r = pair ?/p1 ?/prm) in
                return (cond1 :: cond2 :: conds, r, p1 :: p2 :: prm :: vars)
            | _ -> failwith "Unexpected Error in UPDATE"
          in
          f n p >>=? fun (conds, prm, vars) ->
          let new_params = prm :: bef_params in
          let new_exists = (p :: q :: vars) @ bef.exists in
          let new_body = Logic.Lang.(conj conds && body bef) in
          Constraint.mk_condition coll_info env new_params new_exists new_body
          |> ret
      | _ -> failwith "error in UPDATE")
  | Prim (dec_info, I_EMPTY_MAP, args, _) ->
      match_cons args >>=? fun (h, t) ->
      sort_of_micheline_ty h >>=? fun key_ty ->
      match_singleton_list t >>=? sort_of_micheline_ty >>=? fun val_ty ->
      let t_map = Sort.S_map (key_ty, val_ty) in
      let p =
        Logic.internal_fresh_var ~x:("empty_map" ^ var_name dec_info) t_map
      in
      let new_params = p :: bef.params in
      let new_exists = bef.exists in
      let new_body = Logic.Lang.(?/p = empty_map key_ty val_ty && body bef) in
      Constraint.mk_condition coll_info env new_params new_exists new_body
      |> ret
  | Prim (dec_info, I_EMPTY_BIG_MAP, args, _) ->
      match_cons args >>=? fun (h, t) ->
      sort_of_micheline_ty h >>=? fun key_ty ->
      match_singleton_list t >>=? sort_of_micheline_ty >>=? fun val_ty ->
      let t_map = Sort.S_big_map (key_ty, val_ty) in
      let p =
        Logic.internal_fresh_var ~x:("empty_big_map" ^ var_name dec_info) t_map
      in
      let new_params = p :: bef.params in
      let new_exists = bef.exists in
      let new_body =
        Logic.Lang.(?/p = empty_big_map key_ty val_ty && body bef)
      in
      Constraint.mk_condition coll_info env new_params new_exists new_body
      |> ret
  | Prim (dec_info, I_GET, args, _) -> (
      match args with
      | [] ->
          match_cons bef.params >>=? fun (e, bef_params) ->
          match_cons bef_params >>=? fun (m, bef_params) ->
          (match snd m with
          | Sort.S_map (_, val_ty) -> return (Logic.Lang.find_opt_map, val_ty)
          | Sort.S_big_map (_, val_ty) ->
              return (Logic.Lang.find_opt_big_map, val_ty)
          | _ -> failwith "error in GET")
          >>=? fun (find, val_ty) ->
          let p =
            Logic.(
              internal_fresh_var
                ~x:("get" ^ var_name dec_info)
                (S_option val_ty))
          in
          let new_params = p :: bef_params in
          let new_exists = e :: m :: bef.exists in
          let new_body = Logic.Lang.(?/p = find ?/e ?/m && body bef) in
          Constraint.mk_condition coll_info env new_params new_exists new_body
          |> ret
      | [ n ] ->
          parse_int n >>=? fun n ->
          match_cons bef.params >>=? fun (((_, ps) as p), bef_params) ->
          let rec f n s exp =
            match (n, s) with
            | 0, _ -> return (exp, s)
            | 1, Sort.S_pair (ls, _) -> return (Logic.Lang.(fst exp), ls)
            | n, Sort.S_pair (_, rs) when n >= 2 ->
                f (n - 2) rs Logic.Lang.(snd exp)
            | _ -> failwith "Unexpected Error in GET"
          in
          f n ps Logic.Lang.(?/p) >>=? fun (exp, s) ->
          let q = Logic.(internal_fresh_var ~x:("get" ^ var_name dec_info) s) in
          let new_params = q :: bef_params in
          let new_exists = p :: bef.exists in
          let new_body = Logic.Lang.(?/q = exp && body bef) in
          Constraint.mk_condition coll_info env new_params new_exists new_body
          |> ret
      | _ -> failwith "error in GET")
  | Prim (dec_info, I_GET_AND_UPDATE, _, _) -> (
      match_cons bef.params >>=? fun (e, bef_params) ->
      match_cons bef_params >>=? fun (((_, val_ty) as b), bef_params) ->
      match_cons bef_params >>=? fun (((_, ty) as s), bef_params) ->
      let p =
        Logic.(internal_fresh_var ~x:("get_and_update" ^ var_name dec_info) ty)
      in
      let q =
        Logic.(
          internal_fresh_var ~x:("get_and_update" ^ var_name dec_info) val_ty)
      in
      let new_params = q :: p :: bef_params in
      match ty with
      | Sort.S_map _ ->
          let new_exists = e :: b :: s :: bef.exists in
          let new_body =
            Logic.Lang.(
              ?/p = update_map ?/e ?/b ?/s
              && ?/q = find_opt_map ?/e ?/s
              && body bef)
          in
          Constraint.mk_condition coll_info env new_params new_exists new_body
          |> ret
      | Sort.S_big_map _ ->
          let new_exists = e :: b :: s :: bef.exists in
          let new_body =
            Logic.Lang.(
              ?/p = update_big_map ?/e ?/b ?/s
              && ?/q = find_opt_big_map ?/e ?/s
              && body bef)
          in
          Constraint.mk_condition coll_info env new_params new_exists new_body
          |> ret
      | _ -> failwith "error in GET_AND_UPDATE")
  | Prim (dec_info, I_EQ, _, _) ->
      match_cons bef.params >>=? fun (p, bef_params) ->
      let q = Logic.(internal_fresh_var ~x:("eq" ^ var_name dec_info) S_bool) in
      let new_params = q :: bef_params in
      let new_exists = p :: bef.exists in
      let new_body = Logic.Lang.(?/q = (?/p = c_int 0) && body bef) in
      Constraint.mk_condition coll_info env new_params new_exists new_body
      |> ret
  | Prim (dec_info, I_NEQ, _, _) ->
      match_cons bef.params >>=? fun (p, bef_params) ->
      let q =
        Logic.(internal_fresh_var ~x:("neq" ^ var_name dec_info) S_bool)
      in
      let new_params = q :: bef_params in
      let new_exists = p :: bef.exists in
      let new_body = Logic.Lang.(?/q = neg (?/p = c_int 0) && body bef) in
      Constraint.mk_condition coll_info env new_params new_exists new_body
      |> ret
  | Prim (dec_info, I_LT, _, _) ->
      match_cons bef.params >>=? fun (p, bef_params) ->
      let q = Logic.(internal_fresh_var ~x:("lt" ^ var_name dec_info) S_bool) in
      let new_params = q :: bef_params in
      let new_exists = p :: bef.exists in
      let new_body = Logic.Lang.(?/q = (?/p < c_int 0) && body bef) in
      Constraint.mk_condition coll_info env new_params new_exists new_body
      |> ret
  | Prim (dec_info, I_GT, _, _) ->
      match_cons bef.params >>=? fun (p, bef_params) ->
      let q = Logic.(internal_fresh_var ~x:("gt" ^ var_name dec_info) S_bool) in
      let new_params = q :: bef_params in
      let new_exists = p :: bef.exists in
      let new_body = Logic.Lang.(?/q = (?/p > c_int 0) && body bef) in
      Constraint.mk_condition coll_info env new_params new_exists new_body
      |> ret
  | Prim (dec_info, I_LE, _, _) ->
      match_cons bef.params >>=? fun (p, bef_params) ->
      let q =
        Logic.(internal_fresh_var ~x:("unit" ^ var_name dec_info) S_bool)
      in
      let new_params = q :: bef_params in
      let new_exists = p :: bef.exists in
      let new_body = Logic.Lang.(?/q = (?/p <= c_int 0) && body bef) in
      Constraint.mk_condition coll_info env new_params new_exists new_body
      |> ret
  | Prim (dec_info, I_GE, _, _) ->
      match_cons bef.params >>=? fun (p, bef_params) ->
      let q = Logic.(internal_fresh_var ~x:("ge" ^ var_name dec_info) S_bool) in
      let new_params = q :: bef_params in
      let new_exists = p :: bef.exists in
      let new_body = Logic.Lang.(?/q = (?/p >= c_int 0) && body bef) in
      Constraint.mk_condition coll_info env new_params new_exists new_body
      |> ret
  | Prim (dec_info, I_COMPARE, _, _) ->
      match_cons bef.params >>=? fun (p, bef_params) ->
      match_cons bef_params >>=? fun (q, bef_params) ->
      let _, ty = p in
      let r =
        Logic.(internal_fresh_var ~x:("compare" ^ var_name dec_info) S_int)
      in
      let new_params = r :: bef_params in
      let rec f (p, q) ty k =
        match ty with
        | Sort.S_unit -> return k
        | Sort.S_bool ->
            return
              Logic.Lang.(
                ite (p = q) k (ite p (?/r = c_int 1) (?/r = c_int (-1))))
        | Sort.S_int | Sort.S_nat | Sort.S_mutez | Sort.S_timestamp ->
            return
              Logic.Lang.(
                ite (p = q) k (ite (p > q) (?/r = c_int 1) (?/r = c_int (-1))))
        | Sort.S_string ->
            return
              Logic.Lang.(
                ite (p = q) k
                  (ite (gt_str p q) (?/r = c_int 1) (?/r = c_int (-1))))
        | Sort.S_bytes ->
            return
              Logic.Lang.(
                ite (p = q) k
                  (ite (gt_bytes p q) (?/r = c_int 1) (?/r = c_int (-1))))
        | Sort.S_key | Sort.S_key_hash | Sort.S_chain_id | Sort.S_signature ->
            return
              Logic.Lang.(
                ite (p = q) k
                  (ite (gt_bytes p q) (?/r = c_int 1) (?/r = c_int (-1))))
        | Sort.S_address ->
            f
              Logic.Lang.(extract_entrypoint p, extract_entrypoint q)
              Sort.S_string k
            >>=? fun k ->
            f Logic.Lang.(extract_address p, extract_address q) Sort.S_string k
        | Sort.S_pair (ty1, ty2) ->
            f Logic.Lang.(snd p, snd q) ty2 k >>=? fun cond ->
            f Logic.Lang.(fst p, fst q) ty1 cond
        | Sort.S_option ty1 ->
            f Logic.Lang.(extract_some p, extract_some q) ty1 k >>=? fun cond ->
            return
              Logic.Lang.(
                ite (is_none p)
                  (ite (is_none q) (?/r = c_int 0) (?/r = c_int (-1)))
                  (ite (is_none q) (?/r = c_int 1) cond))
        | Sort.S_or (ty1, ty2) ->
            f Logic.Lang.(extract_left p, extract_left q) ty1 k
            >>=? fun k_left ->
            f Logic.Lang.(extract_right p, extract_right q) ty2 k
            >>=? fun k_right ->
            return
              Logic.Lang.(
                ite (is_left p)
                  (ite (is_left q) k_left (?/r = c_int (-1)))
                  (ite (is_left q) (?/r = c_int 1) k_right))
        | _ -> failwith "error in COMPARE"
      in
      f Logic.Lang.(?/p, ?/q) ty Logic.Lang.(?/r = c_int 0) >>=? fun cond ->
      let new_exists = p :: q :: bef.exists in
      let new_body = Logic.Lang.(cond && body bef) in
      Constraint.mk_condition coll_info env new_params new_exists new_body
      |> ret
  | Prim (dec_info, I_OR, _, _) -> (
      match_cons bef.params >>=? fun (p, bef_params) ->
      match_cons bef_params >>=? fun (q, bef_params) ->
      match snd p with
      | Sort.S_bool ->
          let r =
            Logic.(internal_fresh_var ~x:("or" ^ var_name dec_info) S_bool)
          in
          let new_params = r :: bef_params in
          let new_exists = p :: q :: bef.exists in
          let new_body = Logic.Lang.(?/r = (?/p || ?/q) && body bef) in
          Constraint.mk_condition coll_info env new_params new_exists new_body
          |> ret
      | Sort.S_nat -> failwith "OR for nat is not implemented"
      | _ -> failwith "error in OR")
  | Prim (dec_info, I_AND, _, _) -> (
      match_cons bef.params >>=? fun (p, bef_params) ->
      match_cons bef_params >>=? fun (q, bef_params) ->
      match (snd p, snd q) with
      | Sort.S_bool, Sort.S_bool ->
          let r =
            Logic.(internal_fresh_var ~x:("and" ^ var_name dec_info) S_bool)
          in
          let new_params = r :: bef_params in
          let new_exists = p :: q :: bef.exists in
          let new_body = Logic.Lang.(?/r = (?/p && ?/q) && body bef) in
          Constraint.mk_condition coll_info env new_params new_exists new_body
          |> ret
      | ty1, ty2 ->
          failwith "Helmholtz has not supported yet AND between %a an %a"
            Sort.pp_sort ty1 Sort.pp_sort ty2)
  | Prim (dec_info, I_XOR, _, _) -> (
      match_cons bef.params >>=? fun (p, bef_params) ->
      match_cons bef_params >>=? fun (q, bef_params) ->
      match snd p with
      | Sort.S_bool ->
          let r =
            Logic.(internal_fresh_var ~x:("xor" ^ var_name dec_info) S_bool)
          in
          let new_params = r :: bef_params in
          let new_exists = p :: q :: bef.exists in
          let new_body = Logic.Lang.(?/r = xor ?/p ?/q && body bef) in
          Constraint.mk_condition coll_info env new_params new_exists new_body
          |> ret
      | Sort.S_nat -> failwith "XOR for nat is not implemented"
      | _ -> failwith "error in XOR")
  | Prim (dec_info, I_NOT, _, _) -> (
      match_cons bef.params >>=? fun (p, bef_params) ->
      match snd p with
      | Sort.S_bool ->
          let q =
            Logic.(internal_fresh_var ~x:("not" ^ var_name dec_info) S_bool)
          in
          let new_params = q :: bef_params in
          let new_exists = p :: bef.exists in
          let new_body = Logic.Lang.(?/q = neg ?/p && body bef) in
          Constraint.mk_condition coll_info env new_params new_exists new_body
          |> ret
      | (Sort.S_nat | Sort.S_int) as ty ->
          failwith "NOT for %a is not implemented" Sort.pp_sort ty
      | _ -> failwith "error in NOT")
  | Prim (dec_info, I_NEG, _, _) -> (
      match_cons bef.params >>=? fun (p, bef_params) ->
      match snd p with
      | Sort.S_int | Sort.S_nat ->
          let q =
            Logic.(internal_fresh_var ~x:("neg" ^ var_name dec_info) S_int)
          in
          let new_params = q :: bef_params in
          let new_exists = p :: bef.exists in
          let new_body = Logic.Lang.(?/p + ?/q = c_int 0 && body bef) in
          Constraint.mk_condition coll_info env new_params new_exists new_body
          |> ret
      | ty -> failwith "NEG for %a is not implemented" Sort.pp_sort ty)
  | Prim (dec_info, I_ABS, _, _) ->
      match_cons bef.params >>=? fun (p, bef_params) ->
      let q = Logic.(internal_fresh_var ~x:("abs" ^ var_name dec_info) S_nat) in
      let new_params = q :: bef_params in
      let new_exists = p :: bef.exists in
      let new_body =
        Logic.Lang.(
          ite (?/p < c_int 0) (?/p + ?/q = c_int 0) (?/p - ?/q = c_int 0)
          && body bef)
      in
      Constraint.mk_condition coll_info env new_params new_exists new_body
      |> ret
  | Prim (dec_info, I_ISNAT, _, _) ->
      match_cons bef.params >>=? fun (p, bef_params) ->
      let q =
        Logic.(
          internal_fresh_var ~x:("isnat" ^ var_name dec_info) (S_option S_nat))
      in
      let new_params = q :: bef_params in
      let new_exists = p :: bef.exists in
      let new_body =
        Logic.Lang.(
          ite
            (?/p < c_int 0)
            (?/q = none S_nat)
            (?/p >= c_int 0 := is_some ?/q && extract_some ?/q - ?/p = c_int 0)
          && body bef)
      in
      Constraint.mk_condition coll_info env new_params new_exists new_body
      |> ret
  | Prim (dec_info, I_INT, _, _) -> (
      match_cons bef.params >>=? fun (p, bef_params) ->
      match snd p with
      | Sort.S_nat ->
          let q =
            Logic.(internal_fresh_var ~x:("int" ^ var_name dec_info) S_int)
          in
          let new_params = q :: bef_params in
          let new_exists = p :: bef.exists in
          let new_body = Logic.Lang.(?/p - ?/q = c_int 0 && body bef) in
          Constraint.mk_condition coll_info env new_params new_exists new_body
          |> ret
      | ty -> failwith "INT for %a is not implemented" Sort.pp_sort ty)
  | Prim (dec_info, I_ADD, _, _) -> (
      match_cons bef.params >>=? fun (p, bef_params) ->
      match_cons bef_params >>=? fun (q, bef_params) ->
      (match (snd p, snd q) with
      | Sort.S_int, Sort.S_int | Sort.S_int, Sort.S_nat | Sort.S_nat, Sort.S_int
        ->
          return Sort.S_int
      | Sort.S_nat, Sort.S_nat -> return Sort.S_nat
      | Sort.S_timestamp, Sort.S_int | Sort.S_int, Sort.S_timestamp ->
          return Sort.S_timestamp
      | Sort.S_mutez, Sort.S_mutez -> return Sort.S_mutez
      | ty1, ty2 ->
          failwith "Helmholtz has not supported yet ADD between %a and %a"
            Sort.pp_sort ty1 Sort.pp_sort ty2)
      >>=? fun r_ty ->
      let r = Logic.(internal_fresh_var ~x:("add" ^ var_name dec_info) r_ty) in
      let new_params = r :: bef_params in
      let new_exists = p :: q :: bef.exists in
      let new_body =
        match r_ty with
        | Sort.S_mutez ->
            Logic.Lang.(c_mutez_max - ?/q >= ?/p && ?/r = ?/p + ?/q && body bef)
        | _ -> Logic.Lang.(?/r = ?/p + ?/q && body bef)
      in
      let coll_info, aft =
        Constraint.mk_condition coll_info env new_params new_exists new_body
      in
      (* ADD between mutez may raise Overflow. *)
      match r_ty with
      | Sort.S_mutez ->
          let new_excpt_body =
            Logic.Lang.(?/err = overflow && c_mutez_max - ?/q < ?/p && body bef)
          in
          let coll_info, excpt =
            Constraint.mk_exception coll_info env (bef.params @ bef.exists)
              new_excpt_body
          in
          ret ~excpt (coll_info, aft)
      | _ -> ret (coll_info, aft))
  | Prim (dec_info, I_SUB, _, _) -> (
      match_cons bef.params >>=? fun (p, bef_params) ->
      match_cons bef_params >>=? fun (q, bef_params) ->
      (match (snd p, snd q) with
      | Sort.S_int, Sort.S_int
      | Sort.S_int, Sort.S_nat
      | Sort.S_nat, Sort.S_int
      | Sort.S_nat, Sort.S_nat
      | Sort.S_timestamp, Sort.S_timestamp ->
          return Sort.S_int
      | Sort.S_timestamp, Sort.S_int -> return Sort.S_timestamp
      | Sort.S_mutez, Sort.S_mutez -> return Sort.S_mutez
      | _ -> failwith "error in SUB")
      >>=? fun r_ty ->
      let r = Logic.(internal_fresh_var ~x:("sub" ^ var_name dec_info) r_ty) in
      let new_params = r :: bef_params in
      let new_exists = p :: q :: bef.exists in
      let new_body =
        match r_ty with
        | Sort.S_mutez -> Logic.Lang.(?/p >= ?/q && ?/r = ?/p - ?/q && body bef)
        | _ -> Logic.Lang.(?/r = ?/p - ?/q && body bef)
      in
      let coll_info, aft =
        Constraint.mk_condition coll_info env new_params new_exists new_body
      in
      match r_ty with
      | Sort.S_mutez ->
          let new_excpt_body =
            Logic.Lang.(?/err = overflow && ?/p < ?/q && body bef)
          in
          let coll_info, excpt =
            Constraint.mk_exception coll_info env (bef.params @ bef.exists)
              new_excpt_body
          in
          ret ~excpt (coll_info, aft)
      | _ -> ret (coll_info, aft))
  | Prim (dec_info, I_SUB_MUTEZ, _, _) ->
      match_cons bef.params >>=? fun (p, bef_params) ->
      match_cons bef_params >>=? fun (q, bef_params) ->
      let r =
        Logic.(
          internal_fresh_var
            ~x:("sub_mutez" ^ var_name dec_info)
            (S_option S_mutez))
      in
      let new_params = r :: bef_params in
      let new_exists = p :: q :: bef.exists in
      let new_body =
        Logic.Lang.(
          ite (?/p >= ?/q) (?/r = some (?/p - ?/q)) (?/r = none S_mutez)
          && body bef)
      in
      Constraint.mk_condition coll_info env new_params new_exists new_body
      |> ret
  | Prim (dec_info, I_MUL, _, _) -> (
      match_cons bef.params >>=? fun (p, bef_params) ->
      match_cons bef_params >>=? fun (q, bef_params) ->
      (match (snd p, snd q) with
      | Sort.S_int, Sort.S_int | Sort.S_int, Sort.S_nat | Sort.S_nat, Sort.S_int
        ->
          return Sort.S_int
      | Sort.S_nat, Sort.S_nat -> return Sort.S_nat
      | Sort.S_mutez, Sort.S_nat | Sort.S_nat, Sort.S_mutez ->
          return Sort.S_mutez
      | ty1, ty2 ->
          failwith "MUL between %a and %a is not implemented" Sort.pp_sort ty1
            Sort.pp_sort ty2)
      >>=? fun r_ty ->
      let r = Logic.(internal_fresh_var ~x:("mul" ^ var_name dec_info) r_ty) in
      let new_params = r :: bef_params in
      let new_exists = p :: q :: bef.exists in
      let new_body =
        match (snd p, snd q) with
        | Sort.S_mutez, Sort.S_nat ->
            Logic.Lang.(c_mutez_max / ?/q >= ?/p && ?/r = ?/p * ?/q && body bef)
        | Sort.S_nat, Sort.S_mutez ->
            Logic.Lang.(c_mutez_max / ?/p >= ?/q && ?/r = ?/p * ?/q && body bef)
        | _ -> Logic.Lang.(?/r = ?/p * ?/q && body bef)
      in
      let coll_info, aft =
        Constraint.mk_condition coll_info env new_params new_exists new_body
      in
      match (snd p, snd q) with
      | Sort.S_mutez, Sort.S_nat ->
          let new_excpt_body =
            Logic.Lang.(?/err = overflow && c_mutez_max / ?/q < ?/p && body bef)
          in
          let coll_info, excpt =
            Constraint.mk_exception coll_info env (bef.params @ bef.exists)
              new_excpt_body
          in
          ret ~excpt (coll_info, aft)
      | Sort.S_nat, Sort.S_mutez ->
          let new_excpt_body =
            Logic.Lang.(?/err = overflow && c_mutez_max / ?/p < ?/q && body bef)
          in
          let coll_info, excpt =
            Constraint.mk_exception coll_info env (bef.params @ bef.exists)
              new_excpt_body
          in
          ret ~excpt (coll_info, aft)
      | _ -> ret (coll_info, aft))
  | Prim (dec_info, I_EDIV, _, _) ->
      match_cons bef.params >>=? fun (p, bef_params) ->
      match_cons bef_params >>=? fun (q, bef_params) ->
      (match (snd p, snd q) with
      | Sort.S_int, Sort.S_int ->
          return (Sort.S_int, Sort.S_nat, Logic.Lang.c_int 0)
      | Sort.S_int, Sort.S_nat ->
          return (Sort.S_int, Sort.S_nat, Logic.Lang.c_nat 0)
      | Sort.S_nat, Sort.S_int ->
          return (Sort.S_int, Sort.S_nat, Logic.Lang.c_int 0)
      | Sort.S_nat, Sort.S_nat ->
          return (Sort.S_nat, Sort.S_nat, Logic.Lang.c_nat 0)
      | Sort.S_mutez, Sort.S_nat ->
          return (Sort.S_mutez, Sort.S_mutez, Logic.Lang.c_nat 0)
      | Sort.S_mutez, Sort.S_mutez ->
          return (Sort.S_nat, Sort.S_mutez, Logic.Lang.c_mutez 0)
      | _ -> failwith "error in EDIV")
      >>=? fun (s_ty, t_ty, c_zero) ->
      let pair_ty = Sort.S_pair (s_ty, t_ty) in
      let r =
        Logic.(
          internal_fresh_var ~x:("ediv" ^ var_name dec_info) (S_option pair_ty))
      in
      let new_params = r :: bef_params in
      let new_exists = p :: q :: bef.exists in
      let new_body =
        Logic.Lang.(
          ite (?/q = c_zero)
            (?/r = none pair_ty)
            (?/r = some (pair (?/p / ?/q) (?/p % ?/q)))
          && body bef)
      in
      Constraint.mk_condition coll_info env new_params new_exists new_body
      |> ret
  | Prim (dec_info, I_PACK, _, _) ->
      match_cons bef.params >>=? fun (p, bef_params) ->
      let _, ty = p in
      let q =
        Logic.(internal_fresh_var ~x:("pack" ^ var_name dec_info) S_bytes)
      in
      let new_params = q :: bef_params in
      let new_exists = p :: bef.exists in
      let new_body =
        Logic.Lang.(
          unpack_opt ty ?/q = some ?/p && pack ty ?/p = ?/q && body bef)
      in
      Constraint.mk_condition coll_info env new_params new_exists new_body
      |> ret
  | Prim (dec_info, I_UNPACK, args, _) ->
      (match args with [ ty ] -> return ty | _ -> failwith "error in UNPACK")
      >>=? fun ty ->
      match_cons bef.params >>=? fun (p, bef_params) ->
      sort_of_micheline_ty ty >>=? fun ty ->
      let q =
        Logic.(
          internal_fresh_var ~x:("unpack" ^ var_name dec_info) (S_option ty))
      in
      let new_params = q :: bef_params in
      let new_exists = p :: bef.exists in
      let new_body =
        Logic.Lang.(
          unpack_opt ty ?/p = ?/q
          && ite (is_some ?/q) (pack ty (extract_some ?/q) = ?/p) (c_bool true)
          && body bef)
      in
      Constraint.mk_condition coll_info env new_params new_exists new_body
      |> ret
  | Prim (dec_info, I_CONCAT, _, _) ->
      match_cons bef.params >>=? fun (p, bef_params) ->
      let _, ty = p in
      (match ty with
      | Sort.S_list (Sort.S_string as sty) ->
          let r =
            Logic.(internal_fresh_var ~x:("concat" ^ var_name dec_info) sty)
          in
          let new_params = r :: bef_params in
          let new_exists = p :: bef.exists in
          let new_body =
            Logic.Lang.(
              mapp Measure.concat_str_list [] [ ?/p ] = ?/r && body bef)
          in
          return (new_params, new_exists, new_body)
      | Sort.S_list (Sort.S_bytes as sty) ->
          let r =
            Logic.(internal_fresh_var ~x:("concat" ^ var_name dec_info) sty)
          in
          let new_params = r :: bef_params in
          let new_exists = p :: bef.exists in
          let new_body =
            Logic.Lang.(
              mapp Measure.concat_bytes_list [] [ ?/p ] = ?/r && body bef)
          in
          return (new_params, new_exists, new_body)
      | Sort.S_string ->
          match_cons bef_params >>=? fun (q, bef_params) ->
          let r =
            Logic.(internal_fresh_var ~x:("concat" ^ var_name dec_info) ty)
          in
          let new_params = r :: bef_params in
          let new_exists = p :: q :: bef.exists in
          let new_body = Logic.Lang.(concat_str ?/p ?/q = ?/r && body bef) in
          return (new_params, new_exists, new_body)
      | Sort.S_bytes ->
          match_cons bef_params >>=? fun (q, bef_params) ->
          let r =
            Logic.(internal_fresh_var ~x:("concat" ^ var_name dec_info) ty)
          in
          let new_params = r :: bef_params in
          let new_exists = p :: q :: bef.exists in
          let new_body = Logic.Lang.(concat_bytes ?/p ?/q = ?/r && body bef) in
          return (new_params, new_exists, new_body)
      | _ -> failwith "error in CONCAT")
      >>=? fun (new_params, new_exists, new_body) ->
      Constraint.mk_condition coll_info env new_params new_exists new_body
      |> ret
  | Prim (dec_info, I_SIZE, _, _) -> (
      match_cons bef.params >>=? fun (((_, ty) as p), bef_params) ->
      match ty with
      | Sort.S_list _ ->
          let q =
            Logic.(internal_fresh_var ~x:("size" ^ var_name dec_info) S_nat)
          in
          let new_params = q :: bef_params in
          let new_exists = p :: bef.exists in
          let new_body = Logic.Lang.(size_list ?/p = ?/q && body bef) in
          Constraint.mk_condition coll_info env new_params new_exists new_body
          |> ret
      | Sort.S_set sty ->
          let q =
            Logic.(internal_fresh_var ~x:("size" ^ var_name dec_info) S_nat)
          in
          let new_params = q :: bef_params in
          let new_exists = p :: bef.exists in
          let new_body =
            Logic.Lang.(mapp Measure.size_set [ sty ] [ ?/p ] = ?/q && body bef)
          in
          Constraint.mk_condition coll_info env new_params new_exists new_body
          |> ret
      | Sort.S_map (key_ty, val_ty) ->
          let q =
            Logic.(internal_fresh_var ~x:("size" ^ var_name dec_info) S_nat)
          in
          let new_params = q :: bef_params in
          let new_exists = p :: bef.exists in
          let new_body =
            Logic.Lang.(
              mapp Measure.size_map [ key_ty; val_ty ] [ ?/p ] = ?/q && body bef)
          in
          Constraint.mk_condition coll_info env new_params new_exists new_body
          |> ret
      | Sort.S_string ->
          let q =
            Logic.(internal_fresh_var ~x:("size" ^ var_name dec_info) S_nat)
          in
          let new_params = q :: bef_params in
          let new_exists = p :: bef.exists in
          let new_body = Logic.Lang.(len_str ?/p = ?/q && body bef) in
          Constraint.mk_condition coll_info env new_params new_exists new_body
          |> ret
      | Sort.S_bytes ->
          let q =
            Logic.(internal_fresh_var ~x:("size" ^ var_name dec_info) S_nat)
          in
          let new_params = q :: bef_params in
          let new_exists = p :: bef.exists in
          let new_body = Logic.Lang.(len_bytes ?/p = ?/q && body bef) in
          Constraint.mk_condition coll_info env new_params new_exists new_body
          |> ret
      | _ -> failwith "error in SIZE")
  | Prim (dec_info, I_SLICE, _, _) -> (
      match_cons bef.params >>=? fun (p, bef_params) ->
      match_cons bef_params >>=? fun (q, bef_params) ->
      match_cons bef_params >>=? fun (r, bef_params) ->
      match snd r with
      | Sort.S_string ->
          let s =
            Logic.(
              internal_fresh_var
                ~x:("slice" ^ var_name dec_info)
                (S_option S_string))
          in
          let new_params = s :: bef_params in
          let new_exists = p :: q :: r :: bef.exists in
          let new_body =
            Logic.Lang.(?/s = substr_str_opt ?/r ?/p ?/q && body bef)
          in
          Constraint.mk_condition coll_info env new_params new_exists new_body
          |> ret
      | Sort.S_bytes ->
          let s =
            Logic.(
              internal_fresh_var
                ~x:("slice" ^ var_name dec_info)
                (S_option S_bytes))
          in
          let new_params = s :: bef_params in
          let new_exists = p :: q :: r :: bef.exists in
          let new_body =
            Logic.Lang.(?/s = substr_bytes_opt ?/r ?/p ?/q && body bef)
          in
          Constraint.mk_condition coll_info env new_params new_exists new_body
          |> ret
      | _ -> failwith "error in SLICE")
  | Prim (_, I_DROP, args, _) ->
      (match args with
      | [] -> return @@ Z.of_int 1
      | [ Int (_, n) ] -> return n
      | _ -> failwith "error in DROP")
      >>=? fun n ->
      pop_n_stack n bef.params >>=? fun (hds, bef_params) ->
      let new_exists = hds @ bef.exists in
      let new_body = body bef in
      Constraint.mk_condition coll_info env bef_params new_exists new_body
      |> ret
  | Prim (dec_info, I_DUP, args, _) ->
      (match args with
      | [] -> return @@ Z.of_int 1
      | [ Int (_, n) ] -> return n
      | _ -> failwith "error in DUP")
      >>=? fun n ->
      pop_n_stack (Z.pred n) bef.params >>=? fun (_, bef_params) ->
      match_cons bef_params >>=? fun (((_, ty) as p), _) ->
      let q = Logic.(internal_fresh_var ~x:("dup" ^ var_name dec_info) ty) in
      let new_params = q :: bef.params in
      let new_exists = bef.exists in
      let new_body = Logic.Lang.(?/q = ?/p && body bef) in
      Constraint.mk_condition coll_info env new_params new_exists new_body
      |> ret
  | Prim (_, I_DIG, args, _) ->
      (match args with
      | [ Int (_, n) ] -> return n
      | _ -> failwith "error in DIG")
      >>=? fun n ->
      pop_n_stack n bef.params >>=? fun (hds, bef_params) ->
      match_cons bef_params >>=? fun (p, bef_params) ->
      let new_params = (p :: hds) @ bef_params in
      let new_exists = bef.exists in
      let new_body = body bef in
      Constraint.mk_condition coll_info env new_params new_exists new_body
      |> ret
  | Prim (_, I_DUG, args, _) ->
      (match args with
      | [ Int (_, n) ] -> return n
      | _ -> failwith "error in DUG")
      >>=? fun n ->
      match_cons bef.params >>=? fun (p, bef_params) ->
      pop_n_stack n bef_params >>=? fun (hds, bef_params) ->
      let new_params = hds @ (p :: bef_params) in
      let new_exists = bef.exists in
      let new_body = body bef in
      Constraint.mk_condition coll_info env new_params new_exists new_body
      |> ret
  | Prim (_, I_SWAP, _, _) ->
      match_cons bef.params >>=? fun (p, bef_params) ->
      match_cons bef_params >>=? fun (q, bef_params) ->
      let new_params = q :: p :: bef_params in
      let new_exists = bef.exists in
      let new_body = body bef in
      Constraint.mk_condition coll_info env new_params new_exists new_body
      |> ret
  | Prim (_, I_DIP, args, _) -> (
      (match args with
      | [ code ] -> return (Z.of_int 1, code)
      | [ Int (_, n); code ] -> return (n, code)
      | _ -> failwith "error in DIP")
      >>=? fun (n, code) ->
      pop_n_stack n bef.params >>=? fun (hds, params) ->
      let new_env = List.rev hds @ env in
      let new_exists = bef.exists in
      let new_body = body bef in
      let coll_info, aft_dec =
        Constraint.mk_condition coll_info new_env params new_exists new_body
      in
      collect new_env (coll_info, aft_dec) code
      >>=? fun (coll_info, j, excpt_opt) ->
      let coll_info, excpt_opt =
        match excpt_opt with
        | None -> (coll_info, None)
        | Some excpt ->
            let coll_info, excpt =
              Constraint.mk_exception coll_info env (hds @ excpt.exists)
                excpt.body
            in
            (coll_info, Some excpt)
      in
      match j with
      | Typed aft_dec ->
          let new_params = hds @ aft_dec.params in
          let new_exists = aft_dec.exists in
          let new_body = body aft_dec in
          let coll_info, aft =
            Constraint.mk_condition coll_info env new_params new_exists new_body
          in
          return (coll_info, Typed aft, excpt_opt)
      | Failed -> return (coll_info, Failed, excpt_opt))
  | Prim (_, I_IF, args, _) ->
      (match args with
      | [ br1; br2 ] -> return (br1, br2)
      | _ -> failwith "error in IF")
      >>=? fun (br1, br2) ->
      match_cons bef.params >>=? fun (p, bef_params) ->
      let new_exists = p :: bef.exists in
      let coll_info, bef_br1 =
        Constraint.mk_condition coll_info env bef_params new_exists
          Logic.Lang.(?/p = c_bool true && body bef)
      in
      let coll_info, bef_br2 =
        Constraint.mk_condition coll_info env bef_params new_exists
          Logic.Lang.(?/p = c_bool false && body bef)
      in
      typing_branch env coll_info bef_br1 br1 bef_br2 br2
  | Prim (dec_info, I_IF_CONS, args, _) ->
      (match args with
      | [ br1; br2 ] -> return (br1, br2)
      | _ -> failwith "error in IF_CONS")
      >>=? fun (br1, br2) ->
      match_cons bef.params >>=? fun (p, bef_params) ->
      (match p with
      | _, Sort.S_list ty -> return ty
      | _ -> failwith "%s" "in IF_CONS")
      >>=? fun ty ->
      let ph =
        Logic.(internal_fresh_var ~x:("if_cons" ^ var_name dec_info) ty)
      in
      let pt =
        Logic.(
          internal_fresh_var ~x:("if_cons" ^ var_name dec_info) (S_list ty))
      in
      let new_params = ph :: pt :: bef_params in
      let new_exists = p :: bef.exists in
      let new_body = Logic.Lang.(?/p = cons ?/ph ?/pt && body bef) in
      let coll_info, bef_br1 =
        Constraint.mk_condition coll_info env new_params new_exists new_body
      in
      let new_exists = p :: bef.exists in
      let new_body = Logic.Lang.(?/p = nil ty && body bef) in
      let coll_info, bef_br2 =
        Constraint.mk_condition coll_info env bef_params new_exists new_body
      in
      typing_branch env coll_info bef_br1 br1 bef_br2 br2
  | Prim (dec_info, I_IF_LEFT, args, _) ->
      (match args with
      | [ br1; br2 ] -> return (br1, br2)
      | _ -> failwith "error in IF_LEFT")
      >>=? fun (br1, br2) ->
      match_cons bef.params >>=? fun (p, bef_params) ->
      (match p with
      | _, Sort.S_or (left_t, right_t) -> return (left_t, right_t)
      | _ -> failwith "%s" "in IF_LEFT")
      >>=? fun (left_t, right_t) ->
      let q =
        Logic.(internal_fresh_var ~x:("if_left" ^ var_name dec_info) left_t)
      in
      let new_params = q :: bef_params in
      let new_exists = p :: bef.exists in
      let new_body = Logic.Lang.(?/p = left right_t ?/q && body bef) in
      let coll_info, bef_br1 =
        Constraint.mk_condition coll_info env new_params new_exists new_body
      in
      let q =
        Logic.(internal_fresh_var ~x:("if_left" ^ var_name dec_info) right_t)
      in
      let new_params = q :: bef_params in
      let new_exists = p :: bef.exists in
      let new_body = Logic.Lang.(?/p = right left_t ?/q && body bef) in
      let coll_info, bef_br2 =
        Constraint.mk_condition coll_info env new_params new_exists new_body
      in
      typing_branch env coll_info bef_br1 br1 bef_br2 br2
  | Prim (dec_info, I_IF_NONE, args, _) ->
      (match args with
      | [ br1; br2 ] -> return (br1, br2)
      | _ -> failwith "error in IF_NONE")
      >>=? fun (br1, br2) ->
      match_cons bef.params >>=? fun (p, bef_params) ->
      (match p with
      | _, Sort.S_option ty -> return ty
      | _ -> failwith "%s" "in IF_NONE")
      >>=? fun ty ->
      let new_exists = p :: bef.exists in
      let new_body = Logic.Lang.(?/p = none ty && body bef) in
      let coll_info, bef_br1 =
        Constraint.mk_condition coll_info env bef_params new_exists new_body
      in
      let q =
        Logic.(internal_fresh_var ~x:("if_none" ^ var_name dec_info) ty)
      in
      let new_params = q :: bef_params in
      let new_exists = p :: bef.exists in
      let new_body = Logic.Lang.(?/p = some ?/q && body bef) in
      let coll_info, bef_br2 =
        Constraint.mk_condition coll_info env new_params new_exists new_body
      in
      typing_branch env coll_info bef_br1 br1 bef_br2 br2
  | Prim (dec_info, I_LAMBDA, args, _) ->
      (* In the case of I_LAMBDA, Micheline.node looks like "Prim (dec_info, I_LAMBDA, [arg_type; res_type; code], _)" *)
      (match args with
      | [ _; _; code ] -> return code
      | _ -> failwith "error in LAMBDA")
      >>=? fun code ->
      retrieve_lambda_annot dec_info >>=? fun lambda_annot ->
      let _, arg_p, arg_cond = lambda_annot.arg in
      let res_loc, res_p, res_cond = lambda_annot.res in
      let err_loc, err_p, excpt_cond = lambda_annot.err in
      let coll_info, lambda_bef =
        Constraint.mk_condition coll_info [] [ arg_p ] [] arg_cond
      in
      let coll_info, lambda_aft =
        Constraint.mk_condition coll_info [ arg_p ] [ res_p ] [] res_cond
      in
      let coll_info, lambda_excpt =
        Constraint.mk_exception coll_info [ arg_p ] ~params:[ err_p ] []
          excpt_cond
      in
      let body_env = arg_p :: lambda_annot.env in
      let parg =
        Logic.internal_fresh_var ~x:("lambda" ^ var_name dec_info) @@ snd arg_p
      in
      let pre_params = [ parg ] in
      Constraint.subst_body ~new_params:[ Logic.Lang.(?/parg) ] lambda_bef
      >>=? fun lambda_bef_body ->
      let pre_cond = Logic.Lang.(?/arg_p = ?/parg && lambda_bef_body) in
      let coll_info, pre =
        Constraint.mk_condition coll_info body_env pre_params [] pre_cond
      in
      (* Type check the body.  We remove axiom (which is about
         self_address and does not hold in the body). *)
      let axiom = coll_info.axiom in
      collect body_env
        ({ coll_info with axiom = Logic.Lang.c_bool true }, pre)
        code
      >>=? fun (coll_info, j, code_excpt_opt) ->
      let coll_info = { coll_info with axiom } in
      (* Add constraint so that lambda body post condition implies annotated result type *)
      let coll_info =
        match j with
        | Typed code_aft ->
            let coll_info, aft_chk =
              Constraint.mk_condition coll_info body_env lambda_aft.params []
                lambda_aft.body
            in
            let coll_info =
              Constraint.add_subsume ~msg:"postcondition of LambdaAnnot"
                coll_info res_loc body_env code_aft aft_chk
            in
            let coll_info, excpt_chk =
              Constraint.mk_exception coll_info body_env
                ~params:lambda_excpt.params lambda_excpt.exists
                lambda_excpt.body
            in
            let coll_info =
              match code_excpt_opt with
              | None ->
                  let coll_info, bottom_excpt =
                    Constraint.mk_exception coll_info body_env []
                      Logic.Lang.(c_bool false)
                  in
                  Constraint.add_subsume ~msg:"ab-postcondition of LambdaAnnot"
                    coll_info err_loc body_env bottom_excpt excpt_chk
              | Some code_excpt ->
                  Constraint.add_subsume ~msg:"ab-postcondition of LambdaAnnot"
                    coll_info err_loc body_env code_excpt excpt_chk
            in
            coll_info
        | Failed -> coll_info
      in
      let p =
        Logic.(
          internal_fresh_var
            ~x:("lambda" ^ var_name dec_info)
            (S_lambda (snd arg_p, snd res_p)))
      in
      let new_params = p :: bef.params in
      let new_exists = bef.exists in
      Constraint.subst_body ~new_env:[ Logic.Lang.(?/parg) ] lambda_aft
      >>=? fun lambda_aft_body ->
      Constraint.subst_body ~new_env:[ Logic.Lang.(?/parg) ] lambda_excpt
      >>=? fun lambda_excpt_body ->
      let new_body =
        Logic.Lang.(
          lambda ?/p parg res_p err_p lambda_bef_body lambda_aft_body
            lambda_excpt_body
          && body bef)
      in
      Constraint.mk_condition coll_info env new_params new_exists new_body
      |> ret
  | Prim (dec_info, I_EXEC, _, _) ->
      match_cons bef.params >>=? fun (a, bef_params) ->
      match_cons bef_params >>=? fun (f, bef_params) ->
      (match f with
      | _, Sort.S_lambda (asort, rsort) -> return (asort, rsort)
      | _ ->
          failwith "The second argument of EXEC insturction is not a function.")
      >>=? fun (_, rsort) ->
      let retp =
        Logic.(internal_fresh_var ~x:("exec" ^ var_name dec_info) rsort)
      in
      let new_params = retp :: bef_params in
      let new_exists = a :: f :: bef.exists in
      let new_body = Logic.Lang.(call ?/f ?/a ?/retp && body bef) in
      let coll_info, aft =
        Constraint.mk_condition coll_info env new_params new_exists new_body
      in
      (* argの条件を付けられればより正確に見積もれそう *)
      let new_excpt_body = Logic.Lang.(call_err ?/f ?/a ?/err && body bef) in
      let coll_info, excpt =
        Constraint.mk_exception coll_info env (bef.params @ bef.exists)
          new_excpt_body
      in
      ret ~excpt (coll_info, aft)
  | Prim (dec_info, I_APPLY, _, _) ->
      match_cons bef.params >>=? fun (a, bef_params) ->
      match_cons bef_params >>=? fun (f, bef_params) ->
      (match f with
      | _, Sort.(S_lambda (S_pair (asort, bsort), rsort)) ->
          return (asort, bsort, rsort)
      | _ ->
          failwith "The second argument of EXEC insturction is not a function.")
      >>=? fun (_, bsort, rsort) ->
      let retp =
        Logic.(internal_fresh_var ~x:("apply" ^ var_name dec_info) rsort)
      in
      let funp =
        Logic.(
          internal_fresh_var
            ~x:("apply" ^ var_name dec_info)
            (S_lambda (bsort, rsort)))
      in
      let errp =
        Logic.(internal_fresh_var ~x:("apply" ^ var_name dec_info) S_exception)
      in
      let b =
        Logic.(internal_fresh_var ~x:("apply" ^ var_name dec_info) bsort)
      in
      let new_params = funp :: bef_params in
      let new_exists = a :: f :: bef.exists in
      let pexp = Logic.Lang.(pair ?/a ?/b) in
      let new_body =
        Logic.Lang.(
          lambda ?/funp b retp errp (c_bool true) (call ?/f pexp ?/retp)
            (call_err ?/f pexp ?/errp)
          && body bef)
      in
      Constraint.mk_condition coll_info env new_params new_exists new_body
      |> ret
  | Prim (dec_info, I_LOOP, args, _) ->
      (match args with
      | [ code ] -> return code
      | _ -> failwith "error in LOOP")
      >>=? fun code ->
      retrieve_loop_inv_annot dec_info >>=? fun inv_annot ->
      typing_loop env coll_info inv_annot bef
        ~continue_cond:(fun p -> Logic.Lang.(?/p = c_bool true))
        ~exit_cond:(fun p -> Logic.Lang.(?/p = c_bool false))
        code
  | Prim (dec_info, I_LOOP_LEFT, args, _) ->
      (match args with
      | [ code ] -> return code
      | _ -> failwith "error in LOOP_LEFT")
      >>=? fun code ->
      (match bef.params with
      | (_, Sort.S_or (left_t, right_t)) :: _ -> return (left_t, right_t)
      | _ -> failwith "Invalid stack top type in LOOP_LEFT")
      >>=? fun (left_t, right_t) ->
      retrieve_loop_inv_annot dec_info >>=? fun inv_annot ->
      let q =
        Logic.(internal_fresh_var ~x:("loop_left" ^ var_name dec_info) left_t)
      in
      let q' =
        Logic.(internal_fresh_var ~x:("loop_left" ^ var_name dec_info) right_t)
      in
      typing_loop env coll_info inv_annot bef ~extra_bef_param:q
        ~extra_aft_param:q'
        ~continue_cond:(fun p -> Logic.Lang.(?/p = left right_t ?/q))
        ~exit_cond:(fun p -> Logic.Lang.(?/p = right left_t ?/q'))
        code
  | Prim (dec_info, I_ITER, args, _) -> (
      (match args with
      | [ code ] -> return code
      | _ -> failwith "error in ITER")
      >>=? fun code ->
      retrieve_loop_inv_annot dec_info >>=? fun inv_annot ->
      match bef.params with
      | (_, Sort.S_list ty) :: _ ->
          let ph =
            Logic.(internal_fresh_var ~x:("iter" ^ var_name dec_info) ty)
          in
          let pt =
            Logic.(
              internal_fresh_var ~x:("iter" ^ var_name dec_info) (S_list ty))
          in
          typing_loop env coll_info inv_annot bef ~replacer:pt
            ~extra_bef_param:ph
            ~continue_cond:(fun p -> Logic.Lang.(?/p = cons ?/ph ?/pt))
            ~exit_cond:(fun p -> Logic.Lang.(?/p = nil ty))
            code
      | (_, Sort.S_set ty) :: _ ->
          let ph =
            Logic.(internal_fresh_var ~x:("iter" ^ var_name dec_info) ty)
          in
          let pt =
            Logic.(
              internal_fresh_var ~x:("iter" ^ var_name dec_info) (S_set ty))
          in
          typing_loop env coll_info inv_annot bef ~replacer:pt
            ~extra_bef_param:ph
            ~continue_cond:(fun p ->
              Logic.Lang.(mem_set ?/ph ?/p && remove_set ?/ph ?/p = ?/pt))
            ~exit_cond:(fun p -> Logic.Lang.(?/p = empty_set ty))
            code
      | (_, Sort.S_map (key_ty, val_ty)) :: _ ->
          let ph =
            Logic.(
              internal_fresh_var
                ~x:("iter" ^ var_name dec_info)
                (S_pair (key_ty, val_ty)))
          in
          let pt =
            Logic.(
              internal_fresh_var
                ~x:("iter" ^ var_name dec_info)
                (S_map (key_ty, val_ty)))
          in
          typing_loop env coll_info inv_annot bef ~replacer:pt
            ~extra_bef_param:ph
            ~continue_cond:(fun p ->
              Logic.Lang.(
                find_opt_map (fst ?/ph) ?/p = some (snd ?/ph)
                && update_map (fst ?/ph) (none val_ty) ?/p = ?/pt))
            ~exit_cond:(fun p -> Logic.Lang.(?/p = empty_map key_ty val_ty))
            code
      | _ -> failwith "Invalid stack top type in ITER")
  | Prim (dec_info, I_MAP, args, _) -> (
      match_singleton_list args >>=? fun code ->
      retrieve_map_inv_annot dec_info >>=? fun map_annot ->
      (* construct internally used declaration *)
      let coll_info, inv_dec =
        Constraint.mk_condition coll_info env map_annot.params [] map_annot.body
      in
      match_cons map_annot.params >>=? fun (res, params) ->
      match_cons params >>=? fun (org, params) ->
      let mk_body_from_inv res org =
        Constraint.subst_body
          ~new_params:(res :: org :: List.map Logic.Lang.( ?/ ) params)
          inv_dec
        >>=? fun inv_dec_body ->
        return Logic.Lang.(inv_dec_body && coll_info.axiom)
      in
      match (res, org) with
      | (_, Sort.S_list res_ty), (_, Sort.S_list org_ty) -> (
          let px =
            Logic.(internal_fresh_var ~x:("map" ^ var_name dec_info) org_ty)
          in
          let py =
            Logic.(internal_fresh_var ~x:("map" ^ var_name dec_info) res_ty)
          in
          let pxs =
            Logic.(
              internal_fresh_var ~x:("map" ^ var_name dec_info) (S_list org_ty))
          in
          let pys =
            Logic.(
              internal_fresh_var ~x:("map" ^ var_name dec_info) (S_list res_ty))
          in
          (* construct pre-condition of map body *)
          let body_env = pys :: pxs :: env in
          let body_bef_params = px :: params in
          mk_body_from_inv Logic.Lang.(?/pys) Logic.Lang.(cons ?/px ?/pxs)
          >>=? fun body_bef_body ->
          let coll_info, body_bef =
            Constraint.mk_condition coll_info body_env body_bef_params []
              body_bef_body
          in
          (* infer post-condition of body *)
          collect body_env (coll_info, body_bef) code
          >>=? fun (coll_info, j, body_excpt_opt) ->
          (* check if inferred condition satisfies typing rule *)
          (match j with
          | Typed body_aft ->
              let body_aft_params = py :: params in
              mk_body_from_inv
                Logic.Lang.(append ?/pys (cons ?/py (nil res_ty)))
                Logic.Lang.(?/pxs)
              >>=? fun body_aft_body ->
              let coll_info, body_aft' =
                Constraint.mk_condition coll_info body_env body_aft_params []
                  body_aft_body
              in
              let coll_info =
                Constraint.add_subsume ~msg:"MapInv after loop iteration"
                  coll_info map_annot.loc body_env body_aft body_aft'
              in
              return coll_info
          | Failed -> return coll_info)
          >>=? fun coll_info ->
          (* construct result condition *)
          let new_params = res :: params in
          mk_body_from_inv Logic.Lang.(?/res) Logic.Lang.(nil org_ty)
          >>=? fun new_body ->
          let coll_info, aft =
            Constraint.mk_condition coll_info env new_params [] new_body
          in
          match body_excpt_opt with
          | None -> ret (coll_info, aft)
          | Some excpt ->
              let coll_info, excpt =
                Constraint.mk_exception coll_info env
                  (pys :: pxs :: excpt.exists)
                  excpt.body
              in
              ret ~excpt (coll_info, aft))
      | (_, Sort.S_map (res_kt, res_vt)), (_, Sort.S_map (org_kt, org_vt))
        when res_kt = org_kt -> (
          (* type environment for map body *)
          let pk =
            Logic.(internal_fresh_var ~x:("map" ^ var_name dec_info) res_kt)
          in
          let pxs =
            Logic.(
              internal_fresh_var
                ~x:("map" ^ var_name dec_info)
                (S_map (res_kt, org_vt)))
          in
          let pys =
            Logic.(
              internal_fresh_var
                ~x:("map" ^ var_name dec_info)
                (S_map (res_kt, res_vt)))
          in
          let body_env = pk :: pys :: pxs :: env in
          (* construct pre-condition of map body *)
          let px =
            Logic.(
              internal_fresh_var
                ~x:("map" ^ var_name dec_info)
                (S_pair (res_kt, org_vt)))
          in
          let body_bef_params = px :: params in
          Logic.Lang.(
            mk_body_from_inv ?/pys (update_map ?/pk (some (snd ?/px)) ?/pxs))
          >>=? fun body_from_inv ->
          let body_bef_body =
            Logic.Lang.(
              ?/pk = fst ?/px
              && find_opt_map ?/pk ?/pxs = none org_vt
              && find_opt_map ?/pk ?/pys = none res_vt
              && body_from_inv)
          in
          let coll_info, body_bef =
            Constraint.mk_condition coll_info body_env body_bef_params []
              body_bef_body
          in
          (* infer post-condition of body *)
          collect body_env (coll_info, body_bef) code
          >>=? fun (coll_info, j, body_excpt_opt) ->
          (match j with
          | Typed body_aft ->
              (* check if inferred condition satisfies typing rule *)
              let py =
                Logic.(internal_fresh_var ~x:("map" ^ var_name dec_info) res_vt)
              in
              let body_aft_params = py :: params in
              Logic.Lang.(
                mk_body_from_inv (update_map ?/pk (some ?/py) ?/pys) ?/pxs)
              >>=? fun body_aft_body ->
              let coll_info, body_aft' =
                Constraint.mk_condition coll_info body_env body_aft_params []
                  body_aft_body
              in
              let coll_info =
                Constraint.add_subsume ~msg:"MapInv after loop iteration"
                  coll_info map_annot.loc body_env body_aft body_aft'
              in
              return coll_info
          | Failed -> return coll_info)
          >>=? fun coll_info ->
          (* construct result condition *)
          let new_params = res :: params in
          mk_body_from_inv
            Logic.Lang.(?/res)
            Logic.Lang.(empty_map res_kt org_vt)
          >>=? fun new_body ->
          let coll_info, aft =
            Constraint.mk_condition coll_info env new_params [] new_body
          in
          match body_excpt_opt with
          | None -> ret (coll_info, aft)
          | Some excpt ->
              let coll_info, excpt =
                Constraint.mk_exception coll_info env
                  (pk :: pys :: pxs :: excpt.exists)
                  excpt.body
              in
              ret ~excpt (coll_info, aft))
      | _ -> failwith "Unexpected error __LOC__")
  | Prim (dec_info, I_BALANCE, _, _) ->
      let p =
        Logic.(internal_fresh_var ~x:("balance" ^ var_name dec_info) S_mutez)
      in
      let new_params = p :: bef.params in
      let new_exists = bef.exists in
      let new_body = Logic.Lang.(?/p = balance && body bef) in
      Constraint.mk_condition coll_info env new_params new_exists new_body
      |> ret
  | Prim (dec_info, I_AMOUNT, _, _) ->
      let p =
        Logic.(internal_fresh_var ~x:("amount" ^ var_name dec_info) S_mutez)
      in
      let new_params = p :: bef.params in
      let new_exists = bef.exists in
      let new_body = Logic.Lang.(?/p = amount && body bef) in
      Constraint.mk_condition coll_info env new_params new_exists new_body
      |> ret
  | Prim (dec_info, I_NOW, _, _) ->
      let p =
        Logic.(internal_fresh_var ~x:("now" ^ var_name dec_info) S_timestamp)
      in
      let new_params = p :: bef.params in
      let new_exists = bef.exists in
      let new_body = Logic.Lang.(?/p = now && body bef) in
      Constraint.mk_condition coll_info env new_params new_exists new_body
      |> ret
  | Prim (dec_info, I_SELF, _, annots) -> (
      let entrypoint =
        annots
        |> List.filter (fun s -> s.[0] = '%')
        |> List.hd
        |> Option.map (fun s -> if s = "%" then "%default" else s)
      in
      match
        List.assoc_opt ~equal:( = )
          Option.(value ~default:"%default" entrypoint)
          coll_info.self_entries
      with
      | Some param_ty ->
          let p =
            Logic.(
              internal_fresh_var
                ~x:("self" ^ var_name dec_info)
                (S_contract param_ty))
          in
          let new_params = p :: bef.params in
          let new_exists = bef.exists in
          let new_body =
            Logic.Lang.(?/p = self ?entrypoint param_ty && body bef)
          in
          Constraint.mk_condition coll_info env new_params new_exists new_body
          |> ret
      | None -> failwith "SELF need self_sort, but it is not set")
  | Prim (dec_info, I_SELF_ADDRESS, _, _) ->
      let p =
        Logic.(
          internal_fresh_var ~x:("self_address" ^ var_name dec_info) S_address)
      in
      let new_params = p :: bef.params in
      let new_exists = bef.exists in
      let new_body = Logic.Lang.(?/p = self_addr && body bef) in
      Constraint.mk_condition coll_info env new_params new_exists new_body
      |> ret
  | Prim (dec_info, I_SOURCE, _, _) ->
      let p =
        Logic.(internal_fresh_var ~x:("source" ^ var_name dec_info) S_address)
      in
      let new_params = p :: bef.params in
      let new_exists = bef.exists in
      let new_body = Logic.Lang.(?/p = source && body bef) in
      Constraint.mk_condition coll_info env new_params new_exists new_body
      |> ret
  | Prim (dec_info, I_SENDER, _, _) ->
      let p =
        Logic.(internal_fresh_var ~x:("sender" ^ var_name dec_info) S_address)
      in
      let new_params = p :: bef.params in
      let new_exists = bef.exists in
      let new_body = Logic.Lang.(?/p = sender && body bef) in
      Constraint.mk_condition coll_info env new_params new_exists new_body
      |> ret
  | Prim (dec_info, I_CONTRACT, args, annots) ->
      let entrypoint =
        annots
        |> List.filter (fun s -> s.[0] = '%')
        |> List.hd
        |> Option.map (fun s -> if s = "%" then "%default" else s)
      in
      match_cons bef.params >>=? fun (p, bef_params) ->
      match_singleton_list args >>=? sort_of_micheline_ty >>=? fun ty ->
      let q =
        Logic.(
          internal_fresh_var
            ~x:("contract" ^ var_name dec_info)
            (S_option (S_contract ty)))
      in
      let new_params = q :: bef_params in
      let new_exists = p :: bef.exists in
      let new_body =
        Logic.Lang.(?/q = contract_opt ?entrypoint ty ?/p && body bef)
      in
      Constraint.mk_condition coll_info env new_params new_exists new_body
      |> ret
  | Prim (dec_info, I_ADDRESS, _, _) ->
      match_cons bef.params >>=? fun (p, bef_params) ->
      (match p with
      | _, Sort.S_contract t -> return t
      | _ -> failwith "The argument of ADDRESS instruction must be contract.")
      >>=? fun t_store ->
      let q =
        Logic.(internal_fresh_var ~x:("address" ^ var_name dec_info) S_address)
      in
      let new_params = q :: bef_params in
      let new_exists = p :: bef.exists in
      let new_body =
        Logic.Lang.(contract_opt t_store ?/q = some ?/p && body bef)
      in
      Constraint.mk_condition coll_info env new_params new_exists new_body
      |> ret
  | Prim (dec_info, I_HASH_KEY, _, _) ->
      match_cons bef.params >>=? fun (p, bef_params) ->
      let q =
        Logic.(
          internal_fresh_var ~x:("hash_key" ^ var_name dec_info) S_key_hash)
      in
      let new_params = q :: bef_params in
      let new_exists = p :: bef.exists in
      (* TODO: hash_uf の premise 生成?  *)
      let new_body = Logic.Lang.(?/q = b58check ?/p && body bef) in
      Constraint.mk_condition coll_info env new_params new_exists new_body
      |> ret
  | Prim (dec_info, I_BLAKE2B, _, _) ->
      match_cons bef.params >>=? fun (p, bef_params) ->
      let q =
        Logic.(internal_fresh_var ~x:("blake2b" ^ var_name dec_info) S_bytes)
      in
      let new_params = q :: bef_params in
      let new_exists = p :: bef.exists in
      let new_body =
        Logic.Lang.(?/q = blake2b ?/p && len_str ?/q = c_nat 32 && body bef)
      in
      Constraint.mk_condition coll_info env new_params new_exists new_body
      |> ret
  | Prim (dec_info, I_KECCAK, _, _) ->
      match_cons bef.params >>=? fun (p, bef_params) ->
      let q =
        Logic.(internal_fresh_var ~x:("keccak" ^ var_name dec_info) S_bytes)
      in
      let new_params = q :: bef_params in
      let new_exists = p :: bef.exists in
      let new_body =
        Logic.Lang.(?/q = keccak ?/p && len_str ?/q = c_nat 32 && body bef)
      in
      Constraint.mk_condition coll_info env new_params new_exists new_body
      |> ret
  | Prim (dec_info, I_SHA256, _, _) ->
      match_cons bef.params >>=? fun (p, bef_params) ->
      let q =
        Logic.(internal_fresh_var ~x:("sha256" ^ var_name dec_info) S_bytes)
      in
      let new_params = q :: bef_params in
      let new_exists = p :: bef.exists in
      let new_body =
        Logic.Lang.(?/q = sha256 ?/p && len_str ?/q = c_nat 32 && body bef)
      in
      Constraint.mk_condition coll_info env new_params new_exists new_body
      |> ret
  | Prim (dec_info, I_SHA512, _, _) ->
      match_cons bef.params >>=? fun (p, bef_params) ->
      let q =
        Logic.(internal_fresh_var ~x:("sha512" ^ var_name dec_info) S_bytes)
      in
      let new_params = q :: bef_params in
      let new_exists = p :: bef.exists in
      let new_body =
        Logic.Lang.(?/q = sha512 ?/p && len_str ?/q = c_nat 64 && body bef)
      in
      Constraint.mk_condition coll_info env new_params new_exists new_body
      |> ret
  | Prim (dec_info, I_SHA3, _, _) ->
      match_cons bef.params >>=? fun (p, bef_params) ->
      let q =
        Logic.(internal_fresh_var ~x:("sha3" ^ var_name dec_info) S_bytes)
      in
      let new_params = q :: bef_params in
      let new_exists = p :: bef.exists in
      let new_body =
        Logic.Lang.(?/q = sha3 ?/p && len_str ?/q = c_nat 32 && body bef)
      in
      Constraint.mk_condition coll_info env new_params new_exists new_body
      |> ret
  | Prim (dec_info, I_CHAIN_ID, _, _) ->
      let p =
        Logic.(
          internal_fresh_var ~x:("chain_id" ^ var_name dec_info) S_chain_id)
      in
      let new_params = p :: bef.params in
      let new_exists = bef.exists in
      let new_body = Logic.Lang.(?/p = chain_id && body bef) in
      Constraint.mk_condition coll_info env new_params new_exists new_body
      |> ret
  | Prim (dec_info, I_LEVEL, _, _) ->
      let p =
        Logic.(internal_fresh_var ~x:("level" ^ var_name dec_info) S_nat)
      in
      let new_params = p :: bef.params in
      let new_exists = bef.exists in
      let new_body = Logic.Lang.(?/p = level && body bef) in
      Constraint.mk_condition coll_info env new_params new_exists new_body
      |> ret
  | Prim (dec_info, I_TOTAL_VOTING_POWER, _, _) ->
      let p =
        Logic.(
          internal_fresh_var ~x:("total_voting_power" ^ var_name dec_info) S_nat)
      in
      let new_params = p :: bef.params in
      let new_exists = bef.exists in
      let new_body = Logic.Lang.(?/p = total_voting_power && body bef) in
      Constraint.mk_condition coll_info env new_params new_exists new_body
      |> ret
  | Prim (dec_info, I_CHECK_SIGNATURE, _, _) ->
      match_cons bef.params >>=? fun (p, bef_params) ->
      match_cons bef_params >>=? fun (q, bef_params) ->
      match_cons bef_params >>=? fun (r, bef_params) ->
      let b =
        Logic.(
          internal_fresh_var ~x:("check_signature" ^ var_name dec_info) S_bool)
      in
      let new_params = b :: bef_params in
      let new_exists = p :: q :: r :: bef.exists in
      let new_body =
        Logic.Lang.(xor (neg ?/b) (signature ?/p ?/r = ?/q) && body bef)
      in
      Constraint.mk_condition coll_info env new_params new_exists new_body
      |> ret
  | Prim (dec_info, I_IMPLICIT_ACCOUNT, _, _) ->
      match_cons bef.params >>=? fun (p, bef_params) ->
      let q =
        Logic.(
          internal_fresh_var
            ~x:("implicit_account" ^ var_name dec_info)
            (S_contract S_unit))
      in
      let new_params = q :: bef_params in
      let new_exists = p :: bef.exists in
      (* typecheck で Contract Unit であることは保証されてるしいらないかも *)
      let new_body =
        Logic.Lang.(
          ?/q = implicit_account ?/p
          && contract_ty_opt (address_of_key_hash ?/p) = some (c_sort S_unit)
          && body bef)
      in
      Constraint.mk_condition coll_info env new_params new_exists new_body
      |> ret
  | Prim (dec_info, I_TRANSFER_TOKENS, _, _) ->
      match_cons bef.params >>=? fun (p, bef_params) ->
      match_cons bef_params >>=? fun (q, bef_params) ->
      match_cons bef_params >>=? fun (r, bef_params) ->
      let _, data_ty = p in
      let op =
        Logic.(
          internal_fresh_var
            ~x:("transfer_tokens" ^ var_name dec_info)
            S_operation)
      in
      let new_params = op :: bef_params in
      let new_exists = p :: q :: r :: bef.exists in
      let new_body =
        Logic.Lang.(transfer_tokens data_ty ?/p ?/q ?/r = ?/op && body bef)
      in
      Constraint.mk_condition coll_info env new_params new_exists new_body
      |> ret
  | Prim (dec_info, I_SET_DELEGATE, _, _) ->
      match_cons bef.params >>=? fun (p, bef_params) ->
      let op =
        Logic.(
          internal_fresh_var ~x:("set_delegate" ^ var_name dec_info) S_operation)
      in
      let new_params = op :: bef_params in
      let new_exists = p :: bef.exists in
      let new_body = Logic.Lang.(set_delegate ?/p = ?/op && body bef) in
      Constraint.mk_condition coll_info env new_params new_exists new_body
      |> ret
  | Prim (dec_info, I_CREATE_CONTRACT, _, _) ->
      match_cons bef.params >>=? fun (p, bef_params) ->
      match_cons bef_params >>=? fun (q, bef_params) ->
      match_cons bef_params >>=? fun (r, bef_params) ->
      let _, storage_ty = r in
      let op =
        Logic.(
          internal_fresh_var
            ~x:("create_contract" ^ var_name dec_info)
            S_operation)
      in
      let s =
        Logic.(
          internal_fresh_var
            ~x:("create_contract" ^ var_name dec_info)
            S_address)
      in
      let new_params = op :: s :: bef_params in
      let new_exists = p :: q :: r :: bef.exists in
      let new_body =
        Logic.Lang.(
          create_contract storage_ty ?/p ?/q ?/r ?/s = ?/op && body bef)
      in
      Constraint.mk_condition coll_info env new_params new_exists new_body
      |> ret
  | Prim (dec_info, I_FAILWITH, _, _) ->
      (match dec_info with { aft; _ } -> return @@ aft) >>=? fun aft_ty ->
      List.map_es (fun ty -> sort_of_micheline_ty (root ty)) aft_ty
      >>=? fun sort_list ->
      let new_params =
        List.map
          (Logic.internal_fresh_var ~x:("failwith" ^ var_name dec_info))
          sort_list
      in
      let new_exists = [] in
      let new_body = Logic.Lang.(c_bool false) in
      let coll_info, _aft =
        Constraint.mk_condition coll_info env new_params new_exists new_body
      in
      let bef_hd = Stdlib.List.hd bef.params in
      let ty = Stdlib.snd bef_hd in
      let new_excpt_body = Logic.Lang.(?/err = error ty ?/bef_hd && body bef) in
      let coll_info, excpt =
        Constraint.mk_exception coll_info env (bef.params @ bef.exists)
          new_excpt_body
      in
      return (coll_info, Failed, Some excpt)
  | Prim (dec_info, I_VOTING_POWER, _, _) ->
      match_cons bef.params >>=? fun (p, bef_params) ->
      let q =
        Logic.internal_fresh_var ~x:("voting_power" ^ var_name dec_info) S_nat
      in
      let new_params = q :: bef_params in
      let new_exists = p :: bef.exists in
      let new_body = Logic.Lang.(?/q = voting_power ?/p && body bef) in
      Constraint.mk_condition coll_info env new_params new_exists new_body
      |> ret
  | Prim (_, (I_CAST | I_RENAME), _, _) ->
      (* I assume that *compatible* mentioned in the document of CAST means only type names differ in. *)
      ret (coll_info, bef)
  | Prim
      ( _,
        (( I_CREATE_ACCOUNT | I_STEPS_TO_QUOTA | I_LSL | I_LSR | I_NEVER
         | I_SAPLING_EMPTY_STATE | I_SAPLING_VERIFY_UPDATE | I_PAIRING_CHECK
         | I_TICKET | I_READ_TICKET | I_SPLIT_TICKET | I_JOIN_TICKETS
         | I_OPEN_CHEST | I_VIEW | I_LAMBDA_REC | I_MIN_BLOCK_TIME
         | I_TICKET_DEPRECATED | I_EMIT | I_BYTES | I_NAT ) as op),
        _,
        _ ) ->
      failwith "%s is not implemented" (string_of_prim op)
  | Prim
      ( _,
        ( T_bool | T_contract | T_int | T_key | T_key_hash | T_lambda | T_list
        | T_map | T_big_map | T_nat | T_option | T_or | T_pair | T_set
        | T_signature | T_string | T_bytes | T_mutez | T_timestamp | T_unit
        | T_operation | T_address | T_sapling_transaction | T_sapling_state
        | T_chain_id | T_never | T_bls12_381_g1 | T_bls12_381_g2
        | T_bls12_381_fr | T_ticket | T_chest_key | T_chest
        | T_tx_rollup_l2_address | T_sapling_transaction_deprecated ),
        _,
        _ )
  | Prim (_, H_constant, _, _)
  | Prim (_, (K_parameter | K_storage | K_code | K_view), _, _)
  | Prim
      ( _,
        ( D_False | D_Elt | D_Left | D_None | D_Pair | D_Right | D_Some | D_True
        | D_Unit | D_Lambda_rec ),
        _,
        _ )
  | Int _ | String _ | Bytes _ ->
      assert false
