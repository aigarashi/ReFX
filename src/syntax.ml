type location = Lexing.position * Lexing.position

let dummy_loc = (Lexing.dummy_pos, Lexing.dummy_pos)

type 'a located = { loc : location; body : 'a }
type variable = string
type constructor = string

type parsed_rtype = parsed_rtype_desc located
and parsed_rtype_desc = parsed_stack * parsed_exp
and parsed_stack = parsed_stack_desc located

and parsed_stack_desc =
  | P_stack_bottom of parsed_pattern
  | P_stack_cons of parsed_pattern * parsed_stack

and parsed_pattern = parsed_pattern_desc located

and parsed_pattern_desc =
  | P_pattern_constr of constructor * Sort.t list * parsed_pattern list
  | P_pattern_var of variable
  | P_pattern_any

and parsed_exp = parsed_exp_desc located

and parsed_exp_desc =
  | P_exp_if of parsed_exp * parsed_exp * parsed_exp
  | P_exp_match of parsed_exp * (parsed_pattern * parsed_exp) list
  | P_exp_bop of string * parsed_exp * parsed_exp
  | P_exp_uop of string * parsed_exp
  | P_exp_capp of constructor * parsed_exp list
  | P_exp_fapp of variable * string option * parsed_exp list
  | P_exp_var of variable
  | P_exp_number of int
  | P_exp_string of string
  | P_exp_bytes of bytes
  | P_exp_type_annot of parsed_exp * Sort.t
  | P_exp_lambda_annot of string * (parsed_rtype * parsed_rtype * parsed_rtype)
  | P_exp_masked_true
  | P_exp_masked_false

let mk_parsed_stack ?(loc = dummy_loc) (s : parsed_stack_desc) : parsed_stack =
  { body = s; loc }

let mk_parsed_pattern ?(loc = dummy_loc) (p : parsed_pattern_desc) :
    parsed_pattern =
  { body = p; loc }

let mk_parsed_exp ?(loc = dummy_loc) (e : parsed_exp_desc) : parsed_exp =
  { body = e; loc }

type parsed_condition = parsed_rtype

type parsed_lambda_annot = {
  arg : parsed_rtype;
  res : parsed_rtype;
  exc : parsed_rtype;
  env : (string * Sort.t * location) list;
}

type parsed_measure = {
  name : string;
  args : (variable * Sort.t) list;
  ret_ty : Sort.t;
  body : parsed_exp;
}

type parsed_decoration =
  | P_assert of parsed_condition
  | P_assume of parsed_condition
  | P_loop_inv of parsed_condition
  | P_map_inv of (variable * Sort.t * location) * parsed_condition
  | P_lambda_annot of parsed_lambda_annot
  | P_contract_annot of parsed_lambda_annot * string option
  | P_measure of parsed_measure
  | Z3Str of string

type 'a located_typed = { loc : location; body : 'a; ty : Sort.t }

(* 型付きの言語表現 *)
type typed_rtype = typed_rtype_desc located
and typed_rtype_desc = typed_stack * typed_exp
and typed_stack = typed_stack_desc located

and typed_stack_desc =
  | T_stack_nil
  | T_stack_cons of typed_pattern * typed_stack

and typed_pattern = typed_pattern_desc located_typed

and typed_pattern_desc =
  | T_pattern_constr of constructor * typed_pattern list
  | T_pattern_var of variable
  | T_pattern_any

and typed_exp = typed_exp_desc located_typed

and typed_exp_desc =
  | T_exp_match of typed_exp * (typed_pattern * typed_exp) list
  | T_exp_capp of constructor * typed_exp list
  | T_exp_fapp of variable * string option * typed_exp list
  | T_exp_mapp of
      (variable * string list * Sort.t list * Sort.t)
      * Sort.t list
      * typed_exp list
  | T_exp_var of variable
  | T_exp_number of int
  | T_exp_string of string
  | T_exp_bytes of bytes
  | T_exp_lambda_annot of string * (typed_rtype * typed_rtype * typed_rtype)

type typed_condition = typed_rtype

type typed_lambda_annot = {
  arg : typed_rtype;
  res : typed_rtype;
  exc : typed_rtype;
  annot_env : (variable * Sort.t) list;
}

type typed_measure = {
  ufid : string;
  tyvars : string list;
  args : (variable * Sort.t) list;
  ret_ty : Sort.t;
  body : typed_exp;
}

type typed_decoration =
  | T_assert of typed_condition
  | T_assume of typed_condition
  | T_loop_inv of typed_condition
  | T_lambda_annot of typed_lambda_annot
  | T_contract_annot of typed_lambda_annot
  | T_measure of typed_measure

type tyschema = TySchema of string list * Sort.t list * Sort.t

let normalize_signature signature =
  let mbytes = Signature.to_bytes signature in
  match Data_encoding.Binary.of_bytes_opt Signature.encoding mbytes with
  | Some v -> return @@ Signature.to_b58check v
  | None -> failwith "Signature decode error"

open Format

let pp_list_space fmt ppf = pp_print_list ~pp_sep:pp_print_space fmt ppf

let pp_list_comma fmt ppf =
  pp_print_list ~pp_sep:(fun fmt () -> fprintf fmt ",@ ") fmt ppf

let pp_location fmt (lpos, rpos) =
  let open Lexing in
  let l = lpos.pos_cnum - lpos.pos_bol in
  let r = rpos.pos_cnum - lpos.pos_bol in
  let line = lpos.pos_lnum in
  fprintf fmt "@[line %d, characters %d-%d@]" line l r

let pp_bytes fmt b = fprintf fmt "0x%s" (Bytes.to_string b)

let rec pp_parsed_rtype fmt ({ body = stack, exp; _ } : parsed_rtype) =
  fprintf fmt "@[{%a |@ %a}@]" pp_parsed_stack stack pp_parsed_exp exp

and pp_parsed_stack fmt (stack : parsed_stack) =
  match stack.body with
  | P_stack_bottom p -> fprintf fmt "%a" pp_parsed_pattern p
  | P_stack_cons (p, s) ->
      fprintf fmt "@[%a:@,%a@]" pp_parsed_pattern p pp_parsed_stack s

and pp_parsed_pattern fmt (pat : parsed_pattern) =
  match pat.body with
  | P_pattern_constr ("Pair", [], [ p1; p2 ]) ->
      fprintf fmt "@[(%a,@ %a)@]" pp_parsed_pattern p1 pp_parsed_pattern p2
  | P_pattern_constr ("Nil", [], []) -> fprintf fmt "[]"
  | P_pattern_constr ("Cons", [], [ p1; p2 ]) ->
      fprintf fmt "@[(%a::%a)@]" pp_parsed_pattern p1 pp_parsed_pattern p2
  | P_pattern_constr (c, [], []) -> fprintf fmt "@[%s@]" c
  | P_pattern_constr (c, [], pl) ->
      fprintf fmt "@[(%s@ %a)@]" c (pp_list_space pp_parsed_pattern) pl
  | P_pattern_constr (c, tyv, pl) ->
      fprintf fmt "@[(%s<%a>@ %a)@]" c
        (pp_list_space Sort.pp_sort)
        tyv
        (pp_list_space pp_parsed_pattern)
        pl
  | P_pattern_var v -> fprintf fmt "%s" v
  | P_pattern_any -> fprintf fmt "_"

and pp_parsed_exp fmt (exp : parsed_exp) =
  match exp.body with
  | P_exp_if (e1, e2, e3) ->
      fprintf fmt "@[(if@ %a@ then@ %a@ else@ %a)@]" pp_parsed_exp e1
        pp_parsed_exp e2 pp_parsed_exp e3
  | P_exp_match (e, pel) ->
      fprintf fmt "@[(match@ %a@ with@ %a)@]" pp_parsed_exp e
        (pp_print_list
           ~pp_sep:(fun fmt () -> fprintf fmt "@ | ")
           (fun fmt (p, e) ->
             fprintf fmt "@[%a ->@ %a@]" pp_parsed_pattern p pp_parsed_exp e))
        pel
  | P_exp_bop (op, e1, e2) ->
      fprintf fmt "@[(%a %s@ %a)@]" pp_parsed_exp e1 op pp_parsed_exp e2
  | P_exp_uop (op, e) -> fprintf fmt "%s%a" op pp_parsed_exp e
  | P_exp_capp ("Pair", [ e1; e2 ]) ->
      fprintf fmt "@[(%a,@ %a)@]" pp_parsed_exp e1 pp_parsed_exp e2
  | P_exp_capp (c, []) -> fprintf fmt "@[%s@]" c
  | P_exp_capp (c, el) ->
      fprintf fmt "@[(%s@ %a)@]" c (pp_list_space pp_parsed_exp) el
  | P_exp_fapp (v, annot, el) ->
      fprintf fmt "@[(%s@ %a@ %a)@]" v
        (pp_print_option pp_print_string)
        annot
        (pp_list_space pp_parsed_exp)
        el
  | P_exp_var v -> fprintf fmt "%s" v
  | P_exp_number n -> fprintf fmt "%d" n
  | P_exp_string s -> fprintf fmt "\"%S\"" s
  | P_exp_bytes b -> fprintf fmt "%a" pp_bytes b
  | P_exp_type_annot (e, s) ->
      fprintf fmt "@[(%a :@ %a)@]" pp_parsed_exp e Sort.pp_sort s
  | P_exp_lambda_annot (s, (r1, r2, r3)) ->
      fprintf fmt "@[(%s :> %a ->@ %a &@ %a)@]" s pp_parsed_rtype r1
        pp_parsed_rtype r2 pp_parsed_rtype r3
  | P_exp_masked_true -> fprintf fmt "....."
  | P_exp_masked_false -> fprintf fmt ",,,,,"

let pp_parsed_condition = pp_parsed_rtype

let pp_parsed_measure fmt ({ name; args; ret_ty; body } : parsed_measure) =
  fprintf fmt "@[{name: %s;@ args: %a;@ ret_ty: %a;@ body: %a}@]" name
    (pp_list_comma (fun fmt (x, t) -> fprintf fmt "(%s : %a)" x Sort.pp_sort t))
    args Sort.pp_sort ret_ty pp_parsed_exp body

let pp_parsed_lambda_annot fmt ({ arg; res; exc; env } : parsed_lambda_annot) =
  fprintf fmt "@[{@[arg: %a;@ res: %a;@ exc: %a;@ env: %a@]}@]" pp_parsed_rtype
    arg pp_parsed_rtype res pp_parsed_rtype exc
    (pp_list_comma (fun fmt (v, s, _) ->
         fprintf fmt "@[%s:%a@]" v Sort.pp_sort s))
    env

let pp_parsed_decoration fmt = function
  | P_assert d -> fprintf fmt "Assert %a" pp_parsed_condition d
  | P_assume d -> fprintf fmt "Assume %a" pp_parsed_condition d
  | P_loop_inv d -> fprintf fmt "LoopInv %a" pp_parsed_condition d
  | P_map_inv ((x, s, _), d) ->
      fprintf fmt "MapInv @[%s:%a@] %a" x Sort.pp_sort s pp_parsed_condition d
  | P_lambda_annot d -> fprintf fmt "LambdaAnnot %a" pp_parsed_lambda_annot d
  | P_contract_annot (d, None) ->
      fprintf fmt "ContractAnnot %a" pp_parsed_lambda_annot d
  | P_contract_annot (d, Some annot) ->
      fprintf fmt "ContractAnnot %s %a" annot pp_parsed_lambda_annot d
  | P_measure m -> fprintf fmt "Measure %a" pp_parsed_measure m
  | Z3Str str -> fprintf fmt "Z3Str %s" str

let rec pp_typed_rtype fmt ({ body = stack, exp; _ } : typed_rtype) =
  fprintf fmt "@[{%a |@ %a}@]" pp_typed_stack stack pp_typed_exp exp

and pp_typed_stack fmt (stack : typed_stack) =
  match stack.body with
  | T_stack_nil -> fprintf fmt "$"
  | T_stack_cons (p, s) ->
      fprintf fmt "@[%a ::@ %a@]" pp_typed_pattern p pp_typed_stack s

and pp_typed_pattern fmt (pattern : typed_pattern) =
  match pattern.body with
  | T_pattern_constr (c, pl) ->
      fprintf fmt "@[(%s@ %a)@]" c (pp_list_space pp_typed_pattern) pl
  | T_pattern_var x -> fprintf fmt "%s" x
  | T_pattern_any -> fprintf fmt "_"

and pp_typed_exp fmt (exp : typed_exp) =
  match exp.body with
  | T_exp_match (me, cl) ->
      fprintf fmt "@[(match %a with@ %a)@]" pp_typed_exp me
        (pp_print_list
           ~pp_sep:(fun fmt () -> fprintf fmt " |@ ")
           (fun fmt (p, e) ->
             fprintf fmt "%a ->@ %a" pp_typed_pattern p pp_typed_exp e))
        cl
  | T_exp_capp (c, el) ->
      fprintf fmt "@[(%s@ %a)@]" c (pp_list_space pp_typed_exp) el
  | T_exp_fapp (f, annot, el) ->
      fprintf fmt "@[(%s@ %a@ %a)@]" f
        (pp_print_option pp_print_string)
        annot
        (pp_list_space pp_typed_exp)
        el
  | T_exp_mapp ((f, _, _, _), sl, el) ->
      fprintf fmt "@[(%s@ %a %a)@]" f
        (pp_list_space Sort.pp_sort)
        sl
        (pp_list_space pp_typed_exp)
        el
  | T_exp_var v -> fprintf fmt "%s" v
  | T_exp_number n -> fprintf fmt "%d" n
  | T_exp_string s -> fprintf fmt "%s" s
  | T_exp_bytes b -> fprintf fmt "%a" pp_bytes b
  | T_exp_lambda_annot (f, (r1, r2, r3)) ->
      fprintf fmt "@[(%s :>@ %a ->@ %a &@ %a)@]" f pp_typed_rtype r1
        pp_typed_rtype r2 pp_typed_rtype r3
