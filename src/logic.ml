(** Predicate logic of refinement types.  The logic is a multi-sorted
   first order theory specialized for smart contracts.  Note that its
   a classical logic, so boolean terms and logical formulae are
   degenerated into one class.  *)

type variable = string * Sort.t
type binding = variable list

(** Constants  *)
type const =
  | K_sort of Sort.t
  | K_string of string
  | K_nat of int
  | K_int of int
  | K_bytes of bytes
  | K_bool of bool
  | K_unit
  | K_timestamp of int
  | K_mutez of int
  | K_mutez_max
  | K_key of string
  | K_key_hash of string
  | K_signature of string
  | K_chain_id of string
  | K_empty_set of Sort.t
  | K_empty_map of Sort.t * Sort.t
  | K_empty_big_map of Sort.t * Sort.t

(** Algebraic data types constructors *)
type constr =
  (* options *)
  | C_none
  | C_some
  (* sums *)
  | C_left
  | C_right
  (* pairs *)
  | C_pair
  (* lists *)
  | C_nil
  | C_cons
  (* operations *)
  | C_set_delegate
  | C_transfer
  | C_create_contract
  (* contracts *)
  | C_contract
  (* address *)
  | C_address
  (* exceptions *)
  | C_error
  | C_overflow

(** Function symbols *)
type fsymbol =
  (* boolean *)
  | F_ite
  | F_imply
  | F_conj
  | F_disj
  | F_neg
  (* comparison *)
  | F_eq
  | F_gt
  (* arithmetic operations *)
  | F_add
  | F_sub
  | F_mul
  | F_div
  | F_mod
  | F_abs
  (* set and map operations *)
  | F_set_add
  | F_set_remove
  | F_set_find
  | F_set_choose_opt
  | F_map_update
  | F_map_find
  | F_map_choose_opt
  | F_big_map_update
  | F_big_map_find
  (* string operations *)
  | F_str_concat
  | F_str_len
  | F_str_substr_opt
  | F_str_at_opt
  | F_str_lt
  (* list operation *)
  | F_append
  | F_size_list
  (* data packing operations *)
  | F_pack
  | F_unpack_opt
  (* tezos specific operations *)
  | F_self_addr
  | F_balance
  | F_amount
  | F_now
  | F_source
  | F_sender
  | F_current_chain_id
  | F_level
  | F_voting_power
  | F_total_voting_power
  | F_b58check
  | F_blake2b
  | F_keccak
  | F_sha256
  | F_sha512
  | F_sha3
  | F_sig
  | F_contract_ty_opt
  (* converter *)
  | F_address_of_key_hash

type msymbol = {
  ufid : string;
  t_vars : string list;
  t_params : Sort.t list;
  t_ret : Sort.t;
}
(** Measure symbol  *)

type exp = { desc : exp_desc; mutable ty : Sort.t }
(** Logical formulae and terms *)

and exp_desc =
  | E_symbol of variable
  (* constants *)
  | E_const of const
  (* datatype operations *)
  | E_data_constr of constr * Sort.t list * exp list
  | E_data_destr of constr * Sort.t list * exp * int
  | E_data_tester of constr * exp
  (* logical constructs *)
  | E_forall of binding * exp
  | E_exists of binding * exp
  | E_fapp of fsymbol * Sort.t list * exp list
  | E_mapp of msymbol * Sort.t list * exp list
  (* lambdas *)
  | E_lambda of exp * variable * variable * variable * exp * exp * exp
  | E_call of exp * exp * exp
  | E_call_err of exp * exp * exp
  (* instance of the axiom of measure functions *)
  (* | E_measure_axiom of exp list *)
  | E_unfold_measure of msymbol * Sort.t list * exp list * exp

let mk_exp ?(ty = Sort.S_var "") desc : exp = { desc; ty }

module Lang = struct
  (* constant literals *)
  let c_sort (s : Sort.t) : exp = mk_exp @@ E_const (K_sort s)
  let c_unit : exp = mk_exp @@ E_const K_unit
  let c_bool (b : bool) : exp = mk_exp @@ E_const (K_bool b)
  let c_int (n : int) : exp = mk_exp @@ E_const (K_int n)

  let c_nat (n : int) : exp =
    assert (n >= 0);
    mk_exp @@ E_const (K_nat n)

  let c_string (s : string) : exp = mk_exp @@ E_const (K_string s)
  let c_chain_id (s : string) : exp = mk_exp @@ E_const (K_chain_id s)
  let c_bytes (s : bytes) : exp = mk_exp @@ E_const (K_bytes s)
  let c_mutez (n : int) : exp = mk_exp @@ E_const (K_mutez n)
  let c_key_hash (s : string) : exp = mk_exp @@ E_const (K_key_hash s)
  let c_key (s : string) : exp = mk_exp @@ E_const (K_key s)
  let c_signature (s : string) : exp = mk_exp @@ E_const (K_signature s)
  let c_timestamp (n : int) : exp = mk_exp @@ E_const (K_timestamp n)

  (* logical constants *)
  let self_addr : exp = mk_exp @@ E_fapp (F_self_addr, [], [])
  let balance : exp = mk_exp @@ E_fapp (F_balance, [], [])
  let amount : exp = mk_exp @@ E_fapp (F_amount, [], [])
  let now : exp = mk_exp @@ E_fapp (F_now, [], [])
  let source : exp = mk_exp @@ E_fapp (F_source, [], [])
  let sender : exp = mk_exp @@ E_fapp (F_sender, [], [])
  let chain_id : exp = mk_exp @@ E_fapp (F_current_chain_id, [], [])
  let level : exp = mk_exp @@ E_fapp (F_level, [], [])
  let total_voting_power : exp = mk_exp @@ E_fapp (F_total_voting_power, [], [])

  (* logical functions *)
  let abs (e : exp) : exp = mk_exp @@ E_fapp (F_abs, [], [ e ])
  let voting_power (e : exp) : exp = mk_exp @@ E_fapp (F_voting_power, [], [ e ])

  let contract_ty_opt (e : exp) : exp =
    mk_exp @@ E_fapp (F_contract_ty_opt, [], [ e ])

  let b58check (k : exp) : exp = mk_exp @@ E_fapp (F_b58check, [], [ k ])
  let blake2b (b : exp) : exp = mk_exp @@ E_fapp (F_blake2b, [], [ b ])
  let keccak (b : exp) : exp = mk_exp @@ E_fapp (F_keccak, [], [ b ])
  let sha256 (b : exp) : exp = mk_exp @@ E_fapp (F_sha256, [], [ b ])
  let sha512 (b : exp) : exp = mk_exp @@ E_fapp (F_sha512, [], [ b ])
  let sha3 (b : exp) : exp = mk_exp @@ E_fapp (F_sha3, [], [ b ])

  let signature (k : exp) (b : exp) : exp =
    mk_exp @@ E_fapp (F_sig, [], [ k; b ])

  let concat_str (s1 : exp) (s2 : exp) : exp =
    mk_exp @@ E_fapp (F_str_concat, [], [ s1; s2 ])

  let len_str (s : exp) : exp = mk_exp @@ E_fapp (F_str_len, [], [ s ])

  let substr_str_opt (s : exp) (offset : exp) (length : exp) : exp =
    mk_exp @@ E_fapp (F_str_substr_opt, [], [ s; offset; length ])

  let at_str_opt (s : exp) (offset : exp) : exp =
    mk_exp @@ E_fapp (F_str_at_opt, [], [ s; offset ])

  let gt_str (s1 : exp) (s2 : exp) : exp =
    mk_exp @@ E_fapp (F_str_lt, [], [ s2; s1 ])

  let concat_bytes (s1 : exp) (s2 : exp) : exp =
    mk_exp @@ E_fapp (F_str_concat, [], [ s1; s2 ])

  let len_bytes (s : exp) : exp = mk_exp @@ E_fapp (F_str_len, [], [ s ])

  let substr_bytes_opt (s : exp) (offset : exp) (length : exp) : exp =
    mk_exp @@ E_fapp (F_str_substr_opt, [], [ s; offset; length ])

  let at_bytes_opt (s : exp) (offset : exp) : exp =
    mk_exp @@ E_fapp (F_str_at_opt, [], [ s; offset ])

  let gt_bytes (s1 : exp) (s2 : exp) : exp =
    mk_exp @@ E_fapp (F_str_lt, [], [ s2; s1 ])

  let append (e1 : exp) (e2 : exp) : exp =
    mk_exp @@ E_fapp (F_append, [], [ e1; e2 ])

  let size_list (e : exp) : exp = mk_exp @@ E_fapp (F_size_list, [], [ e ])
  let pack (t : Sort.t) (e : exp) : exp = mk_exp @@ E_fapp (F_pack, [ t ], [ e ])

  let unpack_opt (t : Sort.t) (e : exp) : exp =
    mk_exp @@ E_fapp (F_unpack_opt, [ t ], [ e ])

  let lambda (f : exp) (arg : variable) (res : variable) (err : variable)
      (pre_cond : exp) (post_cond : exp) (ab_cond : exp) : exp =
    mk_exp @@ E_lambda (f, arg, res, err, pre_cond, post_cond, ab_cond)

  let call (f : exp) (a : exp) (r : exp) : exp = mk_exp @@ E_call (f, a, r)

  let call_err (f : exp) (a : exp) (r : exp) : exp =
    mk_exp @@ E_call_err (f, a, r)

  (* datatype *)
  (* Note that [extract_*] is underspecified, i.e., the result is
     undefined if extract target is not intended value.  For instance,
     the result value of [extract_some none] is undefined. *)
  (* option *)
  let is_none (e : exp) : exp = mk_exp @@ E_data_tester (C_none, e)
  let none (t : Sort.t) : exp = mk_exp @@ E_data_constr (C_none, [ t ], [])
  let is_some (e : exp) : exp = mk_exp @@ E_data_tester (C_some, e)
  let some (e : exp) : exp = mk_exp @@ E_data_constr (C_some, [], [ e ])
  let extract_some (e : exp) : exp = mk_exp @@ E_data_destr (C_some, [], e, 0)

  (* or *)
  let is_left (e : exp) : exp = mk_exp @@ E_data_tester (C_left, e)

  let left (t_right : Sort.t) (e : exp) =
    mk_exp @@ E_data_constr (C_left, [ t_right ], [ e ])

  let extract_left (e : exp) : exp = mk_exp @@ E_data_destr (C_left, [], e, 0)
  let is_right (e : exp) : exp = mk_exp @@ E_data_tester (C_right, e)

  let right (t_left : Sort.t) (e : exp) =
    mk_exp @@ E_data_constr (C_right, [ t_left ], [ e ])

  let extract_right (e : exp) : exp = mk_exp @@ E_data_destr (C_right, [], e, 0)

  (* pair *)
  let pair (e1 : exp) (e2 : exp) : exp =
    mk_exp @@ E_data_constr (C_pair, [], [ e1; e2 ])

  let fst (e : exp) : exp = mk_exp @@ E_data_destr (C_pair, [], e, 0)
  let snd (e : exp) : exp = mk_exp @@ E_data_destr (C_pair, [], e, 1)

  (* list *)
  let is_nil (e : exp) : exp = mk_exp @@ E_data_tester (C_nil, e)
  let nil (t : Sort.t) : exp = mk_exp @@ E_data_constr (C_nil, [ t ], [])
  let is_cons (e : exp) : exp = mk_exp @@ E_data_tester (C_cons, e)

  let cons (e_hd : exp) (e_tl : exp) : exp =
    mk_exp @@ E_data_constr (C_cons, [], [ e_hd; e_tl ])

  let extract_hd (e : exp) : exp = mk_exp @@ E_data_destr (C_cons, [], e, 0)
  let extract_tl (e : exp) : exp = mk_exp @@ E_data_destr (C_cons, [], e, 1)

  (* operation *)
  let is_set_delegate (e : exp) : exp =
    mk_exp @@ E_data_tester (C_set_delegate, e)

  let set_delegate (e : exp) : exp =
    mk_exp @@ E_data_constr (C_set_delegate, [], [ e ])

  let extract_set_delegate (e : exp) : exp =
    mk_exp @@ E_data_destr (C_set_delegate, [], e, 0)

  let is_transfer (e : exp) : exp = mk_exp @@ E_data_tester (C_transfer, e)

  let transfer_tokens (t_data : Sort.t) (e1 : exp) (e2 : exp) (e3 : exp) : exp =
    mk_exp @@ E_data_constr (C_transfer, [], [ pack t_data e1; e2; e3 ])

  let extract_transfer_tokens_data (e : exp) : exp =
    mk_exp @@ E_data_destr (C_transfer, [], e, 0)

  let extract_transfer_tokens_mutez (e : exp) : exp =
    mk_exp @@ E_data_destr (C_transfer, [], e, 1)

  let extract_transfer_tokens_contract (s : Sort.t) (e : exp) : exp =
    mk_exp @@ E_data_destr (C_transfer, [ s ], e, 2)

  let is_create_contract (e : exp) : exp =
    mk_exp @@ E_data_tester (C_create_contract, e)

  let create_contract (t_store : Sort.t) (e1 : exp) (e2 : exp) (e3 : exp)
      (e4 : exp) : exp =
    mk_exp
    @@ E_data_constr (C_create_contract, [], [ e1; e2; pack t_store e3; e4 ])

  let extract_create_contract_keyhash (e : exp) : exp =
    mk_exp @@ E_data_destr (C_create_contract, [], e, 0)

  let extract_create_contract_mutez (e : exp) : exp =
    mk_exp @@ E_data_destr (C_create_contract, [], e, 1)

  let extract_create_contract_data (e : exp) : exp =
    mk_exp @@ E_data_destr (C_create_contract, [], e, 2)

  let extract_create_contract_address (e : exp) : exp =
    mk_exp @@ E_data_destr (C_create_contract, [], e, 3)

  (* address *)
  let address_of_key_hash (e_key_hash : exp) : exp =
    mk_exp @@ E_fapp (F_address_of_key_hash, [], [ e_key_hash ])

  let create_address (e_address : exp) (e_entry : exp) : exp =
    mk_exp @@ E_data_constr (C_address, [], [ e_address; e_entry ])

  let extract_address (e : exp) : exp =
    mk_exp @@ E_data_destr (C_address, [], e, 0)

  let extract_entrypoint (e : exp) : exp =
    mk_exp @@ E_data_destr (C_address, [], e, 1)

  (* contract *)
  let implicit_account (e : exp) : exp =
    mk_exp
    @@ E_data_constr (C_contract, [ Sort.S_unit ], [ address_of_key_hash e ])

  let extract_contract (e : exp) : exp =
    mk_exp @@ E_data_destr (C_contract, [], e, 0)

  (* exceptions *)
  let is_error (e : exp) : exp = mk_exp @@ E_data_tester (C_error, e)

  let error (t : Sort.t) (e : exp) : exp =
    mk_exp @@ E_data_constr (C_error, [], [ pack t e ])

  let extract_error (e : exp) : exp = mk_exp @@ E_data_destr (C_error, [], e, 0)
  let is_overflow (e : exp) : exp = mk_exp @@ E_data_tester (C_overflow, e)
  let overflow : exp = mk_exp @@ E_data_constr (C_overflow, [], [])

  (* operations on set and map *)
  let empty_set (t : Sort.t) : exp = mk_exp @@ E_const (K_empty_set t)

  let add_set (elt : exp) (set : exp) : exp =
    mk_exp @@ E_fapp (F_set_add, [], [ elt; set ])

  let remove_set (elt : exp) (set : exp) : exp =
    mk_exp @@ E_fapp (F_set_remove, [], [ elt; set ])

  let mem_set (elt : exp) (set : exp) : exp =
    mk_exp @@ E_fapp (F_set_find, [], [ elt; set ])

  let choose_opt_set (set : exp) : exp =
    mk_exp @@ E_fapp (F_set_choose_opt, [], [ set ])

  let empty_map (t_key : Sort.t) (t_elt : Sort.t) : exp =
    mk_exp @@ E_const (K_empty_map (t_key, t_elt))

  let update_map (key : exp) (uelt : exp) (map : exp) : exp =
    mk_exp @@ E_fapp (F_map_update, [], [ key; uelt; map ])

  let find_opt_map (key : exp) (map : exp) : exp =
    mk_exp @@ E_fapp (F_map_find, [], [ key; map ])

  let choose_opt_map (map : exp) : exp =
    mk_exp @@ E_fapp (F_map_choose_opt, [], [ map ])

  let empty_big_map (t_key : Sort.t) (t_elt : Sort.t) : exp =
    mk_exp @@ E_const (K_empty_big_map (t_key, t_elt))

  let update_big_map (key : exp) (uelt : exp) (map : exp) : exp =
    mk_exp @@ E_fapp (F_big_map_update, [], [ key; uelt; map ])

  let find_opt_big_map (key : exp) (map : exp) : exp =
    mk_exp @@ E_fapp (F_big_map_find, [], [ key; map ])

  (* logical connectives *)
  let conj (el : exp list) : exp = mk_exp @@ E_fapp (F_conj, [], el)
  let disj (el : exp list) : exp = mk_exp @@ E_fapp (F_disj, [], el)
  let neg (e : exp) : exp = mk_exp @@ E_fapp (F_neg, [], [ e ])

  let ite (e1 : exp) (e2 : exp) (e3 : exp) : exp =
    mk_exp @@ E_fapp (F_ite, [], [ e1; e2; e3 ])

  let ( := ) (e1 : exp) (e2 : exp) : exp =
    mk_exp @@ E_fapp (F_imply, [], [ e1; e2 ])

  let ( && ) (e1 : exp) (e2 : exp) : exp = conj [ e1; e2 ]
  let ( || ) (e1 : exp) (e2 : exp) : exp = disj [ e1; e2 ]
  let xor (e1 : exp) (e2 : exp) : exp = (e1 || e2) && (neg e1 || neg e2)
  let ( = ) (e1 : exp) (e2 : exp) : exp = mk_exp @@ E_fapp (F_eq, [], [ e1; e2 ])
  let ( > ) (e1 : exp) (e2 : exp) : exp = mk_exp @@ E_fapp (F_gt, [], [ e1; e2 ])

  let ( + ) (e1 : exp) (e2 : exp) : exp =
    mk_exp @@ E_fapp (F_add, [], [ e1; e2 ])

  let ( - ) (e1 : exp) (e2 : exp) : exp =
    mk_exp @@ E_fapp (F_sub, [], [ e1; e2 ])

  let ( * ) (e1 : exp) (e2 : exp) : exp =
    mk_exp @@ E_fapp (F_mul, [], [ e1; e2 ])

  let ( / ) (e1 : exp) (e2 : exp) : exp =
    mk_exp @@ E_fapp (F_div, [], [ e1; e2 ])

  let ( % ) (e1 : exp) (e2 : exp) : exp =
    mk_exp @@ E_fapp (F_mod, [], [ e1; e2 ])

  let ( <> ) (e1 : exp) (e2 : exp) : exp = neg (e1 = e2)
  let ( < ) (e1 : exp) (e2 : exp) : exp = e2 > e1
  let ( >= ) (e1 : exp) (e2 : exp) : exp = e1 = e2 || e1 > e2
  let ( <= ) (e1 : exp) (e2 : exp) : exp = e1 = e2 || e1 < e2
  let ( ?/ ) (v : variable) : exp = mk_exp @@ E_symbol v

  let mapp (f : msymbol) (sl : Sort.t list) (el : exp list) : exp =
    mk_exp @@ E_mapp (f, sl, el)

  (* let measure_axiom (el : exp list) : exp = mk_exp @@ E_measure_axiom el *)

  let contract_opt ?entrypoint (t_store : Sort.t) (e_address : exp) : exp =
    match entrypoint with
    | None ->
        ite
          (contract_ty_opt e_address = some (c_sort t_store))
          (some
             (mk_exp @@ E_data_constr (C_contract, [ t_store ], [ e_address ])))
          (none (S_contract t_store))
    | Some entry ->
        let e_entry = c_string entry in
        ite
          (extract_entrypoint e_address = c_string "%default")
          (ite
             (contract_ty_opt
                (create_address (extract_address e_address) e_entry)
             = some (c_sort t_store))
             (some
                (mk_exp
                @@ E_data_constr
                     ( C_contract,
                       [ t_store ],
                       [ create_address (extract_address e_address) e_entry ] )
                ))
             (none (S_contract t_store)))
          (none (S_contract t_store))

  let self ?entrypoint ty : exp =
    match entrypoint with
    | None -> mk_exp @@ E_data_constr (C_contract, [ ty ], [ self_addr ])
    | Some entry ->
        mk_exp
        @@ E_data_constr
             ( C_contract,
               [ ty ],
               [ create_address (extract_address self_addr) (c_string entry) ]
             )

  (* special variables *)
  let err : variable = ("err", S_exception)

  (* TODO: we could use E_mutez if we use Z.t instead of int as container. *)
  let c_mutez_max : exp = mk_exp @@ E_const K_mutez_max
end

module QLang = struct
  include Lang

  let forall (x : binding) (p : exp) = mk_exp @@ E_forall (x, p)
  let exists (x : binding) (p : exp) = mk_exp @@ E_exists (x, p)
end

let axiom : exp list =
  [
    Lang.(amount <= balance);
    Lang.(contract_ty_opt source = some (c_sort S_unit));
    Lang.(extract_entrypoint source = c_string "%default");
    Lang.(extract_entrypoint sender = c_string "%default");
    Lang.(extract_entrypoint self_addr = c_string "%default");
  ]

let cnt = ref 0

let fresh_var ?(x = "v") (ty : Sort.t) : variable =
  let p = !cnt in
  incr cnt;
  (Format.sprintf "%s%04d" x p, ty)

let internal_fresh_var ?(x = "v") (ty : Sort.t) : variable =
  fresh_var ~x:("_internal_" ^ x) ty

let fresh_binding (b : binding) : binding =
  List.map (fun (_, ty) -> fresh_var ~x:"_fresh_binding_" ty) b

let type_subst_binding (s : (string * Sort.t) list) (b : binding) : binding =
  List.map (fun (f, t) -> (f, Sort.subst_sort s t)) b

let type_subst_const (s : (string * Sort.t) list) (c : const) : const =
  let ss = Sort.subst_sort s in
  match c with
  | K_sort s -> K_sort (ss s)
  | K_unit | K_bool _ | K_int _ | K_nat _ | K_string _ | K_chain_id _
  | K_bytes _ | K_mutez _ | K_mutez_max | K_key_hash _ | K_key _ | K_signature _
  | K_timestamp _ ->
      c
  | K_empty_set s -> K_empty_set (ss s)
  | K_empty_map (s1, s2) -> K_empty_map (ss s1, ss s2)
  | K_empty_big_map (s1, s2) -> K_empty_big_map (ss s1, ss s2)

let rec type_subst_exp (s : (string * Sort.t) list) (e : exp) : exp =
  let ss = Sort.subst_sort s in
  let sb = type_subst_binding s in
  let se = type_subst_exp s in
  let desc =
    match e.desc with
    | E_symbol (x, s) -> E_symbol (x, ss s)
    | E_const c -> E_const (type_subst_const s c)
    | E_data_constr (c, sl, el) ->
        E_data_constr (c, List.map ss sl, List.map se el)
    | E_data_destr (c, sl, e, n) -> E_data_destr (c, List.map ss sl, se e, n)
    | E_data_tester (c, e) -> E_data_tester (c, se e)
    | E_forall (b, e) -> E_forall (sb b, se e)
    | E_exists (b, e) -> E_exists (sb b, se e)
    | E_fapp (f, sl, el) -> E_fapp (f, List.map ss sl, List.map se el)
    | E_mapp (f, sl, el) -> E_mapp (f, List.map ss sl, List.map se el)
    | E_lambda (e1, (x1, s1), (x2, s2), (x3, s3), e2, e3, e4) ->
        E_lambda
          (se e1, (x1, ss s1), (x2, ss s2), (x3, ss s3), se e2, se e3, se e4)
    | E_call (e1, e2, e3) -> E_call (se e1, se e2, se e3)
    | E_call_err (e1, e2, e3) -> E_call_err (se e1, se e2, se e3)
    (* | E_measure_axiom el -> E_measure_axiom (List.map se el) *)
    | E_unfold_measure (f, sl, el, e) ->
        E_unfold_measure (f, List.map ss sl, List.map se el, se e)
  in
  { e with desc }

(** Substitution for variables.  Note that this substitution is not
   capture avoiding. *)
let rec subst_exp (s : (variable * exp) list) (e : exp) : exp =
  let desc =
    match e.desc with
    | E_symbol v -> (
        match List.assoc ~equal:( = ) v s with
        | Some e -> e.desc
        | None -> E_symbol v)
    | E_const c -> E_const c
    | E_data_constr (c, sl, el) ->
        E_data_constr (c, sl, List.map (subst_exp s) el)
    | E_data_destr (c, sl, e, n) -> E_data_destr (c, sl, subst_exp s e, n)
    | E_data_tester (c, e) -> E_data_tester (c, subst_exp s e)
    | E_forall (b, e) -> E_forall (b, subst_exp s e)
    | E_exists (b, e) -> E_exists (b, subst_exp s e)
    | E_fapp (f, sl, el) -> E_fapp (f, sl, List.map (subst_exp s) el)
    | E_mapp (f, sl, el) -> E_mapp (f, sl, List.map (subst_exp s) el)
    | E_lambda (e1, v1, v2, v3, e2, e3, e4) ->
        E_lambda
          ( subst_exp s e1,
            v1,
            v2,
            v3,
            subst_exp s e2,
            subst_exp s e3,
            subst_exp s e4 )
    | E_call (e1, e2, e3) ->
        E_call (subst_exp s e1, subst_exp s e2, subst_exp s e3)
    | E_call_err (e1, e2, e3) ->
        E_call_err (subst_exp s e1, subst_exp s e2, subst_exp s e3)
    (* | E_measure_axiom el -> E_measure_axiom (List.map (subst_exp s) el) *)
    | E_unfold_measure (f, sl, el, e) ->
        E_unfold_measure (f, sl, List.map (subst_exp s) el, subst_exp s e)
  in
  { e with desc }

open Format

let pp_variable fmt ((x, t) : variable) =
  fprintf fmt "(%s : %a)" x Sort.pp_sort t

let pp_binding fmt (vl : binding) =
  fprintf fmt "(%a)" (pp_print_list pp_variable) vl

let pp_const fmt (c : const) =
  match c with
  | K_sort s -> fprintf fmt "%a" Sort.pp_sort s
  | K_unit -> fprintf fmt "Unit"
  | K_bool b -> fprintf fmt "%a" pp_print_bool b
  | K_int n -> fprintf fmt "%d%%int" n
  | K_nat n -> fprintf fmt "%d%%nat" n
  | K_string s -> fprintf fmt "%s%%string" s
  | K_chain_id s -> fprintf fmt "%s%%chain_id" s
  | K_bytes s -> fprintf fmt "%s%%bytes" (Bytes.to_string s)
  | K_mutez n -> fprintf fmt "%d%%mutez" n
  | K_mutez_max -> fprintf fmt "max_mutez"
  | K_key_hash s -> fprintf fmt "%s%%key_hash" s
  | K_key s -> fprintf fmt "%s%%key" s
  | K_signature s -> fprintf fmt "%s%%signature" s
  | K_timestamp n -> fprintf fmt "%n%%timestamp" n
  | K_empty_set s -> fprintf fmt "EmptySet%%%a" Sort.pp_sort (S_set s)
  | K_empty_map (s1, s2) ->
      fprintf fmt "EmptyMap%%%a" Sort.pp_sort (S_map (s1, s2))
  | K_empty_big_map (s1, s2) ->
      fprintf fmt "EmptyBigMap%%%a" Sort.pp_sort (S_map (s1, s2))

let pp_constr fmt (c : constr) =
  pp_print_string fmt
  @@
  match c with
  | C_none -> "None"
  | C_some -> "Some"
  | C_left -> "Left"
  | C_right -> "Right"
  | C_pair -> "Pair"
  | C_nil -> "Nil"
  | C_cons -> "Cons"
  | C_set_delegate -> "SetDelegate"
  | C_transfer -> "Transfer"
  | C_create_contract -> "CreateContract"
  | C_contract -> "Contract"
  | C_address -> "Address"
  | C_error -> "Error"
  | C_overflow -> "Overflow"

let pp_fsymbol fmt (f : fsymbol) =
  pp_print_string fmt
  @@
  match f with
  | F_ite -> "ite"
  | F_imply -> "=>"
  | F_conj -> "and"
  | F_disj -> "or"
  | F_neg -> "not"
  | F_eq -> "="
  | F_gt -> ">"
  | F_add -> "+"
  | F_sub -> "-"
  | F_mul -> "*"
  | F_div -> "/"
  | F_mod -> "mod"
  | F_abs -> "abs"
  | F_set_add -> "set_add"
  | F_set_remove -> "set_remove"
  | F_set_find -> "set_find"
  | F_set_choose_opt -> "set_choose_opt"
  | F_map_update -> "map_update"
  | F_map_choose_opt -> "map_choose_opt"
  | F_big_map_find -> "big_map_find"
  | F_big_map_update -> "big_map_update"
  | F_map_find -> "map_find"
  | F_str_concat -> "str_concat"
  | F_str_len -> "str_len"
  | F_str_substr_opt -> "str_substr_opt"
  | F_str_at_opt -> "str_at_opt"
  | F_str_lt -> "str_lt"
  | F_append -> "append"
  | F_size_list -> "size_list"
  | F_pack -> "pack"
  | F_unpack_opt -> "unpack_opt"
  | F_self_addr -> "self_addr"
  | F_balance -> "balance"
  | F_amount -> "amount"
  | F_now -> "now"
  | F_source -> "source"
  | F_sender -> "sender"
  | F_current_chain_id -> "current_chain_id"
  | F_level -> "level"
  | F_voting_power -> "voting_power"
  | F_total_voting_power -> "total_voting_power"
  | F_b58check -> "b58check"
  | F_blake2b -> "blake2b"
  | F_keccak -> "keccak"
  | F_sha256 -> "sha256"
  | F_sha512 -> "sha512"
  | F_sha3 -> "sha3"
  | F_sig -> "sig"
  | F_contract_ty_opt -> "contract_ty_opt"
  | F_address_of_key_hash -> "address_of_key_hash"

let rec pp_exp fmt (e : exp) =
  let pp_desc fmt = function
    | E_symbol x -> fprintf fmt "%a" pp_variable x
    | E_const c -> fprintf fmt "%a" pp_const c
    | E_data_constr (c, _, el) ->
        fprintf fmt "(@[%a@ %a@])" pp_constr c
          (pp_print_list ~pp_sep:pp_print_space pp_exp)
          el
    | E_data_destr (c, _, e, n) ->
        fprintf fmt "(@[extract%a%%%d@ %a@])" pp_constr c n pp_exp e
    | E_data_tester (c, e) -> fprintf fmt "(@[is%a@ %a@])" pp_constr c pp_exp e
    | E_forall (b, e) ->
        fprintf fmt "(@[Forall@ %a@ %a@])" pp_binding b pp_exp e
    | E_exists (b, e) ->
        fprintf fmt "(@[Exists@ %a@ %a@])" pp_binding b pp_exp e
    | E_fapp (f, _, el) ->
        fprintf fmt "(@[%a@ %a@])" pp_fsymbol f
          (pp_print_list ~pp_sep:pp_print_space pp_exp)
          el
    | E_mapp (f, _, el) ->
        fprintf fmt "(@[%s@ %a@])" f.ufid
          (pp_print_list ~pp_sep:pp_print_space pp_exp)
          el
    | E_lambda (e1, v1, v2, v3, e2, e3, e4) ->
        fprintf fmt "(@[Lambda@ %a@ %a@ %a@ %a@ %a@])" pp_exp e1 pp_binding
          [ v1; v2; v3 ] pp_exp e2 pp_exp e3 pp_exp e4
    | E_call (e1, e2, e3) ->
        fprintf fmt "(@[Call@ %a@ %a@ %a@])" pp_exp e1 pp_exp e2 pp_exp e3
    | E_call_err (e1, e2, e3) ->
        fprintf fmt "(@[Call_err@ %a@ %a@ %a@])" pp_exp e1 pp_exp e2 pp_exp e3
    (* | E_measure_axiom el -> *)
    (*     fprintf *)
    (*       fmt *)
    (*       "(@[m_axiom@ %a@])" *)
    (*       (pp_print_list ~pp_sep:pp_print_space pp_exp) *)
    (*       el *)
    | E_unfold_measure (f, sl, el, e) ->
        fprintf fmt "(@[u_measure %s[%a](%a) =@ %a@])" f.ufid
          (pp_print_list ~pp_sep:pp_print_space Sort.pp_sort)
          sl
          (pp_print_list ~pp_sep:pp_print_space pp_exp)
          el pp_exp e
  in
  Format.fprintf fmt "%a" pp_desc e.desc

let rec type_of exp : Sort.t tzresult Lwt.t =
  let aux : exp_desc -> Sort.t tzresult Lwt.t = function
    | E_symbol (_, t) -> return t
    | E_const c ->
        return
          (match c with
          | K_sort _ -> Sort.S_string
          | K_unit -> Sort.S_unit
          | K_bool _ -> Sort.S_bool
          | K_int _ -> Sort.S_int
          | K_nat _ -> Sort.S_nat
          | K_string _ -> Sort.S_string
          | K_chain_id _ -> Sort.S_chain_id
          | K_bytes _ -> Sort.S_bytes
          | K_mutez _ | K_mutez_max -> Sort.S_mutez
          | K_key_hash _ -> Sort.S_key_hash
          | K_key _ -> Sort.S_key
          | K_signature _ -> Sort.S_signature
          | K_timestamp _ -> Sort.S_timestamp
          | K_empty_set t -> Sort.S_set t
          | K_empty_map (t_key, t_val) -> Sort.S_map (t_key, t_val)
          | K_empty_big_map (t_key, t_val) -> Sort.S_big_map (t_key, t_val))
    | E_data_constr (c, sl, el) -> (
        List.map_es type_of el >>=? fun tyl ->
        match (c, sl, tyl) with
        | C_none, [ t ], [] -> return @@ Sort.S_option t
        | C_some, [], [ t ] -> return @@ Sort.S_option t
        | C_left, [ t_right ], [ t_left ] ->
            return @@ Sort.S_or (t_left, t_right)
        | C_right, [ t_left ], [ t_right ] ->
            return @@ Sort.S_or (t_left, t_right)
        | C_pair, [], [ t1; t2 ] -> return @@ Sort.S_pair (t1, t2)
        | C_nil, [ t ], [] -> return Sort.(S_list t)
        | C_cons, [], [ t_hd; t_tl ] when t_tl = Sort.S_list t_hd -> return t_tl
        | C_set_delegate, [], [ Sort.(S_option S_key_hash) ] ->
            return Sort.S_operation
        | C_transfer, [], [ Sort.S_bytes; Sort.S_mutez; Sort.S_contract _ ] ->
            return Sort.S_operation
        | ( C_create_contract,
            [],
            [
              Sort.(S_option S_key_hash);
              Sort.S_mutez;
              Sort.S_bytes;
              Sort.S_address;
            ] ) ->
            return Sort.S_operation
        | C_contract, [ t_store ], [ Sort.S_address ] ->
            return @@ Sort.S_contract t_store
        | C_address, [], [ Sort.S_string; Sort.S_string ] ->
            return Sort.S_address
        | C_error, [], [ Sort.S_bytes ] -> return Sort.S_exception
        | C_overflow, [], [] -> return Sort.S_exception
        | _ -> failwith "arity mismatch")
    | E_data_destr (c, sl, e, n) -> (
        type_of e >>=? fun ty ->
        match (c, sl, ty, n) with
        | C_some, [], Sort.S_option t, 0 -> return t
        | C_left, [], Sort.S_or (t, _), 0 -> return t
        | C_right, [], Sort.S_or (_, t), 0 -> return t
        | C_pair, [], Sort.S_pair (t, _), 0 -> return t
        | C_pair, [], Sort.S_pair (_, t), 1 -> return t
        | C_cons, [], Sort.S_list t, 0 -> return t
        | C_cons, [], Sort.S_list t, 1 -> return Sort.(S_list t)
        | C_set_delegate, [], Sort.S_operation, 0 ->
            return Sort.(S_option S_key_hash)
        | C_transfer, [], Sort.S_operation, 0 -> return Sort.S_bytes
        | C_transfer, [], Sort.S_operation, 1 -> return Sort.S_mutez
        | C_transfer, [ s ], Sort.S_operation, 2 -> return Sort.(S_contract s)
        | C_create_contract, [], Sort.S_operation, 0 ->
            return Sort.(S_option S_key_hash)
        | C_create_contract, [], Sort.S_operation, 1 -> return Sort.S_mutez
        | C_create_contract, [], Sort.S_operation, 2 -> return Sort.S_bytes
        | C_create_contract, [], Sort.S_operation, 3 -> return Sort.S_address
        | C_contract, [], Sort.S_contract _, 0 -> return Sort.S_address
        | C_address, [], Sort.S_address, 0 -> return Sort.S_string
        | C_address, [], Sort.S_address, 1 -> return Sort.S_string
        | C_error, [], Sort.S_exception, 0 -> return Sort.S_bytes
        | _ -> failwith "arity mismatch")
    | E_data_tester (c, e) -> (
        type_of e >>=? fun ty ->
        match (c, ty) with
        | C_none, Sort.S_option _
        | C_some, Sort.S_option _
        | C_left, Sort.S_or _
        | C_right, Sort.S_or _
        | C_nil, Sort.S_list _
        | C_cons, Sort.S_list _
        | C_set_delegate, Sort.S_operation
        | C_transfer, Sort.S_operation
        | C_create_contract, Sort.S_operation
        | C_error, Sort.S_exception
        | C_overflow, Sort.S_exception ->
            return Sort.S_bool
        | _ -> failwith "invalid tester target")
    | E_forall (_, e) ->
        type_of e >>=? fun ty ->
        fail_unless (ty = Sort.S_bool) (error_of_fmt "not a Boolean expression")
        >>=? fun () -> return Sort.S_bool
    | E_exists (_, e) ->
        type_of e >>=? fun ty ->
        fail_unless (ty = Sort.S_bool) (error_of_fmt "not a Boolean expression")
        >>=? fun () -> return Sort.S_bool
    | E_fapp (f, sl, el) -> (
        List.map_es type_of el >>=? fun tyl ->
        match (f, sl, tyl) with
        | F_ite, [], [ Sort.S_bool; t_then; t_else ] when t_then = t_else ->
            return t_then
        | F_imply, [], [ Sort.S_bool; Sort.S_bool ] -> return Sort.S_bool
        | F_conj, [], tl ->
            fail_unless (List.for_all (( = ) Sort.S_bool) tl) (error_of_fmt "")
            >>=? fun () -> return Sort.S_bool
        | F_disj, [], tl ->
            fail_unless (List.for_all (( = ) Sort.S_bool) tl) (error_of_fmt "")
            >>=? fun () -> return Sort.S_bool
        | F_neg, [], [ Sort.S_bool ] -> return Sort.S_bool
        | F_eq, [], [ t1; t2 ] when t1 = t2 -> return Sort.S_bool
        | F_gt, [], [ t1; t2 ] when t1 = t2 ->
            return Sort.S_bool (* TODO: restrict to arithmetic types *)
        | F_add, [], tl -> (
            match tl with
            | [ Sort.S_int; Sort.S_int ]
            | [ Sort.S_int; Sort.S_nat ]
            | [ Sort.S_nat; Sort.S_int ] ->
                return Sort.S_int
            | [ Sort.S_nat; Sort.S_nat ] -> return Sort.S_nat
            | [ Sort.S_timestamp; Sort.S_int ]
            | [ Sort.S_int; Sort.S_timestamp ] ->
                return Sort.S_timestamp
            | [ Sort.S_mutez; Sort.S_mutez ] -> return Sort.S_mutez
            | _ -> failwith "arity mismatch")
        | F_sub, [], tl -> (
            match tl with
            | [ Sort.S_int; Sort.S_int ]
            | [ Sort.S_int; Sort.S_nat ]
            | [ Sort.S_nat; Sort.S_int ]
            | [ Sort.S_nat; Sort.S_nat ]
            | [ Sort.S_timestamp; Sort.S_timestamp ] ->
                return Sort.S_int
            | [ Sort.S_timestamp; Sort.S_int ] -> return Sort.S_timestamp
            | [ Sort.S_mutez; Sort.S_mutez ] -> return Sort.S_mutez
            | _ -> failwith "arity mismatch")
        | F_mul, [], tl -> (
            match tl with
            | [ Sort.S_int; Sort.S_int ]
            | [ Sort.S_int; Sort.S_nat ]
            | [ Sort.S_nat; Sort.S_int ] ->
                return Sort.S_int
            | [ Sort.S_nat; Sort.S_nat ] -> return Sort.S_nat
            | [ Sort.S_mutez; Sort.S_nat ] | [ Sort.S_nat; Sort.S_mutez ] ->
                return Sort.S_mutez
            | _ -> failwith "arith mismatch")
        | F_div, [], tl -> (
            match tl with
            | [ Sort.S_int; Sort.S_int ]
            | [ Sort.S_int; Sort.S_nat ]
            | [ Sort.S_nat; Sort.S_int ] ->
                return Sort.S_int
            | [ Sort.S_nat; Sort.S_nat ] -> return Sort.S_nat
            | [ Sort.S_mutez; Sort.S_nat ] -> return Sort.S_mutez
            | [ Sort.S_mutez; Sort.S_mutez ] -> return Sort.S_nat
            | _ -> failwith "arith mismatch")
        | F_mod, [], tl -> (
            match tl with
            | [ Sort.S_int; Sort.S_int ]
            | [ Sort.S_int; Sort.S_nat ]
            | [ Sort.S_nat; Sort.S_int ]
            | [ Sort.S_nat; Sort.S_nat ] ->
                return Sort.S_nat
            | [ Sort.S_mutez; Sort.S_nat ] -> return Sort.S_mutez
            | [ Sort.S_mutez; Sort.S_mutez ] -> return Sort.S_mutez
            | _ -> failwith "arity mismatch")
        | F_abs, [], [ Sort.S_int ] -> return Sort.S_nat
        | F_set_add, [], [ t_elt; t ] when t = Sort.S_set t_elt -> return t
        | F_set_remove, [], [ t_elt; t ] when t = Sort.S_set t_elt -> return t
        | F_set_find, [], [ t_elt; t ] when t = Sort.S_set t_elt ->
            return Sort.S_bool
        | F_set_choose_opt, [], [ Sort.S_set t ] -> return Sort.(S_option t)
        | F_map_update, [], [ t_key; Sort.S_option t_val; t ]
          when t = Sort.S_map (t_key, t_val) ->
            return t
        | F_map_find, [], [ t_key; Sort.S_map (tk, tv) ] when t_key = tk ->
            return Sort.(S_option tv)
        | F_map_choose_opt, [], [ Sort.S_map (tk, tv) ] ->
            return Sort.(S_option (S_pair (tk, tv)))
        | F_big_map_update, [], [ t_key; Sort.S_option t_val; t ]
          when t = Sort.S_big_map (t_key, t_val) ->
            return t
        | F_big_map_find, [], [ t_key; Sort.S_big_map (tk, tv) ] when t_key = tk
          ->
            return Sort.(S_option tv)
        | F_str_concat, [], tl -> (
            match tl with
            | [ Sort.S_string; Sort.S_string ] -> return Sort.S_string
            | [ Sort.S_bytes; Sort.S_bytes ] -> return Sort.S_bytes
            | _ -> failwith "arith mismatch")
        | F_str_len, [], [ Sort.(S_string | S_bytes) ] -> return Sort.S_nat
        | ( F_str_substr_opt,
            [],
            [ (Sort.(S_string | S_bytes) as t_s); Sort.S_nat; Sort.S_nat ] ) ->
            return Sort.(S_option t_s)
        | F_str_at_opt, [], [ (Sort.(S_string | S_bytes) as t_s); Sort.S_nat ]
          ->
            return Sort.(S_option t_s)
        | ( F_str_lt,
            [],
            ( [ Sort.S_string; Sort.S_string ]
            | [ Sort.S_bytes; Sort.S_bytes ]
            | [ Sort.S_key; Sort.S_key ]
            | [ Sort.S_key_hash; Sort.S_key_hash ]
            | [ Sort.S_address; Sort.S_address ]
            | [ Sort.S_chain_id; Sort.S_chain_id ]
            | [ Sort.S_signature; Sort.S_signature ] ) ) ->
            return Sort.S_bool
        | F_append, [], [ Sort.S_list t1; Sort.S_list t2 ] when t1 = t2 ->
            return Sort.(S_list t1)
        | F_size_list, [], [ Sort.S_list _ ] -> return Sort.S_nat
        | F_pack, [ t ], [ t' ] when t = t' -> return Sort.S_bytes
        | F_unpack_opt, [ t ], [ Sort.S_bytes ] -> return Sort.(S_option t)
        | F_self_addr, [], [] -> return Sort.S_address
        | F_balance, [], [] -> return Sort.S_mutez
        | F_amount, [], [] -> return Sort.S_mutez
        | F_now, [], [] -> return Sort.S_timestamp
        | F_source, [], [] -> return Sort.S_address
        | F_sender, [], [] -> return Sort.S_address
        | F_current_chain_id, [], [] -> return Sort.S_chain_id
        | F_level, [], [] -> return Sort.S_nat
        | F_voting_power, [], [ Sort.S_key_hash ] -> return Sort.S_nat
        | F_total_voting_power, [], [] -> return Sort.S_nat
        | F_b58check, [], [ Sort.S_key ] -> return Sort.S_key_hash
        | F_blake2b, [], [ Sort.S_bytes ] -> return Sort.S_bytes
        | F_keccak, [], [ Sort.S_bytes ] -> return Sort.S_bytes
        | F_sha256, [], [ Sort.S_bytes ] -> return Sort.S_bytes
        | F_sha512, [], [ Sort.S_bytes ] -> return Sort.S_bytes
        | F_sha3, [], [ Sort.S_bytes ] -> return Sort.S_bytes
        | F_sig, [], [ Sort.S_key; Sort.S_bytes ] -> return Sort.S_signature
        | F_contract_ty_opt, [], [ Sort.S_address ] ->
            return Sort.(S_option S_string)
        | F_address_of_key_hash, [], [ Sort.S_key_hash ] ->
            return Sort.S_address
        | _ -> failwith "arity mismatch")
    | E_mapp (f, sl, el) ->
        List.combine
          ~when_different_lengths:[ error_of_fmt "arity mismatch" ]
          f.t_vars sl
        >>?= fun t_subst ->
        let t_params = List.map (Sort.subst_sort t_subst) f.t_params in
        let t_ret = Sort.subst_sort t_subst f.t_ret in
        List.map_es type_of el >>=? fun t_args ->
        fail_unless (t_params = t_args) (error_of_fmt "arity mismatch")
        >>=? fun () -> return t_ret
    | E_lambda (e_f, v_arg, v_ret, _, e_pre, e_post, e_excpt) ->
        type_of e_f >>=? fun ty_f ->
        type_of e_pre >>=? fun ty_pre ->
        type_of e_post >>=? fun ty_post ->
        type_of e_excpt >>=? fun ty_excpt ->
        fail_unless
          ((ty_f = Sort.(S_lambda (snd v_arg, snd v_ret)))
          && ty_pre = Sort.S_bool && ty_post = Sort.S_bool
          && ty_excpt = Sort.S_bool)
          (error_of_fmt "type error")
        >>=? fun () -> return Sort.S_bool
    | E_call (e1, e2, e3) ->
        type_of e1 >>=? fun ty_e1 ->
        type_of e2 >>=? fun ty_e2 ->
        type_of e3 >>=? fun ty_e3 ->
        fail_unless
          (ty_e1 = Sort.(S_lambda (ty_e2, ty_e3)))
          (error_of_fmt "type error")
        >>=? fun () -> return Sort.S_bool
    | E_call_err (e1, e2, e3) -> (
        type_of e1 >>=? fun ty_e1 ->
        type_of e2 >>=? fun ty_e2 ->
        type_of e3 >>=? fun ty_e3 ->
        match (ty_e1, ty_e3) with
        | Sort.S_lambda (t_arg, _), Sort.S_exception when t_arg = ty_e2 ->
            return Sort.S_bool
        | _ -> failwith "type error")
    (* | E_measure_axiom el -> return (E_measure_axiom el, Sort.S_bool) *)
    | E_unfold_measure (f, sl, el, e) ->
        List.combine
          ~when_different_lengths:[ error_of_fmt "arity mismatch" ]
          f.t_vars sl
        >>?= fun t_subst ->
        let t_params = List.map (Sort.subst_sort t_subst) f.t_params in
        let t_ret = Sort.subst_sort t_subst f.t_ret in
        List.map_es type_of el >>=? fun t_args ->
        type_of e >>=? fun ty ->
        fail_unless
          (t_params = t_args && ty = t_ret)
          (error_of_fmt "type mismatch")
        >>=? fun () -> return Sort.S_bool
  in
  if exp.ty = Sort.S_var "" then (
    (* trace (error_of_fmt "Typing: %a" pp_exp exp) *) aux exp.desc
    >>=? fun ty ->
    exp.ty <- ty;
    return ty)
  else return exp.ty
