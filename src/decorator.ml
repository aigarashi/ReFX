(** Michelson type checking for an input program.

    The goal is transform an input program typically of the form of string into a verification-ready data structure.

    We can use a particular Tezos protocol typechecking function.  However, it does not record all stack types before and after each instruction, which we want.  Functions in this module make simple type checking by means of the protocol typechecking function but also recover the missing type information.
*)

open Environment
open Protocol
open Alpha_context
open Result_syntax
module IntMap = Map.Make (Int)

type text_location = Tezos_micheline.Micheline_parser.location

(** [tezos_typecheck ctxt program] calls a target protocol typechecker to type check [program] and obtains and returns the [type_map] that records the stack types before and after each node but not all.  *)
let tezos_typecheck (ctxt : Alpha_context.t)
    (program : Alpha_context.Script.expr) : Script_tc_errors.type_map tzresult =
  Script_ir_translator.typecheck_code ~legacy:false ~show_types:true ctxt
    program
  |> Lwt_main.run
  |> function
  | Ok (type_map, _) -> return type_map
  | Error e ->
      error_with "simple typechecking fails@.%a"
        Environment.Error_monad.pp_trace e

module DecorationSet = Set.Make (struct
  type t = int * Syntax.parsed_decoration

  let compare (i1, _) (i2, _) = compare i1 i2
end)

(** [split_decoration source] gathers the texts "<< ... >>" in the [source] and returns [source] filling the parts with whitespaces preserving newlines.  The gathered texts are parsed as {!Syntax.parsed_decoration} and returned with the offset from the begin of [source] in which each text was originally placed. *)
let split_decoration (source : string) : (string * DecorationSet.t) tzresult =
  let src = Bytes.of_string source in
  let length = Bytes.length src in
  let rec fill_except_newline l r =
    if l < r then (
      if Bytes.get src l = '\n' then () else Bytes.set src l ' ';
      fill_except_newline (l + 1) r)
    else ()
  in
  let rec expr1 (index, line, col) decs =
    if index >= length then return decs
    else if Bytes.get src index = '<' && Bytes.get src (index + 1) = '<' then (
      let* ((next_index, _, _) as next_loc), dec =
        expr2 (index + 2, line, col + 2)
      in
      fill_except_newline index next_index;
      expr1 next_loc (DecorationSet.add dec decs))
    else if Bytes.get src index = '\n' then expr1 (index + 1, line + 1, 0) decs
    else expr1 (index + 1, line, col + 1) decs
  and expr2 (l, line, col) =
    (* TODO: unbalanced << >> causes out_of_index failure *)
    let rec find_right (i, line, col) =
      if Bytes.get src i = '>' && Bytes.get src (i + 1) = '>' then (i, line, col)
      else if Bytes.get src i = '\n' then find_right (i + 1, line + 1, 0)
      else find_right (i + 1, line, col + 1)
    in
    let r, new_line, new_col = find_right (l, line, col) in
    let* dec =
      Lwt_main.run
      @@ Decoration.parse_decoration (Bytes.sub_string src l (r - l)) (line, col)
    in
    return ((r + 2, new_line, new_col + 2), (l, dec))
  in
  let* decs = expr1 (0, 1, 0) DecorationSet.empty in
  return (Bytes.to_string src, decs)

(** [assoc_decorations decorations program] associates each decoration in [decorations] to a primitive in the expanded [program] according to text locations. *)
let assoc_decorations (decorations : DecorationSet.t)
    (tloc_map : int -> text_location tzresult) (program : Script.node) :
    (Syntax.parsed_decoration list * Syntax.parsed_decoration list) IntMap.t
    tzresult =
  (* [inject l r info] inserts decorations originally decorated before and after a node, for which [info] is attached, placed between [l] and [r] into the payload of [info].

       .  <<bef1>> ... <<befn>>  [prim]  <<aft1>> ... <<aftn>>  .
       ^                         ^    ^                         ^
       |                         |    |                         |
       l      info.tloc.start.point  info.tloc.stop.point       r
  *)
  let attach_decs l r c table =
    let check_aft_dec =
      let open Syntax in
      function
      | P_loop_inv _ | P_map_inv _ | P_assert _ | P_assume _ -> true
      | P_lambda_annot _ | P_contract_annot _ | P_measure _ | Z3Str _ -> false
    in
    let* tloc = tloc_map c in
    let bef_decs =
      DecorationSet.filter
        (fun (i, _) -> l < i && i < tloc.start.point)
        decorations
      |> DecorationSet.elements
      |> List.map (fun (_, dec) -> dec)
    in
    let aft_decs =
      DecorationSet.filter
        (fun (i, dec) -> tloc.stop.point < i && i < r && check_aft_dec dec)
        decorations
      |> DecorationSet.elements
      |> List.map (fun (_, dec) -> dec)
    in
    assert (IntMap.mem c table = false);
    return @@ IntMap.add c (bef_decs, aft_decs) table
  in
  let rec seqloop l r items table =
    match items with
    | h1 :: h2 :: t ->
        let* tloc1 = tloc_map (Micheline.location h1) in
        let* tloc2 = tloc_map (Micheline.location h2) in
        let* table = for_node l tloc2.start.point h1 table in
        seqloop tloc1.stop.point r (h2 :: t) table
    | [ h ] -> for_node l r h table
    | [] -> return table
  and for_node l r item table =
    match item with
    | Seq (_, items) ->
        (* decorations annotated before and after sequence { ... } are associated to the first and last item of the sequence, respectively. *)
        seqloop l r items table
    | Prim (c, _, items, _) ->
        let* table = attach_decs l r (Micheline.location item) table in
        let* tloc = tloc_map c in
        seqloop tloc.start.point tloc.stop.point items table
    | Int _ | String _ | Bytes _ -> return table
  in
  for_node Int.min_int Int.max_int program IntMap.empty

(** [check_type_map type_map node] checks if [type_map] has enough records about [node] for the following verification process.

   According to our investigation, the following nodes will have no type record.
   - non-empty sequence
   - non-instruction primitives
   - IF | IF_CONS | IF_LEFT | IF_NONE
   - every node in parameters of PUSH and CREATE_CONTRACT, which can be program code
*)
let rec check_type_map ?(skip = false) (type_map : Script_tc_errors.type_map)
    (node : Script.node) : unit tzresult =
  match node with
  | Seq (l, []) ->
      if skip || List.exists (fun (i, _) -> i = l) type_map then return_unit
      else error_with "Seq"
  | Seq (_, items) -> List.iter_e (check_type_map type_map) items
  | Prim (l, I_PUSH, _, _) ->
      if List.exists (fun (i, _) -> i = l) type_map then return_unit
      else error_with "PUSH"
  | Prim (l, I_CREATE_CONTRACT, _, _) ->
      if List.exists (fun (i, _) -> i = l) type_map then return_unit
      else error_with "CREATE_CONTRACT"
  | Prim (l, p, items, _) ->
      let* _ = List.iter_e (check_type_map type_map) items in
      if Michelson_v1_primitives.namespace p = Instr_namespace then
        match p with
        | I_IF | I_IF_CONS | I_IF_LEFT | I_IF_NONE -> return_unit
        | _ ->
            if List.exists (fun (i, _) -> i = l) type_map then return_unit
            else error_with "%s@." (Michelson_v1_primitives.string_of_prim p)
      else return_unit
  | Int _ | String _ | Bytes _ -> return_unit

(** [recover_stack_ty node] fills missing stack types commented in {!check_type_map}.  Nodes in code of parameters of PUSH and CREATE_CONTRACT are left missing. *)
let recover_stack_ty node : 'a tzresult =
  let open Node_info in
  let open Micheline in
  let open Michelson_v1_primitives in
  let merge_branch_aft bt bf =
    match ((location bt).aft, (location bf).aft) with
    | [], [] -> []
    | [], s -> s
    | s, _ -> s
  in
  let rec aux = function
    | Seq (_, []) as n -> return n
    | Seq (info, [ item ]) ->
        let* item = aux item in
        let bef = (location item).bef in
        let aft = (location item).aft in
        return @@ Seq ({ info with bef; aft }, [ item ])
    | Seq (info, h :: t) -> (
        let* h = aux h in
        let bef = (location h).bef in
        let* t = aux (Seq (info, t)) in
        match t with
        | Seq ({ aft; _ }, t) -> return @@ Seq ({ info with bef; aft }, h :: t)
        | _ -> error_with "internal: %s" __LOC__)
    | Prim (_, (I_PUSH | I_CREATE_CONTRACT), _, _) as n -> return n
    | Prim (info, I_IF, [ bt; bf ], annot) ->
        let* bt = aux bt in
        let* bf = aux bf in
        let top_t = strip_locations @@ Prim (dummy_location, T_bool, [], []) in
        let bef = top_t :: (location bt).bef in
        let aft = merge_branch_aft bt bf in
        return @@ Prim ({ info with bef; aft }, I_IF, [ bt; bf ], annot)
    | Prim (info, I_IF_CONS, [ bt; bf ], annot) ->
        let* bt = aux bt in
        let* bf = aux bf in
        let* bef =
          match (location bt).bef with
          | _ :: t -> return t
          | _ -> error_with "internal: %s" __LOC__
        in
        let aft = merge_branch_aft bt bf in
        return @@ Prim ({ info with bef; aft }, I_IF_CONS, [ bt; bf ], annot)
    | Prim (info, I_IF_LEFT, [ bt; bf ], annot) ->
        let* bt = aux bt in
        let* bf = aux bf in
        let* bef =
          match ((location bt).bef, (location bf).bef) with
          | h1 :: t, h2 :: _ ->
              let or_t =
                strip_locations
                @@ Prim (dummy_location, T_or, [ root h1; root h2 ], [])
              in
              return @@ (or_t :: t)
          | _ -> error_with "internal: %s" __LOC__
        in
        let aft = merge_branch_aft bf bt in
        return @@ Prim ({ info with bef; aft }, I_IF_LEFT, [ bt; bf ], annot)
    | Prim (info, I_IF_NONE, [ bt; bf ], annot) ->
        let* bt = aux bt in
        let* bf = aux bf in
        let* bef =
          match (location bf).bef with
          | h :: t ->
              let option_t =
                strip_locations
                @@ Prim (dummy_location, T_option, [ root h ], [])
              in
              return @@ (option_t :: t)
          | _ -> error_with "internal: %s" __LOC__
        in
        let aft = merge_branch_aft bf bt in
        return @@ Prim ({ info with bef; aft }, I_IF_NONE, [ bt; bf ], annot)
    | Prim (info, prim, items, annots) ->
        let* items = List.map_e aux items in
        return @@ Prim (info, prim, items, annots)
    | (Int _ | String _ | Bytes _) as n -> return n
  in
  aux node

let inject_decorations decorations type_map
    (program : Michelson_v1_parser.parsed) : 'a tzresult Lwt.t =
  let open Micheline in
  let open Lwt_result_syntax in
  let tloc_map n =
    let open Result_syntax in
    let* m =
      List.assoc_opt ~equal:( = ) n program.unexpansion_table
      |> Option.value_e ~error:[ error_of_fmt "" ]
    in
    let* tloc, _ =
      List.assoc_opt ~equal:( = ) m program.expansion_table
      |> Option.value_e ~error:[ error_of_fmt "" ]
    in
    return tloc
  in
  let decs =
    List.fold_left
      (fun acc (i, _, d) -> DecorationSet.add (i, d) acc)
      DecorationSet.empty decorations
  in
  let*? dec_table = assoc_decorations decs tloc_map (root program.expanded) in
  let program =
    Tezos_micheline.Micheline.inject_locations
      (fun i ->
        let tloc = match tloc_map i with Ok l -> l | _ -> assert false in
        let bef, aft =
          match List.assoc_opt ~equal:( = ) i type_map with
          | Some (bef, aft) -> (bef, aft)
          | None -> ([], [])
        in
        let bef_dec, aft_dec =
          match IntMap.find_opt i dec_table with
          | Some (bef, aft) -> (bef, aft)
          | None -> ([], [])
        in
        { Node_info.loc = tloc; bef; aft; bef_dec; aft_dec })
      program.expanded
  in
  let*? program = recover_stack_ty program in
  return program

(* let process_annotated_string (ctxt : Alpha_context.t) (code : string) : *)
(*     ('a refx_node * DecorationSet.t) tzresult = *)
(*   let* code, decorations = split_decoration code in *)
(*   (\* Note: we can ignore indent check if we pass false for [~check]. *\) *)
(*   let* program = *)
(*     match Michelson_v1_parser.parse_toplevel ~check:true code with *)
(*     | program, [] -> return program *)
(*     | parsed, errors -> *)
(*         error_with "parsing error@.%a" *)
(*           (Michelson_v1_error_reporter.report_errors ~details:true *)
(*              ~show_source:true ~parsed) *)
(*           errors *)
(*   in *)
(*   let* type_map = tezos_typecheck ctxt program.expanded in *)
(*   let module LM = Map.Make (Int) in *)
(*   let loc_table = *)
(*     List.fold_left *)
(*       (fun tb (_, (loc, l)) -> *)
(*         List.fold_left (fun tb i -> LM.add i loc tb) tb l) *)
(*       (\* Note: expansion_table is a map from canonical locations of an unexpanded node into the pair of the text location and the canonical location of the corresponding expanded node. *\) *)
(*       LM.empty program.expansion_table *)
(*   in *)
(*   let located_expr = *)
(*     Tezos_micheline.Micheline.inject_locations *)
(*       (fun i -> *)
(*         let uloc = *)
(*           List.assoc_opt ~equal:( = ) i program.unexpansion_table *)
(*           |> WithExceptions.Option.get ~loc:__LOC__ *)
(*         in *)
(*         let eloc = i in *)
(*         let tloc = *)
(*           LM.find_opt i loc_table |> WithExceptions.Option.get ~loc:__LOC__ *)
(*         in *)
(*         let bef_stack_ty_opt, aft_stack_ty_opt = *)
(*           match List.assoc_opt ~equal:( = ) i type_map with *)
(*           | Some (bef, aft) -> (Some bef, Some aft) *)
(*           | None -> (None, None) *)
(*         in *)
(*         { *)
(*           uloc; *)
(*           eloc; *)
(*           tloc; *)
(*           bef_stack_ty_opt; *)
(*           aft_stack_ty_opt; *)
(*           bef_decs = []; *)
(*           aft_decs = []; *)
(*         }) *)
(*       program.expanded *)
(*   in *)
(*   return (located_expr, decorations) *)
