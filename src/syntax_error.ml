open Syntax

exception Unclosed of location * string * location * string
exception Expecting of location * string
exception NotExpecting of location * string
exception UnknownAnnotation
exception Syntax_error

let unclosed opening_name opening_loc closing_name closing_loc =
  raise @@ Unclosed (opening_loc, opening_name, closing_loc, closing_name)

let expecting loc nonterm = raise @@ Expecting (loc, nonterm)
let not_expecting loc nonterm = raise @@ NotExpecting (loc, nonterm)
let unknown_annotation () = raise UnknownAnnotation
let syntax_error () = raise Syntax_error

exception Invalid_escape_sequence of location * char

let invalid_escape_sequence loc c = raise @@ Invalid_escape_sequence (loc, c)
