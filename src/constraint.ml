type condition = {
  id : string;  (** Unique id *)
  env : Logic.binding;  (** Type environment where a condition is made *)
  params : Logic.binding;  (** Stack binder *)
  exists : Logic.binding;
      (** Existential binder.  Except few situation, the 0-degree Skolem
     normal form suffices for refinement predicates.  So we split
     existential binders from the body. *)
  body : Logic.exp;
      (** Predicate formula.  As mentioned, it is ideally QF-formula.  In
     such a case, verification condition becomes (theoretically)
     decidable. *)
}
(** Representation of refinement stack types *)

type t = {
  conditions : condition list;  (** 篩型の集合 *)
  measures : Measure.t;
  constraints :
    (Syntax.location * string option * Logic.binding * condition * condition)
    list;
      (** condition => condition を表す assertion *)
  self_entries : (string * Sort.t) list;
  axiom : Logic.exp;
}
(** 型検査中に出てくる篩型の集合, assertion の集合, 環境変数の集合 のタプル
    [Refx.collect] で集めていく情報 *)

let empty (self_entries : (string * Sort.t) list) : t =
  {
    conditions = [];
    measures = Measure.empty;
    constraints = [];
    self_entries;
    axiom = Logic.Lang.(c_bool true);
  }

let counter = ref 0

let mk_condition (coll_info : t) (env : Logic.binding) (params : Logic.binding)
    (exists : Logic.binding) (body : Logic.exp) : t * condition =
  incr counter;
  let id = Format.sprintf "f%04d" !counter in
  let cond = { id; env; params; exists; body } in
  ({ coll_info with conditions = cond :: coll_info.conditions }, cond)

let mk_exception (coll_info : t) (env : Logic.binding)
    ?(params = [ Logic.Lang.err ]) (exists : Logic.binding) (body : Logic.exp) :
    t * condition =
  incr counter;
  let id = Format.sprintf "e%04d" !counter in
  let cond = { id; env; params; exists; body } in
  ({ coll_info with conditions = cond :: coll_info.conditions }, cond)

let add_subsume (coll_info : t) (loc : Syntax.location) ?(msg : string option)
    (env : Logic.binding) (sub : condition) (sup : condition) : t =
  {
    coll_info with
    constraints = (loc, msg, env, sub, sup) :: coll_info.constraints;
  }

let add_measure (coll_info : t) (m : Decoration_def.measure_annot) : t =
  let measures = Measure.add_measure m coll_info.measures in
  { coll_info with measures }

let subst_body ?new_env ?new_params ?new_exists (c : condition) :
    Logic.exp tzresult Lwt.t =
  let mk_subst binding = function
    | None -> return @@ List.map (fun v -> (v, Logic.Lang.( ?/ ) v)) binding
    | Some el ->
        List.map2_es
          ~when_different_lengths:
            [ error_of_fmt "length mismatch new parameters" ]
          (fun v e -> return (v, e))
          binding el
  in
  mk_subst c.env new_env >>=? fun subst_env ->
  mk_subst c.params new_params >>=? fun subst_params ->
  mk_subst c.exists new_exists >>=? fun subst_exists ->
  return @@ Logic.subst_exp (subst_env @ subst_params @ subst_exists) c.body
