open Logic

(** [is_of_sort ty exp] generates condition under which [exp] is member of [ty]
   after converted into SMT-LIB expression where some of information of [ty] is
   lost, e.g, nat becomes int in z3 representation.  *)
let rec is_of_sort (ty : Sort.t) (exp : Logic.exp) =
  let c_true = Logic.Lang.c_bool true in
  match ty with
  | S_var _ -> assert false
  | S_unit | S_bool | S_int | S_string -> c_true
  | S_bytes -> c_true
  | S_nat -> Logic.Lang.(c_nat 0 <= exp)
  | S_mutez -> Logic.Lang.(c_mutez 0 <= exp && exp <= c_mutez_max)
  | S_chain_id | S_key_hash | S_key | S_signature | S_timestamp | S_address ->
      c_true
      (* TODO: Is there some condition for these sorts?  Indeed, each has
         a specific format in strings, but ignore it for now. *)
  | S_option t ->
      Logic.Lang.(ite (is_some exp) (is_of_sort t (extract_some exp)) c_true)
  | S_or (t1, t2) ->
      Logic.Lang.(
        ite (is_left exp)
          (is_of_sort t1 (extract_left exp))
          (is_of_sort t2 (extract_right exp)))
  | S_pair (t1, t2) ->
      Logic.Lang.(is_of_sort t1 (fst exp) && is_of_sort t2 (snd exp))
  | S_list _ | S_set _ | S_map _ | S_big_map _ ->
      c_true (* TODO: we need measure function ... *)
  | S_operation -> c_true
  | S_contract t ->
      Logic.Lang.(contract_ty_opt (extract_contract exp) = some (c_sort t))
  | S_lambda _ -> c_true
  | S_exception -> c_true

let rec bound_quantify (exp : Logic.exp) : Logic.exp =
  let desc =
    match exp.desc with
    | E_symbol _ | E_const _ -> exp.desc
    | E_data_constr (c, sl, el) ->
        E_data_constr (c, sl, List.map bound_quantify el)
    | E_data_destr (c, sl, e, n) -> E_data_destr (c, sl, bound_quantify e, n)
    | E_data_tester (c, e) -> E_data_tester (c, bound_quantify e)
    | E_forall (b, e) ->
        let binder_preds =
          List.map (fun x -> is_of_sort (snd x) Logic.Lang.(?/x)) b
        in
        E_forall (b, bound_quantify Logic.Lang.(conj binder_preds := e))
    | E_exists (b, e) ->
        let binder_preds =
          List.map (fun x -> is_of_sort (snd x) Logic.Lang.(?/x)) b
        in
        E_exists (b, bound_quantify Logic.Lang.(conj binder_preds && e))
    | E_fapp (f, sl, el) -> E_fapp (f, sl, List.map bound_quantify el)
    | E_mapp (f, sl, el) -> E_mapp (f, sl, List.map bound_quantify el)
    | E_lambda _ | E_call _ | E_call_err _ -> exp.desc (* TODO *)
    | E_unfold_measure (f, sl, el, e) ->
        E_unfold_measure (f, sl, List.map bound_quantify el, bound_quantify e)
  in
  { exp with desc }

(** Instantiate universally quantified axioms by heuristically found
   concrete terms.  Such axioms cannot be well dealt with by z3.  So
   we use instantiate one instead of quantified precise one. *)
let inst_axiom (exp : Logic.exp) : Logic.exp =
  let module ESet = Set.Make (struct
    type t = Logic.exp

    let compare = compare
  end) in
  let union_list sets =
    List.fold_left (fun es es' -> ESet.union es es') ESet.empty sets
  in
  let rec aux (exp : Logic.exp) : ESet.t =
    match exp.desc with
    | E_symbol _ | E_const _ -> ESet.empty
    | E_data_constr (_, _, el) -> List.map aux el |> union_list
    | E_data_destr (_, _, e, _) -> aux e
    | E_data_tester (_, e) -> aux e
    | E_forall _ | E_exists _ ->
        (* TODO: Currently we do not use the terms inside forall and exists
           because instantiate hyps will contain variables bound by these
           quantifiers, but we could pass through hyps which free from the binder
           or there would be more nice solution. *)
        ESet.empty
    | E_fapp (f, sl, el) -> (
        let hyp = List.map aux el |> union_list in
        match (f, sl, el) with
        | F_amount, _, _ | F_balance, _, _ ->
            hyp |> ESet.add (is_of_sort Sort.S_mutez exp)
        | F_level, _, _ | F_total_voting_power, _, _ | F_str_len, _, _ ->
            hyp |> ESet.add (is_of_sort Sort.S_nat exp)
        | F_voting_power, _, _ ->
            hyp
            |> ESet.add (is_of_sort Sort.S_nat exp)
            |> ESet.add Logic.Lang.(exp <= total_voting_power)
        | (F_set_add | F_set_remove | F_set_find), _, [ e_elt; e_set ] ->
            hyp
            |> ESet.add
                 Logic.Lang.(
                   ite (mem_set e_elt e_set)
                     (* (choose_opt_set e_set = some e_elt) *)
                     (c_bool true)
                     (choose_opt_set (add_set e_elt e_set) = some e_elt))
        (* | (F_map_update, _, [e_key; e_uelt; e_map]) -> *)
        (*     hyp *)
        (*     |> ESet.add *)
        (*          Logic.Lang.( *)
        (*            ite *)
        (*              (is_some (find_opt_map e_key e_map)) *)
        (*              (choose_opt_map e_map *)
        (*              = some *)
        (*                  (pair e_key (extract_some (find_opt_map e_key e_map))) *)
        (*              ) *)
        (*              (is_some e_uelt := *)
        (*                 choose_opt_map (update_map e_key e_uelt e_map) *)
        (*                 = some (pair e_key (extract_some e_uelt)))) *)
        (* | (F_map_find, _, [e_key; e_map]) -> *)
        (*     hyp *)
        (*     |> ESet.add *)
        (*          Logic.Lang.( *)
        (*            is_some (find_opt_map e_key e_map) *)
        (*            := choose_opt_map e_map *)
        (*               = some *)
        (*                   (pair e_key (extract_some (find_opt_map e_key e_map)))) *)
        | F_pack, [ ty ], [ e ] ->
            hyp |> ESet.add Logic.Lang.(unpack_opt ty (pack ty e) = some e)
        | F_unpack_opt, [ ty ], [ e ] ->
            hyp
            |> ESet.add
                 Logic.Lang.(is_of_sort ty (extract_some (unpack_opt ty e)))
        | F_set_choose_opt, [ ty ], _ ->
            hyp |> ESet.add Logic.Lang.(choose_opt_set (empty_set ty) = none ty)
        | F_map_choose_opt, [ t_key; t_elt ], _ ->
            hyp
            |> ESet.add
                 Logic.Lang.(
                   choose_opt_map (empty_map t_key t_elt)
                   = none Sort.(S_pair (t_key, t_elt)))
        | _ -> hyp)
    | E_mapp (_, _, el) -> List.map aux el |> union_list
    | E_lambda _ | E_call _ | E_call_err _ -> ESet.empty
    (* | E_measure_axiom el -> List.map aux el |> union_list *)
    | E_unfold_measure (_, _, el, e) -> List.map aux (e :: el) |> union_list
  in
  aux exp |> ESet.elements |> Logic.Lang.conj

(** [unfold_measures mdef exp] unfolds the measure applications in [exp]
   following [mdef] and returns the equations between each application and
   corresponding unfolded body.  Note that recursive measures are once unfolded.
   *)
let unfold_measures (mdef : Measure.t) (exp : Logic.exp) : Logic.exp list =
  let module S = Set.Make (struct
    type t = Logic.exp

    let compare = compare
  end) in
  let open Decoration_def in
  let unfold_measure f sl el m =
    List.combine ~when_different_lengths:() m.tyvars sl >>? fun s ->
    let args = Logic.type_subst_binding s m.args in
    let body = Logic.type_subst_exp s m.body in
    List.combine ~when_different_lengths:() args el >>? fun s ->
    ok @@ Logic.(mk_exp @@ E_unfold_measure (f, sl, el, subst_exp s body))
  in
  let rec aux (m : measure_annot) (exp : Logic.exp) =
    match exp.desc with
    | E_symbol _ | E_const _ -> S.empty
    | E_data_constr (_, _, el) ->
        List.fold_left (fun s e -> S.union s @@ aux m e) S.empty el
    | E_data_destr (_, _, e, _) -> aux m e
    | E_data_tester (_, e) -> aux m e
    | E_forall _ | E_exists _ ->
        S.empty (* TODO: how should we do inside quantifiers? *)
    | E_fapp (_, _, el) ->
        List.fold_left (fun s e -> S.union s @@ aux m e) S.empty el
    | E_mapp (f, sl, el) ->
        let s =
          if f.ufid = m.ufid then
            unfold_measure f sl el m >|? S.singleton
            |> Result.value ~default:S.empty
          else S.empty
        in
        List.fold_left (fun s e -> S.union s @@ aux m e) s el
    | E_lambda _ | E_call _ | E_call_err _ -> S.empty
    (* | E_measure_axiom el -> *)
    (*     List.fold_left (fun s e -> S.union s @@ aux m e) S.empty el *)
    | E_unfold_measure (_, _, _, e) -> aux m e
  in
  let us, _ =
    List.fold_left
      (fun (us, exp) m ->
        let u = aux m exp in
        (S.union us u, Logic.Lang.(exp && (conj @@ S.elements u))))
      (S.empty, exp) (List.rev mdef)
    (* TODO: much dependening on mdef order... *)
  in
  S.elements us

let inst_lambda (exp : Logic.exp) : Logic.exp =
  let module M = Map.Make (Sort) in
  let module S = Set.Make (struct
    type t = Logic.exp

    let compare = compare
  end) in
  let union_map f l =
    List.fold_left
      (fun s x -> M.merge (fun _ -> Option.merge S.union) s @@ f x)
      M.empty l
  in
  let rec correct e =
    match e.desc with
    | E_symbol _ | E_const _ -> M.empty
    | E_data_constr (_, _, el) -> union_map correct el
    | E_data_destr (_, _, e, _) -> correct e
    | E_data_tester (_, e) -> correct e
    | E_forall _ | E_exists _ -> M.empty
    | E_fapp (_, _, el) | E_mapp (_, _, el) -> union_map correct el
    | E_lambda _ -> M.empty
    | E_call (e_f, _, _) -> M.singleton e_f.ty @@ S.singleton e
    | E_call_err (e_f, _, _) -> M.singleton e_f.ty @@ S.singleton e
    | E_unfold_measure (_, _, el, e) -> union_map correct (e :: el)
  in
  let calls = correct exp in
  let rec inst e =
    let desc =
      match e.desc with
      | E_symbol _ | E_const _ | E_data_constr _ | E_data_destr _
      | E_data_tester _ | E_forall _ | E_exists _ | E_mapp _ | E_call _
      | E_call_err _ | E_unfold_measure _ ->
          e.desc
      | E_fapp (f, sl, el) -> (
          match f with
          | F_conj | F_disj -> E_fapp (f, sl, List.map inst el)
          | _ -> e.desc)
      | E_lambda (e_f, v_arg, v_ret, v_err, e_bef, e_aft, e_excpt) ->
          M.find e_f.ty calls |> Option.value ~default:S.empty |> fun s ->
          S.fold
            (fun e p ->
              match e.desc with
              | E_call (_, e_arg, e_ret) ->
                  let subst = [ (v_arg, e_arg); (v_ret, e_ret) ] in
                  let bef = Logic.subst_exp subst e_bef in
                  let aft = Logic.subst_exp subst e_aft in
                  Logic.Lang.(p && (call e_f e_arg e_ret := bef := aft))
              | E_call_err (_, e_arg, e_err) ->
                  let subst = [ (v_arg, e_arg); (v_err, e_err) ] in
                  let bef = Logic.subst_exp subst e_bef in
                  let excpt = Logic.subst_exp subst e_excpt in
                  Logic.Lang.(p && (call_err e_f e_arg e_err := bef := excpt))
              | _ -> assert false)
            s
            Logic.Lang.(c_bool true)
          |> fun e -> e.desc
    in
    { e with desc }
  in
  inst exp
